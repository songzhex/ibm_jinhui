  subroutine getWave(u, phi, x, t) 
    use mpi
    use params
    use globdata
    implicit none
  
    integer, parameter :: NSD = 3
    real(8), parameter :: pi = 3.14159265358979323846264338328d0
    real(8) :: u(NSD), phi, x(NSD),x_interface(3),u_interface(3), t
    real(8) :: kxwt, kzh, amp, xy, z, fac, He           
    real(8) :: wave_periode,water_depth 
    real(8) :: eps
    real(8) :: U_air(3)
  ! because these two variables are defined in param.f90
    real(8) :: sea_bed
    x(1) = x(1) - shift_wave
    sea_bed =  mesh_fluid%minX(3)
  !  sea_bed = -350d0      
    water_depth  = water_level - sea_bed   
    wave_periode = sqrt(gravity*wave_length* &
                     tanh(wave_length*water_depth))

    amp = wave_periode*(wave_amp/sinh(wave_length*water_depth))  
    xy  = cos(wave_angle)*x(1) + sin(wave_angle)*x(2)

    kxwt = wave_length*(xy - Uin*t) - wave_periode*t
   ! kxwt = wave_length*xy - wave_periode*t
    kzh  = wave_length*min(x(3)-mesh_fluid%minX(3),water_depth)
   ! kzh  = wave_length*(x(3)-water_level+water_depth)
    u(1) = amp*cosh(kzh)*cos(kxwt)*cos(wave_angle)  
    u(2) = amp*cosh(kzh)*cos(kxwt)*sin(wave_angle) 
    u(3) = amp*sinh(kzh)*sin(kxwt)  
    phi  = wave_amp*cos(kxwt) + water_level - x(3) 
    if(flag_wave) then
   !   if(ismaster) then 
       write(222,*) "wave_length",wave_length
       write(222,*) "water_depth:",water_depth
       write(222,*) "wave_periode:",wave_periode
       write(222,*) "amp", amp
       write(222,*)  "max", amp*cosh(water_depth)
   !   endifgggg
      flag_wave = .false.
    endif 
   if (phi < lower) then  
     ! z   = (x(3)-water_level)/(1.5d0*wave_amp+1d-16)
     ! fac = min(1d0,z**2)
     !  u(1) = Uin + min(u(1), Uwind*fac)
      x_interface=x
      x_interface(3)=wave_amp*cos(kxwt) + water_level -lower
      u_interface = 0d0
      U_air = 0d0
      call getWave_u(u_interface,x_interface,t)
      u_interface(1) = u_interface(1) + Uin
      U_air(1) = Uwind
      u=0d0
     ! eps=thickAW  
     ! if(abs(phi).gt.5) then
     !  He = 0d0
     ! else
     !  He = 5d-1*(1d0 + ((phi+eps)/eps) + (sin(pi*(phi+eps)/eps)/pi))
     ! endif
     ! u(1)=u_interface(1)*He+Uwind*(1-He) 
     ! Too complicated, sha ji yan yong niu dao
     ! The following is the easier one
     if(phi< upper) then
       u(1:3) = U_air(1:3)
     endif
     if((phi>= upper).and.(phi<lower)) then
       eps=(lower-phi)/(lower-upper)
       u(1:3)=U_air(1:3)*eps+u_interface(1:3)*(1-eps)
     endif 
    else
      u(1) = u(1) + Uin 
   endif
   if(level_flag.eq.0) then
      u = 0d0
      u(1) = Uin
      phi = water_level - x(3) 
   endif
  end subroutine getWave

  subroutine getWave_u(u, x, t) 
  use mpi
    use params
    use globdata
    implicit none
  
    integer, parameter :: NSD = 3
    real(8) :: u(NSD), x(NSD), t
    real(8) :: kxwt, kzh, amp, xy, z, fac           
    real(8) :: wave_periode,water_depth 
  ! because these two variables are defined in param.f90
    real(8) :: sea_bed
    sea_bed = mesh_fluid%minX(3) !-350d0            
    water_depth  = water_level - sea_bed! mesh_fluid%minX(3)    
    wave_periode = sqrt(gravity*wave_length* &
                     tanh(wave_length*water_depth))

    amp = wave_periode*(wave_amp/sinh(wave_length*water_depth))  
    xy  = cos(wave_angle)*x(1) + sin(wave_angle)*x(2)

    kxwt = wave_length*(xy - Uin*t) - wave_periode*t
    kzh  = wave_length*min(x(3)-mesh_fluid%minX(3),water_depth)

    u(1) = amp*cosh(kzh)*cos(kxwt)*cos(wave_angle)  
    u(2) = amp*cosh(kzh)*cos(kxwt)*sin(wave_angle) 
    u(3) = amp*sinh(kzh)*sin(kxwt)  

 end subroutine getWave_u
  
  module boundaryConditions  

  !======================================================================
  contains
  !======================================================================
  !-----------------------------------------
  function getBCval_NS(d,valType,msh,st1,k,t)
 
    use params
    use meshData
    use solution
    implicit none

    integer, parameter  :: NSD = 3  
    integer, intent(in) :: d,valType,k     
    type(meshStruct), intent(in) :: msh   
    type(state),      intent(in) :: st1   
    real(8), intent(in) :: t
 
    real(8) :: getBCval_NS,u(NSD), phi 
      
    if      (valType == 1 ) then                    
      getBCval_NS = UinVec(d)    
    else if (valType == 2 ) then
      getBCval_NS = st1%ugm(k,d)     
    else if (valType == 3 ) then           
      call  getWave(u, phi, msh%xg(k,:)+st1%dg(k,:), t)           
      getBCval_NS = u(d) 
    else if (valType == 4 ) then                 
      getBCval_NS = 0d0      
    else
      write(*,*) "getBCval_NS :: Wrong value type",ValType 
      stop           
    endif

  end function
  
  !======================================================================
  function getBCval_LSC(valType,x,t)
    use params
    implicit none

    integer, parameter :: NSD = 3 
   
    integer :: valType
    real(8) :: x(NSD), t
    real(8) :: getBCval_LSC, u(NSD), phi                                    
                                                                       
    if      (valType == 1) then
      getBCval_LSC = water_level - x(3)       
    else if (valType == 2) then 
      getBCval_LSC = 0d0                                  
    else if (valType == 3) then
      call  getWave(u, phi, x, t)  
      getBCval_LSC = phi    
    else
      write(*,*) "getBCval_LSC :: Wrong value type",valType 
      stop           
    endif

  end function        

subroutine setBCs_CFD_turbine(IBC, msh, st0, st1)
  use commonvars
  use aAdjkeep
  use mpi
  use params
  use meshData
  use solution
  use globdata

  implicit none
  
  integer :: b, i, j, k, n, d, dir, ptmp, iface
  type(meshStruct), intent(in)    :: msh   
  integer,          intent(out)   :: IBC(msh%NNODE,msh%NSD+1)
  type(state),      intent(in)    :: st0
  type(state),      intent(inout) :: st1
  real(8) :: u(msh%NSD), phi
  real(8) :: Uinflow(3)
  IBC  = 0
  ogam = 1.0d0/gami
  mgam = gami - 1.0d0

  Uinflow = 0d0
  Uinflow(1) = Uin
  ugbc = 0d0

 do b = 1, msh%NBOUND       
      do d = 1, msh%NSD
        if (msh%bound(b)%FACE_ID == 21) then
          do j = 1, msh%bound(b)%NNODE
            k = msh%bound(b)%BNODES(j)
            ugbc(k,d) = ushBC(k,d)
          enddo                    
     else
       do j = 1, msh%bound(b)%NNODE
            k = msh%bound(b)%BNODES(j)   
            ugbc(k,d) = getBCval_NS(d,3,msh,st1,k,time)                
        enddo          
        endif   
      enddo
    enddo  ! end loop: all faces


  do b = 1, msh%NBOUND
    do d = 1, msh%NSD
      if (BCugType(msh%bound(b)%FACE_ID,d) == 1) then
        if(msh%bound(b)%FACE_ID.eq.5) then
         do j = 1, msh%bound(b)%NNODE
            k = msh%bound(b)%BNODES(j)
           ! ugbc(k,d) = getBCval_NS(d,3,msh,st1,k,time)
            IBC(k,d) = 1        
            st1%ug(k,d)  = ugbc(k,d)           
            st1%acg(k,d) = ( (st1%ug(k,d)-st0%ug(k,d))*Dtgl  &
                          + (gami-1.0d0)*st0%acg(k,d)  )/gami
          enddo
        endif
        if(msh%bound(b)%FACE_ID.ne.5) then
         do j = 1, msh%bound(b)%NNODE
          k = msh%bound(b)%BNODES(j)
          IBC(k,d) = 1
        !  ugbc(k,d) = BCugValu(msh%bound(b)%FACE_ID,d)
       !   ugbc(k,d) = 0d0!BCugValu(msh%bound(b)%FACE_ID,d)
          st1%ug (k,d) = ugbc(k,d)
          st1%acg(k,d) = ((st1%ug(k,d)-st0%ug(k,d))*Dtgl  &
                        + (gami-1.0d0)*st0%acg(k,d))/gami
        end do
       endif
      endif
   enddo
 enddo  ! end loop: all faces


end subroutine setBCs_CFD_turbine

 subroutine setBCs_Mesh_turbine(IBC, msh, st0, st1)

    use meshData
    use params
    use solution
    use globdata
    use elasticBody
    use mpi
  
    implicit none
    type(meshStruct), intent(in)  :: msh   
    integer,          intent(out) :: IBC(msh%NNODE,msh%NSD)
    type(state),      intent(in)    :: st0
    type(state),      intent(inout) :: st1
 !   real(8) :: Rmt(3,3),    Rmdt(3,3),    Rmddt(3,3)        
    integer :: b, d, j, k, id, bo, idx, iel,i,dd
    real(8) :: disp(msh%NSD)
    logical :: findIndex

    IBC  = 0
 !   call get_Rmat(theta, thetd, thedd, Rmt, Rmdt, Rmddt)
    do b = 1, msh%NBOUND
    if(msh%bound(b)%FACE_ID < 21) then 
      do i = 1, msh%bound(b)%NNODE
        j = msh%bound(b)%BNODES(i)     !A Points to volumetric node from surface node
        IBC(j,1:3) = 1
        do dd = 1, 3
     !     st1%dg(j,dd) = sum(Rmt(dd,:)*msh%xg(j,1:3))
     !     st1%ugm(j,dd) = sum(Rmdt(dd,:)*msh%xg(j,1:3))
     !     st1%acgm(j,dd) = sum(Rmddt(dd,:)*msh%xg(j,1:3))

          st1%dg(j,dd) = 0d0
          st1%ugm(j,dd) = 0d0
          st1%acgm(j,dd) = 0d0

        end do
      end do

     else if  ((msh%bound(b)%FACE_ID == 21) ) then
      do i = 1, msh%bound(b)%NNODE
     !   j = msh%bound(b)%BNODES(i)     !mesh BC on a rotor
         k = msh%bound(b)%BNODES(i)
         do d = 1, msh%NSD
           IBC(k,d) = 1
         !  write(*,*) "dshBC(k,d)", dshBC(k,d)
           st1%dg(k,d)   = dshBC(k,d) ! Jinhui: grab the boundary condtion from the dshBC, ashBC and ushBC
           st1%acgm(k,d) = ashBC(k,d)
           st1%ugm (k,d) = ushBC(k,d)
          enddo 
       end do
    end if

    enddo  ! end loop: all faces
     
    
  end subroutine setBCs_Mesh_turbine
!=======================================================================
subroutine setBCs_CFD_Fin(IBC, msh, st0, st1)
 use commonvars
  use aAdjkeep
  use mpi
  use params
  use meshData
  use solution
  use globdata

  implicit none
  
  integer :: b, i, j, k, n, d, dir, ptmp, iface
  type(meshStruct), intent(in)    :: msh   
  integer,          intent(out)   :: IBC(msh%NNODE,msh%NSD+1)
  type(state),      intent(in)    :: st0
  type(state),      intent(inout) :: st1
  real(8) :: u(msh%NSD), phi
  real(8) :: Uinflow(3)
  IBC  = 0
  ogam = 1.0d0/gami
  mgam = gami - 1.0d0

  Uinflow = 0d0
  Uinflow(1) = Uin
  ugbc = 0d0

 do b = 1, msh%NBOUND       
    do d = 1, msh%NSD
     if (msh%bound(b)%FACE_ID == 21) then
       do j = 1, msh%bound(b)%NNODE
         k = msh%bound(b)%BNODES(j)
         if(solfsi) then 
          ugbc(k,d) = ushBC(k,d)
         else
          ugbc(k,d) = st1%ugm(k,d)
         endif
       enddo                    
     else
       do j = 1, msh%bound(b)%NNODE
         k = msh%bound(b)%BNODES(j)   
         ugbc(k,d) = getBCval_NS(d,3,msh,st1,k,time)                
        enddo          
     endif   
   enddo
 enddo  ! end loop: all faces


 do b = 1, msh%NBOUND
    do d = 1, msh%NSD
      if (BCugType(msh%bound(b)%FACE_ID,d) == 1) then
        if(msh%bound(b)%FACE_ID.eq.5) then
         do j = 1, msh%bound(b)%NNODE
            k = msh%bound(b)%BNODES(j)
           ! ugbc(k,d) = getBCval_NS(d,3,msh,st1,k,time)
            IBC(k,d) = 1        
            st1%ug(k,d)  = ugbc(k,d)           
            st1%acg(k,d) = ((st1%ug(k,d)-st0%ug(k,d))*Dtgl  &
                          + (gami-1.0d0)*st0%acg(k,d)  )/gami
          enddo
        endif
        if(msh%bound(b)%FACE_ID.ne.5) then
         do j = 1, msh%bound(b)%NNODE
          k = msh%bound(b)%BNODES(j)
          IBC(k,d) = 1
          st1%ug (k,d) = BCugValu(msh%bound(b)%FACE_ID,d)
          st1%acg(k,d) = ((st1%ug(k,d)-st0%ug(k,d))*Dtgl  &
                        + (gami-1.0d0)*st0%acg(k,d))/gami
        end do
       endif
      endif
   enddo
 enddo  ! end loop: all faces

end subroutine setBCs_CFD_Fin
!=======================================================================
subroutine setBCs_Mesh_Fin(IBC, msh, st0, st1)

    use meshData
    use params
    use solution
    use globdata
    use elasticBody
    use mpi
  
    implicit none
    type(meshStruct), intent(in)  :: msh   
    integer,          intent(out) :: IBC(msh%NNODE,msh%NSD)
    type(state),      intent(in)    :: st0
    type(state),      intent(inout) :: st1
 !   real(8) :: Rmt(3,3),    Rmdt(3,3),    Rmddt(3,3)        
    integer :: b, d, j, k, id, bo, idx, iel,i,dd, nn
    real(8) :: disp(msh%NSD)
    logical :: findIndex
    IBC  = 1
    st1%dg = 0d0
    st1%ugm = 0d0
    st1%acgm= 0d0

  end subroutine setBCs_Mesh_Fin
  !======================================================================
!=======================================================================
subroutine setBCs_Mesh_Fin_move(IBC, msh, st0, st1)

    use meshData
    use params
    use solution
    use globdata
    use elasticBody
    use mpi
  
    implicit none
    type(meshStruct), intent(in)  :: msh   
    integer,          intent(out) :: IBC(msh%NNODE,msh%NSD)
    type(state),      intent(in)    :: st0
    type(state),      intent(inout) :: st1
 !   real(8) :: Rmt(3,3),    Rmdt(3,3),    Rmddt(3,3)        
    integer :: b, d, j, k, id, bo, idx, iel,i,dd, nn
    real(8) :: disp(msh%NSD)
    logical :: findIndex
    IBC  = 1

   do nn = 1, msh%NNODE
    if((msh%Coord_ID(nn) == 31).or. (msh%Coord_ID(nn) == 32)) then
       IBC(nn,1:3) = 1
       do dd = 1,3
         st1%dg(nn,dd) =   sum(NRmat(dd,:,nn)*(msh%xg(nn,1:3)-Center_Rot(1,1:3)))
         st1%ugm(nn,dd) =  sum(NRdot(dd,:,nn)*(msh%xg(nn,1:3)-Center_Rot(1,1:3)))
         st1%acgm(nn,dd) = sum(NRddt(dd,:,nn)*(msh%xg(nn,1:3)-Center_Rot(1,1:3)))
       enddo
     endif
    enddo

    do b = 1, msh%NBOUND
       if(msh%bound(b)%FACE_ID < 7) then 
        do i = 1, msh%bound(b)%NNODE
             j = msh%bound(b)%BNODES(i)     !A Points to volumetric node from surface node
             IBC(j,1:3) = 1
             st1%dg(j,:) = 0d0
             st1%ugm(j,:) = 0d0
             st1%acgm(j,:) = 0d0
         end do
       endif

     if ((msh%bound(b)%FACE_ID == 11) .or.  (msh%bound(b)%FACE_ID == 12)) then 
       do i = 1, msh%bound(b)%NNODE
          j = msh%bound(b)%BNODES(i)     !A Points to volumetric node from surface node
          IBC(j,1:3) = 1
           do dd = 1,3
            st1%dg(j,dd) =   sum(NRmat(dd,:,j)*(msh%xg(j,1:3)-Center_Rot(1,1:3)))
            st1%ugm(j,dd) =  sum(NRdot(dd,:,j)*(msh%xg(j,1:3)-Center_Rot(1,1:3)))
            st1%acgm(j,dd) = sum(NRddt(dd,:,j)*(msh%xg(j,1:3)-Center_Rot(1,1:3)))
           enddo
       end do
     endif
   enddo
   do b = 1, msh%NBOUND
     if ((msh%bound(b)%FACE_ID == 21) ) then
      do i = 1, msh%bound(b)%NNODE
         j = msh%bound(b)%BNODES(i)     !mesh BC on a rotor
         do dd = 1, msh%NSD
           IBC(j,dd) = 1
           st1%dg(j,dd) =   sum(NRmat(dd,:,j)*(msh%xg(j,1:3)-Center_Rot(1,1:3)))
           st1%ugm(j,dd) =  sum(NRdot(dd,:,j)*(msh%xg(j,1:3)-Center_Rot(1,1:3)))
           st1%acgm(j,dd) = sum(NRddt(dd,:,j)*(msh%xg(j,1:3)-Center_Rot(1,1:3)))       
          enddo 
        enddo
       endif
    enddo  ! end loop: all faces

  end subroutine setBCs_Mesh_Fin_move
  !======================================================================

  subroutine setBCs_Mesh(IBC, msh, st0, st1)
    use meshData
    use params
    use solution
    use globdata
    use elasticBody
    use mpi
  
    implicit none
    type(meshStruct), intent(in)  :: msh   
    integer,          intent(out) :: IBC(msh%NNODE,msh%NSD)
    type(state),      intent(in)    :: st0
    type(state),      intent(inout) :: st1
          
    integer :: b, d, j, k, id, bo, idx, iel,i
    real(8) :: disp(msh%NSD)
    logical :: RBmove = .false.
    logical :: findIndex

    IBC  = 0

    do b = 1, msh%NBOUND
       id = b
       do d = 1, msh%NSD
        if (MESH_BC_Type(id,d) == 3) RBmove = .true.
       enddo
    enddo
  
 if (RBmove) then

      if (NBODIES /= 1) then 
        write(*,*) "Wrong body count"
        stop
      endif
   
      do iel = 1, msh%NELEM
         if (msh%ELMNSHL(iel) == 6) then
           do i = 1, msh%ELMNSHL(iel) 
            k = msh%IEN(iel,i)
            IBC (k,:) = 1
           enddo
         endif
      enddo
      call icommu(IBC(:,1)) 

      do k = 1, msh%NNODE
        if (IBC(k,1) >= 1 ) then
            IBC  (k,:) = 1
      !      call getBodyDisp(disp,bst1(1),bodies_param(1),msh%xg(k,:),k)
            st1%dg(k,:)   = disp
            st1%acgm(k,:) = (1.0d0/beti)*(-(st0%ugm(k,:)/Delt) &
                          + ((st1%dg(k,:)-st0%dg(k,:))/(Delt*Delt)) &
                          + (beti-0.5d0)*st0%acgm(k,:))

            st1% ugm (k,:) = st0%ugm(k,:) &
                           + Delt*((1.0d0-gami)*st0%acgm(k,:)+ gami*st1%acgm (k,:))
        endif
      enddo
 ! Jinhui: since we do not do nonmatch at this point, and the structure does not do rigid body motion, the RBmove is no longer useful 
  endif  

    do b = 1, msh%NBOUND
      id = b
      do d = 1, msh%NSD
        if (MESH_BC_Type(id,d) == 1) then
          do j = 1, msh%bound(b)%NNODE
            k = msh%bound(b)%BNODES(j)
            IBC (k,d) = 1
            st1%dg  (k,d) = 0.0d0
            st1%ugm (k,d) = 0.0d0
            st1%acgm(k,d) = 0.0d0
          enddo        
        else if (MESH_BC_Type(id,d) == 2) then    
 
          do j = 1, msh%bound(b)%NNODE
            k = msh%bound(b)%BNODES(j)
            IBC(k,d) = 1 
            st1%dg(k,d)   = dshBC(k,d) ! Jinhui: grab the boundary condtion from the dshBC, ashBC and ushBC
            st1%acgm(k,d) = obet*(-Dtgl*st0%ugm(k,d) &
                          + Dtgl*Dtgl*(st1%dg(k,d)-st0%dg(k,d)) & 
                          + (beti-0.5d0)*st0%acgm(k,d))
            st1%ugm (k,d) = st0%ugm(k,d) &  
                          + Delt*((1.0d0-gami)*st0%acgm(k,d)+ gami*st1%acgm (k,d))
       
          enddo         
      
        endif
      enddo
    enddo  ! end loop: all faces
     
    
  end subroutine setBCs_Mesh
      
  !======================================================================
  subroutine setBCs_LSC_turbine(IBC, msh, st0, st1)
    use mpi
    use meshData
    use params
    use solution
    use globdata
    implicit none
    
    type(meshStruct), intent(in)  :: msh   
    integer,          intent(out) :: IBC(msh%NNODE)
    type(state),      intent(in)    :: st0
    type(state),      intent(inout) :: st1  
    
    integer :: b, j, k, id
    logical :: findIndex,tmp 
   
    IBC  = 0
    phigbc  = 0d0

    do b = 1, msh%NBOUND
      id = msh%bound(b)%FACE_ID
      do j = 1, msh%bound(b)%NNODE
        k = msh%bound(b)%BNODES(j)  
        phigbc(k) = getBCval_LSC(3,msh%xg(k,:)+st1%dg(k,:), time)
      enddo        
    enddo  ! end loop: all faces
           
    do b = 1, msh%NBOUND
      id = msh%bound(b)%FACE_ID
      if (LSC_BC_Type(id) == 1) then
        do j = 1, msh%bound(b)%NNODE
          k = msh%bound(b)%BNODES(j)
          IBC (k) = 1
          st1%phig  (k) = phigbc  (k) 
          st1%rphig(k) = ( (st1%phig(k)-st0%phig(k))*Dtgl  &
                        + (gami-1.0d0)*st0%rphig(k)  )/gami
        enddo        
      endif
    enddo  ! end loop: all faces
     
  end subroutine setBCs_LSC_turbine
  !======================================================================
  !======================================================================
  subroutine setBCs_LSC_Fin(IBC, msh, st0, st1)

    use mpi
    use meshData
    use params
    use solution
    use globdata
    implicit none
    
    type(meshStruct), intent(in)  :: msh   
    integer,          intent(out) :: IBC(msh%NNODE)
    type(state),      intent(in)    :: st0
    type(state),      intent(inout) :: st1  
    
    integer :: b, j, k, id
    logical :: findIndex,tmp 
   
    IBC  = 0
    phigbc  = 0d0

    do b = 1, msh%NBOUND
      id = msh%bound(b)%FACE_ID
      do j = 1, msh%bound(b)%NNODE
        k = msh%bound(b)%BNODES(j)  
        phigbc(k) = getBCval_LSC(3,msh%xg(k,:)+st1%dg(k,:), time)
      enddo        
    enddo  ! end loop: all faces
           
    do b = 1, msh%NBOUND
      id = msh%bound(b)%FACE_ID
      if (LSC_BC_Type(id) == 1) then
        do j = 1, msh%bound(b)%NNODE
          k = msh%bound(b)%BNODES(j)
          IBC (k) = 1
          st1%phig  (k) = phigbc  (k) 
          st1%rphig(k) = ( (st1%phig(k)-st0%phig(k))*Dtgl  &
                        + (gami-1.0d0)*st0%rphig(k)  )/gami
        enddo        
      endif
    enddo  ! end loop: all faces

  end subroutine setBCs_LSC_Fin
  !======================================================================
  !======================================================================
  subroutine applyBCs(nshl, NVAR, IBC, LHSloc, RHSloc)

    implicit none
 
    integer,            intent(in)    :: nshl, NVAR, IBC(nshl,NVAR)  
    real(8),            intent(inout) :: LHSloc(NVAR*NVAR,NSHL,NSHL), &
                                         RHSloc(NVAR,NSHL) 

    integer :: aa, bb ,i,j,c

    do aa = 1, NSHL
      do i = 1, NVAR
        if (IBC(aa,i) >= 1) then 
          ! Zero row       
          do j = 1, NVAR
            c = NVAR*(i-1) + j 
            LHSloc(c,aa,:) = 0d0
          enddo
          ! Zero column
          do j = 1, NVAR
            c = NVAR*(j-1) + i
            LHSloc(c,:,aa) = 0d0
          enddo           
          
          c = NVAR*(i-1) + i                    
          LHSloc(c,aa,aa) = 1d0      
          
          RHSloc(i,aa) = 0d0    
        endif      
      enddo  
    enddo
    
  end subroutine applyBCs  
  !======================================================================
  subroutine applyRowBCs(nshl, IBC, LHSloc)

    implicit none
 
    integer,            intent(in)    :: nshl, IBC(nshl)  
    real(8),            intent(inout) :: LHSloc(NSHL,NSHL)
    integer :: aa, bb

    do aa = 1, NSHL
      if (IBC(aa) >= 1)  LHSloc(aa,:) = 0d0                               
    enddo
    
  end subroutine applyRowBCs
    
  !======================================================================
  subroutine applyColBCs(nshl, IBC, LHSloc)

    implicit none
 
    integer,            intent(in)    :: nshl, IBC(nshl)  
    real(8),            intent(inout) :: LHSloc(NSHL,NSHL)
    integer :: aa, bb 

    do aa = 1, NSHL
      if (IBC(aa) >= 1) LHSloc(:,aa) = 0d0     
    enddo
    
  end subroutine applyColBCs    
  
end module boundaryConditions  
