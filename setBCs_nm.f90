!======================================================================
! Set the BC for the case of rotating 5MW with tower
!======================================================================
subroutine setNMBCs(NM)
  use mpi
  use meshData
  use params
  use solution
  use globdata
  use commonvars
  use aAdjkeep
  use defs_shell
  implicit none
  
  type(shell_nmb), intent(inout) :: NM
  integer :: b, i, j, k, dd
  integer,parameter::NSD=3
  ! for the fixed outer box domain (including the tower)
!A  do i = 1, NNODE
!A    if (NodeID(i) == 31) then   
!A      IBC(i,4:6) = 1 
!A    end if
!A  end do

  ! for the disk and the blade




      do i = 1, NM%FEM(1)%NNODE
!        j = bound(b)%BNODES(i)     
!A-------------------------------------S 05/11/13 Apply rotation to
!A the surface of rotating cylinder corresponding to the structural
!A rotation computed from Rt
        do dd = 1, NSD

          NM%FEM(1)%dsh(i,dd) = sum((Rmat(dd,:) - Identity1(dd,:))*(NM%FEM(1)%B_NET(i,1:3) - &
                     xrotOld(1:3))) + (xrot(dd) - xrotOld(dd))

          NM%FEM(1)%ash(i,dd) = (((NM%FEM(1)%dsh(i,dd)-NM%FEM(1)%dshold(i,dd)-Delt*NM%FEM(1)%ushold(i,dd)))* &
                       2.0d0/(Delt*Delt) - &
                       (1.0d0-2.0d0*beti)*NM%FEM(1)%ashold(i,dd))/(2.0d0*beti)

          NM%FEM(1)%ush(i,dd) = NM%FEM(1)%ushold(i,dd) + &
                       Delt*((1.0d0-gami)*NM%FEM(1)%ashold(i,dd)+gami*NM%FEM(1)%ash(i,dd))
        end do
!A-------------------------------------E          
      end do
!A fixed the boundary of outer domain
      do i = 1, NM%FEM(2)%NNODE
        do dd = 1, NSD
          NM%FEM(2)%dsh(i,dd)   = sum((Rtang(dd,:) - Identity1(dd,:))*(NM%FEM(2)%B_NET(i,1:3) - &
                     xrotOld(1:3))) + (xrot(dd) - xrotOld(dd))

        
          NM%FEM(2)%ash(i,dd) = (((NM%FEM(2)%dsh(i,dd)-NM%FEM(2)%dshold(i,dd)-Delt*NM%FEM(2)%ushold(i,dd)))* &
                       2.0d0/(Delt*Delt) - &
                       (1.0d0-2.0d0*beti)*NM%FEM(2)%ashold(i,dd))/(2.0d0*beti)

          NM%FEM(2)%ush(i,dd) = NM%FEM(2)%ushold(i,dd) + &
                       Delt*((1.0d0-gami)*NM%FEM(2)%ashold(i,dd)+gami*NM%FEM(2)%ash(i,dd))

        end do
      end do
   
end subroutine setNMBCs
