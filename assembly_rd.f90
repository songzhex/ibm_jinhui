subroutine assemble_rd(st, rhsg, rhsg_dg,  newJac, flag, NM)
   
  use globdata   
  use params   
  use boundaryConditions
  use aAdjKeep
  use mpi
  use defs_shell
  use types_beam
  use types_structure
  implicit none

   
  type(state),      intent(inout) :: st        
  real(8),          intent(inout) :: rhsg(mesh_fluid%NNODE,13) 
  real(8),          intent(inout) :: rhsg_dg(mesh_fluid%NNODE,6)   
  integer,          intent(in)    :: newJac
  integer,          intent(in) :: flag(4)
  type(shell_nmb), intent(inout) :: NM
  integer :: b

  ! Initialize Jacobians
  if (newJac >= 1) then
    LHSlsrd = 0d0
  endif
  ! Initialize Residuals
  rhsg   = 0d0  
  rhsg_dg = 0d0         
  ! Get interface quantaties
  if(move) then
    call setBCs_Mesh_Fin_move(IBCmesh, mesh_fluid, st, st) 
  endif    
  !if(ismaster) write(*,*) "set BCs for mesh"
  call ComputeNodalRhoMu(st, mesh_fluid)  
  ! redistancing  
  if (mesh_fluid%IM_flag == 1) then
     call IntAssembly_rd_IM (st, mesh_fluid, rhsg(:,1:9), newJac,flag)  
     ! no need to remove interior here, should've been taken care of by IBClsc
     ! hopefully...
     ! TODO: ADD remove interior here 
     call remove_interior(mesh_fluid, rhsg(:,1:9))
  else
     call IntAssembly_rd (st, mesh_fluid, rhsg(:,1:9), newJac,flag) 
   end if
  if (move) then
!  write(*,*) myid,"interior"
  call FaceAsse_DG_RD_w1t1(st, mesh_fluid, rhsg(:,1:6), newJac, NM)  
!  write(*,*) myid,"11"
  call FaceAsse_DG_RD_w1t2(st, mesh_fluid, rhsg_dg(:,1:6), newJac,NM)  
  endif  
!  write(*,*) myid,"12"
  if(level_flag.eq.0) then          
     rhsg(:,5) = 0d0
     rhsg_dg(:,5:6) = 0d0 
  endif       
  call commu(rhsg, 13, 'in ')
  call commu(rhsg_dg, 6, 'in ')
  rhsg(:,1:6) = rhsg(:,1:6) + rhsg_dg(:,1:6)

end subroutine assemble_rd


subroutine assemble_DG_RD_PC(st, rhsg_dg,  newJac, flag, NM)
   
  use globdata   
  use params   
  use boundaryConditions
  use aAdjKeep
  use mpi
  use defs_shell
  use types_beam
  use types_structure
  implicit none
  type(state),      intent(inout) :: st        
  real(8),          intent(inout) :: rhsg_dg(mesh_fluid%NNODE,6)   
  integer,          intent(in)    :: newJac
  integer,          intent(in) :: flag(4)
  type(shell_nmb), intent(inout) :: NM
  integer :: b
  ! Initialize Jacobians
  if (newJac >= 1) then
    LHSlsrd = 0d0
  endif
  rhsg_dg = 0d0  
  if(move) then
    call setBCs_Mesh_Fin_move(IBCmesh, mesh_fluid, st, st) 
  endif         
  call ComputeNodalRhoMu(st, mesh_fluid)         
  call FaceAsse_DG_RD_w1t2(st, mesh_fluid, rhsg_dg(:,1:6), newJac,NM) 
  call commu(rhsg_dg, 6, 'in ')

end subroutine assemble_DG_RD_PC

!======================================================================
!
!======================================================================
subroutine IntAssembly_rd(st, msh,  rhsg, newJac,flag)
                        
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use BoundaryConditions
  implicit none
  
  type(state),      intent(in) :: st
  type(meshStruct), intent(in) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,2*msh%NSD+3)  
  integer, intent(in) :: newJac

  integer :: iel, igauss, i, j, k,ii,jj,kk, hess_flag, idx, nshl, NGAUSS,aa,bb
  integer :: IBCns_loc(msh%maxNSHL,msh%NSD+1), IBClsc_loc(msh%maxNSHL,1), &
             IBCmesh_loc(msh%maxNSHL,msh%NSD)

  real(8), allocatable :: eKns(:,:,:), eKlsc(:,:),eKlsrd(:,:),eKmesh(:,:,:),&
                          eKlscu(:,:,:),eKrdphi(:,:)
  real(8), allocatable :: eRns(:,:), eRlsc(:),eRlsrd(:), eRmesh(:,:)

  real(8), allocatable :: gp(:,:), gw(:)                          
  real(8), allocatable :: shl(:), shgradg(:,:),shgradgold(:,:), shhessg(:,:,:)
  real(8) :: Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD),&
             dxdxi(msh%NSD,msh%NSD),dxidx(msh%NSD,msh%NSD)
  real(8) :: DetJ, detJgwt,shconv(msh%maxNSHL)
  
  real(8), allocatable :: xl(:,:),wl(:), dl(:,:),dlold(:,:),uml(:,:), acml(:,:),&
                          ul(:,:),acl(:,:),pl(:),rhol(:),mul(:),&
                          ulold(:,:),rho0l(:), &
                          phil(:),rphil(:), philold(:),&
                          phi1l(:),phidl(:), phidl_old(:), &
                          hl(:), hel(:), hepl(:)

  real(8) :: di(msh%NSD), umi(msh%NSD), acmi(msh%NSD),           &
             ui(msh%NSD), aci(msh%NSD), pri, rho, mu, rho0,  &
             phi , rphi, phi1, phid, phid_old,he, hep,hi 
  real(8) :: ddidxi(msh%NSD,msh%NSD), duidxi(msh%NSD,msh%NSD),duidxixj(msh%NSD,msh%NSD,msh%NSD),&
             duiolddxi(msh%NSD,msh%NSD), dpridxi(msh%NSD), &
             dphidxi(msh%NSD),dphiolddxi(msh%NSD),dphiddxi(msh%NSD), &
             drhodxi(msh%NSD), rhoconv, DetJold, uiold(msh%NSD)

  real(8) :: uadvi(msh%NSD), Se, ai(msh%NSD),ai1(msh%NSD), fi(msh%NSD)
  real(8) :: res_u(msh%NSD), res_c, res_rd
  real(8) :: tauM_ns, tauC_ns, tauP_ns, tauBar_ns, tau_c, tau_rd, gradmag
  real(8) :: kdc_ns, kdc_c, kdc_rd, Emod, phimax
  real(8) :: fact1,fact2
    
  real(8), parameter :: damp = 0.5d0
  real(8) :: kappa, Ftens(msh%NSD,msh%NSD), Stens(msh%NSD,msh%NSD), Ctens(msh%NSD,msh%NSD,msh%NSD,msh%NSD) 
  real(8) :: cfl(2), gcfl(2), cfl_loc(2)
  real(8) :: pse_time_loc,pse_time_loc1,pse_time_glob,pse_time_glob1,tmp_val
  integer, intent(in) :: flag(4)
  cfl=0.0d0

  fact1 = almi
  fact2 = alfi*gami*Delt 

  phimax = max(maxval(st%phig),-minval(st%phig))


  phi = phimax
  call MPI_ALLREDUCE (phi, phimax ,1, MPI_DOUBLE_PRECISION, &
                      MPI_SUM, MPI_COMM_WORLD, mpi_err)  

!...  Loop over elements
  NSHL   = -1  
  do iel = 1, msh%NELEM 

!...  Allocate        
    if (NSHL /= msh%ELMNSHL(iel)) then
      
      if (NSHL >= 0) then
        deallocate(shl, shgradg,  shgradgold,shhessg, gp, gw,              &
                   eRns, eRlsc,eRlsrd,eRmesh,                  &
                   xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul,       &
                   ulold,rho0l,phil,rphil,philold,phi1l,phidl,phidl_old, &
                   hl, hel, hepl)
        !if (newJac >= 1) then          
           deallocate(eKns,eKlsc,eKlsrd, eKmesh,eKlscu,eKrdphi)
        !endif           
      endif
     
      NSHL   = msh%ELMNSHL(iel)
      NGAUSS = msh%ELMNGAUSS(iel)

      allocate(shl(NSHL), shgradg(NSHL,msh%NSD), shgradgold(NSHL,msh%NSD),shhessg(NSHL,msh%NSD,msh%NSD),&
               gp(NGAUSS,msh%NSD), gw(NGAUSS),                        &
               eRns(msh%NSD+1,NSHL), eRlsc(NSHL),eRlsrd(NSHL),eRmesh(msh%NSD,NSHL),  &
               dl(NSHL,msh%NSD),dlold(NSHL,msh%NSD), ul(NSHL,msh%NSD),wl(NSHL),      &
               acl(NSHL,msh%NSD), uml(NSHL,msh%NSD), acml(NSHL,msh%NSD),             &
               pl(NSHL), xl(NSHL,msh%NSD), rhol(NSHL),mul(NSHL),                     &
               ulold(NSHL,msh%NSD),rho0l(NSHL),phil(NSHL),rphil(NSHL),philold(NSHL), &
               phi1l(NSHL),phidl(NSHL),phidl_old(NSHL),hl(NSHL),hel(NSHL),hepl(NSHL))
      !if (newJac >= 1) then                    
        allocate(eKns((msh%NSD+1)*(msh%NSD+1),NSHL,NSHL), &
                 eKlsc(NSHL,NSHL),eKlsrd(NSHL,NSHL),      & 
                 eKmesh(msh%NSD*msh%NSD,NSHL,NSHL),      &
                 eKlscu(msh%NSD,NSHL,NSHL), eKrdphi(NSHL,NSHL))
      !endif        

      call genGPandGW(gp, gw, NGAUSS, NSHL)
    end if

!...  Get local solution arrays 
    do i = 1, NSHL 
    
      idx = msh%IEN(iel,i)
      xl   (i,:) = msh%xg    (idx,:)     
      wl   (i)   = msh%wg    (idx)
      
      dl   (i,:) = st%dg   (idx,:)      
      dlold(i,:) = old_rd%dg  (idx,:)
      uml  (i,:) = st%ugm  (idx,:)
      acml (i,:) = st%acgm (idx,:)
            
      ul   (i,:) = st%ug   (idx,:)
      acl  (i,:) = st%acg  (idx,:)
      pl   (i)   = st%pg   (idx)
      rhol (i)   = st%rhog (idx)          
      mul  (i)   = st%mug  (idx) 
      ulold(i,:) = old_rd%ug  (idx,:)   
      rho0l(i)   = st%rho0g(idx)
      
      phil(i)    = st%phig (idx)       
      rphil(i)   = st%rphig(idx)     
      philold(i) = old_rd%phig(idx)
     
      phi1l(i)   = st%phig   (idx)

      phidl(i)   = st%phidg  (idx)
      phidl_old(i)   = old_rd%phidg  (idx)

      hl(i)      = st%hg  (idx)             
      hel(i)     = st%heg (idx)             
      hepl(i)    = st%hepg(idx)     
    end do
         
!...  Initialize element jacobians
    if (newJac >= 1) then
      eKns   = 0d0           
      eKlsc  = 0d0
      eKlsrd = 0d0  
      eKmesh = 0d0     
    endif
    if (newJac >= 2) then    
      eKlscu  = 0d0     
      eKrdphi = 0d0      
    endif   
        
!...  Initialize element residuals    
    eRns   = 0d0
    eRlsc  = 0d0
    eRlsrd = 0d0
    eRmesh = 0d0

!---------------------------------------------------------------------------------------    
!...  Fluid
!---------------------------------------------------------------------------------------  
    if (msh%EL_TYP(iel) == 0) then    
         
!...  Loop over integration points
      do igauss = 1, NGAUSS
        shl = 0d0;  shgradg = 0d0; shhessg = 0d0; 
        
        call eval_shape(nshl, iel, gp(igauss,:), xl+dlold, wl,         &
                        shl, shgradgold, shhessg,  DetJold,  &
                        dxdxi,dxidx,Gij,Ginv,0, msh%iga)  
 
        
        call eval_shape(nshl, iel, gp(igauss,:), xl+dl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,NS_hess_flag, msh%iga)  

        detJgwt = DetJ*gw(igauss) 
       
!...  Interpolate     

        do i = 1, msh%NSD
          ui (i) = sum(ul (:,i)*shl)
          uiold (i) = sum(ulold (:,i)*shl)
          umi(i) = sum(uml(:,i)*shl)
          aci(i) = sum(acl(:,i)*shl)
          dpridxi(i) = sum(pl*shgradg(:,i))

          dphidxi(i)    = sum(phil*shgradg(:,i))
          dphiolddxi(i) = sum(philold *shgradg(:,i))
          dphiddxi(i)   = sum(phidl*shgradg(:,i)) 
          
          do j = 1, msh%NSD
            duidxi(i,j) = sum(ul(:,i)*shgradg(:,j))
            duiolddxi(i,j) = sum(ulold(:,i)*shgradg(:,j))
            ddidxi(i,j) = sum((dl(:,i)-dlold(:,i))*shgradgold(:,j))
            do k = 1, msh%NSD
              duidxixj(i,j,k) = sum (ul(:,i)*shhessg(:,j,k))         
            end do
          end do
        end do  
        
        pri  = sum(pl*shl)
        rho  = sum(rhol *shl)

        do i = 1, msh%NSD
           drhodxi(i) = sum(rhol(:)*shgradg(:,i))
        enddo

        mu   = sum(mul  *shl)    
        
        rho0 = sum(rho0l*shl)
        
        phi  = sum(phil *shl)         
        rphi = sum(rphil*shl)                
        
        phid = sum(phidl*shl) 
        
        phid_old = sum(phidl_old*shl)  

        phi1 = sum(phi1l*shl)  
        
        He   = sum(hel *shl)
        Hep  = sum(hepl*shl)
             
!...  Other variables             
        uadvi = ui - umi    
        rhoconv = 0d0

        Se = 2d0*(He-0.5d0) 
        ai(:) = dphiddxi(:)*Se/(sqrt(sum(dphiddxi*dphiddxi))+1d-15) 
     !   ai1(:)= dphidxi(:)*Se/(sqrt(sum(dphidxi*dphidxi))+1d-15) 
        ! Jinhui: it does not matter which one will be used, since we are only using
        ! one NL iteration, assemble will be used only once. They are equal at the beginning
!...  Strong residuals
        fi    = (rho-rho0)*gravvec         
        res_u = rho*aci + &
                rho*(duidxi(:,1)*uadvi(1) + &
                     duidxi(:,2)*uadvi(2) + &
                     duidxi(:,3)*uadvi(3)) + &
                dpridxi - fi - mu*(duidxixj(:,1,1)+duidxixj(:,2,2)+duidxixj(:,3,3))
         
        res_c  = rphi + sum(uadvi*dphidxi)

        gradmag = sqrt(sum(dphiddxi*dphiddxi))

        res_rd = (phid-phid_old)/pse_time+sum(ai*dphiddxi) - Se
       
!...  NavSto - Numerical parameters 
        call e3STAB_3D(Gij, Ginv, uadvi, res_u, rho, mu,  &
                       tauM_ns, tauP_ns, tauC_ns, tauBar_ns,cfl_loc)
        cfl(1)  = max(cfl(1), cfl_loc(1))

        call e3DC_beta2(ui,duidxi,Gij,Ginv,res_u,dxidx,tauM_ns,kdc_ns)
        
        kdc_ns = (1d0-He)*NS_kdc_a*kdc_ns + He*NS_kdc_w*kdc_ns

!...  Convection - Numerical parameters 
        call e3STAB_TAU(uadvi, Gij, tau_c)
        call e3DC_CAU(dphidxi,uadvi,Gij,Ginv,res_c,tau_c,kdc_c)
                   
        kdc_c = LSC_kdc*kdc_c  
            
!...  Redistance - Numerical parameters         
        
        if(flag(3).eq.1) then !!!!!!!!!!!!!if of compute tau_rd and kdc_rd
        tau_rd = 0d0
        do i = 1, msh%NSD
          do j = 1, msh%NSD
          !  tau_rd = tau_rd + ai(i)*Gij(i,j)*ai(j)
            tau_rd = tau_rd + ai1(i)*Gij(i,j)*ai1(j)
          enddo
        enddo 
      !  tau_rd = 1d0/sqrt(4/(pse_time*pse_time)+tau_rd + 1d-15)
        tau_rd = 1d0/sqrt(4/(pse_time*pse_time)+tau_rd)
        kdc_rd = 0d0
        tmp_val=0
        do ii=1,3
          do jj=1,3
            ! tmp_val=tmp_val+dphiddxi(ii)*Gij(ii,jj)*dphiddxi(jj)
            tmp_val=tmp_val+dphidxi(ii)*Gij(ii,jj)*dphidxi(jj)
          enddo
        enddo   
        kdc_rd = LSRD_kdc*abs(res_rd)/sqrt(tmp_val+1d-15) 
       endif !!!!!!!!!!!!!end if of compute tau_rd and kdc_rd
        Emod = 1d0

!...  Redistance Residual
       
     if(flag(3).eq.1) then
        do aa = 1, NSHL
          eRlsrd(aa) = eRlsrd(aa) -  &
            ( shl(aa)*res_rd &
            + sum(ai(:)*shgradg(aa,:))*tau_rd*res_rd &
            + kdc_rd*sum(shgradg(aa,:)*dphiddxi(:)) &
            + shl(aa)*LSRD_penalty_fac*Hep*(phid-phi1) )*detJgwt
        enddo
      endif  ! endif of flag(3).eq.1
        

   if (newJac >= 1) then
!...  Redistance Jacobian
        if(flag(3).eq.1) then
         do aa = 1, NSHL
            shconv(aa) = sum(shgradg(aa,:)*ai(:))
         enddo
          do bb = 1, NSHL
            do aa = 1, NSHL
       !      eKlsrd(aa,bb) = eKlsrd(aa,bb) +     &
       !                     ( shl(aa)*shconv(bb) &
       !                     + shconv(aa)*tau_rd*shconv(bb) &
       !                     + kdc_rd*sum(shgradg(aa,:)*shgradg(bb,:)) &
       !                     + shl(aa)*LSRD_penalty_fac*Hep*shl(bb) )*fact2*detJgwt
             eKlsrd(aa,bb) = eKlsrd(aa,bb) +     &
                            ( shl(aa)*shl(bb)/pse_time & ! add for pse_time
                            + shl(aa)*shconv(bb) &
                            + shconv(aa)*tau_rd*shconv(bb) &
                            + shconv(aa)*tau_rd*shl(bb)/pse_time  & ! add for pse_time
                            + kdc_rd*sum(shgradg(aa,:)*shgradg(bb,:)) &
                            + shl(aa)*LSRD_penalty_fac*Hep*shl(bb) )*detJgwt

            enddo
          enddo 
        endif !if(flag(3).eq.1) then                        
      endif
        if (newJac >= 2) then     
                                       
!...  Convection Jacobian wrt u   
          do bb = 1, NSHL
             do aa = 1, NSHL
               eKlscu(:,aa,bb) = eKlscu(:,aa,bb) + &
                   ( shl(aa)*fact2*shl(bb)*dphidxi  + &
                     tau_c*shgradg(aa,:)*fact2*shl(bb)*res_c )*detJgwt
            enddo
          enddo  
          
!...  Redist Jacobian wrt phi 
            
          do bb = 1, NSHL
             do aa = 1, NSHL
               eKrdphi(aa,bb) = eKrdphi(aa,bb) - &
                   fact2*shl(aa)*LSRD_penalty_fac*Hep*shl(bb)*detJgwt  
            enddo
          enddo            
                 
        endif                      
      enddo  ! end  loop integration points 
    

! *********************************************************************************
! To impose strong boundary condition for redistancing
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        if(flag(3).eq.1) then
         IBClsc_loc (aa,1) = IBClsc (bb)
        endif
      enddo

      if(flag(3).eq.1) then                 
       call applyBCs(nshl, 1, IBClsc_loc (1:nshl,:), eKlsrd, eRlsrd)  
      endif 
!********************************************************************************** 

             
!... Assemble global residual           
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        if(flag(3).eq.1) then
         RHSG(bb,6)   = RHSG(bb,6)   + eRlsrd(aa) 
        endif
      enddo

!... Assemble global jacobian           
      if (newJac >= 1) then  
        if(flag(3).eq.1) then  
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl),1, eKlsrd, sparseMat, LHSlsrd)   
        endif   
      end if 
      
      if (newJac >= 2) then         
        do i = 1 , msh%NSD
          call applyColBCs(nshl, IBCns_loc(1:nshl,i), eKlscu(i,:,:))
          call applyRowBCs(nshl,IBClsc_loc(1:nshl,1), eKlscu(i,:,:))
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKlscu(i,:,:),   sparseMat, LHSlscu(i,:) ) 
        enddo 
                                             
        call applyColBCs(nshl,IBClsc_loc(1:nshl,1),eKrdphi) 
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKrdphi,   sparseMat, LHSrdphi) 
      end if         
        
                           
!---------------------------------------------------------------------------------------    
!...  Solid
!--------------------------------------------------------------------------------------- 
    else if (msh%EL_TYP(iel) == 1) then

!...  Loop over integration points
      do igauss = 1, NGAUSS       
        Ftens = 0.0d0; Stens = 0.0d0; Ctens = 0.0d0
      
        call eval_shape(nshl, iel, gp(igauss,:), xl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,hess_flag, msh%iga)  

        do i = 1, msh%NSD
          di (i) = sum(dl(:,i)*shl)
          ui(i)  = sum(ul(:,i)*shl)
          aci(i) = sum(acl(:,i)*shl)          
          do j = 1, msh%NSD
            ddidxi(i,j) = sum(dl(:,i)*shgradg(:,j))
          end do
        end do
        fi = 0d0
     
        call e3tensors_nonlin(msh%NSD, ddidxi, mu, kappa, Ftens, &
                              Stens, Ctens)

        Stens = Stens !!+ FPKS(iel,:,:)

!...  Solid Residual        
        call e3Rhs_3D_struct(nshl, aci, ui, fi, Ftens, Stens, gw(igauss), &
                              shl, shgradg, eRns(1:3,:), damp)
       
!...  Solid Jacobian        
        if (newJac >= 1) then
          call e3LHS_3D_struct(nshl, gw(igauss), shl, shgradg, Ftens, &
                               Stens, Ctens, eKns, damp)
        end if
    
      enddo  ! end of integration point loop
       
!... Enforce strong BCs
      !call applyBCs(nshl,msh%IEN(iel,1:nshl), msh%NSD+1, IBC, eKns , eRns) 
                         
!... Assemble global residual

      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        RHSG(bb,1:3) = RHSG(bb,1:3) + eRns  (aa,1:3)  
      enddo
              
!... Assemble global jacobian           
      !if (newJac >= 1) then
      !  call FillSparseMat_3D(nshl, iel, eKns, eGns, eDns, eMns)             
      !end if     
    endif ! end Fluid/Solid if 
      
  enddo ! end of element loop 
  
!...  Deallocate
  if (NSHL >= 0) then
    deallocate(shl, shgradg,shgradgold, shhessg, gp, gw, &
               eRns,eRlsc,eRlsrd, eRmesh, &
               xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul, &
               ulold,rho0l,phil,rphil,philold,phi1l,phidl, &
               hl, hel, hepl)
               
    !if (newJac >= 1) then          
       deallocate(eKns,eKlsc,eKlsrd, eKmesh,eKlscu,eKrdphi)
    !endif           
  endif
    
end subroutine IntAssembly_rd

