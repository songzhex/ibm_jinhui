module postpro
  save 
  type pointData
    real(8), allocatable :: coeff(:)
    integer, allocatable :: idx(:)
    integer :: NSHL = -1
    integer :: Proc 
    real(8) :: x(3)
  end type pointData
  
  integer , parameter  :: npres    = 8  
  integer , parameter  :: nheight  = 4
  integer , parameter  :: hsamples = 21 
      
  type(pointData) presPnts   (npres)
  type(pointData) heightPnts (nheight, hsamples)

  !----------------------------------------------------------------------
  contains
  !----------------------------------------------------------------------
  subroutine findpoint(pt,fx)

  use mpi
  use globdata

  implicit none
  type(pointData), intent(out) :: pt
  real(8),         intent(in)  :: fx(3)
      
  ! Local
  integer :: iel,NSHL,inside,hess_flag,iter, i,idx
  
  real(8), allocatable :: shg(:),shgradg(:,:),shhessg(:,:,:)
  real(8), allocatable :: xl(:,:),dl(:,:), wl(:)  
  
  real(8) :: x(mesh_fluid%NSD),xi(mesh_fluid%NSD), dxi(mesh_fluid%NSD)

  real(8) :: dxdxi(mesh_fluid%NSD,mesh_fluid%NSD), dxidx(mesh_fluid%NSD,mesh_fluid%NSD), &
             Gij(mesh_fluid%NSD,mesh_fluid%NSD), Ginv(mesh_fluid%NSD,mesh_fluid%NSD), DetJ

  integer :: tmp 
  
  pt%proc = -1
  pt%x     = fx
  NSHL    = -1
  hess_flag = 0  
  do iel = 1, mesh_fluid%NELEM
         
    if (NSHL /= mesh_fluid%ELMNSHL(iel)) then      
      if (NSHL >= 0) then
        deallocate(shg, shgradg, shhessg, xl,dl,wl)
      end  if
     
      NSHL   = mesh_fluid%ELMNSHL(iel)

      allocate(shg(NSHL), shgradg(NSHL,mesh_fluid%NSD),shhessg(NSHL,mesh_fluid%NSD,mesh_fluid%NSD),&
               xl(NSHL,mesh_fluid%NSD), dl(NSHL,mesh_fluid%NSD),wl(NSHL))
    end if
    
    ! Get local node coords            	   
    do i = 1, NSHL
       idx = mesh_fluid%IEN(iel,i)       
       xl(i,:) = mesh_fluid%xg(idx,:)!! + dg(idx,:)
       wl(i)   = mesh_fluid%wg(idx)
    enddo

    ! Quick check wether point might be in element
    inside = 0
    do i = 1, mesh_fluid%NSD
      if ((fx(i).le.(maxval(xl(1:nshl,i),1)+1d-6 )).and. &
          (fx(i).ge.(minval(xl(1:nshl,i),1)-1d-6 ))) then
        inside = inside + 1
      endif
    enddo
		  
		  
    if (inside.eq.mesh_fluid%NSD) then
      xi = 0d0

      do iter = 1,15
          hess_flag = 0
          call eval_shape(nshl, iel, xi, xl, wl,         &
                      shg, shgradg, shhessg,  DetJ,  &
                      dxdxi,dxidx,Gij,Ginv,hess_flag, mesh_fluid%iga)     
       
	  ! Interpolate
          do i = 1, mesh_fluid%NSD
            x(i) = sum(xl(:,i)*shg)
          enddo
  
          ! Solve update
          do i = 1, mesh_fluid%NSD
            dxi(i) = sum(dxidx(i,:)*(x - fx))
          enddo 
	
	  ! Update 
          xi = xi - dxi
	  	  
	  ! Check convergence
          if (isNAN(sum(dxi*xi))) then
            write(*,*) iel,iter
            write(*,*) "NAN found:", fx
            write(*,*) shg
            write(*,*) xi
            write(*,*) dxi
            stop
          endif
          
          if (sum(dxi*dxi).lt.1d-16) then
            exit
          endif

	  ! Check convergence
          if (iter .eq. 15 ) then
            write(*,*) "NOT converged:", fx
            write(*,*) xi
          endif       
      enddo 
      
      ! Check if xi is inside element -- assuming possitivity of shapefunctions
      if (minval(shg(1:nshl),1).ge.-1d-8) then
      
         if (pt%nshl /= -1) deallocate(pt%coeff,pt%idx)
         
         allocate(pt%coeff(nshl),pt%idx(nshl))  
         
         pt%nshl  = NSHL
         pt%proc  = myid
         
         pt%coeff = shg
         pt%idx   = mesh_fluid%IEN(iel,1:nshl)
                  
         if (minval(shg(1:nshl),1).ge.0d0) exit   
      endif
	      
    endif ! inside if

  enddo ! elem loop
  
  
  if (NSHL >= 0) then
      deallocate(shg, shgradg, shhessg, xl,dl,wl)
  endif

  ! Communicate proc which contains point  
    tmp = pt%proc
    call MPI_ALLREDUCE(tmp, pt%proc, 1, MPI_INTEGER,  &
                       MPI_MAX, MPI_COMM_WORLD,mpi_err) 
  
  ! Check if at least one proc found point
  if (ismaster.and.(pt%proc.eq.-1)) then
    write(*,'(a,1x,3F12.6)') "NOT FOUND:", fx
    stop
  endif
    	       
  end subroutine findpoint

  !----------------------------------------------------------------------
  subroutine interpolate(val, pt, sol)
    use mpi
    
    real(8),         intent(out) :: val
    type(pointData), intent(in)  :: pt
    real(8),         intent(in)  :: sol(*)
    integer :: i
    
    if (pt%Proc.eq.myid) then
      val = 0d0
      do i = 1, pt%NSHL
        val  = val + pt%coeff(i)*sol(pt%idx(i))
      enddo      
    endif 
     
    call MPI_BCAST(val,  1, MPI_DOUBLE_PRECISION,&
                   pt%Proc, MPI_COMM_WORLD,status,mpi_err)                     

  end subroutine interpolate 

  !----------------------------------------------------------------------
  !     Routine for interpolating the pressure  
  !       for the marin problem   
  !----------------------------------------------------------------------
  subroutine marinPressure(pg)

    use mpi
    use params
    implicit none
  	      
    real(8) :: pg  (*)           
    real(8) :: pres(npres) 
    integer :: i
    integer, save :: pfile = -1

    if (pfile.eq.-1) then
      pfile = 123  
    
      call findpoint(presPnts(1), (/2.395d0-1d-6, 0.5d0-0.2015d0+0.176d0,   0.02d0/))
      call findpoint(presPnts(2), (/2.395d0-1d-6, 0.5d0-0.2015d0+0.176d0,   0.10d0/)) 
      call findpoint(presPnts(3), (/2.4195d0,     0.5d0+0.2015d0 - 0.176d0, 0.161d0+1d-6/)) 
      call findpoint(presPnts(4), (/2.4995d0,     0.5d0+0.2015d0 - 0.176d0, 0.161d0+1d-6/))
      call findpoint(presPnts(5), (/2.395d0-1d-6, 0.5d0,                    0.02d0/))
      call findpoint(presPnts(6), (/2.395d0-1d-6, 0.5d0,                    0.10d0/))
      call findpoint(presPnts(7), (/2.4195d0,     0.5d0,                    0.161d0+1d-6/))   
      call findpoint(presPnts(8), (/2.4995d0,     0.5d0,                    0.161d0+1d-6/))
      
      if(ismaster) call openStreamFile (pfile,"pres.dat") 
    endif

    do i = 1, npres  
      call interpolate(pres(i), presPnts(i), pg)
    enddo 
               
    ! Print data to file  	    
    if (ismaster) then 
      write(pfile,'(9ES15.7)') time, pres
    endif

  end subroutine marinPressure
      
  !----------------------------------------------------------------------
  !     Routine for interpolating the height 
  !       for the marin problem  
  !----------------------------------------------------------------------
  subroutine marinHeight(phig)
    use mpi
    use params
    implicit none
         
    real(8) :: phig  (*)           
    real(8) :: height(nheight) 
    integer :: i,s
    integer, save :: hfile = -1      
    real(8) :: z, phi,phi0,phi1,x(3),x0(3),x1(3)  
    type(pointData) :: pd
  
    if (hfile.eq.-1) then
      hfile = 124  
     
      do i = 1,  hsamples   
        z = real(i-1)/real(hsamples-1) 
        call findpoint(heightPnts(1,i), (/3.22d0 - 0.496d0, 0.5d0, z/))
        call findpoint(heightPnts(2,i), (/3.22d0 - 0.992d0, 0.5d0, z/))
        call findpoint(heightPnts(3,i), (/3.22d0 - 1.488d0, 0.5d0, z/))
        call findpoint(heightPnts(4,i), (/3.22d0 - 2.638d0, 0.5d0, z/))
      enddo     
      
      if(ismaster) call openStreamFile (hfile,"height.dat") 
    endif      
  
    do i = 1, nheight    
      phi  = 0d0
      x    = 0d0
      ! Loop over samples to find heighest positive phi point
      do s = hsamples, 1 , -1
       
        call interpolate(phi1, heightPnts(i,s), phig)
   
        if (phi1.gt.0d0) then 
          x0 = heightPnts(i,s+1)%x
          x1 = heightPnts(i,s)%x       
          x = x0 - (x1 - x0)/(phi1 - phi0)*phi0
          phi = 1d0
          exit     
        else
          phi = 0d0
          height(i) = 0d0
        endif   
      
        phi0 = phi1      
      enddo
      
      call MPI_BARRIER (MPI_COMM_WORLD,mpi_err)    

      ! Iterate - bisection 
      s = 0
      do s = 1, 10
        if (abs(phi).le.1d-6) exit
           
        x = x0 - (x1 - x0)/(phi1 - phi0)*phi0
       
        call findpoint(pd,x)       
        call interpolate(phi, pd, phig)  
       
        ! Update min/max 
        if (phi.ge.0d0) then
          phi1 = phi
          x1 = x
        else
          phi0 = phi
          x0 = x      
        endif      
      enddo
    
      height(i) = x(3)
    enddo 

               
    ! Print data to file  	    
    if (ismaster) then 
      write(hfile,'(9ES15.7)') time, height
    endif

  end subroutine marinHeight

end module postpro 
