
!======================================================================
! Build traction on the surface, then do l2-projection put to the 
! control points
! NMLM:  Lumped mass for non-matching
! NMTra: Traction for non-matching
! project 
!======================================================================
subroutine FaceAsse_Tra(msh, NNODE, NSD, dgAlpha, ugAlpha, pgAlpha, rhoAlpha, muAlpha, NMLMs, NMTra, NM)
  use aAdjKeep
  use mpi
  use commonvars
  use defs_shell
  implicit none
     

  type(meshStruct), intent(in) :: msh
  integer, intent(in) :: NNODE, NSD
  type(shell_nmb), intent(in) :: NM

  real(8), intent(in) :: dgAlpha(NNODE,NSD),   ugAlpha(NNODE,NSD), &
                         pgAlpha(NNODE), rhoAlpha(NNODE), muAlpha(NNODE)

  real(8), intent(out) :: NMLMs(NNODE), NMTra(NNODE,NSD)

  ! Local variables
  integer :: b, ifac, iel, igauss, i, j, k, hess_flag, aa, bb, nshl, &
                      FID, itemp, iturb
  
!**************************************************************************
  real(8) :: RHSGu2(NNODE,NSD), RHSGp2(NNODE)
  integer :: NSHL,NSHLB, NGAUSSB 
!  real(8), allocatable :: eKns(:,:,:),eKlsc(:,:),eKlscu (:,:,:) 
  real(8), allocatable :: eRns(:,:),eRlsc(:)
  real(8), allocatable :: gp(:,:), gw(:) 
  real(8), allocatable :: Rhsm(:,:) 
  real(8), allocatable ::  acml(:,:), acl(:,:), rhol(:),mul(:), gl(:,:), phil(:), philbc(:)
  integer ::  faceor 
  integer :: IBCns_loc(msh%maxNSHL,msh%NSD+1), IBClsc_loc(msh%maxNSHL,1)
  real(8) :: DetJb
  real(8) :: rho, mu
!**************************************************************************
  real(8), allocatable :: shlu(:), shgradgu(:,:)
  real(8), allocatable :: dl(:,:), ul(:,:), pl(:), &
                          xl(:,:), wl(:)
  real(8), allocatable :: rhs1(:), rhs2(:,:)
  real(8) :: Detjb, rho, mu

  real(8) :: pri, duidxi(NSD,NSD), dxidx(NSD,NSD), dxdxi(NSD,NSD), &
             Gij(NSD,NSD), Ginv(NSD,NSD), ti(NSD), &
             nor(NSD)
		
  NSHL = -1
	 
!  call genGPandGWb(gp, gw, NGAUSSb)

  ! loop over faces
  do b = 1, msh%NBOUND

    ! Non-matching boundaries
    FID = msh%bound(b)%Face_ID
   itemp = 9
   do iturb = 1, NM%NTurb
    itemp = itemp + 1
    if (FID==itemp + iturb .or. FID==itemp + 1 + iturb) then
!    if (FID==11 .or. FID==12) then

      do ifac = 1,msh% bound(b)%NFACE
  
        iel = msh%bound(b)%F2E(ifac)
        faceor =  msh%bound(b)%FACE_OR(ifac)     
    !    if (NSHL /= msh%ELMNSHL(iel)) then
       if ( (NSHL    /= msh%ELMNSHL(iel)).or. &
             (NSHLB   /= msh%bound(b)%NSHLB(ifac)).or. &
             (NGAUSSB /= msh%bound(b)%NGAUSSB(ifac)) ) then

          if (NSHL >= 0) then
            deallocate(shlu, shgradgu, dl, ul, pl, xl, wl, rhs1, rhs2)
          end if

      !   NSHL = ELMNSHL(iel)
          NSHL    = msh%ELMNSHL(iel)
          NSHLB   = msh%bound(b)%NSHLB(ifac) 
          NGAUSSB = msh%bound(b)%NGAUSSB(ifac)
          allocate(shlu(NSHL), shgradgu(NSHL,NSD), dl(NSHL,NSD),  &
                   ul(NSHL,NSD), pl(NSHL), xl(NSHL,NSD), wl(NSHL), &
                   rhs1(NSHL), rhs2(NSD,NSHL))
          allocate(gp(NGAUSSB,msh%NSD-1),gw(NGAUSSB), &
                            rhol(NSHL),mul(NSHL),&
                            eRns(msh%NSD+1,NSHL), eRlsc(NSHL))
          call genGPandGWb(gp,gw,NGAUSSB,NSHLB)  
        end if
	
        ! get the mapping between face element and volume surface
      !  call genGPMap(nshl, NGAUSSb, bound(b)%FACE_OR(ifac), iga, mgp)
     	  
        ! Get local solution arrays   	       	     
        do aa = 1, NSHL
          k = IEN(iel,aa)
          xl  (aa,:) = xg       (k,:)
          dl  (aa,:) = dgAlpha  (k,:)
          ul  (aa,:) = ugAlpha  (k,:)
          pl  (aa)   = pgAlpha  (k)
          wl  (aa)   = 1d0 ! wg       (k)
          rhol (i)   =  rhoAlpha(k)
          mul (i)    = muAlpha (k)
        end do

        ! Get the density and viscosity. 
        ! Now just set it to water and constant for each element
        ! initialize local load vector
        rhs1 = 0.0d0; rhs2 = 0.0d0
	  
        ! Loop over integration points
        do igauss = 1, NGAUSSb

 !         call eval_faceshape(nshl, iel, gp(igauss,:), mgp(igauss,:), &
 !                            bound(b)%FACE_OR(ifac), &
 !                            xl, dl, wl, shlu, shgradgu, dxidx, &
 !                            Gij, Ginv, nor)

          call eval_faceshape(nshl, iel, gp(igauss,:), &
                              faceor,xl+dl, wl, &
                              shlu, shgradgu,Detjb, dxdxi,dxidx, &
                              Gij, Ginv, nor, msh%iga)  
          
          !-----------------------------------------------
          ! Get the quantities at the intergration point
          !-----------------------------------------------

          ! pressure
          pri = sum(pl*shlu)	      
	    
          ! velocity gradient
          do j = 1, NSD
            do i = 1, NSD
              duidxi(i,j) = sum(ul(:,i)*shgradgu(:,j))
            end do
          end do
          rho = sum(rhol*shlu)
          mu  = sum(mul *shlu)  
          ! traction
          ti(:) = -pri*nor(:) !+ mu*(duidxi(:,1)*nor(1) + &
                              !      duidxi(:,2)*nor(2) + &
                              !      duidxi(:,3)*nor(3) + &
                              !      duidxi(1,:)*nor(1) + &
                              !      duidxi(2,:)*nor(2) + &
                              !      duidxi(3,:)*nor(3))

          do aa = 1, NSHL
            ! lumped mass
            rhs1(aa) = rhs1(aa) + shlu(aa)*DetJb*gw(igauss)
            ! traction
            rhs2(:,aa) = rhs2(:,aa) + shlu(aa)*ti(:)*DetJb*gw(igauss)
          end do

        end do

         ! Assemble LM and Tra
        do aa = 1, NSHL
          k = msh%IEN(iel,aa)    
          NMLMs(k)   = NMLMs(k)   + rhs1(aa)  
          NMTra(k,:) = NMTra(k,:) + rhs2(:,aa)
        end do

      end do ! NFACE
    end if
   end do ! End iturb
  end do  ! NBOUND

  if (NSHL >= 0) then
    deallocate(shlu, shgradgu, dl, ul, pl, xl, wl, rhol, mul, rhs1, rhs2)
  end if

  ! communicate
  if (numnodes > 1) then
    call commu(NMLMs, 1  , 'in ')
    call commu(NMTra, NSD, 'in ')
  end if

  ! devided by lumped mass to get nodal traction
  forall (i = 1:NNODE, NMLMs(i) > 1.0d-15) 
    NMTra(i,:) = NMTra(i,:)/NMLMs(i)
  end forall
 
end subroutine FaceAsse_Tra

