!======================================================================
! FaceAssembly to build local RHS and LHS for non-matching boundaries.
!======================================================================
!subroutine FaceAsse_DG_w1t1(dgAlpha, ugAlpha, ugmAlpha, pgAlpha, NM)
subroutine FaceAsse_DG_w1t1(st, msh,  rhsg, newJac,flag,NM, flag_NS, flag_LS)
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use boundaryConditions  
  use matrix  
  use aAdjKeep
  implicit none
  type(state),      intent(in) :: st
  type(meshStruct), intent(in) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,5)  
  integer, intent(in) :: newJac
  integer, intent(in) :: flag(4), flag_NS, flag_LS
  type(shell_nmb), intent(in) :: NM

 ! real(8), intent(in) :: dgAlpha(NNODE,NSD), ugAlpha(NNODE,NSD), &
 !                        ugmAlpha(NNODE,NSD), pgAlpha(NNODE)                        
 ! Local variables
!**************************************************************************
  integer, parameter :: NSD = 3
  integer :: NSHL,NSHLB, NGAUSSB 
  real(8), allocatable :: eKns(:,:,:),eKlsc(:,:),eKlscu (:,:,:) 
  real(8), allocatable :: eRns(:,:),eRlsc(:)
  real(8), allocatable :: gp(:,:), gw(:) 
  real(8), allocatable ::  acml(:,:), acl(:,:), rhol(:),mul(:), gl(:,:), phil(:), philbc(:)
  integer :: faceor 
  integer :: IBCns_loc(msh%maxNSHL,NSD+1), IBClsc_loc(msh%maxNSHL,1)
  real(8) :: DetJb
  real(8) :: rho, mu
!**************************************************************************
  real(8), allocatable :: shlu(:), shgradgu(:,:), shhessgu(:,:,:)       
  real(8), allocatable :: dl(:,:), ul(:,:), uml(:,:), pl(:), xl(:,:), wl(:)
  real(8), allocatable :: xKebe11(:,:,:), xMebe(:,:), xGebe(:,:,:),  xDebe1(:,:,:)
  real(8), allocatable :: Rhsu(:,:), Rhsp(:)

 integer :: a, b, ifac, iel, igauss, i, j, k, hess_flag
 integer :: aa, bb, aaaa, bbbb, FID, iturb, itemp
  
!  real(8) :: gp(NGAUSSb,2), gw(NGAUSSb), mgp(NGAUSSb,3)    
  real(8) :: ui(NSD), pri, phi, duidxi(NSD,NSD), Gij(NSD,NSD), &
                    Ginv(NSD,NSD), ti(NSD), nor(NSD), umi(NSD), &
                    dxidx(NSD,NSD), dxdxi(NSD,NSD)
  real(8) :: tmp1(NSD), tmp2(NSD,NSD), tauB, tauC

  NSHL = 4
  NSHLB = 3
  NGAUSSB = 3

allocate(shlu(NSHL), shgradgu(NSHL,NSD), shhessgu(NSHL,NSD,NSD), &
                 dl(NSHL,NSD), ul(NSHL,NSD), uml(NSHL,NSD), pl(NSHL), &
                 xl(NSHL,NSD), wl(NSHL), &
                 xKebe11(NSD*NSD,NSHL,NSHL), xMebe(NSHL,NSHL), &
                 xGebe(NSD,NSHL,NSHL), xDebe1(NSD,NSHL,NSHL), &      
                 Rhsu(NSD,NSHL), Rhsp(NSHL))

allocate(gp(NGAUSSB,NSD-1),gw(NGAUSSB), &
                 acl(NSHL,NSD), acml(NSHL,NSD), gl(NSHL,NSD),&
                 rhol(NSHL),mul(NSHL),&
                 phil(NSHL),philbc(NSHL),&
                 eKns((NSD+1)*(NSD+1),NSHL,NSHL) , &
                 eKlsc(NSHL,NSHL), eKlscu(NSD,NSHL,NSHL), &   
                 eRns(NSD+1,NSHL), eRlsc(NSHL))
!  call genGPandGWb(gp, gw, NGAUSSb) ! Old code
! loop over faces
!return
  do b = msh%NBOUND,1, -1
    ! non-matching boundaries
   FID = msh%bound(b)%Face_ID
   itemp = 9
   do iturb = 1, NM%NTurb
    itemp = itemp + 1
    if (FID==itemp + iturb .or. FID==itemp + 1 + iturb) then
      do ifac = 1, msh%bound(b)%NFACE   
         iel = msh%bound(b)%F2E(ifac)
         faceor =  msh%bound(b)%FACE_OR(ifac)      
         NSHL    = msh%ELMNSHL(iel)
      !    NSHLB   = msh%bound(b)%NSHLB(ifac) 
      !    NGAUSSB = msh%bound(b)%NGAUSSB(ifac)
      !    if(NSHL /= 4) write(*,*) "Element type changed"
          call genGPandGWb(gp,gw,NGAUSSB,NSHLB)  
        !  write(*,*) myid, msh%bound(b)%Face_ID,"gauss", newJac
        ! get the mapping between face element and volume surface
        ! call genGPMap(nshl, NGAUSSb, bound(b)%FACE_OR(ifac), iga, mgp)
        ! Chec   
        ! Get local solution arrays   	       	     
        do i = 1, NSHL
          j = msh%IEN(iel,i)
          xl (i,:) = msh%xg(j,:)
         ! dl (i,:) = dgAlpha (j,:)
          dl (i,:) = st%dg  (j,:)
         ! ul (i,:) = ugAlpha (j,:)
          ul (i,:) = st%ug  (j,:)
         ! uml(i,:) = ugmAlpha(j,:)
          uml  (i,:) = st%ugm (j,:)
         ! pl (i)   = pgAlpha (j)
          pl  (i)   = st%pg  (j)
         ! wl (i)   = wg  (j)
          rhol (i)   = old%rhog(j)
          mul (i)    = old%mug (j)
          phil(i)    = st%phig(j) 
          gl (i,:)   = ugbc(j,:)
          philbc (i) = phigbc(j)
          wl (i)   = 1d0
        end do
        !  write(*,*) myid, msh%bound(b)%Face_ID,"nodal"
        ! Get the density and viscosity. 
        ! Now just set it to water and constant for each element
        ! rho = rhow
        ! mu  = muw
        ! initialize local load vector
        Rhsu = 0.0d0    
        Rhsp = 0.0d0   
        eRlsc = 0.0d0
        ! initialize local resistance matrix	
        if (newJac >= 1) then
          eKns = 0d0         
          eKlsc = 0d0
        ! initialize local resistance matrix	
         xKebe11 = 0.0d0         
         xGebe   = 0.0d0
         xDebe1  = 0.0d0
         xMebe   = 0.0d0
    
        endif
        if (newJac >= 2) then       
          eKlscu = 0d0
        endif
        !   write(*,*) myid, msh%bound(b)%Face_ID,"noda111111111111111l"
        ! Loop over integration points (NGAUSSb in each direction) 
        do igauss = 1, NGAUSSb

       !   call eval_faceshape(nshl, iel, gp(igauss,:), mgp(igauss,:), &
       !                       bound(b)%FACE_OR(ifac), &
       !                       xl, dl, wl, shlu, shgradgu, dxidx, &
       !                       Gij, Ginv, nor)
          call eval_faceshape(nshl, iel, gp(igauss,:), &
                              faceor,xl+dl, wl, &
                              shlu, shgradgu,Detjb, dxdxi,dxidx, &
                              Gij, Ginv, nor, msh%iga)
      !  write(*,*) myid, msh%bound(b)%Face_ID,"shape"

          ! Interpolate	    
          pri = sum(pl*shlu)
          phi = sum(phil*shlu)
     
          do i = 1, NSD
            ui (i) = sum(ul (:,i)*shlu)
            umi(i) = sum(uml(:,i)*shlu)
          end do
    
          do j = 1, NSD
            do i = 1, NSD
              duidxi(i,j) = sum(ul(:,i)*shgradgu(:,j))
            end do
          end do

         rho = sum(rhol*shlu)
         mu  = sum(mul *shlu)  

          ! traction
          ti(:) = -pri*nor(:) + mu*(duidxi(:,1)*nor(1) + &
                                    duidxi(:,2)*nor(2) + &
                                    duidxi(:,3)*nor(3) + &
                                    duidxi(1,:)*nor(1) + &
                                    duidxi(2,:)*nor(2) + &
                                    duidxi(3,:)*nor(3))

        !!!          ti(:) = -pri*nor(:)

          call e3bSTAB_DG(tauB, tauC,  ui-umi, nor, dxidx, rho, mu)

        !   write(*,*) myid, msh%bound(b)%Face_ID,"paramer"
          if(flag_NS.eq.1) then
           call e3bRHS_DG(NSD, nshl, shlu, shgradgu, gw(igauss), ui, &
                         ti, nor, tauB, Rhsu, Rhsp, rho, mu, Detjb, 1.0d0)
          endif

          if(flag_LS.eq.1) then
           call e3bRHS_DG_LS(NSD, nshl, shlu, shgradgu, gw(igauss),phi, ui, &
                             nor, tauC, eRlsc, rho, mu, Detjb, 1.0d0)
          endif
          

      !    write(*,*) myid, msh%bound(b)%Face_ID,"residual"
         if (newJac >= 1) then
          if(flag_NS.eq.1) then
            call e3bLHS_DG(NSD, nshl, shlu, shgradgu, ui, umi, duidxi, tauB, &
                          gw(igauss), nor, xKebe11, xGebe, xDebe1, rho, mu, Detjb) 
          endif
          if(flag_LS.eq.1) then

           do aaaa = 1, NSHL
            do bbbb = 1, NSHL
              eKlsc(aaaa,bbbb) = eKlsc(aaaa,bbbb) + shlu(aaaa)*tauC*shlu(bbbb)*DetJb*gw(igauss)
             enddo
            enddo

           endif
          endif    
        end do
!**************************************************************************************
! Copy right hand side residual
       eRns(1: NSD, 1: NSHL) = Rhsu(1: NSD, 1: NSHL) 
       eRns(1+ NSD, 1: NSHL) = Rhsp(1: NSHL) 
    
!**************************************************************************************
! Copy the left-hand side matrix
    if (newJac >= 1) then
      eKns(1:3,:,:)      =  xKebe11(1:3,:,:) 
      eKns(5:7,:,:)      =  xKebe11(4:6,:,:) 
      eKns(9:11,:,:)   =  xKebe11(7:9,:,:) ! velocity with respect to velocity
      eKns(16,:,:)       =     xMebe(:,:)  ! pressure with respect to pressure 
      eKns(4,:,:)   =  xGebe(1,:,:) 
      eKns(8,:,:)   =  xGebe(2,:,:) 
      eKns(12,:,:)   = xGebe(3,:,:) 
      eKns(13:15,:,:) = xDebe1(1:3,:,:)  ! corss term
  endif
!**************************************************************************************

 !... Enforce strong BCs         
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        IBCns_loc  (aa,:) = IBCns  (bb,:)
        IBClsc_loc (aa,1) = IBClsc (bb) 
      enddo
      
       
      call applyBCs(nshl, msh%NSD+1, IBCns_loc(1:nshl,:),  eKns,  eRns)        
      call applyBCs(nshl, 1 ,       IBClsc_loc(1:nshl,:), eKlsc, eRlsc)

!... Assemble global residual           
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        RHSG(bb,1:4) = RHSG(bb,1:4) + eRns  (:,aa)              
        RHSG(bb,5)   = RHSG(bb,5)   + eRlsc (aa)              
      enddo
!           write(*,*) myid, msh%bound(b)%Face_ID,"assmeble"
!... Assemble global jacobian           
      if (newJac >= 1) then           
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl), NSD+1, eKns,   sparseMat, LHSns )            
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl),         1, eKlsc,  sparseMat, LHSlsc )         
      end if    
   !     call BCLhs_3D(nshl, iel, xKebe11, xGebe, xDebe1, xMebe, Rhsu, Rhsp)
   !     call FillSparseMat_3D(nshl, iel, xKebe11, xGebe, xDebe1, xMebe) 
   !     call LocaltoGlobal_3D(nshl, iel, Rhsu, Rhsp)
         end do
      end if
     end do   
 !   write(*,*) "Haha",myid, b, msh%bound(b)%Face_ID
  end do
   
deallocate(shlu, shgradgu, shhessgu, &
                     dl, ul, uml, pl, &
                     xl, wl, &
                     xKebe11, xMebe, &
                     xGebe, xDebe1, &      
                     Rhsu, Rhsp)

deallocate(gp, gw, &
                      acl, acml, gl,&
                      rhol,mul,&
                      phil,philbc,&
                      eKns , &
                     eKlsc, eKlscu, &   
                     eRns,eRlsc)
end subroutine FaceAsse_DG_w1t1


!======================================================================
! FaceAssembly to build non-local RHS for Matrix-Free method at the
! non-matching boundaries. For the approximation of R'du
!======================================================================
!subroutine FaceAsse_DG_w1t2(dgAlpha, ugAlpha, ugmAlpha, pgAlpha, &
!                            RHSGu2, RHSGp2, NM)
subroutine FaceAsse_DG_w1t2(st, msh,  rhsg, newJac,flag,NM, flag_NS, flag_LS)
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use boundaryConditions  
  use matrix  
  use aAdjKeep
  implicit none
     
  type(state),      intent(in) :: st
  type(meshStruct), intent(in) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,5)  
  integer, intent(in) :: newJac
  integer, intent(in) :: flag(4),flag_NS, flag_LS
  type(shell_nmb), intent(inout) :: NM
       
 ! real(8), intent(in) :: dgAlpha(NNODE,NSD),  ugAlpha(NNODE,NSD), &
 !                        ugmAlpha(NNODE,NSD), pgAlpha(NNODE)
 integer, parameter :: NSD = 3 
!**************************************************************************
  real(8) :: RHSGu2(msh%NNODE,NSD), RHSGp2(msh%NNODE)
  integer :: NSHL,NSHLB, NGAUSSB 
 real(8), allocatable :: eKns(:,:,:),eKlsc(:,:)
  real(8), allocatable :: eRns(:,:),eRlsc(:)
  real(8), allocatable :: gp(:,:), gw(:) 
  real(8), allocatable ::  acml(:,:), acl(:,:), rhol(:),mul(:), gl(:,:), phil(:), philbc(:)
  integer :: faceor 
  integer :: IBCns_loc(msh%maxNSHL,NSD+1), IBClsc_loc(msh%maxNSHL,1)
  real(8) :: DetJb
  real(8) :: rho, mu
!**************************************************************************

  ! Local variables
  integer :: a, b, ifac, iel, igauss, i, j, k, hess_flag
  integer :: aa, bb, FID, itemp, iturb
  
 ! real(8) :: gp(NGAUSSb,2), gw(NGAUSSb), mgp(NGAUSSb,3)

  real(8), allocatable :: shlu(:), shgradgu(:,:)
  real(8), allocatable :: dl(:,:), ul(:,:), ul_current(:,:), uml(:,:), &
                          pl(:), xl(:,:), wl(:), tl(:,:)
  real(8), allocatable :: Rhsu(:,:), Rhsp(:) 

  real(8) :: ui(NSD), ui_current(NSD), pri, phi, duidxi(NSD,NSD), dxidx(NSD,NSD), dxdxi(NSD,NSD), &
                    Gij(NSD,NSD), Ginv(NSD,NSD), ti(NSD), &
                    nor(NSD), umi(NSD)

  real(8) :: tauB, tauC, unor, upos, uneg, sgn

  ! lumped mass and traction
  real(8) :: NMLMs(msh%NNODE), NMTra(msh%NNODE,NSD), NMRHS1(msh%NNODE,NSD), &
                    NMRHS2(msh%NNODE,NSD), tgLocal(msh%NNODE,NSD), &
                    ugLocal(msh%NNODE,NSD), pgLocal(msh%NNODE,NSD), philLocal(msh%NNODE), r2tmp(msh%NNODE,NSD),r3tmp(msh%NNODE)

  real(8) ::  NMTra_from(msh%NNODE,2*NSD+1), NMTra_to(msh%NNODE,2*NSD+1)  ! 1:3, traction, 4:6, velocity, 7, level set

  NSHL = 4
  NSHLB = 3
  NGAUSSB = 3
allocate(shlu(NSHL), shgradgu(NSHL,NSD), dl(NSHL,NSD),     &
                   ul(NSHL,NSD),ul_current(NSHL,NSD), uml(NSHL,NSD), tl(NSHL,NSD),     &
                   pl(NSHL), xl(NSHL,NSD), wl(NSHL), & 
                   Rhsu(NSD,NSHL), Rhsp(NSHL))

allocate(gp(NGAUSSB,NSD-1),gw(NGAUSSB), &
                              acl(NSHL,NSD), acml(NSHL,NSD), gl(NSHL,NSD),&
                              rhol(NSHL),mul(NSHL),&
                              phil(NSHL),philbc(NSHL),&
                              eKns((NSD+1)*(NSD+1),NSHL,NSHL) , &
                              eKlsc(NSHL,NSHL), &   
                              eRns(NSD+1,NSHL), eRlsc(NSHL))


 ! write(*,*) myid, "compute traction1"
 ! First compute the traction (or viscous force) then put
 ! on the surface nodes (stored in the volume array)
  NMLMs = 0.0d0; NMTra = 0.0d0
! Projection for the traction
! call FaceAsse_Tra(dgAlpha, ugAlpha, pgAlpha, NMLMs, NMTra, NM)
  call FaceAsse_Tra(msh, msh%NNODE, NSD, st%dg, st%ug, st%pg, old%rhog, old%mug, NMLMs, NMTra, NM)
  ! project the traction, velocity and pressure
  ! NMTra already has zero at slaves
  NMTra_from(:,1:3) = NMTra(:,1:3)
  r2tmp = st%ug
  call zeroslaves(r2tmp, NSD)
  NMTra_from(:,4:6) = r2tmp(:,1:3)
  r3tmp = st%phig
  call zeroslaves(r3tmp, 1)
  NMTra_from(:,7) = r3tmp
  NMTra_to = 0d0

  do i = 1, NM%NTurb
    call FaceAsse_Proj(msh, msh%NNODE, 2*NSD+1, NM%FEM(i,1), NM%FEM(i,2), NMTra_from,NMTra_to)
    call FaceAsse_Proj(msh, msh%NNODE, 2*NSD+1, NM%FEM(i,2), NM%FEM(i,1), NMTra_from, NMTra_to)
  end do
 ! we want to zero out ug at slaves for the projection
 ! r2tmp = ugAlpha

  tgLocal = NMTra_to(:,1:3)
  ugLocal = NMTra_to(:,4:6)
  philLocal = NMTra_to(:,7)

  
! write(*,*) myid, "project velocity"
! instead of projecting the traction, project the pressure only,
! then use the local normal vector
!!!  r2tmp      = 0.0d0
!!!  r2tmp(:,1) = pgAlpha
!!!  call zeroslaves(r2tmp, NSD)
!!!  pgLocal = 0.0d0
!!!  call FaceAsse_Proj(NM%FEM(1), NM%FEM(2), r2tmp, pgLocal)
!!!  call FaceAsse_Proj(NM%FEM(2), NM%FEM(1), r2tmp, pgLocal)



!  write(*,*) "Haha1:",myid, ifac, NSHL,    NSHLB , NGAUSSB
 ! call genGPandGWb(gp, gw, NGAUSSb)

  ! loop over faces
  !write(*,*) myid, "ready to assemble"
  do b = 1, msh%NBOUND
   FID = msh%bound(b)%Face_ID
   itemp = 9
   do iturb = 1, NM%NTurb
    itemp = itemp + 1
    if (FID==itemp + iturb .or. FID==itemp + 1 + iturb) then
!    if (FID==11 .or. FID==12) then
!      write(*,*) myid, "NBOUND",b,msh%bound(b)%Face_ID
      do ifac = 1, msh%bound(b)%NFACE
        iel = msh%bound(b)%F2E(ifac)
        faceor =  msh%bound(b)%FACE_OR(ifac)      
        NSHL    = msh%ELMNSHL(iel)
      !    NSHLB   = msh%bound(b)%NSHLB(ifac) 
     !     NGAUSSB = msh%bound(b)%NGAUSSB(ifac)
        if(NSHL /= 4) write(*,*) "Element type changed"
        call genGPandGWb(gp,gw,NGAUSSB,NSHLB)  
 !       write(*,*) myid, msh%bound(b)%Face_ID,"gauss"
       ! get the mapping between face element and volume surface
       !  call genGPMap(nshl, NGAUSSb, bound(b)%FACE_OR(ifac), iga, mgp)
       ! Get local solution arrays   	       	     
        do aa = 1, NSHL
          k = msh%IEN(iel,aa)
          xl (aa,:) = msh%xg      (k,:)
          dl (aa,:) = st%dg (k,:)
          wl (aa)   =  1d0! wg      (k)
          uml(aa,:) = st%ugm (k,:)
          ul_current(aa,:) = st%ug (k,:)
          ul (aa,:) = ugLocal (k,:)
          tl (aa,:) = tgLocal (k,:)  
          phil(aa) = philLocal(k)
          rhol (aa)   = old%rhog(k)
          mul (aa)    = old%mug (k)
!!!          pl (aa)   = pgLocal (k,1)
        enddo
   !     write(*,*) myid, msh%bound(b)%Face_ID,"nodal information"
        ! Get the density and viscosity. 
        ! Now just set it to water and constant for each element

        ! initialize local load vector
        Rhsu = 0.0d0    
        Rhsp = 0.0d0   
        eRlsc = 0.0d0
        ! Loop over integration points (NGAUSSb in each direction) 
        do igauss = 1, NGAUSSb

!          call eval_faceshape(nshl, iel, gp(igauss,:), mgp(igauss,:), &
!                              bound(b)%FACE_OR(ifac), &
!                              xl, dl, wl, shlu, shgradgu, dxidx, &
!                              Gij, Ginv, nor)
 
          call eval_faceshape(nshl, iel, gp(igauss,:), &
                              faceor,xl+dl, wl, &
                              shlu, shgradgu,Detjb, dxdxi,dxidx, &
                              Gij, Ginv, nor, msh%iga)  

         !   write(*,*) myid, msh%bound(b)%Face_ID,"shape function"
          ! Get the quantities from projection
!!!          pri = sum(pl*shlu)

          phi = sum(phil*shlu)   
          do i = 1, NSD
            ui(i)  = sum(ul(:,i)*shlu)
            ui_current(i)  = sum(ul_current(:,i)*shlu)
            umi(i) = sum(uml(:,i)*shlu)
            ti(i)  = sum(tl(:,i)*shlu)
          end do     
          rho = sum(rhol*shlu)
          mu  = sum(mul *shlu)  
          ! traction is computed using the projected pressure and
          ! the local normal vector. (notice the sign of "pri")
!!!          ti(:) = pri*nor(:)
          
           call e3bSTAB_DG(tauB, tauC, ui_current-umi, nor, dxidx,rho, mu)

          if(flag_NS.eq.1) then
           call e3bRHS_DG(NSD, nshl, shlu, shgradgu, gw(igauss), ui, &
                         ti, nor, tauB, Rhsu, Rhsp, rho, mu, Detjb, -1.0d0)
          endif

          if(flag_LS.eq.1) then
           call e3bRHS_DG_LS(NSD, nshl, shlu, shgradgu, gw(igauss),phi, ui, &
                             nor, tauC, eRlsc, rho, mu, Detjb, -1.0d0)
          endif

     !   write(*,*) myid, msh%bound(b)%Face_ID,"lef hand side"
   
        end do


       eRns(1: NSD, 1: NSHL) = Rhsu(1: NSD, 1: NSHL) 
       eRns(1+ NSD, 1: NSHL) = Rhsp(1: NSHL) 

 !... Enforce strong BCs         
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        IBCns_loc  (aa,:) = IBCns  (bb,:)
        IBClsc_loc (aa,1) = IBClsc (bb) 
      enddo   
      eKns = 0d0  
      eKlsc = 0d0  
      call applyBCs(nshl, msh%NSD+1, IBCns_loc(1:nshl,:),  eKns,  eRns)   
      call applyBCs(nshl, 1 ,       IBClsc_loc(1:nshl,:), eKlsc, eRlsc)     

        ! Assemble RHS
        do aa = 1, NSHL
          k = msh%IEN(iel,aa)      
          RHSGu2(k,:) = RHSGu2(k,:) +   eRns(1: NSD, aa)  ! Rhsu(:,aa)    
          RHSGp2(k)   = RHSGp2(k)   +   eRns(1+ NSD, aa)  !Rhsp(aa)    
          rhsg(k,1:NSD) = rhsg(k,1:NSD) + eRns(1: NSD, aa)!Rhsu(:,aa)    
          rhsg(k, NSD+1) = rhsg(k,NSD+1) + eRns(1+ NSD, aa) !Rhsp(aa) 
          RHSG(k,5)   = RHSG(k,5)   + eRlsc (aa)
        end do
      !  write(*,*) myid, msh%bound(b)%Face_ID,"assmeble"
      end do
   !write(*,*) myid, "NBOUND",b,msh%bound(b)%Face_ID
    end if
   end do !End iturb
!write(*,*) myid, "NBOUND",b,msh%bound(b)%Face_ID
  end do


deallocate(shlu, shgradgu, dl,     &
                     ul, ul_current,uml, tl,     &
                     pl, xl, wl, & 
                     Rhsu, Rhsp)

deallocate(gp,gw, &
                     acl, acml, gl,&
                     rhol,mul,&
                     phil,philbc,&
                     eRns, eRlsc, eKns, eKlsc)
end subroutine FaceAsse_DG_w1t2




!======================================================================
! Projection between non-matching surfaces
!======================================================================
subroutine FaceAsse_Proj(msh, NNODE, NVAR, FEM1, FEM2, NMRHS1, NMRHS2)  
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use boundaryConditions  
  use matrix  
  use aAdjKeep
  implicit none

  !type(shell_nmb), intent(inout) :: NM
  integer, parameter :: NSD = 3 
  type(meshStruct), intent(in) :: msh
  type(mesh), intent(inout) :: FEM1, FEM2
  integer, intent(in) :: NNODE, NVAR
  real(8),    intent(in)    :: NMRHS1(NNODE,NVAR)
  real(8),    intent(inout) :: NMRHS2(NNODE,NVAR)

  real(8), allocatable :: r2tmp1(:,:), r2tmp2(:,:)

 real(8), allocatable:: FORCE1(:,:), FORCE2(:,:)

  integer :: i, bb

  !--------------------------------------------------------------
  ! assign the local quantities from volume element boundary to
  ! shell mesh
  !--------------------------------------------------------------
  ! loop through each non-matching boundaries
  allocate(r2tmp1(FEM1%NNODE,NVAR))
  allocate(FORCE1(FEM1%NNODE,NVAR))
  allocate(FORCE2(FEM2%NNODE,NVAR))
  r2tmp1 = 0.0d0
  bb = FEM1%iBound
  do i = 1, msh%bound(bb)%NNODE
    r2tmp1(msh%bound(bb)%L2SNODE(i),:) = NMRHS1(msh%bound(bb)%BNODES(i),:)
  end do

 FORCE1 = r2tmp1

  ! sum up the local force such that the shell force vector is complete
  if (numnodes > 1) then
    call MPI_ALLREDUCE(r2tmp1, FORCE1, FEM1%NNODE*NVAR, &
                       MPI_DOUBLE_PRECISION,                   &
                       MPI_SUM, MPI_COMM_WORLD, mpi_err)
  end if

  deallocate(r2tmp1)

  ! project from surface 1 to surface 2
  call f2f_l2project_LM(FEM1, FEM2, NVAR, FORCE1, &
                        FORCE2)

!  write(*,*) FEM2%FORCE
!  stop
  !--------------------------------------------------------------
  ! map the solutions back to volume boundaries
  !--------------------------------------------------------------
  bb = FEM2%iBound
  do i = 1,msh%bound(bb)%NNODE
    NMRHS2(msh%bound(bb)%BNODES(i),:) = FORCE2(msh%bound(bb)%L2SNODE(i),:)
  end do

  if (numnodes > 1) call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)

end subroutine FaceAsse_Proj




!=======================================================================
! L2Projection: project quantities from surface 1 to surface 2
! Element loop through surface 2, get quantities from closest points
! on surface 1
!=======================================================================
subroutine f2f_l2project_LM(FEM1, FEM2, NVAR, fg1, fg2)
  use params 
  use defs_shell
  implicit none
  integer, parameter :: NSD = 3 
  type(mesh), intent(in)  :: FEM2, FEM1
  integer,    intent(in)  :: NVAR
  real(8),    intent(in)  :: fg1(FEM1%NNODE,NVAR)
  real(8),    intent(out) :: fg2(FEM2%NNODE,NVAR)
                             
  !  Local variables
  integer :: nshl, iel, igauss, i, j, aa, bb, FID
  real(8) :: gp(FEM2%NGAUSS,2), gw(FEM2%NGAUSS), DetJb, &
             xu(nsd), xd(NSD), xd1(NSD), dxdxi(NSD,2), nor(NSD), &
             mg2(FEM2%NNODE)
  real(8) :: fg2_LOC(FEM2%NNODE,NVAR), mg2_LOC(FEM2%NNODE),fi(NVAR)
  real(8) :: temp_together_LOC (FEM2%NNODE,NVAR+1)
  real(8) :: temp_together (FEM2%NNODE,NVAR+1)
  integer, allocatable :: lIEN(:)
  real(8), allocatable :: shl(:), shgradl(:,:), Rhs(:,:), lm(:)

  fg2 = 0.0d0; mg2 = 0.0d0
  fg2_LOC = 0.0d0; mg2_LOC = 0.0d0; mg2 = 0.0d0
  gp = 0.0d0; gw = 0.0d0; DetJb = 0.0d0

  ! get Gaussian points and weights
  call genGPandGW_tri(gp, gw, FEM2%NGAUSS)

  ! loop over elements
  do iel = 1, FEM2%NEL

    !*******************************************************
    ! every processor only computes part of the array
    !*******************************************************
    if (mod(iel, numnodes) == myid) then 

    FID = FEM2%FaceID
    nshl = FEM2%NSHL(iel)

    allocate(shl(nshl), shgradl(nshl,2), lIEN(nshl), &
             Rhs(NVAR,nshl), lM(nshl))

    lIEN = -1
    do i = 1, nshl
      lIEN(i) = FEM2%IEN(iel,i)
    end do      

    ! initialization  
    Rhs   = 0.0d0
    lM    = 0.0d0

    ! Loop over integration points (NGAUSS in each direction) 
    do igauss = 1, FEM2%NGAUSS

      ! Get Element Shape functions and their gradients
      shl = 0.0d0; shgradl = 0.0d0
      xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0
      call eval_SHAPE_tri(gp(igauss,:), shl, shgradl, nor, &
                          xu, xd, dxdxi, nsd, nshl, &
                          FEM2%IEN(iel,1:nshl), FEM2%NNODE, &
                          FEM2%B_NET_U, FEM2%B_NET_D, DetJb)


      ! get the closest quantities on the other surface
      fi = 0.0d0
      call f2f_vint(FEM1, NVAR, FEM2%CLE(iel,igauss), &
                    FEM2%CLP(iel,igauss,:), igauss, fg1, fi, xd1)
!      write(*,*),myid, xd(:)-xd1(:)
         
      do aa = 1, nshl
        ! lumped mass
        lM(aa) = lM(aa) + shl(aa)*DetJb*gw(igauss)
        ! right-hand-side: quantities of the other surface
        Rhs(:,aa) = Rhs(:,aa) + fi(:)*shl(aa)*DetJb*gw(igauss)
      end do

    end do ! end loop gauss points

    ! Assemble vectors
    do aa = 1, NSHL
      ! Right-hand-side
      fg2_LOC(lIEN(aa),:) = fg2_LOC(lIEN(aa),:) + Rhs(:,aa)
 
      ! lumped mass
      mg2_LOC(lIEN(aa)) = mg2_LOC(lIEN(aa)) + lM(aa)
    end do
    deallocate(shl, shgradl, lIEN, Rhs, lM)
  end if !if (mod(iel, numnodes) == myid) then 
 enddo    ! end loop elements

!  temp_together_LOC(:,1:NVAR) = fg2_LOC
!  temp_together_LOC(:, NVAR+1) = mg2_LOC
!  temp_together = 0d0
  
  CALL MPI_ALLREDUCE(fg2_LOC, fg2, FEM2%NNODE*NVAR, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  CALL MPI_ALLREDUCE(mg2_LOC, mg2, FEM2%NNODE, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)

!   CALL MPI_ALLREDUCE(temp_together_LOC, temp_together, FEM2%NNODE*(NVAR+1), MPI_REAL,MPI_SUM, MPI_COMM_WORLD, mpi_err)
  ! devided by lumped mass to get nodal traction
!  fg2 = temp_together(:,1:NVAR)
!  mg2 = temp_together(:,NVAR+1)
  forall (i = 1:FEM2%NNODE, mg2(i) > 1.0d-15) 
    fg2(i,:) = fg2(i,:)/mg2(i)
  end forall

end subroutine f2f_l2project_LM



!======================================================================
! Compute values at the closest point on surface 1
!======================================================================
subroutine f2f_vint(FEM, NVAR, iel, clp, igauss,  fg, fi, xd1)
  use params 
  use defs_shell
  implicit none
  integer, parameter :: NSD = 3 
  type(mesh), intent(in)  :: FEM
  integer,    intent(in)  :: NVAR, iel , igauss
  real(8),    intent(in)  :: clp(2), fg(FEM%NNODE,NVAR)
  real(8),    intent(in)  :: xd1(3)
  real(8),    intent(out) :: fi(NVAR)

  !  Local variables
  integer :: i, j, nshl, FID
  real(8) :: xu(nsd), xd(nsd), dxdxi(nsd,2), DetJb, nor(NSD)

  real(8), allocatable :: shl(:), shgradl(:,:), fl(:,:)

  nshl = FEM%NSHL(iel)
  FID = FEM%FaceID
      
  allocate(shl(nshl), shgradl(nshl,2), fl(nshl,NVAR))
      
  ! Get local solution arrays            
  do i = 1, nshl
    fl(i,:) = fg(FEM%IEN(iel,i),:)
  end do

  ! Get Element Shape functions and their gradients
  shl = 0.0d0; shgradl = 0.0d0; xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0
  call eval_SHAPE_tri(clp, shl, shgradl, nor, xu, xd, dxdxi, &
                      nsd, nshl, FEM%IEN(iel,1:nshl), FEM%NNODE, &
                      FEM%B_NET_U, FEM%B_NET_D, DetJb)

  fi = 0.0d0
  do i = 1, nshl
    fi(:) = fi(:) + fl(i,:)*shl(i)
  end do
!  xd1 = xd
  deallocate(shl, shgradl, fl)
end subroutine f2f_vint



!======================================================================
! Build traction on the surface, then do l2-projection put to the 
! control points
! NMLM:  Lumped mass for non-matching
! NMTra: Traction for non-matching
! project 
!======================================================================
subroutine FaceAsse_Tra(msh, NNODE, NSD, dgAlpha, ugAlpha, pgAlpha, rhoAlpha, muAlpha, NMLMs, NMTra, NM)

! call FaceAsse_Tra(msh, msh%NNODE, NSD, st%dg, st%ug, st%pg, st%rhog, st%mug, NMLMs, NMTra, NM)

  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use boundaryConditions  
  use matrix  
  use aAdjKeep
  implicit none
     
  type(meshStruct), intent(in) :: msh
  integer, intent(in) :: NNODE, NSD
  type(shell_nmb), intent(in) :: NM

  real(8), intent(in) :: dgAlpha(NNODE,NSD),   ugAlpha(NNODE,NSD), &
                         pgAlpha(NNODE), rhoAlpha(NNODE), muAlpha(NNODE)

  real(8), intent(out) :: NMLMs(NNODE), NMTra(NNODE,NSD)

  ! Local variables
  integer :: b, ifac, iel, igauss, i, j, k, hess_flag, aa, bb, &
                      FID, itemp, iturb
  
!**************************************************************************
  integer :: NSHL,NSHLB, NGAUSSB 
!  real(8), allocatable :: eKns(:,:,:),eKlsc(:,:),eKlscu (:,:,:) 
  real(8), allocatable :: eRns(:,:),eRlsc(:)
  real(8), allocatable :: gp(:,:), gw(:) 
  real(8), allocatable ::  acml(:,:), acl(:,:), rhol(:),mul(:), gl(:,:), phil(:), philbc(:)
  integer ::  faceor 
  integer :: IBCns_loc(msh%maxNSHL,NSD+1), IBClsc_loc(msh%maxNSHL,1)
  real(8) :: DetJb
  real(8) :: rho, mu
!**************************************************************************
  real(8), allocatable :: shlu(:), shgradgu(:,:)
  real(8), allocatable :: dl(:,:), ul(:,:), pl(:), &
                          xl(:,:), wl(:)
  real(8), allocatable :: rhs1(:), rhs2(:,:)

  real(8) :: pri, duidxi(NSD,NSD), dxidx(NSD,NSD), dxdxi(NSD,NSD), &
             Gij(NSD,NSD), Ginv(NSD,NSD), ti(NSD), &
             nor(NSD)
		
  NSHL = 4
  NSHLB = 3
  NGAUSSB =3
  allocate(shlu(NSHL), shgradgu(NSHL,NSD), dl(NSHL,NSD),  &
                   ul(NSHL,NSD), pl(NSHL), xl(NSHL,NSD), wl(NSHL), &
                   rhs1(NSHL), rhs2(NSD,NSHL))
  allocate(gp(NGAUSSB,NSD-1),gw(NGAUSSB), &
                            rhol(NSHL),mul(NSHL))
	 
!  call genGPandGWb(gp, gw, NGAUSSb)

  ! loop over faces
  do b = 1, msh%NBOUND

    ! Non-matching boundaries
   FID = msh%bound(b)%Face_ID
   itemp = 9
   do iturb = 1, NM%NTurb
    itemp = itemp + 1
    if (FID==itemp + iturb .or. FID==itemp + 1 + iturb) then
!    if (FID==11 .or. FID==12) then

      do ifac = 1,msh% bound(b)%NFACE

        iel = msh%bound(b)%F2E(ifac)
        faceor =  msh%bound(b)%FACE_OR(ifac)     
        NSHL    = msh%ELMNSHL(iel)
        NSHLB   = msh%bound(b)%NSHLB(ifac) 
        NGAUSSB = msh%bound(b)%NGAUSSB(ifac)
         call genGPandGWb(gp,gw,NGAUSSB,NSHLB)  

	
        ! get the mapping between face element and volume surface
      !  call genGPMap(nshl, NGAUSSb, bound(b)%FACE_OR(ifac), iga, mgp)
     	  
        ! Get local solution arrays   	       	     
        do aa = 1, NSHL
          k = msh%IEN(iel,aa)
          xl  (aa,:) = msh%xg       (k,:)
          dl  (aa,:) = dgAlpha  (k,:)
          ul  (aa,:) = ugAlpha  (k,:)
          pl  (aa)   = pgAlpha  (k)
          wl  (aa)   = 1d0 ! wg       (k)
          rhol (aa)   =  rhoAlpha(k)
          mul (aa)    = muAlpha (k)
        end do
        ! Get the density and viscosity. 
        ! Now just set it to water and constant for each element
        ! initialize local load vector
        rhs1 = 0.0d0; rhs2 = 0.0d0
	  
        ! Loop over integration points
        do igauss = 1, NGAUSSB

 !         call eval_faceshape(nshl, iel, gp(igauss,:), mgp(igauss,:), &
 !                            bound(b)%FACE_OR(ifac), &
 !                            xl, dl, wl, shlu, shgradgu, dxidx, &
 !                            Gij, Ginv, nor)

          call eval_faceshape(nshl, iel, gp(igauss,:), &
                              faceor,xl+dl, wl, &
                              shlu, shgradgu,Detjb, dxdxi,dxidx, &
                              Gij, Ginv, nor, msh%iga)  
          
          !-----------------------------------------------
          ! Get the quantities at the intergration point
          !-----------------------------------------------
          ! pressure
          pri = sum(pl*shlu)	      
	    
          ! velocity gradient
          do j = 1, NSD
            do i = 1, NSD
              duidxi(i,j) = sum(ul(:,i)*shgradgu(:,j))
            end do
          end do
          rho = sum(rhol*shlu)
          mu  = sum(mul *shlu)  
       !   write(*,*) myid, rho, mu
       !   traction
          ti(:) = -pri*nor(:)  + mu*(duidxi(:,1)*nor(1) + &
                                                     duidxi(:,2)*nor(2) + &
                                                     duidxi(:,3)*nor(3) + &
                                                     duidxi(1,:)*nor(1) + &
                                                     duidxi(2,:)*nor(2) + &
                                                     duidxi(3,:)*nor(3))

          do aa = 1, NSHL
            ! lumped mass
            rhs1(aa) = rhs1(aa) + shlu(aa)*DetJb*gw(igauss)
            ! traction
            rhs2(:,aa) = rhs2(:,aa) + shlu(aa)*ti(:)*DetJb*gw(igauss)
          end do

        end do

         ! Assemble LM and Tra
        do aa = 1, NSHL
          k = msh%IEN(iel,aa)    
          NMLMs(k)   = NMLMs(k)   + rhs1(aa)  
          NMTra(k,:) = NMTra(k,:) + rhs2(:,aa)
        end do

      end do ! NFACE
    end if
   end do ! End iturb
  end do  ! NBOUND

deallocate(shlu, shgradgu, dl,  &
                   ul, pl, xl, wl, &
                   rhs1, rhs2)
deallocate(gp,gw, &
                       rhol,mul)
  ! communicate
!  if (numnodes > 1) then
!    call commu(NMLMs, 1  , 'in ')
!    call commu(NMTra,3, 'in ')
!  end if

    call commu(NMLMs,  1,'in ')    
    call commu(NMTra, 3,'in ')
            
    call MPI_BARRIER (MPI_COMM_WORLD,mpi_err)

    call commu(NMLMs,  1,'out')    
    call commu(NMTra, 3,'out')


  ! devided by lumped mass to get nodal traction
 ! forall (i = 1:NNODE, NMLMs(i) > 1.0d-15) 
 !   NMTra(i,:) = NMTra(i,:)/NMLMs(i)
 ! end forall

do i = 1, NNODE
   if(abs(NMLMs(i)) > 1.0d-15)  NMTra(i,:) = NMTra(i,:)/NMLMs(i)
enddo
 
end subroutine FaceAsse_Tra


