! module for ray tracing, using a hashed octree to accelerate

module raytrace

  use dynamic_arrays
  use hashed_octree

  real(8), parameter :: RAY_EPS=1.0d-7
  integer, parameter :: RAY_INIT_CAP = 3

  ! global octree for ray tracing
  type(octree) :: global_octree
  real(8), allocatable :: global_trilist(:,:,:)

  ! a geometrical ray
  type ray
     real(8) :: origin(3)
     real(8) :: direction(3)
     ! parameters of intersection points
     type(dynamic_array_r) :: params
  end type ray

  contains

    subroutine ray_allocate(r)
      implicit none
      type(ray), intent(inout) :: r
      call dynamic_array_r_allocate(r%params, RAY_INIT_CAP)
    end subroutine ray_allocate

    subroutine ray_free(r)
      implicit none
      type(ray), intent(inout) :: r
      call dynamic_array_r_free(r%params)
    end subroutine ray_free

    ! does the ray r intersect the box?
    subroutine ray_intersect_box(r, box, intersects)
      implicit none
      type(ray), intent(in) :: r
      real(8), intent(in) :: box(3,2)
      logical, intent(out) :: intersects
      
      integer :: dy, dz, d, side
      real(8) :: hitparam, y, z

      ! check each side of the box, with short circuiting
      intersects = .false.
      do d=1,3
         do side=1,2

            ! ignore true glancing hits, to avoid divide-by-zero
            if(r%direction(d) == 0.0d0) then
               cycle
            end if 

            ! find parameter along which ray intersects plane and cycle if
            ! negative
            hitparam = (box(d,side)-r%origin(d))/r%direction(d)
            if(hitparam < 0.0d0) then
               cycle
            end if

            ! if parameter was non-negative, find other two coordinates of 
            ! intersection (called y and z here, but, in general, they are
            ! just "the other two")
            dy = mod(d,3)+1
            dz = mod(d+1,3)+1
            y = r%origin(dy) + hitparam*r%direction(dy)
            z = r%origin(dz) + hitparam*r%direction(dz)

            ! check whether the other two coordinates are within the 
            ! appropriate bounds
            if(y < box(dy,2) .and. y > box(dy,1) .and. &
                 z < box(dz,2) .and. z > box(dz,1)) then
               intersects = .true.
               exit
            end if
            
         end do ! end loop over sides

         ! don't check unnecessary sides
         if(intersects) then
            exit
         end if

      end do ! end loop over directions

    end subroutine ray_intersect_box

    ! REPLACE with other kernel (e.g. Bezier element) if needed

    ! ray intersect triangle, given by three vertices.  do not "return" 
    ! anything, but, if there is a unique parametric intersection along
    ! the ray (up to tolerance RAY_EPS), add it
    subroutine ray_intersect_triangle(r, t)
      implicit none
      type(ray), intent(inout) :: r
      real(8), intent(in) :: t(3,3) ! 1st index is vertex, 2nd is coordinate
      
      real(8) :: ot(3),u(3),v(3),lhs(3,3),lhsinv(3,3),rhs(3),param(3),detj
      logical :: unique
      integer :: i

      ! assemble the system of equations for the triangle parametric vertices
      ! and the ray parameter
      ot = t(1,:)
      u = t(2,:) - ot
      v = t(3,:) - ot
      rhs = r%origin - ot
      lhs(:,1) = u
      lhs(:,2) = v
      lhs(:,3) = -r%direction
      call get_inverse_3x3(lhs,lhsinv,detj)
      ! first two are triangle parameters, third is ray parameter
      param = matmul(lhsinv,rhs)

      ! if triangle parametric verticies are in (0,1) and ray parameter
      ! is positive, then it's a hit
      if(param(1) + param(2) < 1.0d0 .and. param(1) > 0.0d0 .and. &
           param(2) > 0.0d0 .and. param(3) > 0.0d0) then

         ! check the other hits in the ray, and, if this one is unique, push it
         ! to the parameter list
         unique = .true.
         do i=1,r%params%size
            if(abs(r%params%content(i) - param(3)) < RAY_EPS) then
               unique = .false.
               exit
            end if
         end do
         if(unique) then
            call dynamic_array_r_push(r%params,param(3))
         end if

      end if ! end if a hit

    end subroutine ray_intersect_triangle

    ! REPLACE with routine to get a bounding box for some other thing (e.g.
    ! Bezier element) if needed

    ! obtain a bounding box for a triangle, for the purpose of adding it to
    ! a hashed octree
    subroutine get_triangle_bounding_box(t,box)
      implicit none
      real(8), intent(in) :: t(3,3)
      real(8), intent(out) :: box(3,2)
      real(8) :: maxcoord, mincoord
      integer :: d,p
      do d=1,3
         maxcoord = t(1,d)
         mincoord = t(1,d)
         do p=1,3
            maxcoord = max(t(p,d),maxcoord)
            mincoord = min(t(p,d),mincoord)
         end do
         box(d,1) = mincoord
         box(d,2) = maxcoord
      end do
    end subroutine get_triangle_bounding_box

    ! REPLACE
    ! add a list of triangles to a hashed octree
    subroutine add_triangle_list_to_octree(tlist,ntris,ot)
      use hashed_octree
      implicit none
      type(octree), intent(inout) :: ot
      integer, intent(in) :: ntris
      real(8), intent(in) :: tlist(ntris,3,3)

      real(8) :: t(3,3), box(3,2)
      integer :: i

      ! for each triangle
      do i=1,ntris

         ! get the bounding box of this triangle
         t = tlist(i,:,:)
         call get_triangle_bounding_box(t,box)

         ! add this triangle('s index) to the octree, using its bounding box
         call octree_add_object(ot,i,box)

      end do ! end loop over triangles

    end subroutine add_triangle_list_to_octree

    ! REPLACE
    ! intersect a ray with a list of triangles that has been added
    ! to a hashed octree.  intersect with cell i at level n
    recursive subroutine ray_intersect_octree_cell(r, ot, i, n, &
         tlist,ntris)
      use hashed_octree
      implicit none
      type(octree), intent(in) :: ot
      integer, intent(in) :: ntris, i, n
      type(ray), intent(inout) :: r
      real(8), intent(in) :: tlist(ntris,3,3)

      logical :: intersects
      real(8) :: box(3,2),t(3,3)
      integer :: ii, subi, ibin

      ! if the cell is totally unoccupied, we don't care
      call octree_hash(ot,i,ibin)
      if(.not. ot%occupied(n+1,ibin)) then
         return
      end if

      ! check whether the ray intersects this cell
      call octree_getbounds(ot,i,n,box)
      call ray_intersect_box(r,box,intersects)

      ! if the ray doesn't intersect the bounding box, leave the subroutine
      if(.not. intersects) then
         return
      end if

      ! if we are at the bottom level of the tree, intersect with each
      ! triangle in the appropriate bin
      if(n == ot%nlevels-1) then
         ! recall: each bin is a dynamic list of integers, which are indexes
         ! of triangles in tlist
         do ii=1,ot%bins(ibin)%size
            ! intersect the ray with this primitive (modifies, ray if there
            ! is an intersection)
            t = tlist(ot%bins(ibin)%content(ii),:,:)
            call ray_intersect_triangle(r,t)
            
         end do ! end loop over contents of cell's bin
      else
         ! otherwise, intersect with each sub-cell (checks occupancy)
         do ii=1,8
            call octree_global_subcell_index(i,n,ii,subi)
            call ray_intersect_octree_cell(r,ot,subi,n+1,&
                 tlist,ntris)
         end do
      end if ! end if at leaf

    end subroutine ray_intersect_octree_cell

    ! to avoid zero versus one errors in calling have a wrapper subroutine
    ! to call at top level of tree
    subroutine ray_intersect_octree(r,ot,tlist,ntris)
      use hashed_octree
      implicit none
      type(octree), intent(in) :: ot
      integer, intent(in) :: ntris
      type(ray), intent(inout) :: r
      real(8), intent(in) :: tlist(ntris,3,3)
      call ray_intersect_octree_cell(r,ot,1,0,tlist,ntris)
    end subroutine ray_intersect_octree


    
    !!!!!!! GET RID OF THIS IN REAL VERSION !!!!!!!!!!!!!!!!!!!!!!!!
    subroutine get_inverse_3x3(Amat, Ainv, DetJ)
      implicit none
      real(8), intent(in)  :: Amat(3,3)
      real(8), intent(out) :: Ainv(3,3), DetJ
      real(8) :: tmp
      
      Ainv = 0.0d0
      
      Ainv(1,1) = Amat(2,2)*Amat(3,3) - Amat(3,2)*Amat(2,3)
      Ainv(1,2) = Amat(3,2)*Amat(1,3) - Amat(1,2)*Amat(3,3)
      Ainv(1,3) = Amat(1,2)*Amat(2,3) - Amat(1,3)*Amat(2,2)
      
      tmp = 1.0d0/(Ainv(1,1)*Amat(1,1) + Ainv(1,2)*Amat(2,1) + &
           Ainv(1,3)*Amat(3,1))
      
      Ainv(1,1) = Ainv(1,1) * tmp
      Ainv(1,2) = Ainv(1,2) * tmp
      Ainv(1,3) = Ainv(1,3) * tmp
      
      Ainv(2,1) = (Amat(2,3)*Amat(3,1) - Amat(2,1)*Amat(3,3)) * tmp
      Ainv(2,2) = (Amat(1,1)*Amat(3,3) - Amat(3,1)*Amat(1,3)) * tmp
      Ainv(2,3) = (Amat(2,1)*Amat(1,3) - Amat(1,1)*Amat(2,3)) * tmp
      Ainv(3,1) = (Amat(2,1)*Amat(3,2) - Amat(2,2)*Amat(3,1)) * tmp
      Ainv(3,2) = (Amat(3,1)*Amat(1,2) - Amat(1,1)*Amat(3,2)) * tmp
      Ainv(3,3) = (Amat(1,1)*Amat(2,2) - Amat(1,2)*Amat(2,1)) * tmp
      
      DetJ = 1.0d0/tmp
    end subroutine get_inverse_3x3

end module raytrace
