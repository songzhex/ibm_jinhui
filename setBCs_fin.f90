 subroutine setBCs_turbine_sliding(SH,NRB_BEA)

  use aAdjKeep
  use commonvars
  use defs_shell
  use types_beam
  use params
  use globdata
  use mpi
  implicit none
  integer:: i, j, ii, jj,ibld,iel,nshl,iglobal,ptype
  integer:: NRotPoints,NRotPoints_FEM
 integer :: NLEPoints1, num_edge1, num_IBC1
 integer :: NLEPoints2, num_edge2, num_IBC2
 integer :: num_IBC_beam
  type(shell_bld), intent(inout) :: SH
  type(mesh_beam), intent(inout) :: NRB_BEA
  real(8), allocatable :: list1(:,:)
  real(8), allocatable :: list2(:,:)
  character(len=50) :: fname

  SH%NRB%IBC = 0
  NRB_BEA%IBC = 0
  num_edge1=0
  num_IBC1 = 0
  num_edge2=0
  num_IBC2 = 0
  num_IBC_beam = 0 
  if(.false.) then
  if(move) then
  fname = 'input/list1.dat'
  open(13, file=fname, status='old')
  read(13,*) NLEPoints1
  allocate(list1(NLEPoints1,3))
  do i = 1, NLEPoints1
   read(13,*) list1(i,1:3)
  end do
  close(13)

  fname = 'input/list2.dat'
  open(13, file=fname, status='old')
  read(13,*) NLEPoints2
  allocate(list2(NLEPoints2,3))
  do i = 1, NLEPoints2
   read(13,*) list2(i,1:3)
  end do
  close(13)
 ! call get_Rmat(theta, thetd, thedd, Rmt, Rmdt, Rmddt)
  num_edge1=0
  num_IBC1 = 0
  num_edge2=0
  num_IBC2 = 0
  num_IBC_beam = 0 
  do i = 1, SH%NRB%NNODE
    do ii = 1, NLEPoints1
         if((abs(SH%NRB%B_NET_U(i,3)-list1(ii,3)) <=1.0d-6).and.&
             (abs(SH%NRB%B_NET_U(i,2)-list1(ii,2)) <=1.0d-6).and.&
             (abs(SH%NRB%B_NET_U(i,1)-list1(ii,1)) <=1.0d-6)) then
              SH%NRB%IBC(i,:) = 1
              NRB_BEA%IBC(i,:) = 1
              num_edge1 = num_edge1 + 1
              num_IBC1 = num_IBC1 + 1
             do j = 1, 3
               SH%NRB%dsh(i,j) = sum(Rmat(1, j,:)*SH%NRB%B_NET_U(i,1:3))
               SH%NRB%ush(i,j) = sum(Rdot(1, j,:)*SH%NRB%B_NET_U(i,1:3))
               SH%NRB%ash(i,j) = sum(Rddt(1, j,:)*SH%NRB%B_NET_U(i,1:3))
            enddo
         endif
    enddo

    do ii = 1, NLEPoints2
         if((abs(SH%NRB%B_NET_U(i, 3)-list2(ii,3)) <=1.0d-6).and.&
             (abs(SH%NRB%B_NET_U(i, 2)-list2(ii,2)) <=1.0d-6).and.&
             (abs(SH%NRB%B_NET_U(i,1)-list2(ii,1)) <=1.0d-6)) then
              SH%NRB%IBC(i,:) = 1
              NRB_BEA%IBC(i,:) = 1
              num_edge2 = num_edge2 + 1
              num_IBC2 = num_IBC2 + 1
             do j = 1, 3
        !       SH%NRB%dsh(i,j) = sum(Rmt(j,:)*SH%NRB%B_NET_U(i,1:3))
        !       SH%NRB%ush(i,j) = sum(Rmdt(j,:)*SH%NRB%B_NET_U(i,1:3))
        !       SH%NRB%ash(i,j) = sum(Rmddt(j,:)*SH%NRB%B_NET_U(i,1:3))
               SH%NRB%dsh(i,j) = sum(Rmat(2,j,:)*SH%NRB%B_NET_U(i,1:3))
               SH%NRB%ush(i,j) = sum(Rdot(2,j,:)*SH%NRB%B_NET_U(i,1:3))
               SH%NRB%ash(i,j) = sum(Rddt(2,j,:)*SH%NRB%B_NET_U(i,1:3))
            enddo
         endif
    enddo

    
    if(abs(SH%NRB%B_NET_U(i,3)) <= 1.0d-7) then
      SH%NRB%IBC(i,:) = 1
      NRB_BEA%IBC(i,:) = 1
      num_IBC_beam =  num_IBC_beam + 1
      SH%NRB%dsh(i,:) = 0d0
      SH%NRB%ush(i,:) = 0d0
      SH%NRB%ash(i,:) = 0d0
    endif
  enddo
!  Jinhui: apply boundary conditions on NURBS
!A                                + Delt**2.0d0/2.0d0* ((1.0d0-2.0d0*beti)*NRB_BEA%B_NET_DDt_old(:,:) &
!A                                + 2.0d0*beti*NRB_BEA%B_NET_DDt(:,:))
!A    NRB_BEA%B_NET_D(:,NSD+1)=NRB_BEA%B_NET_D_old(:,NSD+1)

  else
 
    write(*,*) "Everything is zero here"
     SH%NRB%IBC= 1
     NRB_BEA%IBC= 1
     SH%NRB%dsh = 0d0
     SH%NRB%ush = 0d0
     SH%NRB%ash= 0d0
  endif

   ! if(ismaster) write(*,*) 'First leading edge NRB ',num_IBC1, num_edge1, Rmat(1,2,3)
   ! if(ismaster) write(*,*) 'Second leading edge NRB ',num_IBC2, num_edge2,  Rmat(2,2,3)
   ! if(ismaster) write(*,*) 'Beam ',  num_IBC_beam
   ! if(ismaster) write(*,*) "Theta for IGA:",  asin(Rmat(1,2,3))*180/acos(-1d0), asin(Rmat(2,2,3))*180/acos(-1d0)
     SH%NRB%B_NET_D(:,1:3) = SH%NRB%B_NET_U(:,1:3) + SH%NRB%dsh(:,1:3)
      NRB_BEA%B_NET_Dt = SH%NRB%ush!NRB_BEA%B_NET_Dt_old
      NRB_BEA%B_NET_DDt = SH%NRB%ash!(gami-1.0d0)/gami*NRB_BEA%B_NET_DDt_old
      NRB_BEA%B_NET_D(:,1:3) = SH%NRB%B_NET_D(:,1:3)!NRB_BEA%B_NET_D_old(:,1:NSD
  endif ! if(.false.) then
     
 end subroutine setBCs_turbine_sliding
