!======================================================================
!
!======================================================================
subroutine IntAssembly_IM(st, msh,  rhsg, newJac,flag)
                        
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use BoundaryConditions
  use aAdjKeep
  implicit none
  
  type(state),      intent(in) :: st
  type(meshStruct), intent(inout) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,2*msh%NSD+3)  
  integer, intent(in) :: newJac
  integer, intent(in) :: flag(4)

  integer :: iel, igauss, i, j, k,ii,jj,kk, hess_flag, idx, nshl, NGAUSS,aa,bb
  integer :: IBCns_loc(msh%maxNSHL,msh%NSD+1), IBClsc_loc(msh%maxNSHL,1), &
             IBCmesh_loc(msh%maxNSHL,msh%NSD)

  real(8), allocatable :: eKns(:,:,:), eKlsc(:,:),eKlsrd(:,:),eKmesh(:,:,:),&
                          eKlscu(:,:,:),eKrdphi(:,:)
  real(8), allocatable :: eRns(:,:), eRlsc(:),eRlsrd(:), eRmesh(:,:)
  real(8) :: volm, vol_ex, vol_int

  real(8), allocatable :: gp(:,:), gw(:)                          
  real(8), allocatable :: shl(:), shgradg(:,:),shgradgold(:,:), shhessg(:,:,:)
  real(8) :: Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD),&
             dxdxi(msh%NSD,msh%NSD),dxidx(msh%NSD,msh%NSD)
  real(8) :: DetJ, detJgwt,shconv(msh%maxNSHL)
  
  real(8), allocatable :: xl(:,:),wl(:), dl(:,:),dlold(:,:),uml(:,:), acml(:,:),&
                          ul(:,:),acl(:,:),pl(:),rhol(:),mul(:),&
                          ulold(:,:),rho0l(:), &
                          phil(:),rphil(:), philold(:),&
                          phi1l(:),phidl(:),  &
                          hl(:), hel(:), hepl(:)

  real(8), allocatable :: Rhsq(:), Rhsl(:)

  real(8) :: di(msh%NSD), umi(msh%NSD), acmi(msh%NSD),           &
             ui(msh%NSD), aci(msh%NSD), pri, rho, mu, rho0,  &
             phi , rphi, phi1, phid, he, hep,hi 
  real(8) :: ddidxi(msh%NSD,msh%NSD), duidxi(msh%NSD,msh%NSD),duidxixj(msh%NSD,msh%NSD,msh%NSD),&
             duiolddxi(msh%NSD,msh%NSD), dpridxi(msh%NSD), &
             dphidxi(msh%NSD),dphiolddxi(msh%NSD),dphiddxi(msh%NSD), &
             drhodxi(msh%NSD), rhoconv, DetJold, uiold(msh%NSD)

  real(8) :: uadvi(msh%NSD), Se, ai(msh%NSD),ai1(msh%NSD), fi(msh%NSD)
  real(8) :: res_u(msh%NSD), res_c, res_rd
  real(8) :: tauM_ns, tauC_ns, tauP_ns, tauBar_ns, tau_c, tau_rd, gradmag
  real(8) :: kdc_ns, kdc_c, kdc_rd, Emod, phimax
  real(8) :: fact1,fact2
  real(8) :: Qi, Omegai(msh%NSD,msh%NSD), Si(msh%NSD,msh%NSD)
    
  real(8), parameter :: damp = 0.5d0
  real(8) :: kappa, Ftens(msh%NSD,msh%NSD), Stens(msh%NSD,msh%NSD), Ctens(msh%NSD,msh%NSD,msh%NSD,msh%NSD) 
  real(8) :: cfl(2), gcfl(2), cfl_loc(2)
  real(8) :: pse_time_loc,pse_time_loc1,pse_time_glob,pse_time_glob1,tmp_val
  cfl=0.0d0
  if(flag(2).eq.1) then
  pse_time=100000000000d0
  pse_time1=100000000000d0
  endif

  if((flag(1) == 1).and.(newJac>=1)) then 
   rhsqu = 0d0
   rhsql = 0d0
  endif

  fact1 = almi
  fact2 = alfi*gami*Delt 

  vol_int = 0d0
  volm = 0d0

  phimax = max(maxval(st%phig),-minval(st%phig))

  phi = phimax
  call MPI_ALLREDUCE (phi, phimax ,1, MPI_DOUBLE_PRECISION, &
                      MPI_SUM, MPI_COMM_WORLD, mpi_err)  

!...  Loop over elements
  NSHL   = -1  
  do iel = 1, msh%NELEM 

!...  Allocate        
    if (NSHL /= msh%ELMNSHL(iel)) then
      
      if (NSHL >= 0) then
        deallocate(shl, shgradg,  shgradgold,shhessg, & ! gp, gw,              &
                   eRns, eRlsc,eRlsrd,eRmesh,                  &
                   xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul,       &
                   ulold,rho0l,phil,rphil,philold,phi1l,phidl, &
                   hl, hel, hepl)
        !if (newJac >= 1) then          
        deallocate(eKns,eKlsc,eKlsrd, eKmesh,eKlscu,eKrdphi,Rhsq, Rhsl)
        !endif           
      endif
     
      NSHL   = msh%ELMNSHL(iel)
      ! NGAUSS = msh%ELMNGAUSS(iel)

      allocate(shl(NSHL), shgradg(NSHL,msh%NSD), shgradgold(NSHL,msh%NSD),shhessg(NSHL,msh%NSD,msh%NSD),&
               !gp(NGAUSS,msh%NSD), gw(NGAUSS),                        &
               eRns(msh%NSD+1,NSHL), eRlsc(NSHL),eRlsrd(NSHL),eRmesh(msh%NSD,NSHL),  &
               dl(NSHL,msh%NSD),dlold(NSHL,msh%NSD), ul(NSHL,msh%NSD),wl(NSHL),      &
               acl(NSHL,msh%NSD), uml(NSHL,msh%NSD), acml(NSHL,msh%NSD),             &
               pl(NSHL), xl(NSHL,msh%NSD), rhol(NSHL),mul(NSHL),                     &
               ulold(NSHL,msh%NSD),rho0l(NSHL),phil(NSHL),rphil(NSHL),philold(NSHL), &
               phi1l(NSHL),phidl(NSHL),hl(NSHL),hel(NSHL),hepl(NSHL))
      !if (newJac >= 1) then                    
        allocate(eKns((msh%NSD+1)*(msh%NSD+1),NSHL,NSHL), &
                 eKlsc(NSHL,NSHL),eKlsrd(NSHL,NSHL),      & 
                 eKmesh(msh%NSD*msh%NSD,NSHL,NSHL),      &
                 eKlscu(msh%NSD,NSHL,NSHL), eKrdphi(NSHL,NSHL),Rhsq(NSHL), Rhsl(NSHL))
      !endif        

      ! call genGPandGW(gp, gw, NGAUSS, NSHL)
    end if

    ! get gauss points and weights from dynamic list from current element
    ngauss = msh%elmgplists(iel)%size

    ! if there are a nonzero number of active gauss points, set includeflags
    if(ngauss > 0) then
       do i=1,nshl
          msh%includeflags(msh%ien(iel,i)) = 1.0d0
       enddo
    endif

    allocate(gp(ngauss,msh%nsd),gw(ngauss))
    do i=1,ngauss
       gp(i,1:3) = msh%elmgplists(iel)%content(1:3,i)
       gw(i)     = msh%elmgplists(iel)%content(4  ,i)
    enddo

!...  Get local solution arrays 
    do i = 1, NSHL 
    
      idx = msh%IEN(iel,i)
      xl   (i,:) = msh%xg    (idx,:)     
      wl   (i)   = msh%wg    (idx)
      
      dl   (i,:) = st%dg   (idx,:)      
      dlold(i,:) = old%dg  (idx,:)
      uml  (i,:) = st%ugm  (idx,:)
      acml (i,:) = st%acgm (idx,:)
            
      ul   (i,:) = st%ug   (idx,:)
      acl  (i,:) = st%acg  (idx,:)
      pl   (i)   = st%pg   (idx)
      rhol (i)   = old%rhog (idx)          
      mul  (i)   = old%mug  (idx) 
      ulold(i,:) = old%ug  (idx,:)   
      rho0l(i)   = st%rho0g(idx)
      
      phil(i)    = st%phig (idx)       
      rphil(i)   = st%rphig(idx)     
      philold(i) = old%phig(idx)
     
      phi1l(i)   = st%phig   (idx)

      phidl(i)   = st%phidg  (idx)

      hl(i)      = st%hg  (idx)             
      hel(i)     = st%heg (idx)             
      hepl(i)    = st%hepg(idx)     
    end do
         
!...  Initialize element jacobians
    if (newJac >= 1) then
      eKns   = 0d0           
      eKlsc  = 0d0
      eKlsrd = 0d0  
      eKmesh = 0d0     
    endif
    if (newJac >= 2) then    
      eKlscu  = 0d0     
      eKrdphi = 0d0      
    endif   
        
!...  Initialize element residuals    
    eRns   = 0d0
    eRlsc  = 0d0
    eRlsrd = 0d0
    eRmesh = 0d0
    Rhsq   = 0d0 
    Rhsl   = 0d0

!---------------------------------------------------------------------------------------    
!...  Fluid
!---------------------------------------------------------------------------------------  
    if (msh%EL_TYP(iel) == 0) then    
         
!...  Loop over integration points
      do igauss = 1, NGAUSS
        shl = 0d0;  shgradg = 0d0; shhessg = 0d0; 
        
        call eval_shape(nshl, iel, gp(igauss,:), xl+dlold, wl,         &
                        shl, shgradgold, shhessg,  DetJold,  &
                        dxdxi,dxidx,Gij,Ginv,0, msh%iga)  
 
        
        call eval_shape(nshl, iel, gp(igauss,:), xl+dl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,NS_hess_flag, msh%iga)  

        detJgwt = DetJ*gw(igauss) 

        vol_int = vol_int + DetJ*gw(igauss)
       
!...  Interpolate     

        do i = 1, msh%NSD
          ui (i) = sum(ul (:,i)*shl)
          uiold (i) = sum(ulold (:,i)*shl)
          umi(i) = sum(uml(:,i)*shl)
          aci(i) = sum(acl(:,i)*shl)
          dpridxi(i) = sum(pl*shgradg(:,i))

          dphidxi(i)    = sum(phil*shgradg(:,i))
          dphiolddxi(i) = sum(philold *shgradg(:,i))
          dphiddxi(i)   = sum(phidl*shgradg(:,i)) 
          
          do j = 1, msh%NSD
            duidxi(i,j) = sum(ul(:,i)*shgradg(:,j))
            duiolddxi(i,j) = sum(ulold(:,i)*shgradg(:,j))
            ddidxi(i,j) = sum((dl(:,i)-dlold(:,i))*shgradgold(:,j))
            do k = 1, msh%NSD
              duidxixj(i,j,k) = sum (ul(:,i)*shhessg(:,j,k))         
            end do
          end do
        end do  
        
        pri  = sum(pl*shl)
        rho  = sum(rhol *shl)

        do i = 1, msh%NSD
           drhodxi(i) = sum(rhol(:)*shgradg(:,i))
        enddo

        mu   = sum(mul  *shl)    
        
        rho0 = sum(rho0l*shl)
        
        phi  = sum(phil *shl)         
        rphi = sum(rphil*shl)                
        
        phid = sum(phidl*shl)  

        phi1 = sum(phi1l*shl)  
        
        He   = sum(hel *shl)
        Hep  = sum(hepl*shl)
             
!...  Other variables             
        uadvi = ui - umi    
        rhoconv = 0d0

        Se = 2d0*(He-0.5d0) 
        ai(:) = dphiddxi(:)*Se/(sqrt(sum(dphiddxi*dphiddxi))+1d-15) 
        ai1(:)= dphidxi(:)*Se/(sqrt(sum(dphidxi*dphidxi))+1d-15) 
        
        if(flag(2).eq.1) then
          pse_time_loc=0
          pse_time_loc1=0
         do ii=1,3
           do jj=1,3
            pse_time_loc= pse_time_loc+ai(ii)*Gij(ii,jj)*ai(jj)
            pse_time_loc1=pse_time_loc1+ai1(ii)*Gij(ii,jj)*ai1(jj)
           enddo
          enddo
            pse_time_loc=1/sqrt(pse_time_loc)
            pse_time_loc1=1/sqrt(pse_time_loc1)
            pse_time=min(pse_time,pse_time_loc)
            pse_time1=min(pse_time1,pse_time_loc1)
            pse_time=2*pse_lamda*pse_time
            pse_time1=2*pse_lamda*pse_time1
       endif
              
     

!...  Strong residuals
        fi    = (rho-rho0)*gravvec         
        res_u = rho*aci + &
                rho*(duidxi(:,1)*uadvi(1) + &
                     duidxi(:,2)*uadvi(2) + &
                     duidxi(:,3)*uadvi(3)) + &
                dpridxi - fi - mu*(duidxixj(:,1,1)+duidxixj(:,2,2)+duidxixj(:,3,3))

!Calculate symmetric S and antisymmetric Omega components of the velocity gradient tensor:
          do i = 1, msh%NSD
            do j = 1, msh%NSD
              Omegai(i,j) = 0.5d0*(duidxi(i,j) - duidxi(j,i))
              Si(i,j) = 0.5d0*(duidxi(i,j) + duidxi(j,i))
            end do
          end do
!Calculate Q-criterion
          Qi = 0d0
          do i = 1, msh%NSD
            Qi = Qi + 0.5d0*(sum(Omegai(i,:)*Omegai(i,:)) - sum(Si(i,:)*Si(i,:)))
          end do
          Rhsq(:) = Rhsq(:) + shl(:)*Qi*DetJ*gw(igauss)
          Rhsl(:) = Rhsl(:) + shl(:)*DetJ*gw(igauss)

         
        res_c  = rphi + sum(uadvi*dphidxi)

        gradmag = sqrt(sum(dphiddxi*dphiddxi))

        res_rd = sum(ai*dphiddxi) - Se

!...  NavSto - Numerical parameters
       if(ido_flag.eq.1) then 
         call e3STAB_3D_ido(Gij, Ginv, uadvi, res_u, rho, mu,  &
                       tauM_ns, tauP_ns, tauC_ns, tauBar_ns,cfl_loc)
  
       else
         call e3STAB_3D(Gij, Ginv, uadvi, res_u, rho, mu,  &
                       tauM_ns, tauP_ns, tauC_ns, tauBar_ns,cfl_loc)
       endif

        cfl(1)  = max(cfl(1), cfl_loc(1))

        call e3DC_beta2(ui,duidxi,Gij,Ginv,res_u,dxidx,tauM_ns,kdc_ns)
       
        kdc_ns = (1d0-He)*NS_kdc_a*kdc_ns + He*NS_kdc_w*kdc_ns

!...  Convection - Numerical parameters 
        call e3STAB_TAU(uadvi, Gij, tau_c)
        call e3DC_CAU(dphidxi,uadvi,Gij,Ginv,res_c,tau_c,kdc_c)
                   
        kdc_c = LSC_kdc*kdc_c  
            
!...  Redistance - Numerical parameters         
        
        if(flag(3).eq.1) then !!!!!!!!!!!!!if of compute tau_rd and kdc_rd
          tau_rd = 0d0
          do i = 1, msh%NSD
           do j = 1, msh%NSD
             tau_rd = tau_rd + ai(i)*Gij(i,j)*ai(j)
           enddo
         enddo 
      !  tau_rd = 1d0/sqrt(4/(pse_time*pse_time)+tau_rd + 1d-15)
         tau_rd = 1d0/sqrt(4/(pse_time*pse_time)+tau_rd)
      !  tau_rd = 0d0
         kdc_rd = 0d0
!!$        do i = 1, msh%NSD
!!$          do j = 1, msh%NSD
!!$            kdc_rd = kdc_rd + dphiddxi(i)*Gij(i,j)*dphiddxi(j)
!!$          enddo
!!$        enddo 

!        kdc_rd = LSRD_kdc*abs(Se)*1d0/sqrt(Gij(1,1)+Gij(2,2)+Gij(3,3))
         tmp_val=0
         do ii=1,3
           do jj=1,3
              tmp_val=tmp_val+dphiddxi(ii)*Gij(ii,jj)*dphiddxi(jj)
           enddo
         enddo
             
    !   kdc_rd = LSRD_kdc*abs(Se)*sqrt(Ginv(1,1)+Ginv(2,2)+Ginv(3,3)) 
        kdc_rd = LSRD_kdc*abs(res_rd)/sqrt(tmp_val+1d-15) 
 
!!$        if (runcase==ship) then
!!$           kdc_rd = LSRD_kdc*abs(Se)*sqrt(Ginv(3,3)) 
!!$        else
!!$           kdc_rd = LSRD_kdc*abs(Se)*sqrt(Ginv(1,1)+Ginv(2,2)+Ginv(3,3)) 
!!$        endif
       
!...  Mesh motion - Numerical parameters  

       endif !!!!!!!!!!!!!end if of compute tau_rd and kdc_rd
        Emod = 1d0
           
!...  NavSto Residual 

     if(flag(1).eq.1) then 

        if(ido_flag.eq.1) then 
         call e3RHS_3D_fluid_ido(nshl, ui, aci, umi, acmi, uadvi,&
                             pri, res_u, fi, duidxi, ddidxi, &
                             tauM_ns, tauP_ns, tauC_ns, tauBar_ns, kdc_ns,&
                             rho, mu, DetJ, gw(igauss), shl, shgradg, &
                             eRns(1:3,:), eRns(4,:))
        else
         call e3RHS_3D_fluid(nshl, ui, aci, umi, acmi, uadvi,&
                             pri, res_u, fi, duidxi, ddidxi, &
                             tauM_ns, tauP_ns, tauC_ns, tauBar_ns, kdc_ns,&
                             rho, mu, DetJ, gw(igauss), shl, shgradg, &
                             eRns(1:3,:), eRns(4,:))
        endif

        do aa = 1, NSHL

           eRns(1:3,aa) = eRns(1:3,aa) - shl(aa)*rhoconv*ui(:)*detJgwt
           
        enddo

!...  Convection Residual	             
        do aa = 1, NSHL
          shconv(aa) = sum(shgradg(aa,:)*uadvi)
          eRlsc(aa) = eRlsc(aa) - &
                    ( (shl(aa) + tau_c*shconv(aa))*res_c &
                    + kdc_c*sum(shgradg(aa,:)*dphidxi(:)) )*detJgwt
        enddo
      endif  ! endif of flag(1).eq.1
     
!...  Redistance Residual
       
     if(flag(3).eq.1) then
        do aa = 1, NSHL
          eRlsrd(aa) = eRlsrd(aa) -  &
            ( shl(aa)*res_rd &
            + sum(ai(:)*shgradg(aa,:))*tau_rd*res_rd &
            + kdc_rd*sum(shgradg(aa,:)*dphiddxi(:)) &
            + shl(aa)*LSRD_penalty_fac*Hep*(phid-phi1) )*detJgwt
        enddo
      endif  ! endif of flag(3).eq.1
        
!...  Mesh Residual
     if(flag(2).eq.1) then ! endif of flag(2).eq.1
        call e3Rhs_3D_mesh(nshl, Emod, ddidxi, gw(igauss), shl, shgradgold, eRmesh)
     endif ! endif of flag(2).eq.1

        if (newJac >= 1) then
!...  NavSto Jacobian     
         
         if(flag(1).eq.1) then

          if(ido_flag.eq.1) then 
           call e3LHS_3D_fluid_ido(nshl, ui, umi, aci, pri, duidxi, dpridxi, &
                                   res_u, tauM_ns, tauP_ns, tauC_ns, tauBar_ns, &
                                   kdc_ns, rho, mu, DetJ,gw(igauss),&
                                   shl, shgradg, shhessg,eKns)
          else
           call e3LHS_3D_fluid(nshl, ui, umi, aci, pri, duidxi, dpridxi, &
                                   res_u, tauM_ns, tauP_ns, tauC_ns, tauBar_ns, &
                                   kdc_ns, rho, mu, DetJ,gw(igauss),&
                                   shl, shgradg, shhessg,eKns)
          endif

           do bb = 1, NSHL
              do aa = 1, NSHL
                 eKns(1,aa,bb) = eKns(1,aa,bb) + shl(aa)*rhoconv*shl(bb)*fact2*detJgwt
                 eKns(6,aa,bb) = eKns(6,aa,bb) + shl(aa)*rhoconv*shl(bb)*fact2*detJgwt
                 eKns(11,aa,bb) = eKns(11,aa,bb) + shl(aa)*rhoconv*shl(bb)*fact2*detJgwt
              enddo
           enddo

!...  Convection Jacobian    
           do bb = 1, NSHL
              do aa = 1, NSHL
                eKlsc(aa,bb) = eKlsc(aa,bb) + &
                    ( (shl(aa) + tau_c*shconv(aa))*(fact1*shl(bb) + fact2*shconv(bb)) &
                    + fact2*kdc_c*sum(shgradg(aa,:)*shgradg(bb,:)) )*detJgwt
             enddo
           enddo 
        
         endif !  if(flag(1).eq.1) then
          

!...  Redistance Jacobian
        if(flag(3).eq.1) then
         do aa = 1, NSHL
            shconv(aa) = sum(shgradg(aa,:)*ai(:))
         enddo
          do bb = 1, NSHL
            do aa = 1, NSHL
       !      eKlsrd(aa,bb) = eKlsrd(aa,bb) +     &
       !                     ( shl(aa)*shconv(bb) &
       !                     + shconv(aa)*tau_rd*shconv(bb) &
       !                     + kdc_rd*sum(shgradg(aa,:)*shgradg(bb,:)) &
       !                     + shl(aa)*LSRD_penalty_fac*Hep*shl(bb) )*fact2*detJgwt

             eKlsrd(aa,bb) = eKlsrd(aa,bb) +     &
                            ( shl(aa)*shl(bb)/pse_time & ! add for pse_time
                            + shl(aa)*shconv(bb) &
                            + shconv(aa)*tau_rd*shconv(bb) &
                            + shconv(aa)*tau_rd*shl(bb)/pse_time  & ! add for pse_time
                            + kdc_rd*sum(shgradg(aa,:)*shgradg(bb,:)) &
                            + shl(aa)*LSRD_penalty_fac*Hep*shl(bb) )*detJgwt



            enddo
          enddo 
        endif !if(flag(3).eq.1) then
          
!...  Mesh Residual    
       if(flag(2).eq.1) then        
          call e3Lhs_3D_mesh(nshl, Emod, gw(igauss), shgradgold, eKmesh)  
       endif
                          
        endif
        if (newJac >= 2) then     
                                       
!...  Convection Jacobian wrt u   
          do bb = 1, NSHL
             do aa = 1, NSHL
               eKlscu(:,aa,bb) = eKlscu(:,aa,bb) + &
                   ( shl(aa)*fact2*shl(bb)*dphidxi  + &
                     tau_c*shgradg(aa,:)*fact2*shl(bb)*res_c )*detJgwt
            enddo
          enddo  
          
!...  Redist Jacobian wrt phi 
            
          do bb = 1, NSHL
             do aa = 1, NSHL
               eKrdphi(aa,bb) = eKrdphi(aa,bb) - &
                   fact2*shl(aa)*LSRD_penalty_fac*Hep*shl(bb)*detJgwt  
            enddo
          enddo            
                 
        endif                      
      enddo  ! end  loop integration points 
          
 !... Enforce strong BCs         
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        if(flag(1).eq.1) then
         IBCns_loc  (aa,:) = IBCns  (bb,:)
         IBClsc_loc (aa,1) = IBClsc (bb)
        endif
        if(flag(2).eq.1) then
         IBCmesh_loc(aa,:) = IBCmesh(bb,:)  
        endif
      enddo

      if(flag(1).eq.1) then
       call applyBCs(nshl, msh%NSD+1, IBCns_loc  (1:nshl,:), eKns ,   eRns)                    
       call applyBCs(nshl,         1, IBClsc_loc (1:nshl,:), eKlsc,   eRlsc)  
      endif 
      if(flag(2).eq.1) then
       call applyBCs(nshl, msh%NSD,   IBCmesh_loc(1:nshl,:), eKmesh , eRmesh)
      endif
             
!... Assemble global residual           
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        if(flag(1).eq.1) then
         RHSG(bb,1:4) = RHSG(bb,1:4) + eRns  (:,aa)              
         RHSG(bb,5)   = RHSG(bb,5)   + eRlsc (aa)
         if(newJac >=1) then 
          rhsqu(bb)    = rhsqu(bb)    + rhsq(aa)
          rhsql(bb)    = rhsql(bb)    + rhsl(aa)
         endif
        endif
        if(flag(3).eq.1) then
         RHSG(bb,6)   = RHSG(bb,6)   + eRlsrd(aa) 
        endif
        if(flag(2).eq.1) then
          RHSG(bb,7:9) = RHSG(bb,7:9) + eRmesh(:,aa)   
        endif             
      enddo

!... Assemble global jacobian           
      if (newJac >= 1) then  
        if(flag(1).eq.1) then    
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl), msh%NSD+1, eKns,   sparseMat, LHSns  )     
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl),         1, eKlsc,  sparseMat, LHSlsc )
        endif
        if(flag(3).eq.1) then  
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl),         1, eKlsrd, sparseMat, LHSlsrd)   
        endif   
        if(flag(2).eq.1) then      
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl),   msh%NSD, eKmesh, sparseMat, LHSmesh)  
        endif 
      end if 
      
      if (newJac >= 2) then         
        do i = 1 , msh%NSD
          call applyColBCs(nshl, IBCns_loc(1:nshl,i), eKlscu(i,:,:))
          call applyRowBCs(nshl,IBClsc_loc(1:nshl,1), eKlscu(i,:,:))
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKlscu(i,:,:),   sparseMat, LHSlscu(i,:) ) 
        enddo 
                                             
        call applyColBCs(nshl,IBClsc_loc(1:nshl,1),eKrdphi) 
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKrdphi,   sparseMat, LHSrdphi) 
      end if         
        
                           
!---------------------------------------------------------------------------------------    
!...  Solid
!--------------------------------------------------------------------------------------- 
    else if (msh%EL_TYP(iel) == 1) then

!...  Loop over integration points
      do igauss = 1, NGAUSS       
        Ftens = 0.0d0; Stens = 0.0d0; Ctens = 0.0d0
      
        call eval_shape(nshl, iel, gp(igauss,:), xl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,hess_flag, msh%iga)  

        do i = 1, msh%NSD
          di (i) = sum(dl(:,i)*shl)
          ui(i)  = sum(ul(:,i)*shl)
          aci(i) = sum(acl(:,i)*shl)          
          do j = 1, msh%NSD
            ddidxi(i,j) = sum(dl(:,i)*shgradg(:,j))
          end do
        end do
        fi = 0d0
     
        call e3tensors_nonlin(msh%NSD, ddidxi, mu, kappa, Ftens, &
                              Stens, Ctens)

        Stens = Stens !!+ FPKS(iel,:,:)

!...  Solid Residual        
        call e3Rhs_3D_struct(nshl, aci, ui, fi, Ftens, Stens, gw(igauss), &
                              shl, shgradg, eRns(1:3,:), damp)
       
!...  Solid Jacobian        
        if (newJac >= 1) then
          call e3LHS_3D_struct(nshl, gw(igauss), shl, shgradg, Ftens, &
                               Stens, Ctens, eKns, damp)
        end if
    
      enddo  ! end of integration point loop
       
!... Enforce strong BCs
      !call applyBCs(nshl,msh%IEN(iel,1:nshl), msh%NSD+1, IBC, eKns , eRns) 
                         
!... Assemble global residual

      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        RHSG(bb,1:3) = RHSG(bb,1:3) + eRns  (aa,1:3)  
      enddo
              
!... Assemble global jacobian           
      !if (newJac >= 1) then
      !  call FillSparseMat_3D(nshl, iel, eKns, eGns, eDns, eMns)             
      !end if     
    endif ! end Fluid/Solid if 

    ! gp and gw are allocated for every element unconditionally now
    deallocate(gp,gw)
      
  enddo ! end of element loop 
  
!...  Deallocate
  if (NSHL >= 0) then
    deallocate(shl, shgradg,shgradgold, shhessg, & !gp, gw, &
               eRns,eRlsc,eRlsrd, eRmesh, &
               xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul, &
               ulold,rho0l,phil,rphil,philold,phi1l,phidl, &
               hl, hel, hepl)
               
    !if (newJac >= 1) then          
       deallocate(eKns,eKlsc,eKlsrd, eKmesh,eKlscu,eKrdphi)
    !endif           
  endif
    volm = vol_int
    if (numnodes > 1) then
       gcfl = cfl
       call MPI_ALLReduce(gcfl, cfl, 2, MPI_DOUBLE_PRECISION, &
                         MPI_MAX, MPI_COMM_WORLD,mpi_err)
       ! call MPI_ALLReduce(vol_int, volm, 1, MPI_DOUBLE_PRECISION, &
            ! MPI_SUM, MPI_COMM_WORLD,mpi_err) 
    endif

    if ((ismaster).and.(flag(1).eq.1)) then
       !write(*,'(40("-"))')
       write(*,*) myid, "***********CFL = ", cfl(1),"****************"
       ! write(*,*) "============ volm =", volm
    endif

  !*************************************************
  ! get the pse_time step
   if((numnodes>1).and.(flag(2).eq.1)) then
     pse_time_glob=pse_time
     call MPI_ALLReduce(pse_time_glob, pse_time, 2, MPI_DOUBLE_PRECISION, &
                         MPI_MIN, MPI_COMM_WORLD,mpi_err)
    endif

   if((numnodes>1).and.(flag(2).eq.1)) then
     pse_time_glob1=pse_time1
     call MPI_ALLReduce(pse_time_glob1, pse_time1, 2, MPI_DOUBLE_PRECISION, &
                         MPI_MIN, MPI_COMM_WORLD,mpi_err)
    endif

  !  if ((ismaster).and.(flag(2).eq.1)) then
  !     write(*,'(40("-"))')
  !     write(*,*) "    pse_time = ", pse_time
  !     write(*,*) "    pse_time1 = ", pse_time1
  !   endif
   ! ************************************************
    
end subroutine IntAssembly_IM

!======================================================================
!
!======================================================================
subroutine IntAssembly_rd_IM(st, msh,  rhsg, newJac,flag)
                        
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use BoundaryConditions
  implicit none
  
  type(state),      intent(in) :: st
  type(meshStruct), intent(in) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,2*msh%NSD+3)  
  integer, intent(in) :: newJac

  integer :: iel, igauss, i, j, k,ii,jj,kk, hess_flag, idx, nshl, NGAUSS,aa,bb
  integer :: IBCns_loc(msh%maxNSHL,msh%NSD+1), IBClsc_loc(msh%maxNSHL,1), &
             IBCmesh_loc(msh%maxNSHL,msh%NSD)

  real(8), allocatable :: eKns(:,:,:), eKlsc(:,:),eKlsrd(:,:),eKmesh(:,:,:),&
                          eKlscu(:,:,:),eKrdphi(:,:)
  real(8), allocatable :: eRns(:,:), eRlsc(:),eRlsrd(:), eRmesh(:,:)

  real(8), allocatable :: gp(:,:), gw(:)                          
  real(8), allocatable :: shl(:), shgradg(:,:),shgradgold(:,:), shhessg(:,:,:)
  real(8) :: Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD),&
             dxdxi(msh%NSD,msh%NSD),dxidx(msh%NSD,msh%NSD)
  real(8) :: DetJ, detJgwt,shconv(msh%maxNSHL)
  
  real(8), allocatable :: xl(:,:),wl(:), dl(:,:),dlold(:,:),uml(:,:), acml(:,:),&
                          ul(:,:),acl(:,:),pl(:),rhol(:),mul(:),&
                          ulold(:,:),rho0l(:), &
                          phil(:),rphil(:), philold(:),&
                          phi1l(:),phidl(:), phidl_old(:), &
                          hl(:), hel(:), hepl(:)

  real(8) :: di(msh%NSD), umi(msh%NSD), acmi(msh%NSD),           &
             ui(msh%NSD), aci(msh%NSD), pri, rho, mu, rho0,  &
             phi , rphi, phi1, phid, phid_old,he, hep,hi 
  real(8) :: ddidxi(msh%NSD,msh%NSD), duidxi(msh%NSD,msh%NSD),duidxixj(msh%NSD,msh%NSD,msh%NSD),&
             duiolddxi(msh%NSD,msh%NSD), dpridxi(msh%NSD), &
             dphidxi(msh%NSD),dphiolddxi(msh%NSD),dphiddxi(msh%NSD), &
             drhodxi(msh%NSD), rhoconv, DetJold, uiold(msh%NSD)

  real(8) :: uadvi(msh%NSD), Se, ai(msh%NSD),ai1(msh%NSD), fi(msh%NSD)
  real(8) :: res_u(msh%NSD), res_c, res_rd
  real(8) :: tauM_ns, tauC_ns, tauP_ns, tauBar_ns, tau_c, tau_rd, gradmag
  real(8) :: kdc_ns, kdc_c, kdc_rd, Emod, phimax
  real(8) :: fact1,fact2
    
  real(8), parameter :: damp = 0.5d0
  real(8) :: kappa, Ftens(msh%NSD,msh%NSD), Stens(msh%NSD,msh%NSD), Ctens(msh%NSD,msh%NSD,msh%NSD,msh%NSD) 
  real(8) :: cfl(2), gcfl(2), cfl_loc(2)
  real(8) :: pse_time_loc,pse_time_loc1,pse_time_glob,pse_time_glob1,tmp_val
  integer, intent(in) :: flag(4)
  cfl=0.0d0

  fact1 = almi
  fact2 = alfi*gami*Delt 

  phimax = max(maxval(st%phig),-minval(st%phig))


  phi = phimax
  call MPI_ALLREDUCE (phi, phimax ,1, MPI_DOUBLE_PRECISION, &
                      MPI_SUM, MPI_COMM_WORLD, mpi_err)  

!...  Loop over elements
  NSHL   = -1  
  do iel = 1, msh%NELEM 

!...  Allocate        
    if (NSHL /= msh%ELMNSHL(iel)) then
      
      if (NSHL >= 0) then
        deallocate(shl, shgradg,  shgradgold,shhessg, &! gp, gw,              &
                   eRns, eRlsc,eRlsrd,eRmesh,                  &
                   xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul,       &
                   ulold,rho0l,phil,rphil,philold,phi1l,phidl,phidl_old, &
                   hl, hel, hepl)
        !if (newJac >= 1) then          
           deallocate(eKns,eKlsc,eKlsrd, eKmesh,eKlscu,eKrdphi)
        !endif           
      endif
     
      NSHL   = msh%ELMNSHL(iel)
      ! NGAUSS = msh%ELMNGAUSS(iel)

      allocate(shl(NSHL), shgradg(NSHL,msh%NSD), shgradgold(NSHL,msh%NSD),shhessg(NSHL,msh%NSD,msh%NSD),&
              ! gp(NGAUSS,msh%NSD), gw(NGAUSS),                        &
               eRns(msh%NSD+1,NSHL), eRlsc(NSHL),eRlsrd(NSHL),eRmesh(msh%NSD,NSHL),  &
               dl(NSHL,msh%NSD),dlold(NSHL,msh%NSD), ul(NSHL,msh%NSD),wl(NSHL),      &
               acl(NSHL,msh%NSD), uml(NSHL,msh%NSD), acml(NSHL,msh%NSD),             &
               pl(NSHL), xl(NSHL,msh%NSD), rhol(NSHL),mul(NSHL),                     &
               ulold(NSHL,msh%NSD),rho0l(NSHL),phil(NSHL),rphil(NSHL),philold(NSHL), &
               phi1l(NSHL),phidl(NSHL),phidl_old(NSHL),hl(NSHL),hel(NSHL),hepl(NSHL))
      !if (newJac >= 1) then                    
        allocate(eKns((msh%NSD+1)*(msh%NSD+1),NSHL,NSHL), &
                 eKlsc(NSHL,NSHL),eKlsrd(NSHL,NSHL),      & 
                 eKmesh(msh%NSD*msh%NSD,NSHL,NSHL),      &
                 eKlscu(msh%NSD,NSHL,NSHL), eKrdphi(NSHL,NSHL))
      !endif        

      ! call genGPandGW(gp, gw, NGAUSS, NSHL)
    end if

    ! get gauss points and weights from dynamic list from current element
    ngauss = msh%elmgplists(iel)%size

    allocate(gp(ngauss,msh%nsd),gw(ngauss))
    do i=1,ngauss
       gp(i,1:3) = msh%elmgplists(iel)%content(1:3,i)
       gw(i)     = msh%elmgplists(iel)%content(4  ,i)
    enddo

!...  Get local solution arrays 
    do i = 1, NSHL 
    
      idx = msh%IEN(iel,i)
      xl   (i,:) = msh%xg    (idx,:)     
      wl   (i)   = msh%wg    (idx)
      
      dl   (i,:) = st%dg   (idx,:)      
      dlold(i,:) = old_rd%dg  (idx,:)
      uml  (i,:) = st%ugm  (idx,:)
      acml (i,:) = st%acgm (idx,:)
            
      ul   (i,:) = st%ug   (idx,:)
      acl  (i,:) = st%acg  (idx,:)
      pl   (i)   = st%pg   (idx)
      rhol (i)   = st%rhog (idx)          
      mul  (i)   = st%mug  (idx) 
      ulold(i,:) = old_rd%ug  (idx,:)   
      rho0l(i)   = st%rho0g(idx)
      
      phil(i)    = st%phig (idx)       
      rphil(i)   = st%rphig(idx)     
      philold(i) = old_rd%phig(idx)
     
      phi1l(i)   = st%phig   (idx)

      phidl(i)   = st%phidg  (idx)
      phidl_old(i)   = old_rd%phidg  (idx)

      hl(i)      = st%hg  (idx)             
      hel(i)     = st%heg (idx)             
      hepl(i)    = st%hepg(idx)     
    end do
         
!...  Initialize element jacobians
    if (newJac >= 1) then
      eKns   = 0d0           
      eKlsc  = 0d0
      eKlsrd = 0d0  
      eKmesh = 0d0     
    endif
    if (newJac >= 2) then    
      eKlscu  = 0d0     
      eKrdphi = 0d0      
    endif   
        
!...  Initialize element residuals    
    eRns   = 0d0
    eRlsc  = 0d0
    eRlsrd = 0d0
    eRmesh = 0d0

!---------------------------------------------------------------------------------------    
!...  Fluid
!---------------------------------------------------------------------------------------  
    if (msh%EL_TYP(iel) == 0) then    
         
!...  Loop over integration points
      do igauss = 1, NGAUSS
        shl = 0d0;  shgradg = 0d0; shhessg = 0d0; 
        
        call eval_shape(nshl, iel, gp(igauss,:), xl+dlold, wl,         &
                        shl, shgradgold, shhessg,  DetJold,  &
                        dxdxi,dxidx,Gij,Ginv,0, msh%iga)  
 
        
        call eval_shape(nshl, iel, gp(igauss,:), xl+dl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,NS_hess_flag, msh%iga)  

        detJgwt = DetJ*gw(igauss) 
       
!...  Interpolate     

        do i = 1, msh%NSD
          ui (i) = sum(ul (:,i)*shl)
          uiold (i) = sum(ulold (:,i)*shl)
          umi(i) = sum(uml(:,i)*shl)
          aci(i) = sum(acl(:,i)*shl)
          dpridxi(i) = sum(pl*shgradg(:,i))

          dphidxi(i)    = sum(phil*shgradg(:,i))
          dphiolddxi(i) = sum(philold *shgradg(:,i))
          dphiddxi(i)   = sum(phidl*shgradg(:,i)) 
          
          do j = 1, msh%NSD
            duidxi(i,j) = sum(ul(:,i)*shgradg(:,j))
            duiolddxi(i,j) = sum(ulold(:,i)*shgradg(:,j))
            ddidxi(i,j) = sum((dl(:,i)-dlold(:,i))*shgradgold(:,j))
            do k = 1, msh%NSD
              duidxixj(i,j,k) = sum (ul(:,i)*shhessg(:,j,k))         
            end do
          end do
        end do  
        
        pri  = sum(pl*shl)
        rho  = sum(rhol *shl)

        do i = 1, msh%NSD
           drhodxi(i) = sum(rhol(:)*shgradg(:,i))
        enddo

        mu   = sum(mul  *shl)    
        
        rho0 = sum(rho0l*shl)
        
        phi  = sum(phil *shl)         
        rphi = sum(rphil*shl)                
        
        phid = sum(phidl*shl) 
        
        phid_old = sum(phidl_old*shl)  

        phi1 = sum(phi1l*shl)  
        
        He   = sum(hel *shl)
        Hep  = sum(hepl*shl)
             
!...  Other variables             
        uadvi = ui - umi    
        rhoconv = 0d0

        Se = 2d0*(He-0.5d0) 
        ai(:) = dphiddxi(:)*Se/(sqrt(sum(dphiddxi*dphiddxi))+1d-15) 
     !   ai1(:)= dphidxi(:)*Se/(sqrt(sum(dphidxi*dphidxi))+1d-15) 
        ! Jinhui: it does not matter which one will be used, since we are only using
        ! one NL iteration, assemble will be used only once. They are equal at the beginning
!...  Strong residuals
        fi    = (rho-rho0)*gravvec         
        res_u = rho*aci + &
                rho*(duidxi(:,1)*uadvi(1) + &
                     duidxi(:,2)*uadvi(2) + &
                     duidxi(:,3)*uadvi(3)) + &
                dpridxi - fi - mu*(duidxixj(:,1,1)+duidxixj(:,2,2)+duidxixj(:,3,3))
         
        res_c  = rphi + sum(uadvi*dphidxi)

        gradmag = sqrt(sum(dphiddxi*dphiddxi))

        res_rd = (phid-phid_old)/pse_time+sum(ai*dphiddxi) - Se
       
!...  NavSto - Numerical parameters 
        call e3STAB_3D(Gij, Ginv, uadvi, res_u, rho, mu,  &
                       tauM_ns, tauP_ns, tauC_ns, tauBar_ns,cfl_loc)
        cfl(1)  = max(cfl(1), cfl_loc(1))

        call e3DC_beta2(ui,duidxi,Gij,Ginv,res_u,dxidx,tauM_ns,kdc_ns)
        
        kdc_ns = (1d0-He)*NS_kdc_a*kdc_ns + He*NS_kdc_w*kdc_ns

!...  Convection - Numerical parameters 
        call e3STAB_TAU(uadvi, Gij, tau_c)
        call e3DC_CAU(dphidxi,uadvi,Gij,Ginv,res_c,tau_c,kdc_c)
                   
        kdc_c = LSC_kdc*kdc_c  
            
!...  Redistance - Numerical parameters         
        
        if(flag(3).eq.1) then !!!!!!!!!!!!!if of compute tau_rd and kdc_rd
        tau_rd = 0d0
        do i = 1, msh%NSD
          do j = 1, msh%NSD
          !  tau_rd = tau_rd + ai(i)*Gij(i,j)*ai(j)
            tau_rd = tau_rd + ai1(i)*Gij(i,j)*ai1(j)
          enddo
        enddo 
      !  tau_rd = 1d0/sqrt(4/(pse_time*pse_time)+tau_rd + 1d-15)
        tau_rd = 1d0/sqrt(4/(pse_time*pse_time)+tau_rd)
        kdc_rd = 0d0
        tmp_val=0
        do ii=1,3
          do jj=1,3
            ! tmp_val=tmp_val+dphiddxi(ii)*Gij(ii,jj)*dphiddxi(jj)
            tmp_val=tmp_val+dphidxi(ii)*Gij(ii,jj)*dphidxi(jj)
          enddo
        enddo   
        kdc_rd = LSRD_kdc*abs(res_rd)/sqrt(tmp_val+1d-15) 
       endif !!!!!!!!!!!!!end if of compute tau_rd and kdc_rd
        Emod = 1d0

!...  Redistance Residual
       
     if(flag(3).eq.1) then
        do aa = 1, NSHL
          eRlsrd(aa) = eRlsrd(aa) -  &
            ( shl(aa)*res_rd &
            + sum(ai(:)*shgradg(aa,:))*tau_rd*res_rd &
            + kdc_rd*sum(shgradg(aa,:)*dphiddxi(:)) &
            + shl(aa)*LSRD_penalty_fac*Hep*(phid-phi1) )*detJgwt
        enddo
      endif  ! endif of flag(3).eq.1
        

   if (newJac >= 1) then
!...  Redistance Jacobian
        if(flag(3).eq.1) then
         do aa = 1, NSHL
            shconv(aa) = sum(shgradg(aa,:)*ai(:))
         enddo
          do bb = 1, NSHL
            do aa = 1, NSHL
       !      eKlsrd(aa,bb) = eKlsrd(aa,bb) +     &
       !                     ( shl(aa)*shconv(bb) &
       !                     + shconv(aa)*tau_rd*shconv(bb) &
       !                     + kdc_rd*sum(shgradg(aa,:)*shgradg(bb,:)) &
       !                     + shl(aa)*LSRD_penalty_fac*Hep*shl(bb) )*fact2*detJgwt
             eKlsrd(aa,bb) = eKlsrd(aa,bb) +     &
                            ( shl(aa)*shl(bb)/pse_time & ! add for pse_time
                            + shl(aa)*shconv(bb) &
                            + shconv(aa)*tau_rd*shconv(bb) &
                            + shconv(aa)*tau_rd*shl(bb)/pse_time  & ! add for pse_time
                            + kdc_rd*sum(shgradg(aa,:)*shgradg(bb,:)) &
                            + shl(aa)*LSRD_penalty_fac*Hep*shl(bb) )*detJgwt

            enddo
          enddo 
        endif !if(flag(3).eq.1) then                        
      endif
        if (newJac >= 2) then     
                                       
!...  Convection Jacobian wrt u   
          do bb = 1, NSHL
             do aa = 1, NSHL
               eKlscu(:,aa,bb) = eKlscu(:,aa,bb) + &
                   ( shl(aa)*fact2*shl(bb)*dphidxi  + &
                     tau_c*shgradg(aa,:)*fact2*shl(bb)*res_c )*detJgwt
            enddo
          enddo  
          
!...  Redist Jacobian wrt phi 
            
          do bb = 1, NSHL
             do aa = 1, NSHL
               eKrdphi(aa,bb) = eKrdphi(aa,bb) - &
                   fact2*shl(aa)*LSRD_penalty_fac*Hep*shl(bb)*detJgwt  
            enddo
          enddo            
                 
        endif                      
      enddo  ! end  loop integration points 
    

! *********************************************************************************
! To impose strong boundary condition for redistancing
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        if(flag(3).eq.1) then
         IBClsc_loc (aa,1) = IBClsc (bb)
        endif
      enddo

      if(flag(3).eq.1) then                 
       call applyBCs(nshl, 1, IBClsc_loc (1:nshl,:), eKlsrd, eRlsrd)  
      endif 
!********************************************************************************** 

             
!... Assemble global residual           
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        if(flag(3).eq.1) then
         RHSG(bb,6)   = RHSG(bb,6)   + eRlsrd(aa) 
        endif
      enddo

!... Assemble global jacobian           
      if (newJac >= 1) then  
        if(flag(3).eq.1) then  
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl),1, eKlsrd, sparseMat, LHSlsrd)   
        endif   
      end if 
      
      if (newJac >= 2) then         
        do i = 1 , msh%NSD
          call applyColBCs(nshl, IBCns_loc(1:nshl,i), eKlscu(i,:,:))
          call applyRowBCs(nshl,IBClsc_loc(1:nshl,1), eKlscu(i,:,:))
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKlscu(i,:,:),   sparseMat, LHSlscu(i,:) ) 
        enddo 
                                             
        call applyColBCs(nshl,IBClsc_loc(1:nshl,1),eKrdphi) 
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKrdphi,   sparseMat, LHSrdphi) 
      end if         
        
                           
!---------------------------------------------------------------------------------------    
!...  Solid
!--------------------------------------------------------------------------------------- 
    else if (msh%EL_TYP(iel) == 1) then

!...  Loop over integration points
      do igauss = 1, NGAUSS       
        Ftens = 0.0d0; Stens = 0.0d0; Ctens = 0.0d0
      
        call eval_shape(nshl, iel, gp(igauss,:), xl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,hess_flag, msh%iga)  

        do i = 1, msh%NSD
          di (i) = sum(dl(:,i)*shl)
          ui(i)  = sum(ul(:,i)*shl)
          aci(i) = sum(acl(:,i)*shl)          
          do j = 1, msh%NSD
            ddidxi(i,j) = sum(dl(:,i)*shgradg(:,j))
          end do
        end do
        fi = 0d0
     
        call e3tensors_nonlin(msh%NSD, ddidxi, mu, kappa, Ftens, &
                              Stens, Ctens)

        Stens = Stens !!+ FPKS(iel,:,:)

!...  Solid Residual        
        call e3Rhs_3D_struct(nshl, aci, ui, fi, Ftens, Stens, gw(igauss), &
                              shl, shgradg, eRns(1:3,:), damp)
       
!...  Solid Jacobian        
        if (newJac >= 1) then
          call e3LHS_3D_struct(nshl, gw(igauss), shl, shgradg, Ftens, &
                               Stens, Ctens, eKns, damp)
        end if
    
      enddo  ! end of integration point loop
       
!... Enforce strong BCs
      !call applyBCs(nshl,msh%IEN(iel,1:nshl), msh%NSD+1, IBC, eKns , eRns) 
                         
!... Assemble global residual

      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        RHSG(bb,1:3) = RHSG(bb,1:3) + eRns  (aa,1:3)  
      enddo
              
!... Assemble global jacobian           
      !if (newJac >= 1) then
      !  call FillSparseMat_3D(nshl, iel, eKns, eGns, eDns, eMns)             
      !end if     
    endif ! end Fluid/Solid if 
    ! gp and gw are allocated for every element unconditionally now
    deallocate(gp,gw)
      
  enddo ! end of element loop 
  
!...  Deallocate
  if (NSHL >= 0) then
    deallocate(shl, shgradg,shgradgold, shhessg, & !gp, gw, &
               eRns,eRlsc,eRlsrd, eRmesh, &
               xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul, &
               ulold,rho0l,phil,rphil,philold,phi1l,phidl, &
               hl, hel, hepl)
               
    !if (newJac >= 1) then          
       deallocate(eKns,eKlsc,eKlsrd, eKmesh,eKlscu,eKrdphi)
    !endif           
  endif
    
end subroutine IntAssembly_rd_IM

subroutine weak_BC_IM(sur, st, msh, rhsg, newJac, flag)

  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use BoundaryConditions
  use aAdjKeep
  use defs_shell
  implicit none
  
  type(smesh),      intent(inout) :: sur
  type(state),      intent(in) :: st
  type(meshStruct), intent(inout) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,2*msh%NSD+3)  
  integer, intent(in) :: newJac
  integer, intent(in) :: flag(4)

  real(8) :: tt_area, tt_area0
    ! Local variables
  logical :: isShell
  integer :: iel, igauss, jgauss, kgauss, i, j, k, aa, bb
  integer :: plusiel,minusiel
  real(8) :: plusPressure, minusPressure
  real(8) :: gwt, volm, Pload, fact, drag, lift, DENS_AIR, asdf

  integer :: igp, jel, dotimes, nshl
  integer :: idx
  integer :: IBCns_loc(msh%maxNSHL,msh%NSD+1)

  real(8) :: sgp(msh%NSD), sgw, snor(msh%NSD), sDetJ, sarea(6), tauB, tauNor, &
             tmp1(msh%NSD), gradpri(msh%NSD), laplacianu(msh%NSD), &
             duidxjdxk(msh%NSD,msh%NSD,msh%NSD), duidxidn(msh%NSD,msh%NSD), &
             gi(msh%nsd), rtmp1(6), hnorm
  ! needed for shape function routine
  real(8) :: Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD), &
             dxdxi(msh%NSD,msh%NSD),dxidx(msh%NSD,msh%NSD)
  real(8) :: DetJ, detJgwt,shconv(msh%maxNSHL)

  real(8) :: di(msh%NSD), umi(msh%NSD), acmi(msh%NSD),           &
             ui(msh%NSD), aci(msh%NSD), pri, rho, mu, rho0,  &
             phi , rphi, phi1, phid, he, hep,hi, xi(msh%NSD) 
  real(8) :: ddidxi(msh%NSD,msh%NSD), duidxi(msh%NSD,msh%NSD),duidxixj(msh%NSD,msh%NSD,msh%NSD),&
             duiolddxi(msh%NSD,msh%NSD), dpridxi(msh%NSD), &
             dphidxi(msh%NSD),dphiolddxi(msh%NSD),dphiddxi(msh%NSD), &
             drhodxi(msh%NSD), rhoconv, DetJold, uiold(msh%NSD)

  real(8), allocatable :: eKns(:,:,:), eKlsc(:,:),eKlsrd(:,:),eKmesh(:,:,:),&
                          eKlscu(:,:,:),eKrdphi(:,:)
  real(8), allocatable :: eRns(:,:), eRlsc(:),eRlsrd(:), eRmesh(:,:)

  real(8), allocatable :: xl(:,:),wl(:), dl(:,:),dlold(:,:),uml(:,:), acml(:,:),&
                          ul(:,:),acl(:,:),pl(:),rhol(:),mul(:),&
                          ulold(:,:),rho0l(:), &
                          phil(:),rphil(:), philold(:),&
                          phi1l(:),phidl(:),  &
                          hl(:), hel(:), hepl(:)

  real(8), allocatable :: Rhsq(:), Rhsl(:)
    
  real(8), allocatable :: gp(:,:), gw(:)                          
  real(8), allocatable :: shl(:), shgradg(:,:),shgradgold(:,:), shhessg(:,:,:)

  real(8), allocatable :: gpLocalTractionBuffer(:,:,:)

  real(8) :: penaltyTraction(msh%NSD), ti(msh%NSD)
  ! allocate a local buffer that is the same size as SUR%gp%traction
  allocate(gpLocalTractionBuffer(SUR%NEL,SUR%maxNGAUSS,msh%NSD))
  ! fill the local buffer with zeros so that an MPI_ALLREDUCE can sum
  ! them into SUR%gp%traction
  gpLocalTractionBuffer = 0.0d0

  nshl = -1
  sgp   = 0.0d0
  sgw   = 0.0d0
  snor  = 0.0d0
  sDetJ = 0.0d0
  sarea = 0.0d0
  tt_area = 0d0
  ! return
  ! loop over surface elements
  do jel = 1, SUR%NEL
     ! cheap check for zero area elements
     if(SUR%gp%gw(jel,1) == 0) then
        cycle
     endif

     ! loop over this element's gauss points
     do igp = 1, SUR%maxNGAUSS  !

        ! if it's this processor's responsibility...
        iel = SUR%gp%backelem(jel,igp)
        if (iel > 0) then
          if (NSHL /= msh%ELMNSHL(iel)) then
      
             if (NSHL >= 0) then
             deallocate(shl, shgradg,  shgradgold,shhessg,              &
                   eRns, &           
                   xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul,       &
                   ulold,rho0l,phil,rphil,philold,phi1l,phidl, &
                   hl, hel, hepl)
              !if (newJac >= 1) then          
              deallocate(eKns)
              !endif           
             endif
     
             NSHL   = msh%ELMNSHL(iel)

             allocate(shl(NSHL), shgradg(NSHL,msh%NSD), shgradgold(NSHL,msh%NSD), &
                    shhessg(NSHL,msh%NSD,msh%NSD), eRns(msh%NSD+1,NSHL),  &
                    dl(NSHL,msh%NSD),dlold(NSHL,msh%NSD), ul(NSHL,msh%NSD),wl(NSHL),      &
                    acl(NSHL,msh%NSD), uml(NSHL,msh%NSD), acml(NSHL,msh%NSD),             &
                    pl(NSHL), xl(NSHL,msh%NSD), rhol(NSHL),mul(NSHL),                     &
                    ulold(NSHL,msh%NSD),rho0l(NSHL),phil(NSHL),rphil(NSHL),philold(NSHL), &
                    phi1l(NSHL),phidl(NSHL),hl(NSHL),hel(NSHL),hepl(NSHL))
                !if (newJac >= 1) then                    
            allocate(eKns((msh%NSD+1)*(msh%NSD+1),NSHL,NSHL))
            !endif        
          end if

          !...  Get local solution arrays 
          do i = 1, NSHL 
    
              idx = msh%IEN(iel,i)
              xl   (i,:) = msh%xg    (idx,:)     
              wl   (i)   = msh%wg    (idx)
      
              dl   (i,:) = st%dg   (idx,:)      
              dlold(i,:) = old%dg  (idx,:)
              uml  (i,:) = st%ugm  (idx,:)
              acml (i,:) = st%acgm (idx,:)
            
              ul   (i,:) = st%ug   (idx,:)
              acl  (i,:) = st%acg  (idx,:)
              pl   (i)   = st%pg   (idx)
              rhol (i)   = old%rhog (idx)          
              mul  (i)   = old%mug  (idx) 
              ulold(i,:) = old%ug  (idx,:)   
              rho0l(i)   = st%rho0g(idx)
      
              phil(i)    = st%phig (idx)       
              rphil(i)   = st%rphig(idx)     
              philold(i) = old%phig(idx)
     
              phi1l(i)   = st%phig   (idx)

              phidl(i)   = st%phidg  (idx)

              hl(i)      = st%hg  (idx)             
              hel(i)     = st%heg (idx)             
              hepl(i)    = st%hepg(idx)     
          end do

          !...  Initialize element jacobians
          ! if (newJac >= 1) then
              eKns   = 0d0  
              eRns   = 0d0 

          !endif
          !if (newJac >= 2) then    
          !   eKlscu  = 0d0     
          !   eKrdphi = 0d0      
          !endif   

          ! surface integration point info
          sgp   = SUR%gp%backxi(jel,igp,:) !IM%SGP(  iel,igauss,:)
          sgw   = SUR%gp%gw(jel,igp) !IM%SGW(  iel,igauss)
          snor  = -SUR%gp%nor(jel,igp,:) !IM%Snor( iel,igauss,:)
          sDetJ = SUR%gp%detJ(jel,igp) !IM%SDetJ(iel,igauss)
           
          tt_area = tt_area + sgw*sDetJ

          ! Get Element Shape functions and their gradients
          shl     = 0.0d0 ! initialize
          shgradg = 0.0d0
          shhessg = 0.0d0

          ! use appropriate shape function routine for tet mesh
          call eval_shape(nshl, iel, sgp, xl+dl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,NS_hess_flag, msh%iga)

          ! Interpolate      
          pri = sum(pl*shl)
          rho  = sum(rhol *shl)
          mu  = sum(mul *shl) 

           do i = 1, msh%NSD
              ui (i) = sum(ul (:,i)*shl)
              umi(i) = sum(uml(:,i)*shl)
              xi (i) = sum(xl (:,i)*shl)
           end do  
           do j = 1, msh%NSD
              do i = 1, msh%NSD! getting gradient of velocity
                 duidxi(i,j) = sum(ul(:,i)*shgradg(:,j))
              end do
           end do

           ! write(*,*) sum(snor*xi)

                      !gi = umi
           ! gi(:) = SUR%gp%ush(jel,igp,:)
           gi(:) = 0d0

           hnorm = 0d0
           do i = 1, 3
              do j = 1, 3
                  hnorm = hnorm + snor(i)*Gij(i,j)*snor(j)
              end do
           end do
           hnorm = sqrt(hnorm)

           ! tauB   = WBC_TauTan
           ! tauNor = WBC_TauNor
           tauB = 4.0d0*hnorm*rho/delt
           tauNor = 100d0*tauB
           ! nothing is has any shell mechanics for the CFD
           isShell = .false.

           call e3bLHS_weak_im( &
                msh%NSD, NSHL, snor, sDetJ, sgw, mu, rho, tauB, &
                tauNor, almi, alfi*gami*Delt, ui, umi, duidxi, &
                shl, shgradg, eKns)

           call e3bRHS_weak_im(msh%NSD, NSHL, snor, sDetJ, &
                sgw, mu, rho, tauB, &
                tauNor, shl, shgradg, ui, umi, pri, duidxi, gi, &
                eRns, ti, tmp1, penaltyTraction, isShell)
               
            sarea(1:3) = sarea(1:3) -   &
                    !ti(1:2)*2.0d0/DENS_AIR/(FlRate**2)*sDetJ*sgw !cylinder
                    ti(1:3)*sDetJ*sgw  !sphere
               ! conservative definition
            sarea(4:6) = sarea(4:6) - &
                    !tmp1(1:2)*2.0d0/DENS_AIR/(FlRate**2)*sDetJ*sgw
                    tmp1(1:3)*sDetJ*sgw

             do aa = 1, NSHL  
                bb = msh%IEN(iel,aa)  
                IBCns_loc  (aa,:) = IBCns  (bb,:)
             end do

             call applyBCs(nshl, msh%NSD+1, IBCns_loc  (1:nshl,:), eKns ,   eRns) 
             ! assemble to global RHS 
             do aa = 1, NSHL  
                bb = msh%IEN(iel,aa) 
                RHSG(bb,1:4) = RHSG(bb,1:4) + eRns  (:,aa)
             end do

             call FillSparseMat(nshl, msh%ien(iel, 1:nshl), msh%NSD+1, eKns,   sparseMat, LHSns  )

        end if ! end if it is this processor's responsibility

     end do ! end loop over this elements GPs

  end do ! end loop over surface elements
  
    tt_area0 = tt_area
    rtmp1 = sarea
    if (numnodes > 1) then
        call MPI_ALLREDUCE(tt_area, tt_area0, 1, MPI_DOUBLE_PRECISION, &
                         MPI_SUM, MPI_COMM_WORLD, mpi_err)
        call MPI_ALLREDUCE(sarea(1:6), rtmp1(1:6), 6, MPI_DOUBLE_PRECISION, &
                         MPI_SUM, MPI_COMM_WORLD, mpi_err)
    end if

    if (ismaster) then
      write(*,*) "drag =", rtmp1(1), rtmp1(4)
      write(*,*) "lift1 =", rtmp1(2), rtmp1(5)
      write(*,*) "lift1 =", rtmp1(3), rtmp1(6)
      write(*,'(40("-"))')
    end if

     if(numnodes > 1) then
        call MPI_ALLREDUCE(gpLocalTractionBuffer, SUR%gp%traction, &
             SUR%NEL*SUR%NGAUSS*msh%NSD, &
             MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
     else
        SUR%gp%traction = gpLocalTractionBuffer
     endif

    ! if (ismaster) then
    !   write(*,*) "area =", tt_area0
    ! end if

  if (NSHL >= 0) then
    deallocate(shl, shgradg,shgradgold, shhessg, &
               eRns, &
               xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul, &
               ulold,rho0l,phil,rphil,philold,phi1l,phidl, &
               hl, hel, hepl)
               
    !if (newJac >= 1) then          
       deallocate(eKns)
    !endif           
  endif

end subroutine weak_BC_IM

!======================================================================
! remove the interial part where the function does not cross
! the boundary
! Trik: check the diag. If zero, then put 1 at diag and 0 and rhs
!======================================================================
subroutine remove_interior(msh,  rhsg)
  use params 
  use mpi
  use meshData  
  use matvec
  use BoundaryConditions
  implicit none
  
  type(meshStruct), intent(inout) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,2*msh%NSD+3)  
  integer :: IBCns_loc(msh%maxNSHL,msh%NSD+1)
  integer :: iel, a, i, j, c, n, locn
  do i = 1, msh%NNODE
    if(msh%includeflags(i) == 0.0d0) then
        do j = sparseMat%col(i), sparseMat%col(i+1)-1
            n = sparseMat%row(j)
            if (n == i) then 
              if (LHSns(1,j) == 0d0) then
                IBCns(i,1) = 1
                rhsg(i,1)  = 0d0
                LHSns(1,j) = 1d0
              end if
              if (LHSns(6,j) == 0d0) then
                IBCns(i,2) = 1
                rhsg(i,2)  = 0d0
                LHSns(6,j) = 1d0
              end if
              if (LHSns(11,j) == 0d0) then
                IBCns(i,3) = 1
                rhsg(i,3)  = 0d0
                LHSns(11,j) = 1d0
              end if
              if (LHSns(16,j) == 0d0) then
                IBCns(i,4) = 1
                rhsg(i,4)  = 0d0
                LHSns(16,j) = 1d0
              end if
              if (LHSlsc(j) == 0d0) then
                ! IBClsc(i) = 1
                rhsg(i,5)  = 0d0
                LHSlsc(j) = 1d0
              end if
              if (LHSlsrd(j) == 0d0) then
                rhsg(i,5)  = 0d0
				LHSlsrd(j) = 1d0
              end if
            end if
        end do
    end if

  end do

end subroutine remove_interior

!======================================================================
! LHS for IMMERSED weak BC
!======================================================================
subroutine e3bLHS_weak_im(nsd, nshl, nor, DetJb, gwt, mu, rho, tauB, tauNor, &
                       fact1, fact2, ui, umi, duidxi, shlu, shgradgu,  &
                       xKebe)
  implicit none
  
  integer, intent(in) :: nsd, nshl
  real(8), intent(in) :: ui(NSD), umi(NSD), duidxi(NSD,NSD), &
                         tauB, tauNor, gwt, nor(NSD), &
                         shlu(NSHL), shgradgu(NSHL,NSD), &
                         fact1, fact2, DetJb, mu, rho
  real(8), intent(inout) :: xKebe((NSD+1)*(NSD+1),NSHL,NSHL)

  integer :: aa, bb
  real(8) :: tmp1(NSHL), tmp2(NSHL,NSHL), &
             unor, umul, munor, gmul, uneg 

  tmp1 = 0.0d0
  tmp2 = 0.0d0

  tmp1(:) = shgradgu(:,1)*nor(1) + shgradgu(:,2)*nor(2) &
          + shgradgu(:,3)*nor(3) 

  unor = sum((ui-umi)*nor(:))  ! u \cdot n
  uneg = 0.5d0*(unor-abs(unor))
  munor = tauNor-tauB
  
  ! gmul =  1d0 => sym
  ! gmul = -1d0 => skew  
  gmul = 1.0d0
  do bb = 1, NSHL      ! Diagonal blocks of K
    do aa = 1, NSHL
      tmp2(aa,bb) = -shlu(aa)*mu*tmp1(bb) &
                  - gmul*tmp1(aa)*mu*shlu(bb) &
                  + shlu(aa)*tauB*shlu(bb) &
                  - shlu(aa)*uneg*rho*shlu(bb)
    end do
  end do


  do bb = 1, NSHL    
    do aa = 1, NSHL
      
      xKebe(1,aa,bb) = xKebe(1,aa,bb) + &
        fact2*( tmp2(aa,bb)                            &
              - shlu(aa)*mu*shgradgu(bb,1)*nor(1)      &
              - gmul*shgradgu(aa,1)*nor(1)*mu*shlu(bb) &
              + shlu(aa)*nor(1)*munor*nor(1)*shlu(bb) )*DetJb*gwt
       
      xKebe(6,aa,bb) = xKebe(6,aa,bb) + &
        fact2*(  tmp2(aa,bb)                           &
              - shlu(aa)*mu*shgradgu(bb,2)*nor(2)      &
              - gmul*shgradgu(aa,2)*nor(2)*mu*shlu(bb) &
              + shlu(aa)*nor(2)*munor*nor(2)*shlu(bb) )*DetJb*gwt
       
      xKebe(11,aa,bb) = xKebe(11,aa,bb) + &
        fact2*( tmp2(aa,bb)                            &
              - shlu(aa)*mu*shgradgu(bb,3)*nor(3)      &
              - gmul*shgradgu(aa,3)*nor(3)*mu*shlu(bb) &
              + shlu(aa)*nor(3)*munor*nor(3)*shlu(bb) )*DetJb*gwt
       
      xKebe(2,aa,bb) = xKebe(2,aa,bb) + &
        fact2*(-shlu(aa)*mu*shgradgu(bb,1)*nor(2)      &
              - gmul*shgradgu(aa,2)*nor(1)*mu*shlu(bb) &
              + shlu(aa)*nor(1)*munor*nor(2)*shlu(bb))*DetJb*gwt

      xKebe(5,aa,bb) = xKebe(5,aa,bb) +  &
        fact2*(-shlu(aa)*mu*shgradgu(bb,2)*nor(1)      &
              - gmul*shgradgu(aa,1)*nor(2)*mu*shlu(bb) &
              + shlu(aa)*nor(2)*munor*nor(1)*shlu(bb))*DetJb*gwt
       
      xKebe(3,aa,bb) = xKebe(3,aa,bb) +  &
        fact2*(-shlu(aa)*mu*shgradgu(bb,1)*nor(3)      &
              - gmul*shgradgu(aa,3)*nor(1)*mu*shlu(bb) &
              + shlu(aa)*nor(1)*munor*nor(3)*shlu(bb))*DetJb*gwt

      xKebe(9,aa,bb) = xKebe(9,aa,bb) +  &
        fact2*(-shlu(aa)*mu*shgradgu(bb,3)*nor(1)      &
              - gmul*shgradgu(aa,1)*nor(3)*mu*shlu(bb) &
              + shlu(aa)*nor(3)*munor*nor(1)*shlu(bb))*DetJb*gwt

      xKebe(7,aa,bb) = xKebe(7,aa,bb) +  &
        fact2*(-shlu(aa)*mu*shgradgu(bb,2)*nor(3)      &
              - gmul*shgradgu(aa,3)*nor(2)*mu*shlu(bb) &
              + shlu(aa)*nor(2)*munor*nor(3)*shlu(bb))*DetJb*gwt

      xKebe(10,aa,bb) = xKebe(10,aa,bb) + &
        fact2*(-shlu(aa)*mu*shgradgu(bb,3)*nor(2)      &
              - gmul*shgradgu(aa,2)*nor(3)*mu*shlu(bb) &
              + shlu(aa)*nor(3)*munor*nor(2)*shlu(bb))*DetJb*gwt

    end do    
  end do
  
  do bb = 1, NSHL   
    do aa = 1, NSHL
      xKebe(13,aa,bb) = xKebe(13,aa,bb) - &
                       fact2*shlu(aa)*shlu(bb)*nor(1)*DetJb*gwt
      xKebe(14,aa,bb) = xKebe(14,aa,bb) - &
                       fact2*shlu(aa)*shlu(bb)*nor(2)*DetJb*gwt
      xKebe(15,aa,bb) = xKebe(15,aa,bb) - &
                       fact2*shlu(aa)*shlu(bb)*nor(3)*DetJb*gwt
    end do
  end do 


  do bb = 1, NSHL
    do aa = 1, NSHL    
      xKebe(4,aa,bb) = xKebe(4,aa,bb) +  & 
                       fact2*shlu(aa)*shlu(bb)*nor(1)*DetJb*gwt
      xKebe(8,aa,bb) = xKebe(8,aa,bb) +  &
                       fact2*shlu(aa)*shlu(bb)*nor(2)*DetJb*gwt
      xKebe(12,aa,bb) = xKebe(12,aa,bb) +  &
                       fact2*shlu(aa)*shlu(bb)*nor(3)*DetJb*gwt    
    end do
  end do

end subroutine e3bLHS_weak_im

!======================================================================
! RHS for IMMERSED weak BC
!======================================================================
subroutine e3bRHS_weak_im(nsd, nshl, nor, DetJb, gwt, mu, rho, tauB, tauNor, &
                       shlu, shgradgu, ui, umi, pri, duidxi, gi, &
                       Rhs, ti, tmp1, penaltyTraction, isShell)  
  implicit none
  
  integer, intent(in) :: nsd, nshl

  real(8), intent(inout) :: Rhs(NSD+1,NSHL)
  real(8), intent(in)    :: shlu(NSHL), shgradgu(NSHL,NSD), &
                            nor(NSD), tauB, tauNor, gwt, &
                            ui(NSD), umi(NSD), pri, duidxi(NSD,NSD), &
                            gi(NSD), DetJb, mu, rho
  real(8), intent(out)   :: ti(NSD), tmp1(NSD), penaltyTraction(NSD)

  integer :: aa, bb, i  
  real(8) :: fact1, fact2, upos, uneg, unor, tr, pt33, gmul, gnor, &
             tmp2(NSD,NSD), utan(NSD)

  logical :: isShell
  
  tmp1 = 0.0d0
  tmp2 = 0.0d0
  
  ti(:) = -pri*nor(:) + mu*(duidxi(:,1)*nor(1) + &
                            duidxi(:,2)*nor(2) + &
                            duidxi(:,3)*nor(3) + &
                            duidxi(1,:)*nor(1) + &
                            duidxi(2,:)*nor(2) + &
                            duidxi(3,:)*nor(3))
  
  ! Relative normal velocity for convective term
  unor = sum((ui-umi)*nor)  ! u \cdot n
  upos = 0.5d0*(unor+abs(unor))
  uneg = 0.5d0*(unor-abs(unor))

  ! Absolute normal for the rest
  unor = sum((ui-gi)*nor)  ! u \cdot n
 
  ! isolate velocity difference terms in traction
  penaltyTraction(:) = tauB*(ui(:)-gi(:)) + (tauNor-tauB)*unor*nor(:) &
                     - uneg*rho*(ui(:)-gi(:))

  ! Conservative definition of traction
  tmp1(:) = penaltyTraction(:)
!  if(.not. isShell) then
     tmp1(:) = tmp1(:) - ti(:) !- uneg*rho*(ui(:)-gi(:))
!  endif

!  tmp1(:) = - ti(:)                      &
!            - uneg*rho*(ui(:)-gi(:))     &
!            + penaltyTraction(:)

  ! gmul =  1.0d0 => sym
  ! gmul = -1.0d0 => skew
  gmul = 1.0d0

!  if(.not. isShell) then
     do aa = 1, NSD
        do bb = 1, NSD
           tmp2(aa,bb) = -gmul*mu*((ui(aa)-gi(aa))*nor(bb) &
                                  +(ui(bb)-gi(bb))*nor(aa))
        end do
     end do
!  endif

  ! gnor = 0.0d0
  do aa = 1, NSHL
    do i = 1, NSD
      Rhs(i,aa) = Rhs(i,aa) - &
                   (shlu(aa)*tmp1(i) + &
                    sum(shgradgu(aa,:)*tmp2(i,:)))*DetJb*gwt
    end do

    Rhs(4,aa) = Rhs(4,aa) + gmul*shlu(aa)*unor*DetJb*gwt     
  end do

  tmp1 = -tmp1 

end subroutine e3bRHS_weak_im

