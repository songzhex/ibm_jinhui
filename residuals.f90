!======================================================================
! RHS for DG/Non-matching
! for w1t1, fact=1.0; for w2t2, fact=-1.0
! The version copied from vertical multiple wind turbines
!======================================================================
subroutine e3bRHS_DG(NSD, nshl,shgu, shgradgu, gwt, ui, ti, nor, tauB, &
                     Rhsu, Rhsp, rho, mu, Detjb,sgn)

  use params
  implicit none
  integer, intent(in)    :: NSD, nshl 
  real(8), intent(inout) :: Rhsu(NSD,NSHL), Rhsp(NSHL)
  real(8), intent(in)    :: shgu(NSHL), shgradgu(NSHL,NSD), gwt, &
                            ui(NSD), ti(NSD),&
                            nor(NSD), tauB, sgn
  real(8), intent(in)    :: rho, mu, Detjb
  integer :: aa, bb, i
  real(8) :: tmp1(NSD), tmp2(NSD,NSD), upos, uneg, unor, gmul
  
  tmp1 = 0.0d0
  tmp2 = 0.0d0
  unor = sum(ui*nor)
  tmp1(:) = -0.5d0*ti(:) + tauB*ui(:)

  ! viscous contribution (in weighting functions)
  do aa = 1, NSD
    do bb = 1, NSD
      tmp2(aa,bb) = -0.5d0*mu*(ui(aa)*nor(bb)+ui(bb)*nor(aa))
    end do
  end do

  do aa = 1, NSHL
    do i = 1, NSD
      Rhsu(i,aa) = Rhsu(i,aa) - sgn*&
                 ( shgu(aa)*tmp1(i) + &
                   sum(shgradgu(aa,:)*tmp2(i,:)) )*DetJb*gwt
    end do
    Rhsp(aa) = Rhsp(aa) + sgn*0.5d0*shgu(aa)*unor*DetJb*gwt
  enddo
end subroutine e3bRHS_DG


!======================================================================
! RHS for DG/Non-matching
! for w1t1, fact=1.0; for w2t2, fact=-1.0
! The version copied from vertical multiple wind turbines
!======================================================================
subroutine e3bRHS_DG_LS(NSD, nshl,shgu, shgradgu, gwt, phi, ui, & 
                        nor, tauC, Rhsls, rho, mu, Detjb,sgn)

      

  use params
  implicit none
  integer, intent(in)    :: NSD, nshl 
  real(8), intent(inout) :: Rhsls(NSHL)
  real(8), intent(in)    :: shgu(NSHL), shgradgu(NSHL,NSD), gwt, phi, &
                            ui(NSD), &
                            nor(NSD), tauC, sgn
  real(8), intent(in)    :: rho, mu, Detjb
  integer :: aa, bb, i
  real(8) :: tmp1(NSD), tmp2(NSD,NSD), upos, uneg, unor, gmul
  
!  tmp1 = 0.0d0
!  tmp2 = 0.0d0
!  unor = sum(ui*nor)
!  tmp1(:) = -0.5d0*ti(:) + tauB*ui(:) 
!  viscous contribution (in weighting functions)

  do aa = 1, NSHL
    Rhsls(aa) = Rhsls(aa) - sgn*shgu(aa)*tauC*phi*DetJb*gwt
  enddo

end subroutine e3bRHS_DG_LS

!======================================================================
! RHS for DG/Non-matching
! for w1t1, fact=1.0; for w2t2, fact=-1.0
! The version copied from vertical multiple wind turbines
!======================================================================
subroutine e3bRHS_DG_LS_RD(NSD, nshl,shgu, shgradgu, gwt, phi,  & 
                        nor, tauC, eRlsrd, Detjb, sgn)

      

  use params
  implicit none
  integer, intent(in)    :: NSD, nshl 
  real(8), intent(inout) :: eRlsrd(NSHL)
  real(8), intent(in)    :: shgu(NSHL), shgradgu(NSHL,NSD), gwt, phi, nor(NSD), tauC, sgn
  real(8), intent(in)    :: Detjb
  integer :: aa, bb, i
  do aa = 1, NSHL
    eRlsrd(aa) = eRlsrd(aa) - sgn*shgu(aa)*tauC*phi*DetJb*gwt
  enddo

end subroutine e3bRHS_DG_LS_RD



!======================================================================
! RHS for DG/Non-matching   : The version in the freesurface, not used before
!======================================================================
subroutine e3bRHS_DG1(nshl,shgu, shgradgu, rho, mu, DetJb,gwt, ui, umi, ti, nor, tauB, &
                     Rhsu, Rhsp)  
  !use aAdjKeep  

  use params
  implicit none
  
  integer, intent(in) :: nshl
  integer, parameter  :: NSD = 3
  real(8), intent(inout) :: Rhsu(NSD,NSHL), Rhsp(NSHL)

  real(8), intent(in) :: shgu(NSHL), shgradgu(NSHL,NSD), gwt, &
                         ui(NSD), umi(NSD), ti(NSD),&
                         nor(NSD), tauB,rho, mu, DetJb

  integer :: aa, bb, i
  real(8) :: tmp1(NSD), tmp2(NSD,NSD), upos, uneg, unor, gmul
  
  tmp1 = 0.0d0
  tmp2 = 0.0d0

  ! Relative normal velocity for convective term
  unor = sum((ui-umi)*nor)
  upos = 0.5d0*(unor+abs(unor))
  uneg = 0.5d0*(unor-abs(unor))  

  unor = sum(ui*nor)
 
  tmp1(:) = -0.5d0*ti(:) - uneg*rho*ui(:) + tauB*ui(:)     

  do aa = 1, NSD
    do bb = 1, NSD
      tmp2(aa,bb) = -0.5d0*mu*(ui(aa)*nor(bb)+ui(bb)*nor(aa))
    end do
  end do

  do aa = 1, NSHL
    do i = 1, NSD
      Rhsu(i,aa) = Rhsu(i,aa) - &
                 ( shgu(aa)*tmp1(i) + &
                   sum(shgradgu(aa,:)*tmp2(i,:)) )*DetJb*gwt
    end do

    Rhsp(aa) = Rhsp(aa) + 0.5d0*shgu(aa)*unor*DetJb*gwt
     
  end do

end subroutine e3bRHS_DG1


!======================================================================
!
!======================================================================
subroutine e3Rhs_3D_fluid(nshl, ui, aci, umi, acmi, uadvi, pri, &
                          rLi, fi, duidxi, ddidxi, tauM, tauP,  &
                          tauC, tauBar, kap_dc, rho, mu, DetJ,gwt, shgu,      &
                          shgradgu, Rhsu, Rhsp)
  !use aAdjKeep
  use params
  implicit none
  
  integer, intent(in) :: nshl        
  integer, parameter :: NSD = 3
  real(8), intent(in) :: ui(NSD), aci(NSD), umi(NSD), acmi(NSD),  &
                         uadvi(NSD), pri, rLi(NSD), fi(NSD),      &
                         duidxi(NSD,NSD), ddidxi(NSD,NSD), rho, mu, DetJ,gwt,   &
                         shgu(NSHL), shgradgu(NSHL,NSD), kap_dc,  &
                         tauM, tauP, tauC, tauBar

  real(8), intent(inout) :: Rhsu(NSD,NSHL), Rhsp(NSHL)

  integer :: aa, bb, i, j, k
  real(8) :: fact1, fact2, mupkdc, divu, ptot, advu(NSD), &
             tmp1(NSD), tmp2(NSD,NSD), tmp4(NSD),uprime(NSD)

  tmp1 = 0.0d0; tmp2 = 0.0d0; tmp4 = 0.0d0; divu = 0.0d0

  uprime(:) = tauM*rLi(:)
      
  mupkdc = mu + kap_dc

  divu = duidxi(1,1) + duidxi(2,2) + duidxi(3,3)

  advu(:) = uadvi(:) - uprime(:)/rho
          
  ptot = pri - tauC*divu

  tmp1(:) = rho*(aci(:) + advu(1)*duidxi(:,1) &
                        + advu(2)*duidxi(:,2) &
                        + advu(3)*duidxi(:,3)) - fi(:)
          
  tmp2(1,1) = -ptot + 2.0d0*mupkdc*duidxi(1,1)   &
            + (uadvi(1)-Fineuse0*uprime(1)/rho)*uprime(1)
      
  tmp2(1,2) = mupkdc*(duidxi(1,2) + duidxi(2,1)) &
            + (uadvi(2)-Fineuse0*uprime(2)/rho)*uprime(1)
      
  tmp2(1,3) = mupkdc*(duidxi(1,3) + duidxi(3,1)) &
            + (uadvi(3)-Fineuse0*uprime(3)/rho)*uprime(1)
      
  tmp2(2,1) = mupkdc*(duidxi(2,1) + duidxi(1,2)) &
            + (uadvi(1)-Fineuse0*uprime(1)/rho)*uprime(2)
      
  tmp2(2,2) = -ptot + 2.0d0*mupkdc*duidxi(2,2)   &
            + (uadvi(2)-Fineuse0*uprime(2)/rho)*uprime(2)
      
  tmp2(2,3) = mupkdc*(duidxi(2,3) + duidxi(3,2)) &
            + (uadvi(3)-Fineuse0*uprime(3)/rho)*uprime(2)
      
  tmp2(3,1) = mupkdc*(duidxi(3,1) + duidxi(1,3)) &
            + (uadvi(1)-Fineuse0*uprime(1)/rho)*uprime(3)
     
  tmp2(3,2) = mupkdc*(duidxi(3,2) + duidxi(2,3)) &
            + (uadvi(2)-Fineuse0*uprime(2)/rho)*uprime(3)
      
  tmp2(3,3) = -ptot + 2.0d0*mupkdc*duidxi(3,3)   &
            + (uadvi(3)-Fineuse0*uprime(3)/rho)*uprime(3)
      
  ! Physics Residual   
  do aa = 1, NSHL     
    do i = 1, NSD
      Rhsu(i,aa) = Rhsu(i,aa) - &
                   ( shgu(aa)*tmp1(i) + &
                     sum(shgradgu(aa,:)*tmp2(i,:)) )*DetJ*gwt
    end do
  end do
      
  do aa = 1, NSHL
    Rhsp(aa) = Rhsp(aa) - &
               ( shgu(aa)*divu + &
                 sum(shgradgu(aa,:)*uprime(:)/rho) )*DetJ*gwt
  end do

end subroutine e3Rhs_3D_fluid
!======================================================================
!
!======================================================================
subroutine e3Rhs_3D_fluid_ido(nshl, ui, aci, umi, acmi, uadvi, pri, &
                          rLi, fi, duidxi, ddidxi, tauM, tauP,  &
                          tauC, tauBar, kap_dc, rho, mu, DetJ,gwt, shgu,      &
                          shgradgu, Rhsu, Rhsp)
  !use aAdjKeep
  use params
  implicit none
  
  integer, intent(in) :: nshl        
  integer, parameter :: NSD = 3
  real(8), intent(in) :: ui(NSD), aci(NSD), umi(NSD), acmi(NSD),  &
                         uadvi(NSD), pri, rLi(NSD), fi(NSD),      &
                         duidxi(NSD,NSD), ddidxi(NSD,NSD), rho, mu, DetJ,gwt,   &
                         shgu(NSHL), shgradgu(NSHL,NSD), kap_dc,  &
                         tauM, tauP, tauC, tauBar

  real(8), intent(inout) :: Rhsu(NSD,NSHL), Rhsp(NSHL)

  integer :: aa, bb, i, j, k
  real(8) :: fact1, fact2, mupkdc, divu, ptot, advu(NSD), &
             tmp1(NSD), tmp2(NSD,NSD), tmp4(NSD),uprime(NSD)

  tmp1 = 0.0d0; tmp2 = 0.0d0; tmp4 = 0.0d0; divu = 0.0d0

  uprime = -tauM*rLi(:)
      
  mupkdc = mu + DCuse0*kap_dc

  divu = duidxi(1,1) + duidxi(2,2) + duidxi(3,3)

  advu(:) = uadvi(:) + uprime(:)
          
  ptot = pri - tauC*divu

  tmp1(:) = rho*(aci(:) + advu(1)*duidxi(:,1) &
                        + advu(2)*duidxi(:,2) &
                        + advu(3)*duidxi(:,3)) - fi(:)
            
  tmp4(:) = uprime(1)*duidxi(:,1) + &
            uprime(2)*duidxi(:,2) + &
            uprime(3)*duidxi(:,3)
          
  tmp2(1,1) = -ptot + 2.0d0*mupkdc*duidxi(1,1)   &
            - rho*(uadvi(1)+Fineuse0*uprime(1))*uprime(1) &
            + rho*uprime(1)*tauBar*tmp4(1)
      
  tmp2(1,2) = mupkdc*(duidxi(1,2) + duidxi(2,1)) &
            - rho*(uadvi(2)+Fineuse0*uprime(2))*uprime(1) &
            + rho*uprime(2)*tauBar*tmp4(1)
      
  tmp2(1,3) = mupkdc*(duidxi(1,3) + duidxi(3,1)) &
            - rho*(uadvi(3)+Fineuse0*uprime(3))*uprime(1) &
            + rho*uprime(3)*tauBar*tmp4(1)
      
  tmp2(2,1) = mupkdc*(duidxi(2,1) + duidxi(1,2)) &
            - rho*(uadvi(1)+Fineuse0*uprime(1))*uprime(2) &
            + rho*uprime(1)*tauBar*tmp4(2) 
      
  tmp2(2,2) = -ptot + 2.0d0*mupkdc*duidxi(2,2)   &
            - rho*(uadvi(2)+Fineuse0*uprime(2))*uprime(2) &
            + rho*uprime(2)*tauBar*tmp4(2)
      
  tmp2(2,3) = mupkdc*(duidxi(2,3) + duidxi(3,2)) &
            - rho*(uadvi(3)+Fineuse0*uprime(3))*uprime(2) &
            + rho*uprime(3)*tauBar*tmp4(2)
      
  tmp2(3,1) = mupkdc*(duidxi(3,1) + duidxi(1,3)) &
            - rho*(uadvi(1)+Fineuse0*uprime(1))*uprime(3) &
            + rho*uprime(1)*tauBar*tmp4(3)
     
  tmp2(3,2) = mupkdc*(duidxi(3,2) + duidxi(2,3)) &
            - rho*(uadvi(2)+Fineuse0*uprime(2))*uprime(3) &
            + rho*uprime(2)*tauBar*tmp4(3)
      
  tmp2(3,3) = -ptot + 2.0d0*mupkdc*duidxi(3,3)   &
            - rho*(uadvi(3)+Fineuse0*uprime(3))*uprime(3) &
            + rho*uprime(3)*tauBar*tmp4(3)
      
  ! Physics Residual   
  do aa = 1, NSHL     
    do i = 1, NSD
      Rhsu(i,aa) = Rhsu(i,aa) - &
                   ( shgu(aa)*tmp1(i) + &
                     sum(shgradgu(aa,:)*tmp2(i,:)) )*DetJ*gwt
    end do
  end do
      
  do aa = 1, NSHL
    Rhsp(aa) = Rhsp(aa) - &
               ( shgu(aa)*divu - &
                 sum(shgradgu(aa,:)*uprime(:)) )*DetJ*gwt
  end do

!!!  do aa = 1, NSHL
!!!    Rhsp(aa) = Rhsp(aa) - &
!!!               ( shgu(aa)*divu + &
!!!                 sum(shgradgu(aa,:)*tauP*rLi(:)) )*DetJ*gwt
!!!  end do

end subroutine e3Rhs_3D_fluid_ido


!======================================================================
!
!======================================================================
subroutine e3Rhs_3D_struct(nshl, aci, ui, fi, Ftens, Stens,rho,DetJ, gwt, &
                           shlu, shgradgu, Rhsu, dc)
  
  !use aAdjKeep
  use params

  implicit none
  
  integer, intent(in) :: nshl
  integer, parameter :: NSD = 3
  integer :: aa, i, j
  
  real(8) :: rho,DetJ,gwt, shlu(NSHL), shgradgu(NSHL,NSD), &
             fact1, aci(NSD), ui(NSD), fi(NSD), &
             Rhsu(NSD,NSHL), Ftens(NSD,NSD), Stens(NSD,NSD), dc
  
  real(8) :: tmp1(NSD), tmp2(NSD,NSD)
  
  tmp1 = 0d+0
  tmp2 = 0d+0
  
!  dC = 5.0d+4
!  dc = 6.0d+5
!  dC = 0.0d0

  tmp1(:) = rho*aci(:) + dC*ui(:) - fi(:)
  
  ! First P-K Stress  
  do j = 1, NSD
     do i = 1, NSD
       tmp2(i,j) = Ftens(i,1)*Stens(1,j) &
                 + Ftens(i,2)*Stens(2,j) &
                 + Ftens(i,3)*Stens(3,j) 
!                + FPKS0(i,j)
     end do
  end do
  
  ! Discrete Residual  
  do aa = 1, NSHL
     
     Rhsu(1,aa) = Rhsu(1,aa) - &
         (shlu(aa)*tmp1(1) + &
         shgradgu(aa,1)*tmp2(1,1) + &
         shgradgu(aa,2)*tmp2(1,2) + &
         shgradgu(aa,3)*tmp2(1,3))*DetJ*gwt
     
     Rhsu(2,aa) = Rhsu(2,aa) - &
         (shlu(aa)*tmp1(2) + &
         shgradgu(aa,1)*tmp2(2,1) + &
         shgradgu(aa,2)*tmp2(2,2) + &
         shgradgu(aa,3)*tmp2(2,3))*DetJ*gwt
     
     Rhsu(3,aa) = Rhsu(3,aa) - &
         (shlu(aa)*tmp1(3) + &
         shgradgu(aa,1)*tmp2(3,1) + &
         shgradgu(aa,2)*tmp2(3,2) + &
         shgradgu(aa,3)*tmp2(3,3))*DetJ*gwt
     
  end do

end subroutine e3Rhs_3D_struct



!======================================================================
!
!======================================================================
subroutine e3Rhs_3D_mesh(nshl,emod, ddidxi, gwt, shlu, shgradgu, Rhsm)
  
  !use aAdjKeep
  use params
  implicit none
  
  integer, intent(in) :: nshl
  integer, parameter :: NSD = 3  
  integer :: aa, bb, i, j, k

  real(8) :: shlu(NSHL), shgradgu(NSHL,NSD)
  real(8) :: ui(NSD), aci(NSD), umi(NSD), acmi(NSD), uadvi(NSD),&
             pri, rLi(NSD), fi(NSD), duidxi(NSD,NSD), ddidxi(NSD,NSD),&
             uprime(NSD), advu(NSD), strn((NSD+1)*NSD/2)
  real(8) :: tauM, tauP, tauC, tauBar, gwt, gwt0
  real(8) :: Rhsm(NSD,NSHL)
  real(8) :: tmp2(NSD,NSD), tmp3(NSD,NSD), &
             divu, tmp4(NSD), mbulk, mshear, vval, numod, Emod
    
  ! Poisson Ratio and Young's modulus
  ! for mesh motion
  numod = 3d-1
  
  ! Mesh "elastic" parameters  
  mbulk  = numod*Emod/((1d+0+numod)*(1d+0-2d+0*numod))
  mshear = Emod/(2d+0*(1d+0+numod)) ! PDE
  
  strn(1) = ddidxi(1,1)
  strn(2) = ddidxi(2,2)
  strn(3) = ddidxi(3,3)
  
  strn(4) = ddidxi(1,2) + ddidxi(2,1)
  strn(5) = ddidxi(2,3) + ddidxi(3,2)
  strn(6) = ddidxi(1,3) + ddidxi(3,1)
    
  
  tmp2(1,1) = (mbulk+2d+0*mshear)*strn(1) + &
               mbulk*strn(2) + mbulk*strn(3)
  
  tmp2(1,2) = mshear*strn(4)
  
  tmp2(1,3) = mshear*strn(6)
  
  tmp2(2,1) = tmp2(1,2)
  
  tmp2(2,2) = (mbulk+2d+0*mshear)*strn(2) + &
               mbulk*strn(1) + mbulk*strn(3)
  
  tmp2(2,3) = mshear*strn(5)
  
  tmp2(3,1) = tmp2(1,3)
  
  tmp2(3,2) = tmp2(2,3)
  
  tmp2(3,3) = (mbulk+2d+0*mshear)*strn(3) + &
               mbulk*strn(1) + mbulk*strn(2)  
    
  do aa = 1, NSHL
    
    Rhsm(1,aa) = Rhsm(1,aa) - &
         (shgradgu(aa,1)*tmp2(1,1) + &
          shgradgu(aa,2)*tmp2(1,2) + &
          shgradgu(aa,3)*tmp2(1,3))*gwt
    
    Rhsm(2,aa) = Rhsm(2,aa) - &
         (shgradgu(aa,1)*tmp2(2,1) + &
          shgradgu(aa,2)*tmp2(2,2) + &
          shgradgu(aa,3)*tmp2(2,3))*gwt
     
    Rhsm(3,aa) = Rhsm(3,aa) - &
         (shgradgu(aa,1)*tmp2(3,1) + &
          shgradgu(aa,2)*tmp2(3,2) + &
          shgradgu(aa,3)*tmp2(3,3))*gwt     
  end do
    
end subroutine e3Rhs_3D_mesh



!======================================================================
! RHS for weak BC
!======================================================================
subroutine wbcStrss(tmp1, tmp2, rho, mu, nor, tauB, tauNor, &
                    Rhsu, Rhsp, nshl, shlu, shgradgu, Detjb, gwt, ti, &
                    pri, ui, umi, duidxi, gi)
! Jinhui: The arguments Rhsu, Rhsp, nshl are added by Jinhui
  use params
  implicit none  
  integer, parameter   :: NSD = 3
  integer, intent(in) ::  nshl
  real(8), intent(in) ::  Detjb, gwt, shlu(NSHL),shgradgu(NSHL,NSD)
  real(8), intent(inout) :: Rhsu(NSD,NSHL), Rhsp(NSHL)
  real(8), intent(out) :: tmp1(NSD), tmp2(NSD,NSD), ti(NSD)
  real(8), intent(in)  :: rho, mu, nor(NSD), tauB, tauNor,&
                          pri, ui(NSD), umi(NSD), duidxi(NSD,NSD), gi(NSD)
    
  real(8) :: gnor, unor, upos, uneg, gmul
  integer :: aa, bb,i
    
  ! Relative normal velocity for convective term
  unor = sum((ui-umi)*nor)  ! u \cdot n
  upos = 0.5d0*(unor+abs(unor))
  uneg = 0.5d0*(unor-abs(unor)) 

  ti(:) = -pri*nor(:) + mu*(duidxi(:,1)*nor(1) + &
                            duidxi(:,2)*nor(2) + &
                            duidxi(:,3)*nor(3) + &
                            duidxi(1,:)*nor(1) + &
                            duidxi(2,:)*nor(2) + &
                            duidxi(3,:)*nor(3)) ! Jinhui: copied from FSI code, in order to compute  
 
  tmp1  =  pri*nor &
         - mu*(duidxi(:,1)*nor(1) + &
               duidxi(:,2)*nor(2) + &
               duidxi(:,3)*nor(3) + &
               duidxi(1,:)*nor(1) + &
               duidxi(2,:)*nor(2) + &
               duidxi(3,:)*nor(3)) &         
           - uneg*rho*(ui-gi) &
          + tauB*(ui-gi)    
  
  ! gmul =  1.0d0 => sym
  ! gmul = -1.0d0 => skew
       
  gmul = 1.0d0 

  do aa = 1, NSD
    do bb = 1, NSD
      tmp2(aa,bb) = -gmul*mu*((ui(aa)-gi(aa))*nor(bb) &
                             +(ui(bb)-gi(bb))*nor(aa))
    end do
  end do

  ! gnor = 0.0d0
  do aa = 1, NSHL
    do i = 1, NSD
      Rhsu(i,aa) = Rhsu(i,aa) - &
                 ( shlu(aa)*tmp1(i) + &
                   sum(shgradgu(aa,:)*tmp2(i,:)) )*DetJb*gwt
    end do
    Rhsp(aa) = Rhsp(aa) + shlu(aa)*(unor-gnor)*DetJb*gwt     
  end do

end subroutine wbcStrss


!======================================================================
!
!======================================================================
subroutine e3bRHS_3D_force(nshl, nor, tauB, tauNor, rho, mu, DetJb,gwt, shlu, shgradgu, &
                           ui, umi, pri, duidxi, gi, xi, efor, emom)
  
  !use aAdjKeep  
  use params  
  
  integer, intent(in) :: nshl
  integer, parameter  :: NSD = 3
  real(8), intent(inout) :: efor(NSD), emom(NSD)

  real(8), intent(in) :: shlu(NSHL), shgradgu(NSHL,NSD), &
                         nor(NSD), tauB, tauNor, rho, mu, DetJb,gwt, &
                         ui(NSD), umi(NSD), pri, duidxi(NSD,NSD), &
                         gi(NSD), xi(NSD)

  integer :: aa, bb  
  real(8) :: fact1, fact2, tmp1(NSD), tmp2(NSD,NSD), &
             upos, uneg, unor, tr, pt33, gmul, ti(NSD)
  
  tmp1 = 0d0
  tmp2 = 0d0  
     
  ti(:) = -pri*nor(:) + mu*(duidxi(:,1)*nor(1) + &
                            duidxi(:,2)*nor(2) + &
                            duidxi(:,3)*nor(3) + &
                            duidxi(1,:)*nor(1) + &
                            duidxi(2,:)*nor(2) + &
                            duidxi(3,:)*nor(3))
  
  unor = sum((ui-umi)*nor(:))  ! u \cdot n
  upos = 0.5d0*(unor+abs(unor))
  uneg = 0.5d0*(unor-abs(unor))  
 
  tmp1(:) = - ti(:) &
            + tauB*(ui-gi) &
            - uneg*rho*(ui-gi)    &  
            + (tauNor-tauB)*sum(nor*(ui-gi))*nor

  ! gmul =  1.0d0 => sym
  ! gmul = -1.0d0 => skew       
  gmul = 1.0d0
  do aa = 1, NSD
    do bb = 1, NSD
      tmp2(aa,bb) = -gmul*mu*((ui(aa)-gi(aa))*nor(bb) &
                             +(ui(bb)-gi(bb))*nor(aa))
    end do
  end do
     
  efor(1) = efor(1) + tmp1(1)*DetJb*gwt     
  efor(2) = efor(2) + tmp1(2)*DetJb*gwt     
  efor(3) = efor(3) + tmp1(3)*DetJb*gwt

  emom(1) = emom(1) + (xi(2)*tmp1(3) - xi(3)*tmp1(2) + &
                       tmp2(3,2) - tmp2(2,3))*DetJb*gwt
 
  emom(2) = emom(2) + (xi(3)*tmp1(1) - xi(1)*tmp1(3) + &
                       tmp2(1,3) - tmp2(3,1))*DetJb*gwt

  emom(3) = emom(3) + (xi(1)*tmp1(2) - xi(2)*tmp1(1) + &
                       tmp2(2,1) - tmp2(1,2))*DetJb*gwt

end subroutine e3bRHS_3D_force


!======================================================================
!
!======================================================================
subroutine e3tensors_nonlin(NSD,ddidxi,mu,lambda,Ftens,Stens,Ctens)

  implicit none
  
  integer :: i,j,k,l,NSD
  real(8) :: ddidxi(NSD,NSD)
  real(8) :: Ftens(NSD,NSD), Stens(NSD,NSD), Ctens(NSD,NSD,NSD,NSD)
  real(8) :: Itens(NSD,NSD), Jdet, Finv(NSD,NSD), tmp, &
             Ct(NSD,NSD), CtInv(NSD,NSD), Jmod, trC, pt33, pt5, &
             fact1, fact2, fact3, mu, lambda, Ctemp(NSD,NSD,NSD,NSD)
    
  pt33 = 1.0d0/3.0d0
  pt5  = 0.5d0
  
  Itens = 0.0d0
  Itens(1,1) = 1.0d0
  Itens(2,2) = 1.0d0
  Itens(3,3) = 1.0d0
  
  Ftens = Itens + ddidxi    ! F = I + du/dX
  
  Jdet = 0.0d0
  Finv = 0.0d0
  tmp  = 0.0d0
         
  ! compute the inverse of deformation gradient and Jacobian  
  Finv(1,1) = Ftens(2,2) * Ftens(3,3) &
            - Ftens(3,2) * Ftens(2,3)
  Finv(1,2) = Ftens(3,2) * Ftens(1,3)  &
            - Ftens(1,2) * Ftens(3,3)
  Finv(1,3) = Ftens(1,2) * Ftens(2,3)  &
            - Ftens(1,3) * Ftens(2,2)
  tmp       = 1.0d0 / ( Finv(1,1) * Ftens(1,1) & 
                      + Finv(1,2) * Ftens(2,1)   &
                      + Finv(1,3) * Ftens(3,1) )
  Finv(1,1) = Finv(1,1) * tmp
  Finv(1,2) = Finv(1,2) * tmp
  Finv(1,3) = Finv(1,3) * tmp
  Finv(2,1) = (Ftens(2,3) * Ftens(3,1)  &
             - Ftens(2,1) * Ftens(3,3)) * tmp
  Finv(2,2) = (Ftens(1,1) * Ftens(3,3)  &
             - Ftens(3,1) * Ftens(1,3)) * tmp
  Finv(2,3) = (Ftens(2,1) * Ftens(1,3)  &
             - Ftens(1,1) * Ftens(2,3)) * tmp
  Finv(3,1) = (Ftens(2,1) * Ftens(3,2)  &
             - Ftens(2,2) * Ftens(3,1)) * tmp
  Finv(3,2) = (Ftens(3,1) * Ftens(1,2)  &
             - Ftens(1,1) * Ftens(3,2)) * tmp
  Finv(3,3) = (Ftens(1,1) * Ftens(2,2)  &
             - Ftens(1,2) * Ftens(2,1)) * tmp
  
  Jdet = 1.0d0/tmp       ! J
  Jmod = Jdet**(-2.0d0/3.0d0) ! J^-2/3
!!!  Jmod = Jdet
  
  Ct = 0.0d0
    
  do i = 1, NSD
    do j = 1, NSD
      Ct(i,j) = sum(Ftens(:,i)*Ftens(:,j))
     end do
  end do
  
     
  ! compute the inverse of the F^T F tensor     
  CtInv = 0.0d0
  tmp = 0.0d0
  trC = 0.0d0
  
  CtInv(1,1) = Ct(2,2) * Ct(3,3)  &
             - Ct(3,2) * Ct(2,3)
  CtInv(1,2) = Ct(3,2) * Ct(1,3)  &
             - Ct(1,2) * Ct(3,3)
  CtInv(1,3) = Ct(1,2) * Ct(2,3)  &
             - Ct(1,3) * Ct(2,2)
  tmp      = 1.0d0 / ( CtInv(1,1) * Ct(1,1)  &
                     + CtInv(1,2) * Ct(2,1)  &
                     + CtInv(1,3) * Ct(3,1) )
  CtInv(1,1) = CtInv(1,1) * tmp
  CtInv(1,2) = CtInv(1,2) * tmp
  CtInv(1,3) = CtInv(1,3) * tmp
  CtInv(2,1) = (Ct(2,3) * Ct(3,1) &
              - Ct(2,1) * Ct(3,3)) * tmp
  CtInv(2,2) = (Ct(1,1) * Ct(3,3) &
              - Ct(3,1) * Ct(1,3)) * tmp
  CtInv(2,3) = (Ct(2,1) * Ct(1,3) &
              - Ct(1,1) * Ct(2,3)) * tmp
  CtInv(3,1) = (Ct(2,1) * Ct(3,2) &
              - Ct(2,2) * Ct(3,1)) * tmp
  CtInv(3,2) = (Ct(3,1) * Ct(1,2) &
              - Ct(1,1) * Ct(3,2)) * tmp
  CtInv(3,3) = (Ct(1,1) * Ct(2,2) &
              - Ct(1,2) * Ct(2,1)) * tmp
    
  trC = Ct(1,1)+Ct(2,2)+Ct(3,3) ! Trace of the F^T Ftensor
  
  ! Compute Second P-K tensor
  ! for Neo-Hookean Mat w/ slight compressibility
  
  Stens = 0.0d0
  
  Stens(:,:) = 0.5d0*lambda*(Jdet*Jdet-1.0d0)*CtInv(:,:) +&
                             mu*Jmod*(Itens(:,:) - pt33*trC*CtInv(:,:)) 

  ! Compute Nonlinear Tangent
  ! Stiffness Tensor C_{IJKL}
  
  Ctens = 0.0d0
  
  fact1 = lambda*Jdet*Jdet+2d+0*pt33*pt33*mu*Jmod*trC
  fact2 = 2d+0*pt33*mu*Jmod
  fact3 = 2d+0*pt33*mu*Jmod*trC-lambda*(Jdet*Jdet-1.0d0)
  fact3 = fact3*0.5d0
  
  do i = 1, NSD
    do j = 1, NSD
      do k = 1, NSD
        do l = 1, NSD
          Ctemp(i,j,k,l) = fact1*CtInv(i,j)*CtInv(k,l)-&
                           fact2*(Itens(i,j)*CtInv(k,l)+&
                                  CtInv(i,j)*Itens(k,l))+&
                           fact3*(CtInv(i,k)*CtInv(j,l)+&
                                  CtInv(i,l)*CtInv(j,k))
        end do
      end do
    end do
  end do

  ! Build C_{ijJI} = F_{iK}C_{KIJL}F_{jL}
  do i = 1, NSD
    do j = 1, NSD 
      do k = 1, NSD
        do l = 1, NSD
          Ctens(i,j,l,k) = Ftens(i,1)*Ftens(j,1)*Ctemp(1,k,l,1) + &
                           Ftens(i,1)*Ftens(j,2)*Ctemp(1,k,l,2) + &
                           Ftens(i,1)*Ftens(j,3)*Ctemp(1,k,l,3) + &
                           Ftens(i,2)*Ftens(j,1)*Ctemp(2,k,l,1) + &
                           Ftens(i,2)*Ftens(j,2)*Ctemp(2,k,l,2) + &
                           Ftens(i,2)*Ftens(j,3)*Ctemp(2,k,l,3) + &
                           Ftens(i,3)*Ftens(j,1)*Ctemp(3,k,l,1) + &
                           Ftens(i,3)*Ftens(j,2)*Ctemp(3,k,l,2) + &
                           Ftens(i,3)*Ftens(j,3)*Ctemp(3,k,l,3)
        end do
      end do
    end do
  end do
  
end subroutine e3tensors_nonlin
