!======================================================================
!
!======================================================================
subroutine solveMono(msh,SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU,bblade,sur)
  
  use mpi
  use params 
  use matvec
  use KrylovSolvers
  use innerprod
  use meshData
  use solution
  use boundaryConditions
  use defs_shell
  use types_beam
  use types_structure
  implicit none  
  type(meshStruct), intent(in) :: msh
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  type(mesh_beam), intent(inout)    :: NRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(structure) ,intent(inout) :: STRU
  type(mesh) ,intent(inout) :: bblade(3)
  type(smesh), intent(inout)  :: SUR
    
  integer :: inewt,GMRES_itermax_fgmres

  real(8), save :: MomRes0, ConRes0
  real(8), save :: MomRes_DG0, ConRes_DG0
  real(8), save :: MomResA0, ConResA0, MomResW0, ConResW0
  real(8), save :: LSCRes0, LSRDRes0, LSMCRes0, MeshRes0, MassErr0 
  real(8), save :: STRURes0 ! Jinhui: to get the initial residual of structure
  real(8) :: MomRes, ConRes  
  real(8) :: MomRes_DG, ConRes_DG
  real(8) :: MomResA, ConResA, MomResW, ConResW
  real(8) :: LSCRes, LSRDRes, LSMCRes, MeshRes, MassErr
  real(8) :: STRURes ! Jinhui: to ge the residual of structure
  real(8) :: max_struc
  integer :: imax
          
  integer :: i,j,ii,jj,kk,b,newJac,verbose,inew_NL
  integer :: flag(4)

  real(8) :: sol(msh%NNODE,13)
  real(8) :: sol_mesh(msh%NNODE,3)  
  real(8) :: sol_rm(msh%NNODE,2) 
  real(8) :: sol_rd(msh%NNODE,1)  
  real(8) :: sol_mass(msh%NNODE,1) 
    
!Jinhui: the new solution vector, will be throwed into fgmres

  real(8) :: sol_fluid(msh%NNODE,5) ! Jinhui: 1~4 navier stokes, 5 level set
  real(8) :: sol_struc(SH%NRB%NNODE,3) ! Jinhui: 1~3 structure
!**********************************
  integer::inew_pse,inew_mass

  real(8) :: loc(12), glob(12)
  integer :: converged_nslb, converged_mesh,converged_mass,converged_rd
  
  character(len=80), parameter :: fomt = "(I2,1x,a,1x,ES12.4,1x,ES12.4,1x,F12.5)"  

  converged_nslb = 0
  converged_mesh = 0
  converged_mass = 0
  converged_rd   = 0	
  do inewt = 1, NL_itermax  

    newJac = 1
    if(ismaster) write(*,*) " ========== Assemble new Jacobian for NS, LS and STRU =================="
    flag(1)=1
    flag(2)=0
    flag(3)=0
    flag(4)=0

    call assemble(new, rhsg0, rhsg0_dg, newJac,flag, &
          SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU,bblade, sur)
    rhsg_fluid(:,1:5)=rhsg0(:,1:5)   ! Velocity, Pressure and Level Set

  !  write(*,*) myid,"assembling is done"
    ! Air/Water norms 
    MomResA = 0d0
    ConResA = 0d0  
    MomResW = 0d0
    ConResW = 0d0
    MomRes  = 0d0
    ConRes  = 0d0 
    LSCRes  = 0d0 
   ! LSRDRes = 0d0 
   ! LSMCRes = 0d0
    MeshRes = 0d0
   ! MassErr = 0d0
    STRURes = 0d0
    MomRes_DG =0d0
    ConRes_DG = 0d0

    do i = 1, msh%NNODE
      if (alpha%rhog(i) < rhoT) then
        MomResA = MomResA + rhsg_fluid(i,1)**2 + rhsg_fluid(i,2)**2 + rhsg_fluid(i,3)**2
        ConResA = ConResA + rhsg_fluid(i,4)**2
      else
        MomResW = MomResW + rhsg_fluid(i,1)**2 + rhsg_fluid(i,2)**2 + rhsg_fluid(i,3)**2
        ConResW = ConResW + rhsg_fluid(i,4)**2
      endif
        MomRes_DG =  MomRes_DG + rhsg0_dg(i,1)**2 + rhsg0_dg(i,2)**2 + rhsg0_dg(i,3)**2
        ConRes_DG =  ConRes_DG + rhsg0_dg(i,4)**2
    enddo 
    ! Level set norms   
    LSCRes    = sum(rhsg_fluid(:,5)**2)

    loc     = 0d0
    glob    = 0d0
    loc(1)  = MomResA
    loc(2)  = ConResA
    loc(3)  = MomResW
    loc(4)  = ConResW
    loc(5)  = LSCRes       
    loc(11) =  MomRes_DG
    loc(12) =  ConRes_DG 
    call MPI_ALLREDUCE (loc, glob, 12, MPI_DOUBLE_PRECISION, &
                        MPI_SUM, MPI_COMM_WORLD, mpi_err)
    MomResA = glob(1)
    ConResA = glob(2)
    MomResW = glob(3)
    ConResW = glob(4)
    LSCRes  = glob(5)
    MomRes_DG = glob(11)
    ConRes_DG = glob(12)
                                

    MomRes  = sqrt(MomResA + MomResW)
    ConRes  = sqrt(ConResA + ConResW)
    MomResA = sqrt(MomResA)
    ConResA = sqrt(ConResA)
    MomResW = sqrt(MomResW)
    ConResW = sqrt(ConResW)    
    LSCRes  = sqrt(LSCRes)
    MomRes_DG = sqrt(MomRes_DG)
    ConRes_DG =sqrt(ConRes_DG)
! Jinhui: compute the residual of structure
    if (ismaster) then
     STRURes=sqrt(sum(STRU%RHSG(:,1)**2 + STRU%RHSG(:,2)**2 + &
                             STRU%RHSG(:,3)**2 ))  
       
    endif     
      
    call MPI_BCAST (STRURes, 1, MPI_DOUBLE_PRECISION, &
                    mpi_master, MPI_COMM_WORLD, mpi_err)    

! Jinhui: since we need to check in every processor              

                                    
    ! Get reference norms 
    if (inewt .eq. 1) then
      MomResA0 = max(MomResA, 1d-5)      
      ConResA0 = max(ConResA, 1d-5)    
      MomResW0 = max(MomResW, 1d-5)      
      ConResW0 = max(ConResW, 1d-5)     
      MomRes0  = max(MomRes,  1d-5)      
      ConRes0  = max(ConRes,  1d-5) 
      LSCRes0  = max(LSCRes,  1d-5)       
      STRURes0 = max(STRURes, 1d-5) 
      MomRes_DG0 = max(MomRes_DG, 1d-5)      
      ConRes_DG0 = max( ConRes_DG0, 1d-5)  
    endif
    
    if (LSCRes0  <= 1d-10 ) LSCRes0  = max(LSCRes , 1d-10)
    ! Print residual  
    if (ismaster) then  ! if of if (ismaster) then 

      write(*,*) "~~~~~~~~~~~~~~~~~~~~~~~fluid residual~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"  
      write(*,*) inewt, ") Air.Mom:", MomResA, MomResA0
      write(*,*) inewt, ") Air.Mom:", 1d2*MomResA/MomResA0,1d2*NS_NL_Utol
      write(*,*) inewt, "-------------------------------------------------------------"
      write(*,*) inewt, ") Air.Cont:", ConResA, ConResA0  
      write(*,*) inewt, ") Air.Cont:", 1d2*ConResA/ConResA0,1d2*NS_NL_Ptol  
      write(*,*) inewt, "-------------------------------------------------------------"
      write(*,*) inewt, ") Wat.Mom:", MomResW, MomResW0
      write(*,*) inewt, ") Wat.Mom:", 1d2*MomResW/MomResW0,1d2*NS_NL_Utol
      write(*,*) inewt, "-------------------------------------------------------------"
      write(*,*) inewt, ") Wat.Cont:", ConResW, ConResW0 
      write(*,*) inewt, ") Wat.Cont:", 1d2*ConResW/ConResW0,1d2*NS_NL_Ptol   
      write(*,*) inewt, "-------------------------------------------------------------"
      write(*,*) inewt, ")Total.Mom:",  MomRes, MomRes0
      write(*,*) inewt, ")Total.Mom:",  1d2*MomRes/MomRes0,1d2*NS_NL_Utol
      write(*,*) inewt, "-------------------------------------------------------------"
      write(*,*) inewt, ")Total.Cont:",  ConRes, ConRes0
      write(*,*) inewt, ")Total.Cont:",  1d2*ConRes/ConRes0,1d2*NS_NL_Ptol   
      write(*,*) inewt, "-------------------------------------------------------------"                        
      write(*,*) inewt, ")Conv.Norm:", LSCRes,  LSCRes0
      write(*,*) inewt, ")Conv.Norm:", 1d2*LSCRes/LSCRes0,1d2*LSC_NL_tol 
      write(*,*) inewt, "-------------------------------------------------------------"  

     write(*,*) "~~~~~~~~~~~~~~~~~~~~~~~structural residual~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
     write(*,*) inewt, ")Stru.Norm:", STRURes, STRURes0      
     write(*,*) inewt, ")Stru.Norm:", 1d2*STRURes/STRURes0 ,1d2*STRU_NL_tol
     write(*,*) inewt, "-------------------------------------------------------------"  

!*********************************************************************************
! Jinhui: This part need to be replaced by the new resiual from the shell and beam
!*********************************************************************************                 

    endif
                                       
    call MPI_BARRIER (MPI_COMM_WORLD,mpi_err) 
        
    ! Check convergence and solve linear system
    converged_nslb = 1
    if (((MomResA/MomResA0) >  NS_NL_Utol).or. &
        ((ConResA/ConResA0) >  NS_NL_Ptol).or. &  
        ((MomResW/MomResW0) >  NS_NL_Utol).or. &
        ((ConResW/ConResW0) >  NS_NL_Ptol).or. &
        ((LSCRes /LSCRes0) >  LSC_NL_tol).or. &
        ((STRURes/STRURes0) > STRU_NL_tol)) then          
        converged_nslb = 0
    endif           


!*********************************************************************************
! Jinhui: This part need to be replaced by the new resiual from the shell and beam
!*********************************************************************************  
        
    if(ismaster) write(*,*) myid,"converged flag:",converged_nslb,converged_mesh

    if((converged_mesh.eq.0).or.(converged_nslb.eq.0)) then  !  if (converged_nslb==.false.) then   
    ! Get Precoditioner                              
    call getBDiagpcNS  (msh%NNODE,msh%NSD) 
    if(ismaster) write(*,*) myid, "~~~~~~~~Diag for NS~~~~~~~~"
    call getBDiagpcLS  (msh%NNODE,flag) 
    if(ismaster) write(*,*) myid, "~~~~~~~~Diag for LS~~~~~~~~"
    !call getBDiagpcMesh(msh%NNODE,msh%NSD)  
    !call getBDiagpcBody(msh%NNODE)  ! Jinhui: need to remove this routine
    !call getEBEpc(mesh,SparseMat,4, LHSns,  PCns)
    !call getEBEpc(mesh,SparseMat,1, LHSlsc, PClsc)
    !call getEBEpc(mesh,SparseMat,1, LHSlsrd,PClsrd)
    !call getEBEpc(mesh,SparseMat,1, LHSlsmc,PClsmc)    
    !call getEBEpc(mesh,SparseMat,3, LHSmesh,PCmesh)       
    ! Solve    
    verbose = -1    
    if (ismaster) verbose = 1          
    sol_fluid = 0d0
    sol_struc = 0d0
    sol=0d0
    if(ismaster) write(*,*) myid,"Arrive to the solver"

    if(ismaster.and.move.and.solfsi) then
      ash_base = 0d0
      ush_base = 0d0
      dsh_base = 0d0
      call n2f_l2project(SH%FEM, SH%NRB, 3, SH%NRB%ash(:,:), ash_base) 
      call n2f_l2project(SH%FEM, SH%NRB, 3, SH%NRB%ush(:,:), ush_base)
      call n2f_l2project(SH%FEM, SH%NRB, 3, SH%NRB%dsh(:,:), dsh_base)
    ! write(*,*) "base ash:", sqrt(sum(ash_base(:,1)**2 + &
    !                       ash_base(:,2)**2 + &
    !                       ash_base(:,3)**2 ))
    !  write(*,*) "base ush:", sqrt(sum(ush_base(:,1)**2 + &
    !                       ush_base(:,2)**2 + &
    !                       ush_base(:,3)**2 ))
    !  write(*,*) "base dsh:", sqrt(sum(dsh_base(:,1)**2 + &
    !                       dsh_base(:,2)**2 + &
    !                       dsh_base(:,3)**2 ))
    endif
 !   GMRES_tol = 0.01d0
     GMRES_itermax_fgmres = GMRES_itermax 
     if(inewt>=5) then
      GMRES_itermax_fgmres = 8
     endif
!    GMRES_itermax_fgmres = GMRES_itermax
    if(inewt >=7)  GMRES_itermax_fgmres = 8
    call fgmres(SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, sur, &
               msh%NNODE, 5, sol_fluid, rhsg_fluid, rhsg0_dg, &
               SH%NRB%NNODE, 3, sol_struc, STRU%RHSG, &
               jfmvMono, bsolveMono,std_iprod, &
               GMRES_tol,GMRES_itermin,GMRES_itermax_fgmres,verbose)


!    call fgmres(SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, &
!               msh%NNODE, 5, sol_fluid, rhsg_fluid, rhsg0_dg, &
!               SH%NRB%NNODE, 3, sol_struc, STRU%RHSG, &
!               jfmvMono, bsolveMono,std_iprod, &
!               GMRES_tol,GMRES_itermin,GMRES_itermax,verbose)

    sol(:,1:5)=sol_fluid(:,1:5) ! Jinhui: grab the solution                       
    call commu(sol, 13, 'out')  
    call commu(sol_fluid, 5, 'out')

    ! Update solution 
    flag(1)=1
    flag(2)=0
    flag(3)=0
    flag(4)=0    
    call incrementState(new, sol(:,1:10) , 1d0, flag)  
! Attention, how to upate solution to all processors
   if(move.and.solfsi) then 
        call incrementState_STRU1(SH%NRB%NNODE,sol_struc(:,1:3), 1d0, SH, STRU, NRB_BEA, BEA, mNRB_BEA,bblade)   ! Jinhui: attention: Need to see how to arrange the solution
    endif
   ! write(*,*) myid, "increment solid solution"
   endif ! endif of  if (converged_nslb==.false.) then
!**********************
!The following is to solve the mesh problem 
   verbose = -1   
   if (ismaster) verbose = 0
   flag(1)=0
   flag(2)=1
   flag(3)=0
   flag(4)=0
   sol_mesh=0d0
   sol=0
   if (ismaster) write(*,*)  " ==== Assembling new Jacobian for Mesh ==== "
   call assemble(new, rhsg0, rhsg0_dg, newJac,flag, & 
                 SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU,bblade, sur)  ! Jinhui: add to include the beam and shell in
   rhsg_mesh(:,1:3)=rhsg0(:,7:9)
   MeshRes = 0d0
   MeshRes = sum(rhsg_mesh(:,1)**2) + sum(rhsg_mesh(:,2)**2) + sum(rhsg_mesh(:,3)**2)
    loc  = 0d0    
    glob = 0d0
    loc(8)  = MeshRes    
    call MPI_ALLREDUCE (loc, glob, 12, MPI_DOUBLE_PRECISION, &
                        MPI_SUM, MPI_COMM_WORLD, mpi_err)
    MeshRes = glob(8)                                    
    MeshRes = sqrt(MeshRes)       ! Get reference norms 
    if (inewt .eq. 1) then
      MeshRes0 = max(MeshRes, 1d0) 
    endif
    if (MeshRes0 <= 1d-14 ) MeshRes0 = max(MeshRes, 1d-14)        
    ! Print residual  
    if (ismaster) then
       if (NBODIES /= 0) then
         write(*,*)
         write(*,*) "~~~~~~~~~~~~~~~~~~~~~~~structural residual~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
         write(*,*) inewt, ") Mesh.Norm:", MeshRes,MeshRes0
         write(*,*) inewt, ") Mesh.Norm:",1d2*MeshRes/MeshRes0,1d2*Mesh_NL_tol  
       endif
     endif
                                        
    call MPI_BARRIER (MPI_COMM_WORLD,mpi_err) 
     converged_mesh = 1
    if ((MeshRes/MeshRes0)>Mesh_NL_tol) then
      converged_mesh = 0
    endif 
    if((converged_mesh.eq.0).or.(converged_nslb.eq.0)) then !esle if of AAAAAAAAAAAAAA
      call getBDiagpcMesh(msh%NNODE,msh%NSD)    
    if(ismaster) write(*,*) myid, "~~~~~~~~Diag for MN~~~~~~~~"  
      call cg (msh%NNODE, 3, sol_mesh(:,1:3), rhsg_mesh(:,1:3),  matvecMesh,bdiagpcMesh,std_iprod, &
           PC_Mesh_tol,PC_Mesh_itermin,PC_Mesh_itermax,verbose) 
      sol(:,7:9)=sol_mesh(:,1:3)

    call commu(sol, 2*msh%NSD+5, 'out')
  
   flag(1)=0
   flag(2)=1
   flag(3)=0
   flag(4)=0
    ! Update solution     
   call incrementState(new,sol(:,1:10),1d0,flag) 
    ! Update solution     
       
endif !!!! endif of AAAAAAAAAAAAAA

  if(ismaster) then
     write(*,*) "converged flag:",converged_nslb,converged_mesh
  endif

  if((converged_nslb.eq.1).and.(converged_mesh.eq.1)) then
    exit
  endif    
  if(ismaster) write(*,*) "pse_time_read:", pse_time_read
enddo ! loop end of nonlinear iteration
 ! loop end of nonlinear iteration
 ! loop end of nonlinear iteration
 ! loop end of nonlinear iteration
!************
!The above is to solve the mesh problem 
!new%phig_check=new%phig  ! For checking the gradient of level set
!LSRD_NL_tol=1d-3
verbose = -1    
if (ismaster) verbose = 0
  if(ismaster) then
    write(*,*) "==========================================================================="
    write(*,*) "============redistancing==================================================="
    write(*,*) "==========================================================================="
  endif

pse_time = pse_time_read
new%phidg=new%phig  ! For redistancing the level set function
old_rd=new
new_rd=new
Num_redistancing = 4
do inew_pse=1,Num_redistancing
  if(ismaster) then 
      write(*,*) "============================================================================"
      write(*,*) "============================================================================"
      write(*,*) inew_pse,"th) iterations,", "pse_time = ", pse_time
   endif
   new_rd=old_rd
   flag(1)=0
   flag(2)=0
   flag(3)=1
   flag(4)=0
   do inew_NL=1,1
   call assemble_rd(new_rd,rhsg0, rhsg0_dg, newJac,flag, NM)

    rhsg_rd(:,1:1)=rhsg0(:,6:6)
    LSRDRes   = sum(rhsg_rd(:,1)**2)

    loc     = 0d0
    glob    = 0d0
    loc(6)  = LSRDRes
    call MPI_ALLREDUCE (loc, glob, 12, MPI_DOUBLE_PRECISION, &
                        MPI_SUM, MPI_COMM_WORLD, mpi_err)
    LSRDRes = glob(6)                          
    LSRDRes = sqrt(LSRDRes)
     if ((inew_NL.eq. 1).and.(inew_pse.eq.1))then 
       LSRDRes0 = max(LSRDRes, 1d-10)
     endif
     if (LSRDRes0 <= 1d-10 ) LSRDRes0 = max(LSRDRes, 1d-10) 
     if (ismaster) then
       write(*,*) LSRDRes,LSRDRes0
       write(*,*) inew_pse,inew_NL,"th):",1d2*LSRDRes/LSRDRes0,1d2*LSRD_NL_tol      
     endif                                     

    call MPI_BARRIER (MPI_COMM_WORLD,mpi_err) 
 
     converged_rd = 1
     if ((LSRDRes/LSRDRes0)>LSRD_NL_tol) then
        converged_rd = 0
     endif 
    if(ismaster) then
     ! write(*,*) "converged_rd:",converged_rd
     ! write(*,*) "---------------------------------------"
    endif
      
     if(converged_rd.eq.0) then !   if(converged_rd.eq.0) then
       PC_LSRD_tol=1d-3
       PC_LSRD_itermin=30
!       PC_LSRD_itermax=150 
      call getBDiagpcLS  (msh%NNODE,flag) 
      sol_rd=0d0
      sol=0
      call gmres_RD(NM, msh%NNODE, 1, sol_rd(:,1:1), rhsg0_dg, rhsg_rd(:,1:1), &
               matvecLSRD,diagpcLSRD,std_iprod, &
               PC_LSRD_tol,PC_LSRD_itermin,PC_LSRD_itermax,verbose)

      sol(:,6:6)=sol_rd(:,1:1)

    call commu(sol, 2*msh%NSD+5, 'out')
    !Update solution     
    call incrementState_rd(new_rd, sol(:,6)) 
    endif !  if(converged_rd.eq.0) then
    if(converged_rd.eq.1) then
      exit
    endif
  enddo ! end of NL loop of redistancing
  old_rd=new_rd 
enddo !! endif of the redistancing time loop


 new=new_rd
 new%phig=new%phidg  ! Update the level set
 if(ismaster) then
     write(*,*) "PHID after redistancing:", sum(new%phig**2)
  endif

! The above is to solve redistancing
! The following is to solve mass fix
  if(ismaster) then
    write(*,*) "==========================================================================="
    write(*,*) "============mass fixing===================================================="
    write(*,*) "==========================================================================="
  endif


 do inew_mass=1,0
   flag(1)=0
   flag(2)=0
   flag(3)=0
   flag(4)=1
   if (ismaster) write(*,*)  inew_mass,"th) ==== Assembling new Jacobian for Mass ================== "
   call assemble(new, rhsg0,newJac,flag, & 
                 SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU,bblade, sur) 
   rhsg_mass(:,1:1)=rhsg0(:,10:10)
   if (LSMC_kdc.ge.0d0) then
      LSMCRes = sum(rhsg_mass(:,1)**2)
   else
      LSMCRes = sum(rhsg_mass(:,1))
   endif   
    MassErr = sum(rhsg_mass(:,1))*Delt

    loc     = 0d0
    loc(7)  = LSMCRes      
    loc(9)  = MassErr          
    call MPI_ALLREDUCE (loc, glob, 12, MPI_DOUBLE_PRECISION, &
                        MPI_SUM, MPI_COMM_WORLD, mpi_err)
    LSMCRes = glob(7)      
    MassErr = glob(9)                                 

    LSMCRes = sqrt(LSMCRes)   
 ! Get reference norms 
    if (inew_mass .eq. 1) then 
      LSMCRes0 = max(LSMCRes, 1d-10)
    endif
    if (LSMCRes0 <= 1d-10 ) LSMCRes0 = max(LSMCRes, 1d-10)                            
    ! Print residual  
    if (ismaster) then               
      write(*,*)  inew_mass,") Mass Cons.Norm = ", LSMCRes,LSMCRes0     
      write(*,*)  inew_mass,") Mass Cons.Norm = ", 1d2*LSMCRes/LSMCRes0,1d2*LSMC_NL_tol    
    endif                                       
    call MPI_BARRIER (MPI_COMM_WORLD,mpi_err) 
 

 converged_mass = 1
  if ((LSMCRes/LSMCRes0)>LSMC_NL_tol) then
      converged_mass = 0
  endif 
 if(ismaster) write(*,*) "converged_mass:",converged_mass

 if(converged_mass.eq.0) then   !if(converged_mass.eq.0) 
       PC_LSMC_tol=1d-3
       PC_LSMC_itermin=30
       PC_LSMC_itermax=150 
      sol_mass=0d0
      sol=0
      call getBDiagpcLS  (msh%NNODE,flag) 
      if(ismaster) write(*,*) "sum( dpcLSMC**2):",sum(dpcLSMC**2)
      call gmres(msh%NNODE, 1, sol_mass(:,1:1),  rhsg_mass(:,1:1), &
               matvecLSMC,diagpcLSMC,std_iprod, &
              PC_LSMC_tol,PC_LSMC_itermin,PC_LSMC_itermax,verbose)
      sol(:,10:10)=sol_mass(:,1:1)
    call commu(sol, 2*msh%NSD+5, 'out')   
    ! Update solution     
    call incrementState_mass(new, sol(:,1:10) , 1d0)  
  endif ! endif(converged_mass.eq.0) 

 if(ismaster) write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
 if(converged_mass.eq.1) exit

enddo  ! end of NL loop of mass fixing
   


  ! Report final convergence data
  if (ismaster) then
    write(*,*)
    loc(1) = MomRes/MomRes0
    loc(2) = ConRes/ConRes0
    loc(3) = LSCRes/LSCRes0
!    loc(4) = LSRDRes/LSRDRes0
!    loc(5) = LSMCRes/LSMCRes0
                                                
    loc(4)=STRURes/STRURes0
    loc(5) = MeshRes/MeshRes0  
    write(*,'(60("-"))')
    write(*,*) 
    !write(*,"(5x,a,1x,I2,1x,a,1x,88F10.5)") "Convergence summary:  Iterations:",inewt," Reduction(%):",loc(1:i+1)
    write(*,"(5x,a,1x,I2,1x,88F10.5)") "Convergence1 summary: ",inewt,1d2*loc(1:5)    
    write(*,"(5x,a,1x,I2,1x,88Es10.3)") "Convergence2 summary: ",inewt,loc(1:5)
    write(*,*)                                               
  endif
  !------------------------------------------------ 
  ! Update phi field 
  !  -- to be consistent with Newmark relation
  !------------------------------------------------     
  if(ismaster) then
     write(*,*) "PHID before adding mass fixing:", sum(new%phig**2)
  endif
 
  new%phig  = new%phidg! + new%phicg  
  new%rphig = ( (new%phig - old%phig)*Dtgl +(gami-1.0d0)*old%rphig)/gami

  call MPI_BARRIER (MPI_COMM_WORLD, mpi_err)

end subroutine solveMono
