!======================================================================
! Stabilization parameters (tauM, tauC) for VMS
!====================================================================== 
subroutine e3STAB_3D(Gij, Ginv, ui, res, rho, mu, tauM, tauP, &
                     tauC, tauBar,cfl)   
  use params
  implicit none
  integer, parameter :: NSD = 3      
  real(8), intent(in)  :: Gij(NSD,NSD), Ginv(NSD,NSD), &
                          ui(NSD), res(NSD),rho, mu
  real(8), intent(out) :: tauM, tauC, tauP, tauBar,cfl(2)
  real(8) uprime(NSD)

  real(8) :: taua, taut, taud, gij2, dtfact, adfact, m_k, nu
  integer :: i, j

  dtfact = 4.0d0 
  adfact = 1.0d0           
  m_k    = 12d0
    
  cfl = 0.0d0
  ! get tauM, tauC, cfl(1)
  taua = 0.0d0
  gij2 = 0.0d0
  do j = 1, NSD
    do i = 1, NSD
      taua = taua + ui(i)*Gij(i,j)*ui(j)
      gij2 = gij2 + Gij(i,j)*Gij(i,j)
    end do
  end do  

  cfl(1) = Delt*sqrt(taua)    

  taut = dtfact*Dtgl*Dtgl
  taua = adfact*taua
  nu   = mu/rho
  taud = nu*nu*m_k*gij2

  ! Get tauSUPS 
  tauM = 1.0d0/sqrt(taut + taua + taud)
  tauP = tauM

  ! Get rho*nuLSIC (no Delta t)
  !tauC = 1.0d0*rho*sqrt(taut + taua + taud)*(Ginv(1,1)+Ginv(2,2)+Ginv(3,3)) 
  !tauC = rho/sqrt(gij2)*tauM   
!  tauC = rho*sqrt((taut + taua + taud)/gij2)
  tauC = rho*sqrt(taua + taud)/(Gij(1,1)+Gij(2,2)+Gij(3,3))  
  ! Get TauBar
  tauBar = 0.0d0

end subroutine e3STAB_3D

!======================================================================
! Stabilization parameter (tauB) for DG
!======================================================================
subroutine e3bSTAB_DG(tauB, tauC, ui, nor, dxidx, rho, mu)
  use params
  implicit none

  real(8), intent(in)  :: ui(3), nor(3), dxidx(3,3), rho, mu
  real(8), intent(out) :: tauB, tauC
  
  real(8) :: temp(3), hn, unor, uneg
  integer :: i

  do i = 1, 3
    temp(i) = sum(dxidx(i,:)*nor)
  end do
  hn   = 1.0d0/(sqrt(sum(temp*temp)))
  
  tauB = 16.0d0*mu/hn

  if(ls_sl_penalty>0.5d0) then 
    tauC = 16.0d0*LSC_kdc/hn
  else
    tauC = 0d0
  endif 

  unor = sum(ui*nor(:))

  uneg = 0.5d0*(unor-abs(unor))
  tauB = tauB - rho*uneg
  tauC = tauC - uneg
!!!  tauB = tauB + rho*abs(unor)

end subroutine e3bSTAB_DG

!===========================================================================================================================
! Stabilization parameter (tauB) for Redistancing of DG, the Se*grad(phi)/||grad(phi)|| is used as the convection velocity
!===========================================================================================================================
subroutine e3bSTAB_DG_RD(tauC, ui, nor, dxidx)
  use params
  implicit none

  real(8), intent(in)  :: ui(3), nor(3), dxidx(3,3)
  real(8), intent(out) :: tauC
  
  real(8) :: temp(3), hn, unor, uneg
  integer :: i

  do i = 1, 3
    temp(i) = sum(dxidx(i,:)*nor)
  end do
  hn  = 1.0d0/(sqrt(sum(temp*temp))) 

  if(ls_sl_penalty>0.5d0) then 
    tauC = 16.0d0*LSC_kdc/hn
  else
    tauC = 0d0
  endif 


  unor = sum(ui*nor(:))
  uneg = 0.5d0*(unor-abs(unor))
  tauC = tauC - uneg

end subroutine e3bSTAB_DG_RD

!======================================================================
! Stabilization parameters (tauM, tauC) for VMS
!====================================================================== 
subroutine e3STAB_3D_ido(Gij, Ginv, ui, res, rho, mu, tauM, tauP, &
                     tauC, tauBar,cfl)   
  use params
  implicit none
  integer, parameter :: NSD = 3      
  real(8), intent(in)  :: Gij(NSD,NSD), Ginv(NSD,NSD), &
                          ui(NSD), res(NSD),rho, mu
  real(8), intent(out) :: tauM, tauC, tauP, tauBar,cfl(2)
  real(8) uprime(NSD)

  real(8) :: taua, taut, taud, gij2, dtfact, adfact, m_k, nu
  integer :: i, j

  dtfact = 4.0d0 
  adfact = 1.0d0           
  m_k    = 3.0d0
    
  ! get tauM, tauC, cfl(1)
  taua = 0.0d0
  gij2 = 0.0d0
  do j = 1, NSD
    do i = 1, NSD
      taua = taua + ui(i)*Gij(i,j)*ui(j)
      gij2 = gij2 + Gij(i,j)*Gij(i,j)
    end do
  end do    

  cfl(1) = Delt*sqrt(taua)    

  taut = dtfact*Dtgl*Dtgl
  taua = adfact*taua
  nu   = mu/rho
  taud = nu*nu*m_k*gij2

  ! Get tauM 
  tauM = 1.0d0/(rho*sqrt(taut + taua + taud))
  tauP = tauM

  ! Get tauC
  !tauC = rho*sqrt((taut + taua + taud)/gij2) 
  tauC = rho*sqrt((taua + taud)/gij2) 
      
  ! Get small scale reconstruction
  uprime = -tauM*res

  ! Get TauBar
  tauBar = 0.0d0
  do i = 1, NSD
    do j = 1, NSD
      tauBar = tauBar + uprime(i)*Gij(i,j)*uprime(j)
    end do
  end do
  tauBar = 1.0d0/sqrt(tauBar+1.0d-15)

end subroutine e3STAB_3D_ido

!======================================================================
!
!======================================================================
subroutine e3DC_beta2(ui, duidxi, Gij, Ginv, res, dxidx, tauM, kdc)

  use params
  
  implicit none
  integer, parameter :: NSD = 3  
  real(8), intent(in) :: Gij(NSD,NSD),Ginv(NSD,NSD), tauM
  real(8) :: ui(NSD),duidxi(NSD,NSD),res(NSD),He,dxidx(NSD,NSD) 
  integer :: i,j,k
  real(8) :: tmp1,tmp2,resnorm,kdc,h2, gunrm
  real(8) :: gabsu(NSD), absu

  He  = 1d0!!!(rho - rhoa)/(rhow - rhoa)

  !  Meshsize   
  tmp1 = 0d0
  tmp2 = 0d0
  do j = 1, NSD
    do i = 1, NSD
      tmp1 = tmp1 + Gij(i,j)*Gij(i,j)
      tmp2 = tmp2 + Ginv(i,j)*Ginv(i,j)
    end do
  end do

!  gunrm = 0d0

!  do k = 1, NSD
!     do j = 1, NSD
!        do i = 1, NSD
!           gunrm = gunrm + res(i)*Ginv(k,i)*Ginv(k,j)*res(j)
!        enddo
!     enddo
!  enddo

  ! kdc = sqrt(r^T Ginv^T Ginv r) ~ h^2 r

!  kdc = sqrt(gunrm)

!!$  absu = sqrt(sum(ui*ui))
!!$
!!$  gabsu(1) = sum(ui(:)*duidxi(:,1))
!!$  gabsu(2) = sum(ui(:)*duidxi(:,2))
!!$  gabsu(3) = sum(ui(:)*duidxi(:,3))
!!$  
!!$  gunrm = 0d0
!!$
!!$  do j = 1, NSD
!!$    do i = 1, NSD
!!$       gunrm = gunrm + gabsu(i)*Gij(i,j)*gabsu(j)
!!$    enddo
!!$  enddo
!!$
!!$  !h2 = (tmp2/tmp1)**0.25d0
  h2 = 1d0/sqrt(Gij(1,1)+Gij(2,2)+Gij(3,3)) 
!  h2 = 1d0/sqrt(tmp1)   
!!$
!!$  !h2 = sqrt(Ginv(1,1) + Ginv(2,2) + Ginv(3,3))
!!$
!!$  ! Residual norm
  resnorm = sqrt(sum(res*res))
!!$!  kdc = resnorm*absu/(sqrt(gunrm + 1d-15))
!!$  
  
  kdc = h2*h2*resnorm
  
end subroutine e3DC_beta2 


!======================================================================
! Stabilization parameter (tauB) for weak BC
!======================================================================
subroutine e3bSTAB_weak(tauB, tauNor, rho, mu,  ui, nor, Gij)

  use params

  implicit none
  integer, parameter :: NSD = 3
  real(8), intent(in)  :: rho, mu, ui(NSD), nor(NSD), Gij(NSD,NSD)
  real(8), intent(out) :: tauB, tauNor
  
  real(8) :: temp, unor, upos, uneg  
  integer :: i,j

  temp =0d0
  do j = 1, NSD
    do i = 1, NSD
      temp = temp + nor(i)*Gij(i,j)*nor(j)
    enddo
  end do

  tauB = 4d0*mu*sqrt(temp)
  
  unor = sum(ui*nor)  
  upos = 0.5d0*(unor+abs(unor))
  uneg = 0.5d0*(unor-abs(unor))
  
  tauNor = 0d0  ! Jinhui: attention, tauNor is different from the tauNor in FSI code

end subroutine e3bSTAB_weak

!------------------------------------------------------------------   
subroutine e3STAB_TAU(ui, Gij, tauM)
           
      use params
      implicit none 
      
      integer, parameter :: NSD = 3
      
      real(8), intent(in) :: ui(NSD), Gij(NSD,NSD)
      real(8) :: tauM
      real(8) :: taua, taut, dtfact, adfact
      integer :: i, j
    
      dtfact = 4d0
      adfact = 1d0

      taua = 0d0
      do j = 1, NSD
        do i = 1, NSD
          taua  = taua  + ui(i)*Gij(i,j)*ui(j)
        enddo
      enddo    

      taut = dtfact*Dtgl*Dtgl   
      taua = adfact*taua         
      tauM = 1d0/sqrt(taut+taua) 

end subroutine e3STAB_TAU
      
!------------------------------------------------------------------           
subroutine e3STAB_TAU_nt(ui, Gij, tauM)
        
      use params
      implicit none 
      
      integer, parameter :: NSD = 3
            
      real(8), intent(in) :: ui(NSD), Gij(NSD,NSD)
      real(8) :: tauM
      real(8) :: taua, adfact
      integer :: i, j
      
      adfact = 1d0
    
      taua = 0d0
      do j = 1, NSD
        do i = 1, NSD
          taua  = taua  + ui(i)*Gij(i,j)*ui(j)
        enddo
      enddo    
      taua = adfact*taua      
      tauM = 1d0/sqrt(taua + 1d-15) 

end subroutine e3STAB_TAU_nt
     
!------------------------------------------------------------------    
subroutine e3DC_CAU(dfidxi,ui,Gij,Ginv,res,tauM,kdc)

      use params
      implicit none
      
      integer, parameter :: NSD = 3
            
      real(8), intent(in) :: dfidxi(NSD), ui(NSD), Gij(NSD,NSD),res
      real(8) :: temp, tauM, kdc, Ginv(NSD,NSD)
      integer :: i, j

      temp = 0d0
      do j = 1, NSD
         do i = 1, NSD
            temp  = temp  + dfidxi(i)*Gij(i,j)*dfidxi(j)
         enddo
      enddo
                
!      kdc = abs(res)/(sqrt(temp + 1d-15))          
      kdc = abs(res)/sqrt(Gij(1,1)+Gij(2,2)+Gij(3,3)) ! assume ||\grad \phi|| = 1
 
      
end subroutine e3DC_CAU
      
!------------------------------------------------------------------    
subroutine e3DC_CAU2(dfidxi,ui,Ginv,res,tauM,kdc)

      use params
      implicit none
      
      integer, parameter :: NSD = 3
      
      real(8), intent(in) :: dfidxi(NSD), ui(NSD), Ginv(NSD,NSD),res
      real(8) :: temp, tauM, kdc
      integer :: i, j

      temp = 0d0
      do j = 1, NSD
         do i = 1, NSD
            temp  = temp  + dfidxi(i)*Ginv(i,j)*dfidxi(j)
         enddo
      enddo
                
      kdc = abs(res)*sqrt(temp)/(sum(dfidxi*dfidxi) + 1d-15)    
      
end subroutine e3DC_CAU2
