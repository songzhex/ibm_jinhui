 module matvec
  use matrix
  use globdata
  implicit none
    
  !----------------------------------------------   
  ! Procedure interfaces    
  !---------------------------------------------- 
  interface matvecNS
    module procedure matvecNS
  end interface
  
  interface matvecLSC
    module procedure matvecLSC
  end interface
  
  interface matvecLSRD
    module procedure matvecLSRD
  end interface
  
  interface matvecLSMC
    module procedure matvecLSMC
  end interface
  
  interface matvecMono
    module procedure matvecMono
  end interface
  
  interface jfmvMono
    module procedure jfmvMono
  end interface  
  
  interface jfmvMono1
    module procedure jfmvMono1 ! Jinhui: central difference for matrix-free procedure
  end interface  

  !----------------------------------------------  
  interface matvecPCNS
    module procedure matvecPCNS
  end interface
  
  interface matvecPCLSC
    module procedure matvecPCLSC
  end interface
  
  interface matvecPCLSRD
    module procedure matvecPCLSRD
  end interface
  
  interface matvecPCLSMC
    module procedure matvecPCLSMC
  end interface
          
  interface matvecPCMono
    module procedure matvecPCMono
  end interface

  !---------------------------------------------- 
  interface bdiagpcNS
    module procedure bdiagpcNS
  end interface 
     
  interface diagpcLSC
    module procedure diagpcLSC
  end interface
   
  interface diagpcLSRD
    module procedure diagpcLSRD
  end interface 
  
  interface diagpcLSMC
    module procedure diagpcLSMC
  end interface 
  
  interface bdiagpcMesh
    module procedure bdiagpcMesh
  end interface  
  
  interface diagpcBody
    module procedure diagpcBody
  end interface  
     
  interface bdiagpcMono
    module procedure bdiagpcMono
  end interface 

  interface bdiagpcMono2
    module procedure bdiagpcMono2
  end interface   
        
  interface bsolveMono
    module procedure bsolveMono
  end interface 
  
  interface ebesolveMono
    module procedure ebesolveMono
  end interface 
            
  interface lsolveMono
    module procedure lsolveMono
  end interface 
  
  interface lesolveMono
    module procedure lesolveMono
  end interface  
     
  interface identity
    module procedure identity
  end interface  
    
  !----------------------------------------------            
  contains
  !----------------------------------------------         
  subroutine matvecNS(NNODE,NVAR, input, output)
    use mpi  
    implicit none  
             
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
    
    call SparseMatVec(NVAR, SparseMat, LHSns, input, output)
            
  end subroutine  matvecNS


  subroutine matvecNS_DG(NNODE,NVAR, input, output)
    use mpi  
    implicit none  
             
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
    
    call SparseMatVec_DG(NVAR, SparseMat, LHSns, input, output)
            
  end subroutine  matvecNS_DG    
  
  !----------------------------------------------  
  subroutine matvecLSC(NNODE,NVAR, input, output)
    use mpi    
    implicit none 
              
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    call SparseMatVec(NVAR, SparseMat, LHSlsc, input, output)
            
  end subroutine  matvecLSC   
  
  !----------------------------------------------  
  subroutine matvecLSRD(NNODE,NVAR, input, output)
    use mpi   
    implicit none  
             
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)

    call SparseMatVec(NVAR, SparseMat, LHSlsrd, input, output)

  end subroutine  matvecLSRD 
  
  !----------------------------------------------  
  subroutine matvecLSMC(NNODE,NVAR, input, output)
    use mpi     
    implicit none       
        
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    call SparseMatVec(NVAR, SparseMat, LHSlsmc, input, output)
  
  end subroutine  matvecLSMC
           
  !----------------------------------------------  
  subroutine matvecMesh(NNODE,NVAR, input, output)
    use mpi     
    implicit none       
        
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    call SparseMatVec(NVAR, SparseMat, LHSmesh, input, output)

  end subroutine  matvecMesh 
  
  !----------------------------------------------  
  subroutine matvecMono(NNODE,NVAR, input, output)
    use mpi     
    implicit none       
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)          
   
    call SparseMatVec(4, SparseMat, LHSns,   input(:,1:4),   output(:,1:4))
    call SparseMatVec(1, SparseMat, LHSlsc,  input(:,5:5),   output(:,5:5))
    call SparseMatVec(1, SparseMat, LHSlsrd, input(:,6:6),   output(:,6:6))
    call SparseMatVec(1, SparseMat, LHSlsmc, input(:,10:10), output(:,10:10))
    call SparseMatVec(3, SparseMat, LHSmesh, input(:,7:9),   output(:,7:9))

  end subroutine matvecMono
  !----------------------------------------------   
  subroutine matvecPCNS(NNODE,NVAR, input, output)
    use mpi  
    implicit none  
             
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
    
    call SparseMatVec(NVAR, SparseMat, PCns, input, output)
            
  end subroutine  matvecPCNS  
  
  !----------------------------------------------  
  subroutine matvecPCLSC(NNODE,NVAR, input, output)
    use mpi    
    implicit none 
              
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    call SparseMatVec(NVAR, SparseMat, PClsc, input, output)
            
  end subroutine  matvecPCLSC   
  
  !----------------------------------------------  
  subroutine matvecPCLSRD(NNODE,NVAR, input, output)
    use mpi   
    implicit none  
             
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)

    call SparseMatVec(NVAR, SparseMat, PClsrd, input, output)

  end subroutine  matvecPCLSRD 
  
  !----------------------------------------------  
  subroutine matvecPCLSMC(NNODE,NVAR, input, output)
    use mpi     
    implicit none       
        
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    call SparseMatVec(NVAR, SparseMat, PClsmc, input, output)
  
  end subroutine  matvecPCLSMC
           
  !----------------------------------------------  
  subroutine matvecPCMesh(NNODE,NVAR, input, output)
    use mpi     
    implicit none       
        
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    call SparseMatVec(NVAR, SparseMat, PCmesh, input, output)

  end subroutine  matvecPCMesh  
 
  !----------------------------------------------  
  subroutine matvecPCMono(NNODE,NVAR, input, output)
    use mpi     
    implicit none       
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input (NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)          
   
    call SparseMatVec(4, SparseMat, PCns,   input(:,1:4),   output(:,1:4))
    call SparseMatVec(1, SparseMat, PClsc,  input(:,5:5),   output(:,5:5))
    call SparseMatVec(1, SparseMat, PClsrd, input(:,6:6),   output(:,6:6))
    call SparseMatVec(1, SparseMat, PClsmc, input(:,10:10), output(:,10:10))
    call SparseMatVec(3, SparseMat, PCmesh, input(:,7:9),   output(:,7:9))

  end subroutine matvecPCMono

  !----------------------------------------------   

subroutine jfmvMono(SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, sur,&  ! Jinhui: to include structure
                     NNODE,NVAR,NNODE1,NVAR1,NNODE2,NVAR2,&
                     rhs,input, output,eps1,eps2)


  use mpi   
  use params 
  use globdata
  use boundaryConditions
  use defs_shell
  use types_structure
  use types_beam
  use aAdjKeep
  
  implicit none   
            
  integer, parameter :: NSD = 3            
            
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  type(smesh), intent(inout) :: sur
  type(mesh_beam), intent(inout)    :: NRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(structure) ,intent(inout) :: STRU

  integer , intent(in)  :: NNODE,NVAR
  integer , intent(in)  :: NNODE1,NVAR1
  integer , intent(in)  :: NNODE2,NVAR2
  real(8) , intent(in)  :: input (NNODE,NVAR)
  real(8) , intent(in)  :: rhs (NNODE,NVAR)
  real(8) , intent(out) :: output(NNODE,NVAR)
  real(8) , intent(in)  :: eps1,eps2
  real(8) :: output_DG(NNODE1,6)
  real(8) , allocatable :: input_fluid(:,:), output_fluid(:,:)
  real(8) , allocatable :: input_struc(:,:), output_struc(:,:)
  real(8) , allocatable :: output_eps(:,:), output_eps1(:,:)

  real(8):: eps_fluid,eps_struc
  integer :: flag(4)
  integer :: b, id,ii,jj 

  allocate(input_fluid(NNODE1,13), output_fluid(NNODE1,13))
  allocate(input_struc(NNODE2,NVAR2), output_struc(NNODE2,NVAR2))
  allocate(output_eps(NNODE,NVAR), output_eps1(NNODE,NVAR))

  eps_fluid=eps1
  eps_struc=eps2

  output_fluid = 0d0
  output_struc = 0d0 

  input_fluid = 0d0
  input_struc = 0d0

  output_eps = 0d0
  output_eps1 = 0d0
 
  input_fluid(1:NNODE1,1:NVAR1)=input(1:NNODE1,1:NVAR1)
  input_struc(1:NNODE2,1:NVAR2)=input(1:NNODE2,NVAR1+1:NVAR)
  
  call commu(input_fluid(:,1:10), 10, 'out')  ! Jinhui: Need to figure out the number
  ! Perturtbed n+1 level 
  new_eps = new 
  SH_eps  = SH

  NRB_BEA_eps = NRB_BEA
  BEA_eps = BEA
  mNRB_BEA_eps = mNRB_BEA
  STRU_eps = STRU
  blade_eps = blade
  ! Jinhui, just in case, copy everything



  flag = 0
  flag(1)=1 
  if(NNODE1.ne.new_eps%NNODE) write(*,*) "Error: the dimension of NNODE1"
  call incrementState(new_eps, input_fluid(:,1:10) , eps_fluid, flag)   ! Jinhui: perturb fluid
  if(NNODE2.ne.SH_eps%NRB%NNODE) write(*,*) "Error: the dimension of NNODE2"
  if(move.and.solfsi) then
    call incrementState_STRU1(NNODE2, input_struc(:,1:3), eps_fluid, SH_eps, STRU_eps, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,blade_eps)! Jinhui: perturb structure
  endif
  if(ismaster) then
    write(*,*) "~~~~~ perturb fluid with eps_fluid",eps_fluid
    write(*,*) "//////////////////////////////////////////////////"
  endif
  ! Assemble 
  flag = 0
  flag(1)=1 
  call assemble1(new_eps, output_fluid, output_DG, 0,flag, &
                SH_eps, NM, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,STRU_eps,blade_eps,0,sur)

  output_eps(1:NNODE1,1:NVAR1)=output_fluid(1:NNODE1,1:NVAR1) ! grab the fluid residual
! Jinhui: assemble the structure
  new_eps = new 
  SH_eps  = SH
  NRB_BEA_eps = NRB_BEA
  BEA_eps = BEA
  mNRB_BEA_eps = mNRB_BEA
  STRU_eps = STRU
  blade_eps = blade
  ! Jinhui, just in case, copy everything
  flag = 0
  flag(1)=1 
  STRU_eps%RHSG = 0d0
!  if(NNODE1.ne.new_eps%NNODE) write(*,*) "Error: the dimension of NNODE1"
!  call incrementState(new_eps, input_fluid(:,1:10) , eps_struc, flag)   ! Jinhui: perturb fluid

!  if(NNODE2.ne.SH_eps%NRB%NNODE) write(*,*) "Error: the dimension of NNODE2"
!  if(move.and.solfsi) then
!    call incrementState_STRU1(NNODE2, input_struc(:,1:3), eps_struc, SH_eps, STRU_eps, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,blade_eps)! Jinhui: perturb structure
!  endif
!  if(ismaster) then
!   write(*,*) "~~~~~ perturb structure with eps_struc",eps_struc
!   write(*,*) "//////////////////////////////////////////////////"
!  endif
   ! Assemble 
!  flag = 0
!  flag(1)=1 
!  call assemble1(new_eps, output_fluid, output_DG, 0,flag, &
!                SH_eps, NM, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,STRU_eps,blade_eps,1)
  output_eps(1:NNODE2,NVAR1+1:NVAR)=STRU_eps%RHSG(1:NNODE2,1:3) ! grab the struc residual
  output(:,1:NVAR1) = (rhs(:,1:NVAR1) - output_eps(:,1:NVAR1))/eps_fluid
  output(:,NVAR1+1:NVAR) = (rhs(:,NVAR1+1:NVAR) - output_eps(:,NVAR1+1:NVAR))/eps_struc

  end subroutine  jfmvMono
!
! Added by Jinhui, adaptively choose eps and use central difference
! 

subroutine jfmvMono_Fin(SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, &  ! Jinhui: to include structure
                     NNODE,NVAR,NNODE1,NVAR1,NNODE2,NVAR2,&
                     rhs,input, output,eps1)

  use mpi   
  use params 
  use globdata
  use boundaryConditions
  use defs_shell
  use types_structure
  use types_beam
  use aAdjKeep
  implicit none   
  integer, parameter :: NSD = 3                      
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  type(mesh_beam), intent(inout)    :: NRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(structure) ,intent(inout) :: STRU
  integer , intent(in)  :: NNODE,NVAR
  integer , intent(in)  :: NNODE1,NVAR1
  integer , intent(in)  :: NNODE2,NVAR2
  real(8) , intent(in)  :: input (NNODE,NVAR)
  real(8) , intent(in)  :: rhs (NNODE,NVAR)
  real(8) , intent(out) :: output(NNODE,NVAR)
  real(8) , intent(in)  :: eps1
  real(8) :: output_DG(NNODE1,6)

  real(8) , allocatable :: input_fluid(:,:), output_fluid(:,:)
  real(8) , allocatable :: input_struc(:,:), output_struc(:,:)
  real(8) , allocatable :: output_eps(:,:), output_eps1(:,:)
  real(8):: eps
  integer :: flag(4)
  integer :: b, id,ii,jj 
  allocate(input_fluid(NNODE1,13), output_fluid(NNODE1,13))
  allocate(input_struc(NNODE2,NVAR2), output_struc(NNODE2,NVAR2))
  allocate(output_eps(NNODE,NVAR), output_eps1(NNODE,NVAR))

!  real(8) :: input_tmp(NNODE,13)
!  real(8) :: output_tmp(NNODE,13)
!  real(8) :: output_tmp1(NNODE,13)
!  real(8) :: output_tmp2(NNODE,13)

  eps=eps1

!  eps  = 5d-5/Delt
! ****************************************************************************************************
! Compute the first resiual
! ****************************************************************************************************
!  input_tmp=0
!  input_tmp(:,1:5)=input(:,1:5)
!  input_tmp(:,11:13)=input(:,6:8)
  

  output_fluid = 0d0
  output_struc = 0d0 

  input_fluid = 0d0
  input_struc = 0d0

  output_eps = 0d0
  output_eps1 = 0d0
 
  input_fluid(1:NNODE1,1:NVAR1)=input(1:NNODE1,1:NVAR1)
  input_struc(1:NNODE2,1:NVAR2)=input(1:NNODE2,NVAR1+1:NVAR)
  call commu(input_fluid(:,1:10), 10, 'out')  ! Jinhui: Need to figure out the number
  ! Perturtbed n+1 level  
  new_eps = new 
  SH_eps  = SH
  NRB_BEA_eps = NRB_BEA
  BEA_eps = BEA
  mNRB_BEA_eps = mNRB_BEA
  STRU_eps = STRU
  blade_eps = blade
  ! Jinhui, just in case, copy everything
  flag(1)=1
  flag(2)=0
  flag(3)=0 
  flag(4)=0
  if(NNODE1.ne.new_eps%NNODE) write(*,*) "Error: the dimension of NNODE1"
  if(ismaster) write(*,*) "~~~~~",myid,"perturb first fluid with", eps
  call incrementState(new_eps, input_fluid(:,1:10) , eps, flag)   ! Jinhui: perturb fluid
  if(NNODE2.ne.SH_eps%NRB%NNODE) write(*,*) "Error: the dimension of NNODE2"
  if(ismaster) write(*,*) "~~~~~",myid,"perturb first structure with", eps
  call incrementState_STRU1(NNODE2, input_struc(:,1:3), eps, SH_eps, STRU_eps, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,blade_eps)! Jinhui: perturb structure
  ! Assemble 
  flag(1)=1
  flag(2)=0
  flag(3)=0 
  flag(4)=0 
  call assemble(new_eps, output_fluid, output_DG, 0,flag, &
                SH_eps, NM, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,STRU_eps,blade_eps)
  output_eps(1:NNODE1,1:NVAR1)=output_fluid(1:NNODE1,1:NVAR1)
  output_eps(1:NNODE2,NVAR1+1:NVAR)=STRU_eps%RHSG(1:NNODE2,1:3) ! Jinhui: grab the solution
  ! Take central difference
  !eps = 1d0/eps
  output = (rhs - output_eps)/eps
!  output(:,1:5)=output_tmp2(:,1:5)
!  output(:,6:8)=output_tmp2(:,11:13)

  end subroutine  jfmvMono_Fin



subroutine jfmvMono1(SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, &  ! Jinhui: to include structure
                     NNODE,NVAR,NNODE1,NVAR1,NNODE2,NVAR2,&
                     rhs,input, output,eps1)
! central difference
  use mpi   
  use params 
  use globdata
  use boundaryConditions
  use defs_shell
  use types_structure
  use types_beam
  use aAdjKeep
  
  implicit none   
            
  integer, parameter :: NSD = 3            
            
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  type(mesh_beam), intent(inout)    :: NRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(structure) ,intent(inout) :: STRU

  integer , intent(in)  :: NNODE,NVAR
  integer , intent(in)  :: NNODE1,NVAR1
  integer , intent(in)  :: NNODE2,NVAR2
  real(8) , intent(in)  :: input (NNODE,NVAR)
  real(8) , intent(out) :: output(NNODE,NVAR)
  real(8) , intent(in)  :: rhs (NNODE,NVAR)
  real(8) , intent(in)  :: eps1

  real(8) , allocatable :: input_fluid(:,:), output_fluid(:,:)
  real(8) , allocatable :: input_struc(:,:), output_struc(:,:)
  real(8) , allocatable :: output_eps(:,:), output_eps1(:,:)

  real(8):: eps
  integer :: flag(4)
  integer :: b, id,ii,jj 

  allocate(input_fluid(NNODE1,13), output_fluid(NNODE1,13))
  allocate(input_struc(NNODE2,NVAR2), output_struc(NNODE2,NVAR2))
  allocate(output_eps(NNODE,NVAR), output_eps1(NNODE,NVAR))

!  real(8) :: input_tmp(NNODE,13)
!  real(8) :: output_tmp(NNODE,13)
!  real(8) :: output_tmp1(NNODE,13)
!  real(8) :: output_tmp2(NNODE,13)

  eps=eps1

!  eps  = 5d-5/Delt
! ****************************************************************************************************
! Compute the first resiual
! ****************************************************************************************************
!  input_tmp=0
!  input_tmp(:,1:5)=input(:,1:5)
!  input_tmp(:,11:13)=input(:,6:8)
  

  output_fluid = 0d0
  output_struc = 0d0 

  input_fluid = 0d0
  input_struc = 0d0

  output_eps = 0d0
  output_eps1 = 0d0
 
  input_fluid(1:NNODE1,1:NVAR1)=input(1:NNODE1,1:NVAR1)
  input_struc(1:NNODE2,1:NVAR2)=input(1:NNODE2,NVAR1+1:NVAR)
  



  call commu(input_fluid(:,1:10), 10, 'out')  ! Jinhui: Need to figure out the number

  ! Perturtbed n+1 level  
  new_eps = new 
  SH_eps  = SH
  NM_eps  = NM
  NRB_BEA_eps = NRB_BEA
  BEA_eps = BEA
  mNRB_BEA_eps = mNRB_BEA
  STRU_eps = STRU
  blade_eps = blade
  ! Jinhui, just in case, copy everything


  flag(1)=1
  flag(2)=0
  flag(3)=0 
  flag(4)=0
 
  if(NNODE1.ne.new_eps%NNODE) write(*,*) "Error: the dimension of NNODE1"
  if(ismaster) write(*,*) "~~~~~",myid,"perturb first fluid"
  call incrementState(new_eps, input_fluid(:,1:10) , eps, flag)   ! Jinhui: perturb fluid

  if(NNODE2.ne.SH_eps%NRB%NNODE) write(*,*) "Error: the dimension of NNODE2"
  if(ismaster) write(*,*) "~~~~~",myid,"perturb first structure"
  call incrementState_STRU(NNODE2, input_struc(:,1:3), eps, SH_eps, STRU_eps, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,blade_eps)! Jinhui: perturb structure


if(.false.) then 
  do b = 1, NBODIES   
    bodies_new_eps(b) = bodies_new(b)
    if (ismaster) then 
 !   call incrementBodyState(bodies_new_eps(b), bodies_old(b), &
  !                          bodies_param(b), input_tmp(:,11), eps)
    endif                      
  enddo  
!Jinhui: do not need this future, just need to figure out which processor will be used

  do b = 1, NBODIES        
    call syncBodyState(bodies_new_eps(b))
  enddo 
! Jinhui: attention: solve in every processor or solve in master processor then broadcast
endif  
 !Jinhui: broadcast 
 
  ! Assemble 
  flag(1)=1
  flag(2)=0
  flag(3)=0 
  flag(4)=0 
  call assemble(new_eps, output_fluid, 0,flag, &
                SH_eps, NM_eps, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,STRU_eps,blade_eps)



  output_eps(1:NNODE1,1:NVAR1)=output_fluid(1:NNODE1,1:NVAR1)
  output_eps(1:NNODE2,NVAR1+1:NVAR)=STRU_eps%RHSG(1:NNODE2,1:3) ! Jinhui: grab the solution
! ****************************************************************************************************
! Compute another resiual
! ****************************************************************************************************

!  input_tmp=0
!  input_tmp(:,1:5)=input(:,1:5)
!  input_tmp(:,11:13)=input(:,6:8)
  if(ismaster) write(*,*) myid,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  if(ismaster) write(*,*) myid,"assembling the first residual is done"
  if(ismaster) write(*,*) myid,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

  input_fluid = 0d0
  input_struc = 0d0
  output_fluid = 0d0
  output_struc = 0d0

  input_fluid(1:NNODE1,1:NVAR1) = input(1:NNODE1,1:NVAR1)
  input_struc(1:NNODE2,1:NVAR2) = input(1:NNODE2,NVAR1+1:NVAR)
  
  call commu(input_fluid(:,1:10), 10, 'out')

  ! Perturtbed n+1 level  
  new_eps = new 
  SH_eps = SH
  NM_eps = NM
  NRB_BEA_eps = NRB_BEA
  BEA_eps = BEA
  mNRB_BEA_eps = mNRB_BEA
  STRU_eps = STRU
  blade_eps = blade


  flag(1)=1
  flag(2)=0
  flag(3)=0 
  flag(4)=0 
!  call incrementState(new_eps, input_tmp(:,1:10) , -eps,flag)  
!  call incrementState_STRU(NNODE, input_tmp(:,11:13), -eps, SH_eps, STRU_eps, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps) !


  if(NNODE1.ne.new_eps%NNODE) write(*,*) "Error: the dimension of NNODE1"
  call incrementState(new_eps, input_fluid(:,1:10) , -eps, flag)   ! Jinhui: perturb fluid
  if(ismaster) write(*,*) "~~~~~",myid,"perturb second fluid"

  if(NNODE2.ne.SH_eps%NRB%NNODE) write(*,*) "Error: the dimension of NNODE2"
  call incrementState_STRU(NNODE2, input_struc(:,1:3), -eps, SH_eps, STRU_eps, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps, blade_eps) ! Jinhui: perturb structure
  if(ismaster) write(*,*) "~~~~~",myid,"perturb second structure"
 if(.false.) then 
   do b = 1, NBODIES   
    bodies_new_eps(b) = bodies_new(b)
    if (ismaster) then 
!    call incrementBodyState(bodies_new_eps(b), bodies_old(b), &
!                            bodies_param(b), input_tmp(:,11), -eps)
    endif                      
   enddo  
  do b = 1, NBODIES        
    call syncBodyState(bodies_new_eps(b))
  enddo 
 endif !Jinhui: will decide whether keep this later
 ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
  ! Assemble 
  flag(1)=1
  flag(2)=0
  flag(3)=0 
  flag(4)=0 

  ! Take 1st order difference
  !eps = 1d0/eps
  call assemble(new_eps, output_fluid, 0, flag, &
                SH_eps, NM_eps, NRB_BEA_eps, BEA_eps, mNRB_BEA_eps,STRU_eps,blade_eps)

  

  output_eps1(1:NNODE1,1:NVAR1)=output_fluid(1:NNODE1,1:NVAR1)
  output_eps1(1:NNODE2,NVAR1+1:NVAR)=STRU_eps%RHSG(1:NNODE2,1:3) ! Jinhui: grab the solution

  if(ismaster) write(*,*) myid,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  if(ismaster) write(*,*) myid,"assembling the second residual is done"
  if(ismaster) write(*,*) myid,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  ! Take central difference
  !eps = 1d0/eps
  output = (output_eps1 - output_eps)/(2d0*eps)
!  output(:,1:5)=output_tmp2(:,1:5)
!  output(:,6:8)=output_tmp2(:,11:13)

  end subroutine  jfmvMono1
      
  !----------------------------------------------  
  subroutine getBDiagpcNS(NNODE,NSD)
    
  use params      
  use mpi   
  implicit none
  
  integer , intent(in)  :: NNODE,NSD
    
  integer :: n,i,j,k,l,c      
  real(8) :: Binv(NSD,NSD), Det

  ! Precondition Residual  
  do i = 1, NNODE         
    do j = sparseMat%col(i), sparseMat%col(i+1)-1
      n = sparseMat%row(j)
      if (n == i) then                             
        do k = 1, NSD
          do l = 1, NSD
            c = (NSD+1)*(k-1) + l
            bdpcNSu(i,k,l) = LHSns(c,j)
          enddo
        enddo
        
        bdpcNSp(i) = LHSns((NSD+1)*(NSD+1),j)                              
      end if
    end do         
  end do
      
  ! communicate the block-diagonal ! LGGL            
  call commu(bdpcNSu,NSD*NSD, 'in ') 
  call commu(bdpcNSp,1,       'in ')
              
  call MPI_BARRIER (MPI_COMM_WORLD,mpi_err)  
    
  call commu(bdpcNSu,NSD*NSD, 'out') 
  call commu(bdpcNSp,1,       'out')
  
  ! Invert block-diagonal     
  do i = 1, NNODE
    call get_inverse_3x3(bdpcNSu(i,:,:), Binv, Det)            
    bdpcNSu(i,:,:) = Binv 
    bdpcNSp(i) = 1.0d0/bdpcNSp(i)               
  end do
  
  end subroutine getBDiagpcNS
  !----------------------------------------------  
  subroutine getBDiagpcLS(NNODE,flag)
        
  use mpi 
  use matrix 
    
  implicit none
  integer , intent(in)  :: NNODE 
  integer , intent(in)  :: flag(4) 
  integer :: n,i,j,k,l,c 
 ! if(ismaster) then 
 !    write(*,*) "flag:", flag
 ! endif 

  ! Precondition Residual
  do i = 1, NNODE         
    do j = sparseMat%col(i), sparseMat%col(i+1)-1
      n = sparseMat%row(j)
      if (n == i) then     
        if(flag(1).eq.1) then       
          dpcLSC (i) = LHSlsc (j)
        endif
        if(flag(3).eq.1) then            
          dpcLSRD(i) = LHSlsrd(j) 
        endif   
        if(flag(4).eq.1) then          
          dpcLSMC(i) = LHSlsmc(j)   
        endif                             
      end if
    end do         
  end do
      
  ! communicate the block-diagonal ! LGGL            
  call commu(dpcLSC,  1, 'in ')
  call commu(dpcLSRD, 1, 'in ')
  call commu(dpcLSMC, 1, 'in ')
              
  call MPI_BARRIER (MPI_COMM_WORLD,mpi_err)  
    
  call commu(dpcLSC,  1, 'out')  
  call commu(dpcLSRD, 1, 'out')    
  call commu(dpcLSMC, 1, 'out') 

  ! Invert block-diagonal 
 if(flag(1).eq.1) then       
   dpcLSC  = 1d0/dpcLSC    
 endif
 if(flag(3).eq.1) then       
  dpcLSRD = 1d0/dpcLSRD 
 endif  
 if(flag(4).eq.1) then       
  dpcLSMC = 1d0/dpcLSMC   
 endif           
    
  end subroutine getBDiagpcLS
  !----------------------------------------------  
  subroutine getBDiagpcMesh(NNODE,NSD)
    
    use params      
    use mpi   
    implicit none
   
    integer , intent(in)  :: NNODE,NSD
    
    integer :: n,i,j,k,l,c      
    real(8) :: Binv(NSD,NSD), Det

    ! Precondition Residual  
    do i = 1, NNODE         
      do j = sparseMat%col(i), sparseMat%col(i+1)-1
       n = sparseMat%row(j)
        if (n == i) then                             
          do k = 1, NSD
            do l = 1, NSD
              c = NSD*(k-1) + l
              bdpcMesh(i,k,l) = LHSmesh(c,j)
            enddo
          enddo                            
        end if
      end do         
    end do
      
  ! communicate the block-diagonal ! LGGL            

    call commu(bdpcMesh,NSD*NSD, 'in ')               
    call MPI_BARRIER (MPI_COMM_WORLD,mpi_err)       
    call commu(bdpcMesh,NSD*NSD, 'out') 

    ! Invert block-diagonal     
    do  i = 1, NNODE
      call get_inverse_3x3(bdpcMesh(i,:,:), Binv, Det)            
      bdpcMesh(i,:,:) = Binv               
    end do
  
  end subroutine getBDiagpcMesh

  !----------------------------------------------  
  subroutine getBDiagpcBody(NNODE)
    
    use params      
    use mpi   
    implicit none
  
    integer , intent(in)  :: NNODE
    integer :: i
   
    ! Precondition Residual  
    do i = 1, NNODE         
      if (LhsBody(i)/=0d0) then
        dpcBody(i) = 1d0/LhsBody(i)
      else  
        dpcBody(i) = 0d0            
      endif
    enddo

  end subroutine getBDiagpcBody
  

    
  !---------------------------------------------- 
  subroutine bdiagpcNS(NNODE,NVAR, input,output)  
   
    implicit none 
   
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    integer i,j
  
    do i = 1, NNODE              
      output(i,1:3) = matmul(bdpcNSu(i,:,:) ,input(i,1:3))
    enddo  
    output(:,4)   = bdpcNSp*input(:,4)
        
  end subroutine bdiagpcNS
  
  !---------------------------------------------- 
  subroutine bdiagpcMono(NNODE,NVAR, input,output)  
     
    implicit none 
   
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    integer i,j
  
    do i = 1, NNODE              
      output(i,1:3) = matmul(bdpcNSu(i,:,:) ,input(i,1:3))
    enddo       
  
    output(:,4)   = bdpcNSp*input(:,4)
    output(:,5)   = dpcLSC *input(:,5)
    output(:,6)   = dpcLSRD*input(:,6)
  
    do i = 1, NNODE              
      output(i,7:9) = matmul(bdpcMesh(i,:,:) ,input(i,7:9))
    enddo  
     
    output(:,10)   = dpcLSMC*input(:,10)
      
  end subroutine bdiagpcMono

  !---------------------------------------------- 
  subroutine bdiagpcMono2(NNODE,NVAR, input,output)  
     
    implicit none 
   
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    integer i,j
  
    do i = 1, NNODE              
      output(i,1:3) = matmul(bdpcNSu(i,:,:) ,input(i,1:3))
    enddo       
  
    output(:,4)   = bdpcNSp*input(:,4)
    output(:,5)   = dpcLSC *input(:,5)
    !output(:,6)   = dpcLSRD*input(:,6)

    call SparseMatVec(1, SparseMat, PClsrd, input(:,6), output(:,6))

    do i = 1, NNODE              
      output(i,7:9) = matmul(bdpcMesh(i,:,:) ,input(i,7:9))
    enddo  
     
    output(:,10)   = dpcLSMC*input(:,10)
      
  end subroutine bdiagpcMono2 

  !---------------------------------------------- 
  subroutine bdiagpcNSp(NNODE,NVAR, input,output)  
     
    implicit none 
   
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
        
    output(:,1) = bdpcNSp*input(:,1) 
        
  end subroutine bdiagpcNSp
  
  !---------------------------------------------- 
  subroutine diagpcLSC(NNODE,NVAR, input,output)  
     
    implicit none 
   
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    output(:,1) = dpcLSC*input(:,1)
        
  end subroutine diagpcLSC  
  
  !---------------------------------------------- 
  subroutine diagpcLSRD(NNODE,NVAR, input,output) 
  
    implicit none 
   
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    output(:,1) = dpcLSRD*input(:,1)
       
  end subroutine diagpcLSRD 
  
  !---------------------------------------------- 
  subroutine diagpcLSMC(NNODE,NVAR, input,output)
  
    implicit none 
   
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    output(:,1) = dpcLSMC*input(:,1)
    
  end subroutine diagpcLSMC     
  !---------------------------------------------- 
  subroutine bdiagpcMesh(NNODE,NVAR, input,output)  
     
    implicit none 
    
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    integer i,j  
  
    do i = 1, NNODE              
      output(i,:) = matmul(bdpcMesh(i,:,:) ,input(i,:))
    enddo  
     
  end subroutine bdiagpcMesh  
  !---------------------------------------------- 
  subroutine diagpcBody(NNODE,NVAR, input,output) 
  
    implicit none 
   
    integer , intent(in)  :: NNODE,NVAR
    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)
  
    output(:,1) = dpcBody*input(:,1)
       
  end subroutine diagpcBody   
  
  !---------------------------------------------- 
  subroutine bsolveMono(SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, &
                        NNODE, NVAR,NNODE1, NVAR1, NNODE2, NVAR2, rhs0_fluid_DG, &
                        input, output)

   use KrylovSolvers 
   use innerprod   
   use params
   use mpi
   use defs_shell
   use types_beam
   use types_structure
   implicit none 

   integer,parameter :: NSD = 3

   type(shell_bld), intent(inout) :: SH
   type(shell_nmb), intent(inout) :: NM
   type(mesh_beam), intent(inout)    :: NRB_BEA
   type(beam), intent(inout)	 :: BEA
   type(mesh_mp_beam), intent(inout) :: mNRB_BEA
   type(structure) ,intent(inout) :: STRU

    integer , intent(in)  :: NNODE,NVAR
    integer , intent(in)  :: NNODE1, NVAR1
    integer , intent(in)  :: NNODE2, NVAR2
    real(8) , intent(in)  :: rhs0_fluid_DG(NNODE1,NVAR1) ! NVAR1 must be 5

    real(8) , intent(in)  :: input(NNODE,NVAR)
    real(8) , intent(out) :: output(NNODE,NVAR)



    real(8), allocatable :: input_fluid(:,:), output_fluid(:,:)
    real(8), allocatable :: input_struc(:,:), output_struc(:,:)

    integer :: i, j, ii, jj
    integer :: verbose

    allocate(input_fluid(NNODE1,NVAR1), output_fluid(NNODE1,NVAR1))
    allocate(input_struc(NNODE2,NVAR2), output_struc(NNODE2,NVAR2))
    


    verbose = -1    
    output = 0d0
    input_fluid = 0d0
    output_fluid = 0d0
    input_struc = 0d0
    output_struc = 0d0

    input_fluid(1:NNODE1,1:NVAR1) = input(1:NNODE1,1:NVAR1)
    input_struc(1:NNODE2,1:NVAR2) = input(1:NNODE2,NVAR1+1:NVAR) ! grab the part for fluid and structure

    if (ismaster) verbose = PC_verbose  
          
    ! Solve Navier Stokes block  
    if(ismaster) write(*,*) "Navier Stokes:"   
    call gmres_NS(NM, NNODE1, 4, output_fluid(:,1:4), input_fluid(:,1:4), rhs0_fluid_DG,  &
               matvecNS,bdiagpcNS,std_iprod, &
               PC_NS_tol,PC_NS_itermin,PC_NS_itermax,verbose)
               
    ! Jinhui: Solve level set convection block 
    if(ismaster) write(*,*) "Level convection:"          
    call gmres_LS(NM,NNODE1, 1, output_fluid(:,5:5), input_fluid(:,5:5), rhs0_fluid_DG,&
                  matvecLSC,diagpcLSC,std_iprod, &
                  PC_LSC_tol,PC_LSC_itermin,PC_LSC_itermax,verbose)
   
    if(NNODE2.NE.SH%NRB%NNODE) then 
      write(*,*) myid,"Error: dimension of the structure in the preconditioner solver"
    endif
    if(ismaster) write(*,*) "Kirchhoff love shell:" 
    if(move.and.(ismaster).and.(solfsi)) then
    write(*,*) 'Before Linear solver:', sum(input_struc(:,1)), sum(input_struc(:,2)),sum(input_struc(:,3))
    call SparseCG_BDIAG_shell(SH%NRB%NNODE, SH%NRB%maxNSHL, NSD, &
                                STRU%icnt, STRU%col, STRU%row, &
                                STRU%LHSK, input_struc, &
                                output_struc, PC_STRU_tol, 1)
     else
        output_struc = 0d0
     endif
    !Jinhui: combine to get the whole vector

    output(1:NNODE1,1:NVAR1) = output_fluid(1:NNODE1,1:NVAR1)
    output(1:NNODE2,NVAR1+1:NVAR) = output_struc(1:NNODE2,1:NVAR2)
                      
  end subroutine bsolveMono
   
  !---------------------------------------------- 
  subroutine ebesolveMono()
      ! Jinhui: deleted, refer to the freesurface code to see what is                
  end subroutine ebesolveMono 
  !---------------------------------------------- 
  subroutine lsolveMono()
     ! Jinhui: deleted, refer to the freesurface code to see what is                        
  end subroutine lsolveMono  
    !---------------------------------------------- 
  subroutine lesolveMono()
   ! Jinhui: deleted, refer to the freesurface code to see what is                        
  end subroutine lesolveMono  
  
  !----------------------------------------------   
  subroutine identity(NNODE,NVAR, input, output) 

  implicit none 
  
  integer , intent(in)  :: NNODE,NVAR
  real(8) , intent(in)  :: input(NNODE,NVAR)
  real(8) , intent(out) :: output(NNODE,NVAR)
  
  output = input   
  
 end subroutine identity        
                    
end module matvec
