module dynamic_arrays
  
  type dynamic_array

     integer :: size
     integer, allocatable :: content(:)
          
  end type dynamic_array

  ! annoying lack of templates... need to re-define for every type that
  ! i want a dynamic list of

  type dynamic_array_r

     integer :: size
     real(8), allocatable :: content(:)
          
  end type dynamic_array_r

  type dynamic_point_array

     integer :: size
     ! assume content(3,capacity), to store coordinates of 3D points
     real(8), allocatable :: content(:,:)
          
  end type dynamic_point_array

  contains


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! integer lists

    ! this allocates a dynamic array
    subroutine dynamic_array_allocate(dynarray, initial_capacity)

      type(dynamic_array), intent(inout) :: dynarray
      integer, intent(in) :: initial_capacity

      if(allocated(dynarray%content)) then
         deallocate(dynarray%content)
      endif

      ! this is the number of occupied entries, NOT
      ! intial_capacity, which is the amount of memory allocated
      dynarray%size = 0
      
      ! this sets the amount of allocated memory
      allocate(dynarray%content(initial_capacity))

    end subroutine dynamic_array_allocate

    ! this adds an element to the end of the dynamic array, re-sizing
    ! if necessary.  only call this if the array is allocated already
    subroutine dynamic_array_push(dynarray, new_element)

      type(dynamic_array), intent(inout) :: dynarray
      integer, intent(in) :: new_element

      integer :: i, s
      integer, allocatable :: temp(:)
      
      ! if we need to re-size, allocate a new array of double size
      ! and put all elements into it
      s = size(dynarray%content)
      if(dynarray%size == s) then
         allocate(temp(2*s))
         temp(1:s) = dynarray%content
         deallocate(dynarray%content)
         allocate(dynarray%content(2*s))
         dynarray%content(1:s) = temp(1:s)
      endif

      ! increase size and add new element at end
      dynarray%size = dynarray%size + 1
      dynarray%content(dynarray%size) = new_element

    end subroutine dynamic_array_push

    ! free the memory and re-set size
    subroutine dynamic_array_free(dynarray)
      type(dynamic_array), intent(inout) :: dynarray
      if(allocated(dynarray%content)) then
         deallocate(dynarray%content)
      endif
      dynarray%size = 0
    end subroutine dynamic_array_free

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! real lists

    ! this allocates a dynamic array
    subroutine dynamic_array_r_allocate(dynarray, initial_capacity)

      type(dynamic_array_r), intent(inout) :: dynarray
      integer, intent(in) :: initial_capacity

      if(allocated(dynarray%content)) then
         deallocate(dynarray%content)
      endif

      ! this is the number of occupied entries, NOT
      ! intial_capacity, which is the amount of memory allocated
      dynarray%size = 0
      
      ! this sets the amount of allocated memory
      allocate(dynarray%content(initial_capacity))

    end subroutine dynamic_array_r_allocate

    ! this adds an element to the end of the dynamic array, re-sizing
    ! if necessary.  only call this if the array is allocated already
    subroutine dynamic_array_r_push(dynarray, new_element)

      type(dynamic_array_r), intent(inout) :: dynarray
      real(8), intent(in) :: new_element

      integer :: i, s
      real(8), allocatable :: temp(:)
      
      ! if we need to re-size, allocate a new array of double size
      ! and put all elements into it
      s = size(dynarray%content)
      if(dynarray%size == s) then
         allocate(temp(2*s))
         temp(1:s) = dynarray%content
         deallocate(dynarray%content)
         allocate(dynarray%content(2*s))
         dynarray%content(1:s) = temp(1:s)
      endif

      ! increase size and add new element at end
      dynarray%size = dynarray%size + 1
      dynarray%content(dynarray%size) = new_element

    end subroutine dynamic_array_r_push

    ! free the memory and re-set size
    subroutine dynamic_array_r_free(dynarray)
      type(dynamic_array_r), intent(inout) :: dynarray
      if(allocated(dynarray%content)) then
         deallocate(dynarray%content)
      endif
      dynarray%size = 0
    end subroutine dynamic_array_r_free


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! lists of points

    ! this allocates a dynamic array
    subroutine dynamic_point_array_allocate(dynarray, initial_capacity)

      type(dynamic_point_array), intent(inout) :: dynarray
      integer, intent(in) :: initial_capacity

      if(allocated(dynarray%content)) then
         deallocate(dynarray%content)
      endif

      ! this is the number of occupied entries, NOT
      ! intial_capacity, which is the amount of memory allocated
      dynarray%size = 0
      
      ! this sets the amount of allocated memory
      allocate(dynarray%content(3,initial_capacity))

    end subroutine dynamic_point_array_allocate

    ! this adds an element to the end of the dynamic array, re-sizing
    ! if necessary.  only call this if the array is allocated already
    subroutine dynamic_point_array_push(dynarray, new_element)

      type(dynamic_point_array), intent(inout) :: dynarray
      real(8), intent(in) :: new_element(3)

      integer :: i, s
      real(8), allocatable :: temp(:,:)
      
      ! if we need to re-size, allocate a new array of double size
      ! and put all elements into it
      s = size(dynarray%content,2)
      if(dynarray%size == s) then
         allocate(temp(3,2*s))
         temp(:,1:s) = dynarray%content
         deallocate(dynarray%content)
         allocate(dynarray%content(3,2*s))
         dynarray%content(:,1:s) = temp(:,1:s)
      endif

      ! increase size and add new element at end
      dynarray%size = dynarray%size + 1
      dynarray%content(:,dynarray%size) = new_element

    end subroutine dynamic_point_array_push

    ! free the memory and re-set size
    subroutine dynamic_point_array_free(dynarray)
      type(dynamic_point_array), intent(inout) :: dynarray
      if(allocated(dynarray%content)) then
         deallocate(dynarray%content)
      endif
      dynarray%size = 0
    end subroutine dynamic_point_array_free

end module dynamic_arrays
