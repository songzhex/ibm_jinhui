!======================================================================
! Main routine to call all the subroutines                               
!======================================================================
program FloatingElasticBody
  use mpi  
  use params
  use globdata
  use postpro
  use commonpars
!***********************************************
!***********************************************
! from fsi code 
  use aAdjKeep
  use commonvars
  use defs_shell
  use types_beam
  use types_structure
  use raytrace
  use hashed_octree
  implicit none
!***********************************************
! The data structure for structure
!***********************************************  
  type(shell_bld) :: SH       ! shell for the blade  
  type(beam) :: BEA           ! beam
  type(mesh_mp_beam) :: mNRB_BEA
  type(mesh_beam)    :: NRB_BEA
  type(shell_nmb) :: NM       ! shell for non-matching (zones)
  type(structure)   :: STRU
  type(smesh) :: SUR ! immersed surface
!***********************************************
! The data structure for structure
!***********************************************  
  integer :: isliding, fin_num1, fin_num2, component_id
  integer :: istep, Rstep, b, offset  
  integer :: i,j,k,ii,jj,kk, dd, ibld, iturb,nn, itemp
  real(8) :: Rmt(3,3),Rmdt(3,3),Rmddt(3,3)
  real(8), allocatable :: dshalpha_NM(:,:)
  real(8) :: dis

  ! NOT related to dshalpha, this is for the shell
  real(8), allocatable :: ushalpha_im(:,:)

!  real(8), allocatable :: tmpIEN(:,:)
  character(len=40) :: iname,fname
  character(len=10) :: cname,ccname
  real(8) :: time1, time_move
  ! Initialize MPI  
  call MPI_INIT(                               mpi_err)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, numnodes, mpi_err)
  call MPI_COMM_RANK(MPI_COMM_WORLD, myid    , mpi_err)
  ismaster = myid.eq.mpi_master
  if (ismaster) then
    write(*,*) "============================================"
    write(*,*) "Number of MPI nodes = ", numnodes 
    write(*,*) "============================================"
  endif

 Identity1 = 0.0d0
 Identity1(1,1) = 1.0d0
 Identity1(2,2) = 1.0d0
 Identity1(3,3) = 1.0d0
  ! From FSI
 solshell = (myid.eq.0)
 nonmatch = .false.
 rotation     =    .false.
 move = .false.
 solfsi = .false.
 NBlade = 1
 offset = 1

  NM%NTurb = 1 
  NM_eps%NTurb = 1
  ! Read parameters  
  if (ismaster) write(*,*) "~~~~~~~~~Get run parameters"
  call getparam() ! This is for floating boat
  ! Read mesh 
  if(ismaster) then
      write(*,*) "NS_BC_Type"
      do i = 1, maxval(BC_FaceID(:))
      write(*,*) i,NS_BC_Type(i,:)
      enddo
      write(*,*) "Mesh_BC_Type"
      do i = 1, maxval(BC_FaceID(:))
      write(*,*) i,Mesh_BC_Type(i,:)
     enddo
      write(*,*) "LSC_BC_Type"
     do i = 1, maxval(BC_FaceID(:))
       write(*,*) i,LSC_BC_Type(i)
     enddo
     write(*,*) "BCugType"
      do i = 1, maxval(BC_FaceID(:))
      write(*,*) i,BCugType(i,:)
     enddo
     write(*,*) "BCugValu"
     do i = 1, maxval(BC_FaceID(:))
      write(*,*) i,BCugValu(i,:)
     enddo
  endif

 
  if (ismaster) write(*,*) "~~~~~~~~~Read mesh and communication data"  
  call readMesh(mesh_fluid, trim(meshname) //"."// trim(cname(myid+1))//".dat")
  call getMeshExtremes(mesh_fluid)
  ! Generate MPI-communication Data   
  call ctypes(mesh_fluid%NNODE) 
  allocate(xO(mesh_fluid%NNODE,3))
  !if (solshell) then ! Jinhui: commnented out by jinhui
  !Read shell mesh  
  if(ismaster) write(*,*) "~~~~~~~~~Read shell and beam mesh"
   call input_shell_blade(mesh_fluid%NSD, SH, BEA, mNRB_BEA, NRB_BEA)
   call input_shell_blade(mesh_fluid%NSD, SH_eps, BEA_eps, mNRB_BEA_eps, NRB_BEA_eps)
  if(ismaster) write(*,*) "~~~~~~~~~Finish reading shell and beam mesh"

!SH_eps  = SH
!NM_eps  = NM
!NRB_BEA_eps = NRB_BEA
!BEA_eps = BEA
!mNRB_BEA_eps = mNRB_BEA
!STRU_eps = STRU ! Jinhui: Just to get the data structure
!endif
!end if  ! Jinhui: commnented out by jinhui
!For the first three processors, get the number of
!fem shell nodes of the blades

  blade(:)%NNODE = 0 
  blade(1)%NNODE = SH%FEM%NNODE
 !  write(*,*) myid, blade(1)%NNODE
 !  endif ! Jinhui: commnented out by jinhui

  ! Broadcast them to all processors for the purpose of 
  ! allocating arrays

!**********************************************************************************************************************************
 !do ibld = 1, NBlade
 ! call MPI_BCAST(blade(ibld)%NNODE, 1, MPI_INTEGER, &
 !                  ibld-1, MPI_COMM_WORLD, mpi_err)
 ! enddo
   
!   call MPI_BCAST(SH%NRB%NNODE, 1, MPI_INTEGER, &
!                   0, MPI_COMM_WORLD, mpi_err)
!   call MPI_BCAST(NRB_BEA%NNODE, 1, MPI_INTEGER, &
!                   0, MPI_COMM_WORLD, mpi_err)

  ! Jinhui: in order to do the mapping
!**********************************************************************************************************************************
  !----------------------------------------------------
  ! find how many surfaces numbered before the object
  !----------------------------------------------------
  SH%bmap = 0
  do i = 1, mesh_fluid%NBOUND
    if (mesh_fluid%bound(i)%Face_ID <= 20) SH%bmap = SH%bmap + 1
  end do
  if(ismaster) write(*,*) "~~~~~~~~~", SH%bmap, "surfaces numbered before the object"

 
  if (nonmatch) then
    call input_shell_nmb(mesh_fluid, mesh_fluid%NSD, NM, Center_Rot)
  ! call input_shell_nmb(mesh_fluid, mesh_fluid%NSD, NM_eps, Center_Rot)
  end if
  ! I will decide whether this part is kept
  allocate(SH%Nnewt(NL_itermax))
  SH%Nnewt = 1

  ! begin inputs for immerso 
  call surf_input_line_new(mesh_fluid%NSD, SUR)
  if(ismaster) write(*,*) "~~~~~~~~~Finish reading immersed surface"

  allocate(mesh_fluid%elmgplists(mesh_fluid%nelem))

  
  ! each element of this array needs to be initialized individually with
  ! some default capacity; choose ngausstet (TODO: how to extend to prisms?)
  ! (the lists are still empty, but this sets the size field and allocates
  ! the initial capcity)
  do i=1,mesh_fluid%nelem
     call quadptlist_allocate(mesh_fluid%elmgplists(i), ngausstet)
  enddo

  call mpi_barrier(mpi_comm_world,mpi_err)
  ! immerso related
  mesh_fluid%IM_flag = use_IM
  allocate(mesh_fluid%includeflags(mesh_fluid%nnode))
  mesh_fluid%includeflags = 0.0d0
  if(ismaster) write(*,*) 'Generating fluid element integration points...'
  call genAQ(sur, mesh_fluid%elmgplists, mesh_fluid%nelem, mesh_fluid%nnode, &
  	         mesh_fluid%nsd, mesh_fluid%maxnshl, mesh_fluid%xg, mesh_fluid%ien)
  if(ismaster) write(*,*) 'Done.'

  ! placeholder for array of zeroes the size of ushalpha
  allocate(ushalpha_im(SUR%nnode,3))
  ushalpha_im = 0.0d0
  ! didn't change name, but this is for trianglated surface mesh, too lazy to...
  call shell_gp_info_im(SUR,ushalpha_im,3)

  if(ismaster) write(*,*) 'Locating surface points in fluid mesh...'
  call locate_pts_in_bg(sur);
  if(ismaster) write(*,*) 'Done.'

!  call FindDofMap(SH,NRB_BEA, mesh_fluid, offset)
  ! Jinhui: This is the new mapping in order to do matrix-free

  ! Generate Sparse Structures   
  call genSparseStruct(sparseMat, mesh_fluid)
  if(ismaster) write(*,*) "~~~~~~~~~Generating sparse structure of fluid"  
  ! Generate the Sparse Structure for fluid mechanics
  ! Allocate Matrices and Vectors     
  call allocData(mesh_fluid%NNODE, sparseMat%icnt, mesh_fluid%NSD) 
  if(ismaster) write(*,*) "~~~~~~~~~Allocating matrices and vectors of fluid"
  ! Allocate the the data for fluid 
  call allocate_LRhs(NRB_BEA, SH%NRB, mesh_fluid%NSD, STRU)
  call allocate_LRhs(NRB_BEA_eps, SH_eps%NRB, mesh_fluid%NSD, STRU_eps)
  if(ismaster) write(*,*) "~~~~~~~~~~~Generating sparse structure of structure"    
  call allocMatVec(SH,sparseMat%icnt,mesh_fluid%NSD,mesh_fluid%NNODE,mesh_fluid%NELEM,NRB_BEA, blade, NM, 1)
  call allocMatVec(SH_eps,sparseMat%icnt,mesh_fluid%NSD,mesh_fluid%NNODE,mesh_fluid%NELEM,NRB_BEA_eps, blade_eps, NM, 0) 
  if(ismaster) write(*,*) "~~~~~~~~Allocating matrices and vectors of structure"   
  ! Read step file
  if(ismaster) then
 ! write(*,*) "undeformed:",NRB_BEA%B_NET_U(2,:)
 ! write(*,*) "deformed:",NRB_BEA%B_NET_D(2,:)
 ! stop
endif

  b = 7
  call getStep(Rstep)
  if(ismaster) write(*,*) "~~~~~~~~~Getting the running step: ",Rstep 
  SH%FEM%already_f2s = .true.
  allocate(SH%FEM%col_f2s(SH%FEM%NNODE+1), SH%FEM%row_f2s(SH%FEM%NNODE*50*SH%FEM%maxNSHL))
  allocate(ash_base(SH%FEM%NNODE,3))
  allocate(ush_base(SH%FEM%NNODE,3))
  allocate(dsh_base(SH%FEM%NNODE,3))
  allocate(ash_pertubation(SH%FEM%NNODE,3))

  allocate(NRmat(3,3,mesh_fluid%NNODE),NRdot(3,3,mesh_fluid%NNODE), NRddt(3,3,mesh_fluid%NNODE),&
                     NRmatOld(3,3,mesh_fluid%NNODE), NRdotOld(3,3,mesh_fluid%NNODE),NRddtOld(3,3,mesh_fluid%NNODE))

  ash_base = 0d0
  ush_base = 0d0
  dsh_base = 0d0
  ash_pertubation = 0d0
  !  if (numnodes > 1) call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)
  !  if (numnodes > 1) call MPI_BCAST(xrotOld, 3, MPI_DOUBLE_PRECISION, 0, &
  !                                   MPI_COMM_WORLD, mpi_err)

  if (Rstep == 0) then
    ! Generate initial condition  
    time = 0d0
    call initState(old) ! init the solution of fluid
    if(ismaster) write(*,*) "~~~~~~~~~~~Initial fluid solution"
    call FinIC_move(old, mesh_fluid) ! init the solution of fluid in case of fin configuration
    if(ismaster) write(*,*) "~~~~~~~~~~~Initial the fluid solution in the case of turbine configuration"
    call generateIC(SH,NM)
    call ComputeNodalRhoMu(old, mesh_fluid)
    if(ismaster) write(*,*)  "~~~~~~~~~~~Initial the turbine struture configuration"
  else
    ! Read solution form file    
    call readState(old, Rstep, time) 
    new = old
    if(ismaster) write(*,*) "~~~~~~~~~~~read current solution of fluid" 
   ! if (nonmatch) call readShellSol_NM(Rstep, NM)
    if (solshell) then     
      call readShellSol(Rstep, SH)
      if(ismaster) write(*,*) "~~~~~~~~~~~read current solution of structure" 
    endif
 ! Will be replaced by the new structure 
  endif 
 ! Compute density, viscosity and body inertia
 ! call ComputeNodalRhoMu(old, mesh_fluid)  ! Jinhui: No structure involved
  if(ismaster) write(*,*) "~~~~~~~~~~~compute nodal density, viscosity of of the mesh" 

!*****************************	**************************************************
 ! Will be replaced by the new structure , do not need this routine anymore
  ! Output    
  if (Rstep == 0) then
    call writeState(old, Rstep, time)
    if(ismaster) write(*,*) "~~~~~~~~~~~write the fluid solution at time = 0 ~~~~~~~~~~~~~~~" 
 endif
! call Flush() 
  !------------------------------------------
  ! Loop over time steps
  !------------------------------------------
!  damping_reading = 0d0
  flag_wave = .true.
  GGG = GGG_reading
  do istep = Rstep+1, Nstep
   time = time + Delt
    if (istep >= move_step) then
    move = .true.
    time1 = time-move_time  
    theta_base = angle_acl*time1
    thetd_base = angle_acl
    thedd_base = 0d0
 !   if(time1<TTT) then      
 !    theta_base  = -2d0*(TTT*angle_acl/pi)*cos(pi*time1/(2d0*TTT))+2d0*(TTT*angle_acl/pi)
 !    thetd_base  = angle_acl*sin(time1*pi/(2*TTT))
 !    thedd_base  = (angle_acl*pi)*cos(time1*pi/(2*TTT))/(2d0*TTT)
 !   else   
 !    theta_base  = 2d0*(TTT*angle_acl/pi)+angle*(time1-TTT)
 !    thetd_base  = angle_acl
 !    thedd_base  = 0d0
 !   endif
    theta(1) =   0d0
    thetd(1) =   0d0
    thedd(1) =   0d0

    theta(2) =   theta_base
    thetd(2) =   thetd_base
    thedd(2) =   thedd_base


    else
      move = .false.
      time1 = 0d0
      thetd_base = 0d0
      thedd_base = 0d0
      theta_base = 0d0
    endif 



    call get_Rmat(theta(1), thetd(1), thedd(1), Rmat(1,:,:), Rdot(1,:,:), Rddt(1,:,:))
    call get_Rmat(thetaold(1), thetdold(1), theddold(1), RmatOld(1,:,:), RdotOld(1,:,:), RddtOld(1,:,:))

    call get_Rmat(theta(2), thetd(2), thedd(2), Rmat(2,:,:), Rdot(2,:,:), Rddt(2,:,:))
    call get_Rmat(thetaold(2), thetdold(2), theddold(2), RmatOld(2,:,:), RdotOld(2,:,:), RddtOld(2,:,:))

    NRmat = 0.0d0; NRdot = 0.0d0; NRddt = 0.0d0
    NRmatOld = 0.0d0; NRdotOld = 0.0d0; NRddtOld = 0.0d0


    do nn = 1, mesh_fluid%NNODE
      if (mesh_fluid%Coord_ID(nn) == 31) then
     ! The volume containing the turbine
        NRmat(:,:,nn) = Rmat(1,:,:)
        NRdot(:,:,nn) = Rdot(1,:,:)
        NRddt(:,:,nn) = Rddt(1,:,:)
        NRmatOld(:,:,nn) = RmatOld(1,:,:)
        NRdotOld(:,:,nn) = RdotOld(1,:,:)
        NRddtOld(:,:,nn) = RddtOld(1,:,:)
     endif
      if (mesh_fluid%Coord_ID(nn) == 32) then
     ! The volume doesn't rotate
        NRmat(:,:,nn) = Rmat(2,:,:)
        NRdot(:,:,nn) = Rdot(2,:,:)
        NRddt(:,:,nn) = Rddt(2,:,:)
        NRmatOld(:,:,nn) = RmatOld(2,:,:)
        NRdotOld(:,:,nn) = RdotOld(2,:,:)
        NRddtOld(:,:,nn) = RddtOld(2,:,:)
      end if
    end do

    if (ismaster) then
      write(*,'(60("="))')
      write(*,"(a,1x,I8,1x,ES14.6)") "Time Step Number:", istep, time
      write(*,'(60("="))')
      write(*,*) "Current theta is:",  theta_base*180.0d0/pi
      write(*,*) "Current velocity is:",thetd_base*180.0d0/pi
      write(*,*) "Current acceleration is:",  thedd_base*180.0d0/pi
      write(*,'(60("="))')
    end if

    !-----------------------------------
    ! Predictor
    !-----------------------------------
    call predictState(new, old)
   ! write(*,*) "max diff beginning:", maxval(abs(new%dg(:,:) - old%dg(:,:)))
     if (solshell ) then
       SH%FEM%ush  = SH%FEM%ushold
       SH%FEM%ash = (gami-1.0d0)/gami*SH%FEM%ashold
       SH%FEM%dsh   = SH%FEM%dshold + Delt*SH%FEM%ushold + &
           (gami-2.0d0*beti)/(2.0d0*gami)*Delt*Delt*SH%FEM%ashold

       SH%NRB%ush  = SH%NRB%ushold
       SH%NRB%ash = (gami-1.0d0)/gami*SH%NRB%ashold
       SH%NRB%dsh   = SH%NRB%dshold + Delt*SH%NRB%ushold + &
           (gami-2.0d0*beti)/(2.0d0*gami)*Delt*Delt*SH%NRB%ashold  

       call setBCs_turbine_sliding(SH,NRB_BEA)
    endif
  ! broadcast such that all processors will have the solutions

    if (nonmatch) then
      do i = 1, NM%NTurb
        call predictor_rot_shell(NM%FEM(i,1), Rmat(i,:,:), Rdot(i,:,:), Rddt(i,:,:), RmatOld(i,:,:), &
                                 RdotOld(i,:,:), RddtOld(i,:,:), Delt, beti, gami, Center_Rot(i,:))

        if(ismaster) write(*,*) "Theta for DG:", asin(Rmat(i,2,3))*180/pi, asin(Rmat(i+1,2,3))*180/pi

        call predictor_rot_shell(NM%FEM(i,2), Rmat(i+1,:,:), Rdot(i+1,:,:), Rddt(i+1,:,:), RmatOld(i+1,:,:), &
                                 RdotOld(i+1,:,:), RddtOld(i+1,:,:), Delt, beti, gami, Center_Rot(i+1,:))
      end do
    end if

    if (nonmatch) then
      do i = 1, NM%NTurb
        do j = 1, 2
   !      allocate(dshAlpha_NM(NM%FEM(i,j)%NNODE,3))
   !      dshAlpha_NM = 0.0d0
   !      dshAlpha_NM = NM%FEM(i,j)%dshOld + alfi*(NM%FEM(i,j)%dsh-NM%FEM(i,j)%dshOld)
   !      NM%FEM(i,j)%B_NET_D(:,1:3) = NM%FEM(i,j)%B_NET(:,1:3) + dshAlpha_NM(:,:)
          NM%FEM(i,j)%B_NET_D(:,1:3) = NM%FEM(i,j)%B_NET(:,1:3)  + NM%FEM(i,j)%dsh(:,1:3)
   !       NM_eps%FEM(i,j)%B_NET_D(:,1:3) = NM_eps%FEM(i,j)%B_NET(:,1:3)  + NM_eps%FEM(i,j)%dsh(:,1:3)
   !       deallocate( dshAlpha_NM )
        end do
      end do

      do i = 1, NM%NTurb
        do j = 1, 2
           NM%FEM(i,j)%CLE = 0; NM%FEM(i,j)%CLP = 0.0d0
        end do
      end do

      do i = 1, NM%NTurb
        call find_close_point_FEM_FEM(NM%FEM(i,1), NM%FEM(i,2), 3, istep, Rstep, 20)
        call find_close_point_FEM_FEM(NM%FEM(i,2), NM%FEM(i,1), 3, istep, Rstep, 20)
      end do
    end if

    !----------------------------------- 
    ! Solve problems   
    !----------------------------------- 
    
    call solveMono(mesh_fluid, SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, blade, sur)
    !-----------------------------------         
    ! Print solution data
    !-----------------------------------
    call IntPostPro(istep)
   
    !-----------------------------------         
    ! Output results    
    !----------------------------------- 
   
    if (mod(istep,ifq) == 0) then
      call writeState(new, istep, time)
      call writeStep(istep)  
!      call writeforce(istep, time) 
      call writeVort(new,istep) 
    endif

    if (mod(istep,5) == 0) then
      call writeforce(istep, time) 
    endif
      ! Output fluid solution
    if (mod(istep,ifq) == 0 .and. solshell) &
      call writeShellSol(istep, SH)
   ! Output shell solution
   ! if (mod(istep,ifq) == 0 .and. ismaster) &
   !   call writetq(istep)
   !  Output torque
   ! if (mod(istep,ifq) == 0 .and. solshell) &
   !   call writeTip(istep, SH)
   ! Output tip
    if (mod(istep,ifq) == 0 .and. ismaster .and. nonmatch) &
      call writeShellSol_NM(istep, NM)
    ! Output Non-matching results
    !-----------------------------------            
    ! Update    
    !-----------------------------------     
    old = new
    call update_sol(SH, NM, NRB_BEA)  
  end do
   
  ! Deallocate  
  if (ismaster) then 
     write(*,*) "Deallocating matrices and vectors"  
  endif
  deallocate(NRmat, NRdot, NRddt, NRmatOld, NRdotOld, NRddtOld)
  call deallocData()
  call deallocParam()
  call deallocMatVec()
  call MPI_FINALIZE(mpi_err)


end program FloatingElasticBody

! Update B_BEAion for the structure and nonmatching part
 subroutine update_sol(SH, NM, NRB_BEA)

  use aAdjKeep
  use commonvars
  use mpi
  use defs_shell
  use types_beam
  use params
  use globdata
  implicit none
 
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  type(mesh_beam), intent(inout) :: NRB_BEA
  integer :: i, j 
  real(8) :: Xmin, Xmax, Ymin, Ymax, Zmin, Zmax
  integer :: iXmin, iXmax,iYmin, iYmax,iZmin,iZmax

  real(8) :: Xmin1, Xmax1, Ymin1, Ymax1, Zmin1, Zmax1
  integer :: iXmin1,  iXmax1, iYmin1,  iYmax1, iZmin1, iZmax1
  ! Update Old Quantities     

  if (solshell) then
!    SH%TSP%dshOld = SH%TSP%dsh
!    SH%TSP%ushOld = SH%TSP%ush
!    SH%TSP%ashOld = SH%TSP%ash

    SH%NRB%dshOld = SH%NRB%dsh
    SH%NRB%ushOld = SH%NRB%ush
    SH%NRB%ashOld = SH%NRB%ash

    SH%FEM%dshOld = SH%FEM%dsh
    SH%FEM%ushOld = SH%FEM%ush
    SH%FEM%ashOld = SH%FEM%ash

    NRB_BEA%B_NET_D_old = NRB_BEA%B_NET_D
    NRB_BEA%B_NET_Dt_old = NRB_BEA%B_NET_Dt
    NRB_BEA%B_NET_DDt_old = NRB_BEA%B_NET_DDt

  end if


  if (nonmatch) then
    do i = 1, NM%NTurb
      do j = 1, 2
        NM%FEM(i,j)%dshOld = NM%FEM(i,j)%dsh
        NM%FEM(i,j)%ushOld = NM%FEM(i,j)%ush
        NM%FEM(i,j)%ashOld = NM%FEM(i,j)%ash
      end do
    end do
  end if
end subroutine update_sol


 !----------------------------------------------------------------------   
 ! Use commu to find out which nodes are shared between procs
 ! Only use dofs that are unique to master
 !----------------------------------------------------------------------


!======================================================================
! apply the exact rotation to the predictor
!======================================================================
subroutine predictor_rot_shell(SHL, Rmat, Rdot, Rddt, RmatOld, RdotOld, &
                               RddtOld, Delt, beti, gami, Rotation_Rot)
  use defs_shell

  implicit none

  type(mesh), intent(inout) :: SHL
  real(8),    intent(in)    :: Rmat(3,3),    Rdot(3,3),    Rddt(3,3),   &
                               RmatOld(3,3), RdotOld(3,3), RddtOld(3,3), &
                               Delt, beti, gami, Rotation_Rot(3)
  integer :: nn, dd

  do nn = 1, SHL%NNODE
    do dd = 1, 3
          SHL%dsh(nn,dd) = sum(Rmat(dd,:)*(SHL%B_NET(nn,1:3)-Rotation_Rot(1:3)))
          SHL%ush(nn,dd) = sum(Rdot(dd,:)*(SHL%B_NET(nn,1:3)-Rotation_Rot(1:3)))
          SHL%ash(nn,dd) = sum(Rddt(dd,:)*(SHL%B_NET(nn,1:3)-Rotation_Rot(1:3)))
   enddo
 enddo


end subroutine predictor_rot_shell
