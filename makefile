NAME := imga_fs_jinhui

##################################
# System definitions
##################################
include ../defs/select
include ../defs/$(defs)

##################################
#  Files
##################################
include file.list

OFILES = $(MODFILES:.f90=.o)\
         $(F90FILES:.f90=.o)\
         $(F77FILES:.f=.o)

OBJ = $(MODFILES:%.f90=obj/%.o)\
      $(F90FILES:%.f90=obj/%.o)\
      $(F77FILES:%.f=obj/%.o)

DOBJ = $(MODFILES:%.f90=obj/%.dbg.o)\
       $(F90FILES:%.f90=obj/%.dbg.o)\
       $(F77FILES:%.f=obj/%.dbg.o)
       
2DOBJ = $(MODFILES:%.f90=obj/%.2d.o)\
       $(F90FILES:%.f90=obj/%.2d.o)\
       $(F77FILES:%.f=obj/%.2d.o)

##################################
# Rules
##################################
default: $(DEST)/$(NAME).exe 
debug: $(DEST)/$(NAME).dbg.exe 

2d: $(DEST)/$(NAME).2d.exe 

info:
	@echo "Host  :" $(host) 
	@echo "Defs  :" $(defs) 
	@echo "NAME  :" $(NAME)
	@echo "DEST  :" $(DEST) 
	@echo "FCOMP :" $(FCOMP)  
	@echo "FLAGS :" $(FLAGS) 
	@echo "DFLAGS:" $(DFLAGS) 
	@echo "LIBS  :" $(LIBS) 	
	@echo "MDF   :" $(MDFILES) 

$(DEST)/$(NAME).exe:$(OBJ) | $(DEST)
	rm -rf $(DEST)/$(NAME).exe
	$(FCOMP)   $(OBJ) $(LIBS) -o  $(DEST)/$(NAME).exe
	ls -lh $(DEST)/$(NAME).exe

$(DEST)/$(NAME).dbg.exe:  $(DOBJ) | $(DEST)
	rm -rf $(DEST)/$(NAME).dbg.exe
	$(FCOMP)   $(DOBJ) $(LIBS) $(DFLAGS) -o  $(DEST)/$(NAME).dbg.exe
	@ls -lh $(DEST)/$(NAME).dbg.exe
        
$(DEST)/$(NAME).2d.exe:  $(2DOBJ) | $(DEST)
	rm -rf $(DEST)/$(NAME).2d.exe
	$(FCOMP)   $(2DOBJ) $(LIBS) $(DFLAGS) -o  $(DEST)/$(NAME).2d.exe
	@ls -lh $(DEST)/$(NAME).2d.exe
        
obj:
	mkdir   obj 

$(DEST):	
	mkdir  -p  $(DEST)
	
clean:
	rm -rf obj *.mod $(DEST)/$(NAME).exe $(DEST)/$(NAME).dbg.exe

# Optimal 	
obj/%.o: %.f90 | obj
	$(FCOMP) $(FLAGS) -c $*.f90 -o obj/$*.o

obj/%.o: %.f | obj
	$(FCOMP) $(FLAGS) -c $*.f -o obj/$*.o

# Debug
obj/%.dbg.o: %.f90 | obj
	$(FCOMP) $(DFLAGS) -c $*.f90 -o obj/$*.dbg.o

obj/%.dbg.o: %.f | obj
	$(FCOMP) $(DFLAGS) -c $*.f  -o obj/$*.dbg.o
  
# Debug  
obj/%.2d.o: %.f90 | obj 
	$(FCOMP) -DUSE2D $(FLAGS) -c $*.f90 -o obj/$*.2d.o
       
obj/%.2d.o: %.f | obj  
	$(FCOMP) -DUSE2D $(FLAGS) -c $*.f  -o obj/$*.2d.o
