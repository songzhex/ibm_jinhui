!======================================================================
!
!======================================================================  
subroutine allocMatVec(SH,icnt,NSD,NNODE,NELEM, NRB_BEA, blade1, NM, flag)
  
  use aAdjKeep
  use commonvars
  use defs_shell
  use types_beam
  use params
  use globdata
  implicit none
    
  type(shell_bld), intent(inout) :: SH
  type(mesh_beam), intent(inout) :: NRB_BEA
  type(shell_nmb), intent(inout) :: NM
  type(mesh), intent(inout) :: blade1(3)
  integer :: NGAUSSMAX, i,  j, ibld
  integer,intent(in)::icnt,NSD,NNODE,NELEM 
  integer, intent(in) :: flag

  !--------------------------------------------
  ! allocation for shell
  !--------------------------------------------
!  if (solshell) then  ! Jinhui: attention
    ! allocate the shell RHS and LHS
!  allocate(SH%RHSG(SH%TSP%NNODE,NSD), &
!           SH%RHSG_EXT(SH%TSP%NNODE,NSD), &
!           SH%RHSG_GRA(SH%TSP%NNODE,NSD))

  do ibld=1,Nblade
    allocate(blade1(ibld)%ash(blade1(ibld)%NNODE,NSD))
    allocate(blade1(ibld)%ush(blade1(ibld)%NNODE,NSD))
    allocate(blade1(ibld)%dsh(blade1(ibld)%NNODE,NSD))
    allocate(blade1(ibld)%FORCE(blade1(ibld)%NNODE,NSD))
  enddo

  allocate(SH%RHSG(SH%NRB%NNODE,NSD), &
           SH%RHSG_EXT(SH%NRB%NNODE,NSD), &
           SH%RHSG_GRA(SH%NRB%NNODE,NSD))

    allocate(SH%LHSK(NSD*NSD,SH%icnt))
    SH%RHSG = 0.0d0; SH%RHSG_EXT = 0.0d0; SH%RHSG_GRA = 0.0d0
    SH%LHSK = 0.0d0

    ! Allocate solution variables for SHELL
    allocate(SH%FEM%dsh(SH%FEM%NNODE,NSD), SH%FEM%dshOld(SH%FEM%NNODE,NSD), &
             SH%FEM%ush(SH%FEM%NNODE,NSD), SH%FEM%ushOld(SH%FEM%NNODE,NSD), &
             SH%FEM%ash(SH%FEM%NNODE,NSD), SH%FEM%ashOld(SH%FEM%NNODE,NSD))
    SH%FEM%dsh = 0.0d0; SH%FEM%dshOld = 0.0d0
    SH%FEM%ush = 0.0d0; SH%FEM%ushOld = 0.0d0
    SH%FEM%ash = 0.0d0; SH%FEM%ashOld = 0.0d0

!  allocate(SH%TSP%dsh(SH%TSP%NNODE,NSD), SH%TSP%dshOld(SH%TSP%NNODE,NSD), &
!           SH%TSP%ush(SH%TSP%NNODE,NSD), SH%TSP%ushOld(SH%TSP%NNODE,NSD), &
!           SH%TSP%ash(SH%TSP%NNODE,NSD), SH%TSP%ashOld(SH%TSP%NNODE,NSD))
!  SH%TSP%dsh = 0.0d0; SH%TSP%dshOld = 0.0d0
!  SH%TSP%ush = 0.0d0; SH%TSP%ushOld = 0.0d0
!  SH%TSP%ash = 0.0d0; SH%TSP%ashOld = 0.0d0

    allocate(SH%NRB%dsh(SH%NRB%NNODE,NSD), SH%NRB%dshOld(SH%NRB%NNODE,NSD), &
           SH%NRB%ush(SH%NRB%NNODE,NSD), SH%NRB%ushOld(SH%NRB%NNODE,NSD), &
           SH%NRB%ash(SH%NRB%NNODE,NSD), SH%NRB%ashOld(SH%NRB%NNODE,NSD))
    SH%NRB%dsh = 0.0d0; SH%NRB%dshOld = 0.0d0
    SH%NRB%ush = 0.0d0; SH%NRB%ushOld = 0.0d0
    SH%NRB%ash = 0.0d0; SH%NRB%ashOld = 0.0d0
!  end if ! Jinhui: attention please

  !--------------------------------------------
  ! Allocate solution variables for Non-matching boundaries
  !--------------------------------------------
  if ((nonmatch ).and. (flag == 1)) then
    do i = 1, NM%NTurb
      do j = 1, 2
        allocate(NM%FEM(i,j)%dsh(NM%FEM(i,j)%NNODE,NSD), NM%FEM(i,j)%dshOld(NM%FEM(i,j)%NNODE,NSD), &
                 NM%FEM(i,j)%ush(NM%FEM(i,j)%NNODE,NSD), NM%FEM(i,j)%ushOld(NM%FEM(i,j)%NNODE,NSD), &
                 NM%FEM(i,j)%ash(NM%FEM(i,j)%NNODE,NSD), NM%FEM(i,j)%ashOld(NM%FEM(i,j)%NNODE,NSD))
        NM%FEM(i,j)%dsh = 0.0d0; NM%FEM(i,j)%dshOld = 0.0d0
        NM%FEM(i,j)%ush = 0.0d0; NM%FEM(i,j)%ushOld = 0.0d0
        NM%FEM(i,j)%ash = 0.0d0; NM%FEM(i,j)%ashOld = 0.0d0
      end do
    end do 
    allocate(RHSGu(NNODE,NSD), RHSGm(NNODE,NSD), &
           RHSGp(NNODE), RHSGls(NNODE))
  end if
end subroutine allocMatVec 

!======================================================================
!
!======================================================================  

subroutine allocate_LRhs(NRB_BEA, NRB_SHL, NSD, STRU)
 
  use params
  use globdata
  use types_beam
  use defs_shell
  use types_structure
  implicit none

  type(mesh_beam),  intent(in)    :: NRB_BEA
  type(mesh),  intent(in)    :: NRB_SHL
  type(structure)   :: STRU

  integer,     intent(in)    :: NSD
  integer :: ier

    allocate(STRU%col(NRB_SHL%NNODE+1), STRU%row(NRB_SHL%NNODE*50*max(NRB_SHL%maxNSHL,NRB_BEA%maxNSHL)), stat=ier)
    if (ier /= 0) stop 'Allocation Error: col'    
    STRU%col = 0; STRU%row = 0  
  !write (*,*) 'icnt=', STRU%icnt  
    call genSparStruc_STRU(NRB_BEA%NEL, NRB_BEA%NNODE, NRB_BEA%maxNSHL, NRB_BEA%IEN, NRB_BEA%NSHL, &
                           NRB_SHL%NEL, NRB_SHL%NNODE, NRB_SHL%maxNSHL, NRB_SHL%IEN, NRB_SHL%NSHL, &
                           STRU%col, STRU%row, STRU%icnt)
  
  !write (*,*) 'icnt=', STRU%icnt
  ! allocate the global RHS and LHS
  allocate(STRU%RHSG(NRB_SHL%NNODE,NSD), &
           STRU%RHSG_beam(NRB_SHL%NNODE,NSD), &
           STRU%RHSG_shell(NRB_SHL%NNODE,NSD), &
           STRU%RHSG_EXT(NRB_SHL%NNODE,NSD), &
           STRU%RHSG_GRA(NRB_SHL%NNODE,NSD))
  STRU%RHSG     = 0.0d0
  STRU%RHSG_beam     = 0.0d0
  STRU%RHSG_shell     = 0.0d0
  STRU%RHSG_EXT = 0.0d0
  STRU%RHSG_GRA = 0.0d0


  allocate(STRU%LHSK(NSD*NSD,STRU%icnt),STRU%LHSK_beam(NSD*NSD,STRU%icnt), STRU%LHSK_shell(NSD*NSD,STRU%icnt))
  STRU%LHSK = 0.0d0
  STRU%LHSK_shell = 0.0d0
  STRU%LHSK_beam = 0.0d0




  allocate(STRU%dg(NRB_SHL%NNODE,NSD),  &
           STRU%tg(NRB_SHL%NNODE),     STRU%mg(NRB_SHL%NNODE),  &
           STRU%dl(NRB_SHL%NNODE,NSD), STRU%ddu(NRB_SHL%NNODE,NSD) )
  STRU%dg = 0.0d0; STRU%tg = 0.0d0; STRU%mg = 0.0d0
  STRU%dl = 0.0d0; STRU%ddu = 0.0d0

end subroutine allocate_LRhs




!======================================================================
!
!======================================================================  
subroutine deallocMatVec 
  
  use aAdjKeep
  use mpi
  use commonvars
  use params
  use globdata
  implicit none
  integer :: ibld
  if (allocated(RHSGu)) then
     deallocate(RHSGu, RHSGm,RHSGp, RHSGls) 
  end if
  do ibld = 1, NBlade
    deallocate(blade(ibld)%ash,blade(ibld)%ush,blade(ibld)%dsh,blade(ibld)%FORCE)
  enddo  
  ! Conditional deallocations  
  !if (numnodes.gt.1) deallocate(ilworku) ! Jinhui

end subroutine deallocMatVec 
