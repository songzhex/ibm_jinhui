subroutine combine_beam_shell_nrb(NSD, NRB_BEA, NRB_SHL)

  use types_beam
  use defs_shell
  use commonpars
  use mpi
  implicit none

  type(mesh_beam),    intent(inout)    :: NRB_BEA
  type(mesh),    intent(inout)    :: NRB_SHL
  integer,     intent(in)    :: NSD

  integer :: ier, i, ip, p, mcp, nnode, nel, nshl, eloc, eglob
  real(8) :: zero_vec(1,3)
  
  zero_vec(1,:) = 0.0d0
  !write (*,*) 'NRB_SHL%NNODE=', NRB_SHL%NNODE
  do i = 1, NRB_SHL%NNODE
    if ( sqrt(sum((NRB_SHL%B_NET(i,1:3)-NRB_BEA%B_NET(i,1:3))**2))  <=1.0d-12 ) then
        !write (*,*) 'case 1'
    elseif (sqrt(sum((NRB_SHL%B_NET(i,1:3)-zero_vec(1,1:3))**2))<=1.0d-12) then
        !write (*,*) 'case 2'
        NRB_SHL%B_NET(i,:)=NRB_BEA%B_NET(i,:)
    else
      !write (*,*) 'case 3'
      NRB_BEA%B_NET(i,:)=NRB_SHL%B_NET(i,:)
    endif
  end do

  NRB_BEA%B_NET_U = NRB_BEA%B_NET
  NRB_BEA%B_NET_D = NRB_BEA%B_NET
 ! NRB_BEA%B_NET_D_old = NRB_BEA%B_NET

  NRB_SHL%B_NET_U = NRB_SHL%B_NET
  NRB_SHL%B_NET_D = NRB_SHL%B_NET
 ! NRB_SHL%B_NET_D_old = NRB_SHL%B_NET

  ! use the coordinates to setup IBC_SH because one also
  ! need to take the bending strips into consideration
  do i = 1, NRB_BEA%NNODE
    ! one end of cable
    if (abs(NRB_BEA%B_NET(i,3)+350.0d0)<=1.0d-12 ) then
    !if ( abs(NRB_BEA%B_NET(i,3)+320.0d0)<=1.0d-12) then
    !original radius is 3.25
      NRB_SHL%IBC(i,:) = 1
      NRB_BEA%IBC(i,:) = 1
      !write (*,*) 'root node =', i
    end if
  end do



end subroutine combine_beam_shell_nrb
