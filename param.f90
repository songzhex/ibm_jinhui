
module params
  use mpi
  implicit none
  save

  type shape_function

    integer :: FACE_ID
    integer :: NFACE  
    integer :: NSHL
    integer :: NGAUSSB
  
    real(8), allocatable :: shu(:,:,:) 
    real(8), allocatable :: shug(:,:,:,:) 
    real(8), allocatable :: nor(:,:,:) 
    real(8), allocatable :: Detjb(:,:) 
    real(8), allocatable :: dxidx(:,:,:,:)

  end type shape_function
  ! Time step 
  real(8) :: Delt, Dtgl, rhoinf, beti, gami, alfi, almi, &
             mbet, mgam, ogam,obet, malf, malm, lambda, time
 ! real(8) :: damping_reading
  integer :: Nstep, ifq, move_step, Num_redistancing
  real(8) :: conv_time, mono_time, move_time, shel_time
  integer :: ifq_sh, ifq_tq, mono_iter
  ! Case
  real(8) :: GGG_reading
  real(8) :: ls_sl_penalty
  integer :: runcase
  real(8) :: DetJinv, hglob
 ! integer :: NSD
  logical ::  flag_wave  
  integer, parameter :: ship  = 0
  integer, parameter :: marin = 1
  integer, parameter :: vawt  = 2  

  real(8) :: TTT 
  real(8) :: AAA 

  ! immersed stuff
  integer NGAUSSTET, NGAUSSPRI
  integer aq_level, genAQ_flag, use_IM
  real(8) :: WBC_TauTan, WBC_TauNor
    
  ! Mesh
  character(len=16)    :: meshname
  integer              :: BC_NBOUND   
  integer, allocatable :: BC_FaceID(:)
  
  ! Boundary Conditions
  integer, allocatable :: NS_BC_Type(:,:)
  integer, allocatable :: LSC_BC_Type(:)
  integer, allocatable :: Mesh_BC_Type(:,:)  

  real(8) :: rot_center(3)
  
  ! Bodies
  integer :: NBODIES
  character(len=16), allocatable :: bodyname (:)
  integer,           allocatable :: bodyfile (:)
              
  ! Newton Solver
  integer :: NL_itermax,NBlade
  real(8) :: NS_NL_Utol, NS_NL_Ptol,&
             LSC_NL_tol, LSRD_NL_tol,LSMC_NL_tol,& 
             Lin_NL_tol,Ang_NL_tol,Mode_NL_tol,Mesh_NL_tol,STRU_NL_tol


  real(8) :: NS_GMRES_tol
  integer :: NS_GMRES_itermin,NS_GMRES_itermax, NS_NL_itermax

       
  ! Linear Solver           
  integer :: GMRES_itermin, GMRES_itermax
  real(8) :: GMRES_tol 
    
  ! Preconditioner 
  integer :: PC_NS_itermax, PC_LSC_itermax,PC_LSRD_itermax, &
             PC_LSMC_itermax,PC_Mesh_itermax  
  integer :: PC_NS_itermin, PC_LSC_itermin,PC_LSRD_itermin, &
             PC_LSMC_itermin,PC_Mesh_itermin  
  real(8) :: PC_NS_tol, PC_LSC_tol,PC_LSRD_tol,&
             PC_LSMC_tol,PC_Mesh_tol,PC_STRU_tol
  integer :: PC_verbose           
    
  ! Fluids and interface  
  real(8) :: useRho0
  real(8) :: mua, rhoa, muw, rhow, rhot 
 
  real(8) :: mp_eps  , hint 
  real(8) :: gravity, gravvec(3)
  real(8) :: lower, upper  
  ! Navier-Stokes solver
  real(8) :: NS_kdc_w, NS_kdc_a
  integer :: NS_hess_flag
  real(8) :: Uin,UinVec(3),Uref,Uwind
    
  ! LevelSet Convection solver  
  real(8) :: LSC_kdc
   
  real(8) :: LSC_GMRES_tol
  integer :: LSC_GMRES_itermin,LSC_GMRES_itermax, LSC_NL_itermax, &
             LSC_pred_step

  ! Rigid body solver
  real(8) :: RB_NL_Ftol,RB_NL_Mtol

   ! Mesh solver  
  real(8) :: Mesh_GMRES_tol
  integer :: Mesh_GMRES_itermin,Mesh_GMRES_itermax


  ! Level Set Redistance solver  
  real(8) :: LSRD_kdc, LSRD_penalty_fac
  real(8) :: LSRD_pseudoDTGL_fac  
  real(8) :: LSRD_GMRES_tol
  integer :: LSRD_GMRES_itermin,LSRD_GMRES_itermax, LSRD_NL_itermax
  real(8) :: pse_time,pse_time1,pse_time_read, pse_lamda    


  ! Level Set massfix       
  real(8) :: LSMC_kdc     
!  real(8) :: Mass_init
  real(8) :: Mass_NL_tol
  integer :: Mass_NL_itermax    

  ! Rigid body         
  real(8) :: massb, Ibhat(3,3), xcg(3), Fb(3),Mb(3)
  integer :: VBC(3), MBC(3)
  ! Domain and Hull
  real(8) :: domain_top,  domain_bottom, &
             domain_left, domain_right, &
             domain_front,domain_back, &
             air_height,hull_length 
 ! real(8) :: wave_periode,water_depth,
  !real(8) ::  mu, rho, dmudphi, drhodphi 
  ! Water surface -- Waves
  real(8) :: water_level    
  real(8) :: wave_length, wave_amp, wave_angle 
  integer :: BCtype(99)
  integer, allocatable :: BCugType(:,:)
  real(8), allocatable :: BCugValu(:,:)
  ! Flags
  logical :: move, mono, conv, shel

  real(8) :: theta(2), thetd(2), thedd(2), thetaOld(2), thetdOld(2), theddOld(2), &
             maxthetd(2), torque1, torque2, torque3, torque4, torque_SH, &
             moment1, moment2, torque5, torque6, torque7, torque8, torque_SH_2, &
             moment3, moment4

  logical :: solshell, nonmatch, rotation, solfsi
 
  real(8) :: Force_x, Force_y, Force_z

  real(8),allocatable :: ash_base(:,:),ush_base(:,:),dsh_base(:,:),ash_pertubation(:,:)
  real(8) :: eps_time1,eps_time2 ! for matrix-free
  real(8) :: eps_DG
  ! Rotation

!  real(8) :: x0_c(3), xt_c(3) !x0_1(3), x0_2(3), x0_3(3), xt_1(3), xt_2(3), xt_3(3), 
!  real(8) :: e0(3,3), et(3,3), Rt(3,3), Identity(3,3) temp_vec1(3), temp_vec2(3), temp1, temp2

  real(8) :: Omega(3), OmegaOld(3), Omegatang(3), &
                    OmegatangOld(3), normal(3), Identity1(3,3) 

   real(8) :: Rmat(2,3,3),    Rdot(2,3,3),    Rddt(2,3,3),    Rtang(2, 3,3),   RtangOld(2, 3,3), &
                    RmatOld(2,3,3), RdotOld(2,3,3), RddtOld(2,3,3), &
                    utmp(3), umtmp(3), Rtmp(2,3,3), xrot(3),  xrotOld(3),  xh_E(3)

  real(8), allocatable :: NRmat(:,:,:), NRdot(:,:,:), NRddt(:,:,:)
  real(8), allocatable :: NRmatOld(:,:,:), NRdotOld(:,:,:), NRddtOld(:,:,:)
  real(8) :: thetd_base,thedd_base,theta_base 
  real(8) :: Center_Rot(2,3)
  integer :: level_flag, ido_flag 

  real(8) :: force_trac(3), force_trac_taub(3)
  real(8) :: torque_trac, torque_trac_taub
  
  real(8) :: force_trac1(3), force_trac_taub1(3)
  real(8) :: torque_trac1, torque_trac_taub1

  real(8) :: DCuse0, Fineuse0
  real(8) :: rotor_tower,shift_wave
  !------------------------------------------------------------------- 
  contains
  !-------------------------------------------------------------------
  subroutine allocParam(maxID,NSD)
    implicit none
  
    integer, intent(in) :: maxID,NSD
  
    allocate(NS_BC_Type(maxID,NSD))
    allocate(LSC_BC_Type(maxID))           
    allocate(Mesh_BC_Type(maxID,NSD))
    
  end subroutine allocParam  

  !-------------------------------------------------------------------  
  subroutine deallocParam()
    implicit none
  
    deallocate(NS_BC_Type)
    deallocate(LSC_BC_Type)           
    deallocate(Mesh_BC_Type)
    
  end subroutine deallocParam    
  
!======================================================================
! subroutine to read in the parameters defined in 'param.dat'          
!======================================================================
subroutine getparam()
  use mpi
  implicit none
   
  integer :: i, j, bb
  character(len=30) :: fname, fnum
  character(len=30) :: fname1(2), fnum1(3)
  character(len=10) :: cname  
  real(8) :: Froude, hull_length ,water_depth
  logical :: found
  real(8), parameter :: pi = 3.14159265358979323846264338328d0
  ! Mesh  


  call cread("mesh", meshname, found)
   if (.not.found) meshname = "part"
  
  call iread("BC_NBOUND",     BC_NBOUND)    
  allocate(BC_FaceID(BC_NBOUND))
     
  do bb = 1,BC_NBOUND  
    call ifind('BC_FaceID('//trim(cname(bb))//')', BC_FaceID(bb),bb)
  enddo

  Center_Rot(1,1) = 0d0
  Center_Rot(1,2) = 0d0
  Center_Rot(1,3) = 0d0
 
  Center_Rot(2,1) = 0d0
  Center_Rot(2,2) = 0d0
  Center_Rot(2,3) = 0d0

  do i = 1, 2
    do j = 1, 3
        write(fnum1(1),'(I4)') i
        write(fnum1(2),'(I4)') j
        fname1(1) = 'Center_Rot('//trim(adjustl(fnum1(1)))//','&
                              //trim(adjustl(fnum1(2)))//')'
!        write(*,*) fname1(1)
        call rread(fname1(1), Center_Rot(i,j))
    enddo
  enddo

  allocate(BCugType(maxval(BC_FaceID(:)),3), &
           BCugValu(maxval(BC_FaceID(:)),3))

  BCugType = 0
  do bb = 1, BC_NBOUND
    i = BC_FaceID(bb)
    write(fnum1(1),'(I4)') i
    fname1(1) = 'BCugType'//trim(adjustl(fnum1(1)))
    call iread3(fname1(1), BCugType(i,1:3))
  end do

  BCugValu = 0.0d0
  do bb = 1, BC_NBOUND
    i = BC_FaceID(bb)
    do j = 1, 3
      if (BCugType(i,j) == 1) then
        write(fnum1(1),'(I4)') i
        write(fnum1(2),'(I4)') j
        fname1(1) = 'BCugValu('//trim(adjustl(fnum1(1)))//','&
                              //trim(adjustl(fnum1(2)))//')'
        call rread(fname1(1), BCugValu(i,j))
      end if
    end do
  end do

  
  ! Boundary conditions    
  call allocParam(maxval(BC_FaceID(:)),3)
  NS_BC_Type = 0 
  Mesh_BC_Type = 0
  LSC_BC_Type = 0
  do bb = 1, BC_NBOUND
    i = BC_FaceID(bb)
   ! i = bb
    call iread3('NS_BC_Type('//trim(cname(i))//')',   NS_BC_Type(i,:))   
    call ifind3('Mesh_BC_Type('//trim(cname(i))//')', Mesh_BC_Type(i,:),1,1,1)
    call ifind ('LSC_BC_Type('//trim(cname(i))//')',  LSC_BC_Type(i),0)
   
  !  call ifind3('Mesh_BC_Type('//trim(cname(i))//')', Mesh_BC_Type(bb,:),1,1,1)    
  enddo  

  do bb=1,3
   ! call rread('rot_center('//trim(cname(bb))//')',   rot_center(bb)) 
  enddo

  ! Bodies
  call ifind("NBODIES",     NBODIES, 0)
  call ifind("level_flag",    level_flag, 0)
  
  if (NBODIES /= 0) then
 !   allocate(bodyname(NBODIES),bodyfile(NBODIES))
 !   bodyfile = -1
 !   do bb = 1,NBODIES
 !     call cread('body('//trim(cname(bb))//')', bodyname(bb), found)
 !     if (.not.found) bodyname(bb) =  'body.'//trim(cname(bb))//'.dat'      
 !   enddo       
    call ifind ("move_step", move_step, 0) 
  endif  
        
  ! Time stepping     
  call iread("Nstep",     Nstep)
  call iread("aq_level",  aq_level)
  call iread("NGAUSSTET", NGAUSSTET)
  call iread("NGAUSSPRI", NGAUSSPRI)
  call rread("WBC_TauTan", WBC_TauTan)
  call rread("WBC_TauNor", WBC_TauNor)
  call iread("genAQ_flag", genAQ_flag)
  call iread("Num_redistancing",     Num_redistancing)
  call iread("ifq",       ifq)
  call rread("Delt",      Delt)  
  call rread("GGG_reading",      GGG_reading)
  call rread("angle_acl",      angle_acl)  
  call rread("clearance",clearance)
  call rread("TTT",TTT)
  call rread("move_time", move_time)
  Dtgl = 1.0d0/Delt
  call rread("lower",lower)
  call rread("upper",upper)
  call rread("ls_sl_penalty",ls_sl_penalty)
  ! Time Integration      
  call rread("rhoinf",    rhoinf)

  if((rhoinf < 0.0d0) .or. (rhoinf > 1.0d0)) then ! backward Euler
    almi = 1.0d0
    alfi = 1.0d0
    gami = 1.0d0
    beti = 1.0d0 
  else   
    almi = (3.0d0-rhoinf)/(1.0d0+rhoinf)/2.0d0
    alfi = 1.0d0/(1.0d0+rhoinf)
    
    gami = 0.5d0+almi-alfi
    beti = 0.25d0*(1.0d0+almi-alfi)*(1.0d0+almi-alfi)
  end if
  ogam = 1.0d0/gami
  obet = 1.0d0/beti
  mgam = 1.0d0 - gami
  malm = 1.0d0 - almi   
  malf = 1.0d0 - alfi   
  mbet = 0.5d0 - beti
  
  if (ismaster) then
    write(*,"(a20,1x,' = ',1x,ES12.4)") "almi", almi
    write(*,"(a20,1x,' = ',1x,ES12.4)") "alfi", alfi
    write(*,"(a20,1x,' = ',1x,ES12.4)") "gami", gami  
    write(*,"(a20,1x,' = ',1x,ES12.4)") "beti", beti  
  endif
           
  ! Newton Solver   
  call iread("NL_itermax",  NL_itermax)  
  call rread("NS_NL_Utol",  NS_NL_Utol)
  call rread("NS_NL_Ptol",  NS_NL_Ptol)  
  call rread("LSC_NL_tol",  LSC_NL_tol) 
  call rread("LSRD_NL_tol", LSRD_NL_tol)
  call rread("LSMC_NL_tol", LSMC_NL_tol)
  call rread("Mesh_NL_tol", Mesh_NL_tol)
  call rread("STRU_NL_tol", STRU_NL_tol)

  use_IM = 0
  call iread("use_IM",  use_IM)  

  
  if (NBODIES /=0) then  
    call rread("Lin_NL_tol",  Lin_NL_tol)
    call rread("Ang_NL_tol",  Ang_NL_tol)    
    call rfind("Mode_NL_tol", Mode_NL_tol,1d0)
  endif
        
  ! GMRES Solver     
  call rread("GMRES_tol",     GMRES_tol)  
  call iread("GMRES_itermin", GMRES_itermin)  
  call iread("GMRES_itermax", GMRES_itermax)

  ! Preconditioner
  call ifind("PC_verbose",      PC_verbose,-1)    
  
  call rread("PC_NS_tol",       PC_NS_tol)   
  call rread("PC_STRU_tol",     PC_STRU_tol) 
  call rread("PC_LSC_tol",      PC_LSC_tol)    
  call rread("PC_LSRD_tol",     PC_LSRD_tol) 
  call rread("PC_LSMC_tol",     PC_LSMC_tol)   
  call rread("PC_Mesh_tol",     PC_Mesh_tol) 
  
  call iread("ido_flag", ido_flag)

  call rread("damping_reading",   damping_reading)
  call rread("DM_reading",   DM_reading)
  call rread("DC_reading",   DC_reading)
  call rread("DB_reading",   DB_reading)
  call rread("rotor_tower",   rotor_tower)
  call rread("shift_wave",   shift_wave)

  call iread("PC_NS_itermax",   PC_NS_itermax)    
  call iread("PC_LSC_itermax",  PC_LSC_itermax)     
  call iread("PC_LSRD_itermax", PC_LSRD_itermax)    
  call iread("PC_LSMC_itermax", PC_LSMC_itermax)    
  call iread("PC_Mesh_itermax", PC_Mesh_itermax)
  
  call ifind("PC_NS_itermin",   PC_NS_itermin,   max(PC_NS_itermax/5,1))     
  call ifind("PC_LSC_itermin",  PC_LSC_itermin,  max(PC_LSC_itermax/5,1))   
  call ifind("PC_LSRD_itermin", PC_LSRD_itermin, max(PC_LSRD_itermax/5,1))      
  call ifind("PC_LSMC_itermin", PC_LSMC_itermin, max(PC_LSMC_itermax/5,1))   
  call ifind("PC_Mesh_itermin", PC_Mesh_itermin, max(PC_Mesh_itermax/5,1))
    
  ! Interface
  call rread("int_eps", mp_eps)  
      
  ! Physics  
  call rread("DENS_AIR",   rhoa)
  call rread("DENS_WATER", rhow)
  call rread("VISC_AIR",   mua)
  call rread("VISC_WATER", muw)
  call rread("useRho0", userho0)

  call rread("DCuse0",DCuse0) 
  call rread("Fineuse0",Fineuse0) 

  call rread("pse_lamda",pse_lamda) 
  call rread("pse_time_read",pse_time_read)   
  rhot = 0.5d0*(rhoa+rhow)
    
  gravity = 9.81d0
  gravvec(1) = 0.0d0 
  gravvec(2) = 0.0d0
  gravvec(3) = -gravity
      
  ! Problem setup  
  call iread("case",runcase)  
  
  if (runcase==ship) then  

    call rread("water_level", water_level)
    call rfind("Froude", Froude, 0d0)
    call rread("Uin",Uin) 
    if (Froude > 0d0) then          
      call rread("hull_length", hull_length)
      call rread("Froude", Froude)    
    !  Uin = Froude*sqrt(gravity*hull_length)    
      if (ismaster) write(*,"(a20,1x,' = ',1x,ES12.4)") "Uin", Uin  
    else
      call rread("Uin",Uin)    
    endif
    Uref = abs(Uin)   
 !   if (Uref == 0d0) call rread("Uref",Uref)
    
    ! Wind data
    call rfind("Uwind", Uwind, 0d0)    

    ! Wave data
    call rfind("wave_amp", wave_amp, 0d0)
    if (wave_amp > 0.0d0) then
      call rread("wave_length", wave_length)
      call rread("wave_angle",  wave_angle)
      wave_angle   = pi*wave_angle/180.0d0
      wave_length  = 2.0d0*pi/wave_length  
      water_depth  = water_level - (-350d0)
      if(ismaster) write(*,*) "period:",sqrt(gravity*wave_length*tanh(wave_length*water_depth)),&
                               2*pi/sqrt(gravity*wave_length*tanh(wave_length*water_depth))
            
    else
      wave_amp      = 0.0d0
      wave_length   = 1.0d0
      wave_angle    = 0.0d0
    end if 

  else if (runcase==marin) then 
    Uin  = 0d0    
    Uref = 1d0    
    if (useRho0 > 0d0) then
      write(*,*) "DONT USE rho0 FOR MARIN RUNS!!!!!!"
      stop
    endif
  else if (runcase==101) then 
    Uin  = 0d0    
    Uref = 1d0    
  else
    call rread("Uin", Uin) 
    Uref = Uin  
    if (Uin == 0d0) call rread("Uref", Uref)   
  endif
  
  UinVec(1) = Uin
  UinVec(2) = 0d0
  UinVec(3) = 0d0  
  
  ! Navier-stokes solver 
  call rread("NS_kdc_w",         NS_kdc_w)
  call rread("NS_kdc_a",          NS_kdc_a)
  call rread("eps_time1",         eps_time1)
  call rread("eps_time2",      eps_time2)
  call rread("eps_DG     ",          eps_DG)
! YB

!  NS_kdc_w = NS_kdc_w/Uref
!  NS_kdc_a = NS_kdc_a/Uref 
  
  call iread("NS_hessian",       NS_hess_flag)

  ! Level Set Convection solver   
  call rread("LSC_kdc",          LSC_kdc)
  
  ! Level Set redistance solver    
  call rread("LSRD_kdc",         LSRD_kdc)
  call rread("LSRD_penalty_fac", LSRD_penalty_fac)
         
  ! Level Set massfix solver     
  call rread("LSMC_kdc",         LSMC_kdc) 
      
end subroutine getparam
  
!======================================================================
!
!======================================================================
subroutine rread(vname, val)
  use mpi
  implicit none

  character(len=*), intent(in)  :: vname
  real(8),          intent(out) :: val
  character(len=16) :: buf  
  logical   found
  
  if (ismaster) then 
    call cread(vname,buf, found)
    if (found) then
      read(buf,*) val
      write(*,"(a16,' = ',ES12.4)")  vname, val  
    else
      write(*,*) "Could not find ", vname , " in param.dat"
      stop
    endif   
  endif
  

    call MPI_BCAST(val,1,MPI_DOUBLE_PRECISION, &
                   mpi_master,MPI_COMM_WORLD,mpi_err)   
  
end subroutine rread

!======================================================================
!
!======================================================================
subroutine rfind(vname, val,def)
  use mpi
  implicit none
  character(len=*), intent(in)  :: vname
  real(8),          intent(out) :: val  
  real(8),          intent(in)  :: def
  character(len=16) :: buf
  logical   found
  
  if (ismaster) then 
    call cread(vname,buf, found)
    if (found) then
      read(buf,*) val 
      write(*,"(a16,' = ',ES12.4)")  vname, val
    else
      val = def      
      write(*,"(a16,' = ',ES12.4,' (default)')")  vname, val
    endif
    
  endif
  call MPI_BCAST(val,1,MPI_DOUBLE_PRECISION, &
                   mpi_master,MPI_COMM_WORLD,mpi_err)   
  
end subroutine rfind
!======================================================================
!
!======================================================================
subroutine iread(vname, val)
  use mpi
  implicit none

  character(len=*), intent(in)  :: vname
  integer,          intent(out) :: val
  character(len=16) :: buf
  logical   found

  if (ismaster) then
    call cread(vname, buf, found)
    if (found) then
      read(buf,*) val
      write(*,"(a16,' = ',I12)")  vname, val  
    else
      write(*,*) "Could not find ", vname , " in param.dat"
      stop
    endif
  endif   
  
  call MPI_BCAST(val,1,MPI_INTEGER, &
                   mpi_master,MPI_COMM_WORLD,mpi_err)  
end subroutine iread

!======================================================================
subroutine ifind(vname, val, def)
  use mpi
  implicit none

  character(len=*), intent(in)  :: vname
  integer,          intent(out) :: val  
  integer,          intent(in)  :: def
  character(len=16) :: buf
  logical   found

  if (ismaster) then
    call cread(vname, buf, found)
    if (found) then
      read(buf,*) val 
      write(*,"(a16,' = ',I12)")  vname, val  
    else
      val = def 
      write(*,"(a16,' = ',I12,' (default)')")  vname, val
    endif

  endif   
  
    call MPI_BCAST(val,1,MPI_INTEGER, &
                   mpi_master,MPI_COMM_WORLD,mpi_err)  

end subroutine ifind
 
!======================================================================
subroutine iread3(vname, val)
  use mpi
  implicit none

  character(len=*), intent(in)  :: vname
  integer,          intent(out) :: val(3)
  character(len=16) :: buf
  logical found
  
  if (ismaster) then
    call cread(vname, buf, found)
    if (found) then
      read(buf,*) val(1:3)
      write(*,"(a16,' = ',3I4)")  vname, val  
    else
      write(*,*) "Could not find ", vname , " in param.dat"
      stop
    endif
  endif   
     
    call MPI_BCAST(val,3,MPI_INTEGER, &
                   mpi_master,MPI_COMM_WORLD,mpi_err)  
       
end subroutine iread3

!======================================================================
subroutine ifind3(vname, val, def1, def2, def3)
  use mpi
  implicit none

  character(len=*), intent(in)  :: vname
  integer, intent(in)  :: def1,def2,def3
  integer,          intent(out) :: val(3)
  character(len=16) :: buf
  logical found
  
  if (ismaster) then
    call cread(vname, buf, found)
    if (found) then
      read(buf,*) val(1:3)
      write(*,"(a16,' = ',3I4)")  vname, val  
    else
      val(1:3) = (/ def1,def2,def3 /)
      write(*,"(a16,' = ',3I4,' (default)')")   vname, val  
    endif
  endif   
     
    call MPI_BCAST(val,3,MPI_INTEGER, &
                   mpi_master,MPI_COMM_WORLD,mpi_err)  
       
end subroutine ifind3

!======================================================================
! subroutine to read in a variable name, check with input file
! and read in the corresponding value.
!======================================================================
subroutine cread(name, val, found)
 implicit none

 character*(*) name 
 character(len=16)  val
 logical found
 character(len=32)  buf,buf1

 integer      paramf,ios,pos

 paramf=45
 open (paramf, file='param.dat', status='old',iostat=ios)
 found = .false.
 do while ((ios.eq.0).and.(.not.found))
   read(paramf,'(A)',iostat=ios) buf
   
   pos  = scan(buf,'=')
   buf1 = buf(1:pos-1) 
   val  = buf(pos+1:)
     
   if (buf1 == name) found = .true.
 enddo
 close(paramf)
      
end subroutine cread

end module params
