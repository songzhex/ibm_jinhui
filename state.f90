module solution

  !-------------------------------------------------------------------------------------------------------   
  type state
   
    integer :: NNODE = 0
    integer :: NSD   = 0
                          
    real(8), allocatable :: dg(:,:),  ugm(:,:), acgm(:,:) ,  &  
                            ug(:,:),  acg(:,:), pg(:), rhog(:),mug(:),  &    
                            phig(:), rphig(:),  phidg(:), phicg(:),  &
                            phig_check(:), &
                            hg(:),     heg(:),  hepg(:),  drhog(:),rho0g(:)                                                
  end type state

  !-------------------------------------------------------------------------------------------------------    
  contains                
  !-------------------------------------------------------------------------------------------------------   
  subroutine allocState(st, NNODE, NSD)
    implicit none
    type(state), intent(inout) :: st    
    integer,     intent(in)    :: NNODE,NSD
    integer :: b
    
    if ((NNODE /= st%NNODE).or. &
        (NSD   /= st%NSD  ) ) then
      
      call deallocState(st)
      
      if (NNODE /= 0) then          
        allocate(st%dg(NNODE,NSD), st%ugm(NNODE,NSD), st%acgm(NNODE,NSD) ,  &  
                 st%ug(NNODE,NSD), st%acg(NNODE,NSD), st%pg(NNODE),    st%rhog(NNODE), st%mug(NNODE),  &    
                 st%phig(NNODE), st%phig_check(NNODE),   st%rphig(NNODE),   st%phidg(NNODE), st%phicg(NNODE),  &
                 st%hg(NNODE),     st%heg(NNODE),     st%hepg(NNODE),  st%drhog(NNODE),  st%rho0g(NNODE))
        st%NNODE = NNODE 
        st%NSD   = NSD
      endif         
    endif
      
  end subroutine allocState

  !-------------------------------------------------------------------------------------------------------     
  subroutine deallocState(st)
    implicit none

    type(state), intent(inout) :: st
    integer b
    
    if (st%NNODE /= 0 ) then     
      deallocate(st%dg,   st%ugm,   st%acgm,  &  
                 st%ug,   st%acg,   st%pg,    st%rhog, st%mug,  &    
                 st%phig, st%rphig, st%phidg, st%phicg,  &
                 st%hg,   st%heg,   st%hepg,  st%drhog,st%rho0g)
      st%NNODE = 0      
      st%NSD = 0
    endif
    
  end subroutine deallocState 
   
  !-------------------------------------------------------------------------------------------------------     
  subroutine initState(st)
    use params
  
    implicit none

    type(state), intent(inout) :: st
    integer :: b
    
    if (st%NNODE /= 0 ) then   
  
      st%dg   = 0d0   
      st%ugm  = 0d0    
      st%acgm = 0d0 
        
      st%ug   = 0d0
      st%ug(:,1) =  BCugValu(1,1)  
      st%acg  = 0d0 
      st%pg   = 0d0 
      st%rhog = 0d0 
      st%mug  = 0d0 
        
      st%phig  = 0d0 
      st%rphig = 0d0 
      
      st%phidg = 0d0 
      st%phicg = 0d0 
                  
      st%hg    = 0d0   
      st%heg   = 0d0 
      st%hepg  = 0d0 
      st%drhog = 0d0
      st%rho0g = 0d0
                
    endif
                  
  end subroutine initState        
  !-------------------------------------------------------------------------------------------------------       
  subroutine readState(st, Rstep, time)  

    use mpi
    implicit none
  
    type(state), intent(inout) :: st
    integer,     intent(inout) :: Rstep 
    real(8),     intent(out)   :: time

    integer :: sfile, ierr, step1, step2
    integer :: NNODE, NSD
    real(8) :: tmp (st%NSD, st%NNODE)         

    character(len=30) :: fname
    character(len=10) :: cname1  
    character(len=10) :: cname2
          
    sfile = 15
    
    write(cname1,'(I8)') Rstep
    write(cname2,'(I8)') myid+1

    fname = 'restart.' // trim(adjustl(cname1))//'.'// trim(adjustl(cname2))
    open(sfile, file=fname, status='old', iostat=ierr)
    if (ierr /= 0) then
      write(*,*) "File does not exist:", fname
      stop
    endif
    
    if (ismaster) write(*,*)  "Reading step:", Rstep
    
    ! Read header
    read(sfile,*) step1, time
    !read(sfile,*) NNODE, NSD
    NNODE = st%NNODE
    NSD = 3 
         
    if ((step1 /= Rstep   ).or. &
        (NNODE /= st%NNODE).or. &
        (NSD   /= st%NSD  ) ) then
      write(*,*) "ERROR IN READING", fname
      write(*,*) " -STEP  = ",  step1,Rstep   
      write(*,*) " -NODES = ",  NNODE,st%NNODE
      write(*,*) " -NSD   = ",  NSD  ,st%NSD  
      stop
    endif 
       
    ! Read mesh function data        
    read(sfile,*) tmp; st%dg   = transpose(tmp)
    read(sfile,*) tmp; st%ug   = transpose(tmp)
    read(sfile,*) tmp; st%ugm  = transpose(tmp)
    read(sfile,*) tmp; st%acg  = transpose(tmp)
    read(sfile,*) tmp; st%acgm = transpose(tmp)
    read(sfile,*) st%phig
    read(sfile,*) st%rphig
    read(sfile,*) st%pg
    
    read(sfile,*) step2
    
    if (step2 /= Rstep   ) then
      write(*,*) "ERROR IN READING", fname
      write(*,*) " -STEP  = ",  Rstep , step2  
      write(*,*) " FILE END CHECK FAILED"
      stop
    endif
        
    read(sfile,*) tmp(1,1:3)                 
    read(sfile,*) tmp(2,1:4) 
    read(sfile,*) tmp(3,1:2) 
    
   !  close(sfile)
    
    if (step2 /= Rstep   ) then
      write(*,*) "ERROR IN READING ", fname
      write(*,*) " -STEP  = ",  Rstep , step2  
      write(*,*) " FILE END CHECK FAILED"
      stop
    endif    
    
    if ((tmp(1,1) /= sum(st%ug)).or.   &
        (tmp(1,2) /= sum(st%acg)).or.  &
        (tmp(1,3) /= sum(st%pg)).or.   &  
        (tmp(2,1) /= sum(st%dg)).or.   &
        (tmp(2,2) /= sum(st%ugm)).or.  &
        (tmp(2,3) /= sum(st%acg)).or.  &
        (tmp(2,4) /= sum(st%acgm)).or. &       
        (tmp(3,1) /= sum(st%phig)).or. &
        (tmp(3,2) /= sum(st%rphig)) ) then          
      write(*,*) "ROUNDING ERROR IN READING ", fname 
      write(*,*) tmp(1,1:3) 
      write(*,*) sum(st%ug),sum(st%acg),sum(st%pg)    
      write(*,*) tmp(2,1:4)                
      write(*,*) sum(st%dg),sum(st%ugm),sum(st%acg),sum(st%acgm) 
      write(*,*) tmp(3,1:2) 
      write(*,*) sum(st%phig),sum(st%rphig)
      !!stop
    endif                    
    read(sfile,*) st%rhog
    read(sfile,*) st%rho0g
    read(sfile,*) st%hg  
    read(sfile,*) st%heg  
    read(sfile,*) st%hepg
    close(sfile)  
  !  st%phidg = st%phig
 !   st%phicg = 0d0 
       
 !   st%rhog  = 0d0
  !  st%mug   = 0d0
  ! st%drhog = 0d0  
 !   st%rho0g = 0d0 
    
  !  st%hg    = 0d0
  !  st%heg   = 0d0
  !  st%hepg  = 0d0    
    
  end subroutine readState  

  !-------------------------------------------------------------------------------------------------------       
  subroutine writeState(st, Rstep, time)  

    use mpi
    implicit none
  
    type(state), intent(inout) :: st
    integer,     intent(in) :: Rstep 
    real(8),     intent(in) :: time

    integer :: sfile,b,i,j
    
    character(len=30) :: fname
    character(len=10) :: cname1  
    character(len=10) :: cname2
      
    sfile = 15
    if (ismaster) write(*,*)  "Writing step:", Rstep
    
    write(cname1,'(I8)') Rstep
    write(cname2,'(I8)') myid+1

    fname = 'restart.' // trim(adjustl(cname1)) //'.'// trim(adjustl(cname2))
    open(sfile, file=fname, status='replace')
  
    write(sfile,*) Rstep, time
    !!write(sfile,*) st%NNODE, st%NSD
        
    write(sfile,'(3Es30.20)') transpose(st%dg)
    write(sfile,'(3Es30.20)') transpose(st%ug)
    write(sfile,'(3Es30.20)') transpose(st%ugm)
    write(sfile,'(3Es30.20)') transpose(st%acg)
    write(sfile,'(3Es30.20)') transpose(st%acgm)
    write(sfile,'(1Es30.20)') st%phig
    write(sfile,'(1Es30.20)') st%rphig
    write(sfile,'(1Es30.20)') st%pg

    write(sfile,*) Rstep    
    
    write(sfile,'(3Es30.20)') sum(st%ug),sum(st%acg),sum(st%pg)                  
    write(sfile,'(3Es30.20)') sum(st%dg),sum(st%ugm),sum(st%acg),sum(st%acgm)
    write(sfile,'(2Es30.20)') sum(st%phig),sum(st%rphig)
    
    ! Additional output for visualization purposes only
    write(sfile,'(1Es30.20)') st%rhog
    write(sfile,'(1Es30.20)') st%rho0g
    write(sfile,'(1Es30.20)') st%hg  
    write(sfile,'(1Es30.20)') st%heg  
    write(sfile,'(1Es30.20)') st%hepg  
    
    close(sfile)
 
  end subroutine writeState 



 subroutine writeforce(Rstep, time1)  
     
    use mpi
    use params 
    implicit none
    integer,     intent(in) :: Rstep 
    real(8),     intent(in) :: time1

    integer :: sfile,b,i,j
    
    character(len=30) :: fname
    character(len=10) :: cname1  

    if(ismaster) then  
     sfile = 15
     write(cname1,'(I8)') Rstep
     fname = 'force.' // trim(adjustl(cname1))
     open(sfile, file=fname, status='replace')
     write(sfile,*)  time1, -force_trac(3),-force_trac(2), -force_trac(1)
     write(sfile,*)  -force_trac_taub(3),-force_trac_taub(2), -force_trac_taub(1)
     write(sfile,*)  torque_trac, torque_trac_taub
     close(sfile)

     sfile = 16
     write(cname1,'(I8)') Rstep
     fname = 'force1.' // trim(adjustl(cname1))
     open(sfile, file=fname, status='replace')
     write(sfile,*)  time1, -force_trac1(3),-force_trac1(2), -force_trac1(1)
     write(sfile,*)  -force_trac_taub1(3),-force_trac_taub1(2), -force_trac_taub1(1)
     write(sfile,*)  torque_trac1, torque_trac_taub1
     close(sfile)
    endif
 
  end subroutine writeforce
!======================================================================
! Output vorticity
!======================================================================
subroutine writeVort(st,istep)
  
  use mpi
  
  implicit none
  type(state), intent(in) :: st
  integer :: solf,i,j,istep
  character(len=30) :: fname
  character(len=10) :: cname1, cname2
    
    call commu(rhsqu, 1  , 'in ')
    call commu(rhsqu, 1  , 'out ')
    call commu(rhsql, 1  , 'in ')
    call commu(rhsql, 1  , 'out ')
  ! Output results  
  solf = 98
  write(cname1,'(I8)') istep
  write(cname2,'(I8)') myid+1

  fname = 'vorticity.' // trim(adjustl(cname1)) //'.'// trim(adjustl(cname2))
  open(solf, file=fname, status='replace')

  do i = 1, st%NNODE
    write(solf,*) rhsqu(i)/rhsql(i)
  end do

  !do i = 1, NNODE
 !   write(solf,*) RHSGq(i)/RHSGl(i)
!  end do

end subroutine writeVort
  !-------------------------------------------------------------------------------------------------------          
  subroutine printState (st) 

    use mpi
    implicit none
  
    type(state), intent(inout) :: st
    integer :: cpu 
          
    do cpu =1, numnodes
      if (cpu == myid+1) then       
        write(*,*) myid
        write(*,*) sum(st%ug),sum(st%acg),sum(st%pg)                  
        write(*,*) sum(st%dg),sum(st%ugm),sum(st%acg),sum(st%acgm)
        write(*,*) sum(st%phig),sum(st%rphig),sum(st%phidg),sum(st%phicg)            
  !      call flush()       
      endif 
      call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)  
    enddo        
    
  end subroutine printState 
    
  !-------------------------------------------------------------------------------------------------------          
  subroutine marinIC (st,msh) 
  
    use meshData
    implicit none
  
    type(state),      intent(inout) :: st
    type(meshStruct), intent(in)    :: msh
                     
    real(8), parameter ::  xi1 = 1.228d0
    real(8), parameter ::  xi3 = 0.550d0 
    real(8) :: xg (msh%NSD)
    integer :: i 

    do i = 1, st%NNODE
      xg = msh%xg(i,:) 
      if ((xg(1).le.xi1 ).and.(xg(3).le.xi3)) then          
        st%phig(i) = min(xi1-xg(1), xi3-xg(3))        
      else  if ((xg(1).ge.xi1 ).and.(xg(3).ge.xi3)) then                          
        st%phig(i) = -sqrt( (xi1-xg(1))**2+(xi3-xg(3))**2 )
      else if (xg(1).ge.xi1 ) then
        st%phig(i) = xi1-xg(1)
      else      
        st%phig(i) = xi3-xg(3)                  
      endif 
       
   enddo
  
   st%phidg = st%phig
   
  end subroutine marinIC    
  
  !-------------------------------------------------------------------------------------------------------          
  subroutine turbineIC (st,mesh) 
    use meshData
    use mpi
    use params
    implicit none
  
    type(state),      intent(inout) :: st
    type(meshStruct), intent(in)    :: mesh
                     
    integer :: i,j,n 
 !   real(8)  :: utmp(3)
    integer id
   
    do i = 1, mesh%NNODE       
      call getWave(st%ug(i,:), st%phig(i), mesh%xg(i,:)+st%dg(i,:), 0.0d0)  
    enddo
  
    do i = 1, mesh%NBOUND
      if (mesh%bound(i)%FACE_ID == 21) then     
        do j = 1, mesh%bound(i)%NNODE
          n = mesh%bound(i)%BNODES(j) 
         ! st%ug(n,:) = 0d0
         ! st%acg(n,:) = 0d0
        end do
      end if
    end do
  
    st%phidg = st%phig     
  end subroutine turbineIC    


  !-------------------------------------------------------------------------------------------------------          
  subroutine FinIC (st,mesh) 
    use meshData
    use mpi
    use params
    implicit none
  
    type(state),      intent(inout) :: st
    type(meshStruct), intent(in)    :: mesh
    real(8), parameter :: pi = 3.14159265358979323846264338 
                     
    integer :: i,  j,  n , nn, dd
 !   real(8)  :: utmp(3)
    integer id
    real(8):: time1
    time1 = 0d0
     do i = 1, mesh%NNODE       
      call getWave(st%ug(i,:), st%phig(i), mesh%xg(i,:)+st%dg(i,:), 0.0d0)   
    enddo
   
  !  thedd_base  = angle_acl
  !  thetd_base  = angle_acl*time1
  !  theta_base  = 0.5d0*angle_acl*time1**2

    thedd_base  = 0d0
    thetd_base  = 0d0
    theta_base  = 0d0

    theta(2) =  theta_base
    thetd(2) =  thetd_base
    thedd(2) =  thedd_base

    theta(1) =  0d0
    thetd(1) =  0d0
    thedd(1) =  0d0


    call get_Rmat(theta(1), thetd(1), thedd(1), Rmat(1,:,:), Rdot(1,:,:), Rddt(1,:,:))
    call get_Rmat(thetaold(1), thetdold(1), theddold(1), RmatOld(1,:,:), RdotOld(1,:,:), RddtOld(1,:,:))

    call get_Rmat(theta(2), thetd(2), thedd(2), Rmat(2,:,:), Rdot(2,:,:), Rddt(2,:,:))
    call get_Rmat(thetaold(2), thetdold(2), theddold(2), RmatOld(2,:,:), RdotOld(2,:,:), RddtOld(2,:,:))

    NRmat = 0.0d0; NRdot = 0.0d0; NRddt = 0.0d0
    NRmatOld = 0.0d0; NRdotOld = 0.0d0; NRddtOld = 0.0d0


    do nn = 1, mesh%NNODE
      if (mesh%Coord_ID(nn) == 31) then
     ! The volume containing the first fin
        NRmat(:,:,nn) = Rmat(1,:,:)
        NRdot(:,:,nn) = Rdot(1,:,:)
        NRddt(:,:,nn) = Rddt(1,:,:)
        NRmatOld(:,:,nn) = RmatOld(1,:,:)
        NRdotOld(:,:,nn) = RdotOld(1,:,:)
        NRddtOld(:,:,nn) = RddtOld(1,:,:)
     endif
      if (mesh%Coord_ID(nn) == 32) then
     ! The volume containing the second fin
        NRmat(:,:,nn) = Rmat(2,:,:)
        NRdot(:,:,nn) = Rdot(2,:,:)
        NRddt(:,:,nn) = Rddt(2,:,:)
        NRmatOld(:,:,nn) = RmatOld(2,:,:)
        NRdotOld(:,:,nn) = RdotOld(2,:,:)
        NRddtOld(:,:,nn) = RddtOld(2,:,:)
      end if
    end do
    ! Predictor: rotation

    st%dg = 0d0
    st%ugm = 0d0
    st%acgm = 0d0
    do nn = 1, mesh%NNODE
     do dd = 1,3
       if((mesh%Coord_ID(nn) == 31).or. (mesh%Coord_ID(nn) == 32)) then
           st%dg(nn,dd) =   sum(NRmat(dd,:,nn)*(mesh%xg(nn,1:3)-Center_Rot(1,1:3)))
           st%ugm(nn,dd) =  sum(NRdot(dd,:,nn)*(mesh%xg(nn,1:3)-Center_Rot(1,1:3)))
           st%acgm(nn,dd) = sum(NRddt(dd,:,nn)*(mesh%xg(nn,1:3)-Center_Rot(1,1:3)))
      endif
     enddo
    enddo
  
    do i = 1, mesh%NBOUND
      if (mesh%bound(i)%FACE_ID == 21) then     
        do j = 1, mesh%bound(i)%NNODE
          n = mesh%bound(i)%BNODES(j) 
          st%ug(n,:) =  st%ugm(n,:)
          st%acg(n,:) = st%acgm(n,:)
        end do
      end if
    end do
  
    st%phidg = st%phig  
   
  end subroutine FinIC    


!-------------------------------------------------------------------------------------------------------          
  subroutine FinIC_move (st,mesh) 
    use meshData
    use mpi
    use params
    implicit none
  
    type(state),      intent(inout) :: st
    type(meshStruct), intent(in)    :: mesh
    real(8), parameter :: pi = 3.14159265358979323846264338 
                     
    integer :: i,  j,  n , nn, dd
    integer id
    real(8):: time1
    time1 = 0d0
    thedd_base  = 0d0
    thetd_base  = angle_acl
    theta_base  = 0d0

    theta(1) =  0d0
    thetd(1) =  0d0
    thedd(1) =  0d0

    theta(2) =  theta_base
    thetd(2) =  thetd_base
    thedd(2) =  thedd_base

    do i = 1, mesh%NNODE       
      call getWave(st%ug(i,:), st%phig(i), mesh%xg(i,:)+st%dg(i,:), 0.0d0)   
    enddo


    call get_Rmat(theta(1), thetd(1), thedd(1), Rmat(1,:,:), Rdot(1,:,:), Rddt(1,:,:))
    call get_Rmat(thetaold(1), thetdold(1), theddold(1), RmatOld(1,:,:), RdotOld(1,:,:), RddtOld(1,:,:))

    call get_Rmat(theta(2), thetd(2), thedd(2), Rmat(2,:,:), Rdot(2,:,:), Rddt(2,:,:))
    call get_Rmat(thetaold(2), thetdold(2), theddold(2), RmatOld(2,:,:), RdotOld(2,:,:), RddtOld(2,:,:))

    NRmat = 0.0d0; NRdot = 0.0d0; NRddt = 0.0d0
    NRmatOld = 0.0d0; NRdotOld = 0.0d0; NRddtOld = 0.0d0


    do nn = 1, mesh%NNODE
      if (mesh%Coord_ID(nn) == 31) then
     ! The volume containing the first fin
        NRmat(:,:,nn) = Rmat(1,:,:)
        NRdot(:,:,nn) = Rdot(1,:,:)
        NRddt(:,:,nn) = Rddt(1,:,:)
        NRmatOld(:,:,nn) = RmatOld(1,:,:)
        NRdotOld(:,:,nn) = RdotOld(1,:,:)
        NRddtOld(:,:,nn) = RddtOld(1,:,:)
     endif
      if (mesh%Coord_ID(nn) == 32) then
     ! The volume containing the second fin
        NRmat(:,:,nn) = Rmat(2,:,:)
        NRdot(:,:,nn) = Rdot(2,:,:)
        NRddt(:,:,nn) = Rddt(2,:,:)
        NRmatOld(:,:,nn) = RmatOld(2,:,:)
        NRdotOld(:,:,nn) = RdotOld(2,:,:)
        NRddtOld(:,:,nn) = RddtOld(2,:,:)
      end if
    end do
    ! Predictor: rotation

    st%dg = 0d0
    st%ugm = 0d0
    st%acgm = 0d0
    do nn = 1, mesh%NNODE
     do dd = 1,3
       if((mesh%Coord_ID(nn) == 31).or. (mesh%Coord_ID(nn) == 32)) then
           st%dg(nn,dd) =   sum(NRmat(dd,:,nn)*(mesh%xg(nn,1:3)-Center_Rot(1,1:3)))
           st%ugm(nn,dd) =  sum(NRdot(dd,:,nn)*(mesh%xg(nn,1:3)-Center_Rot(1,1:3)))
           st%acgm(nn,dd) = sum(NRddt(dd,:,nn)*(mesh%xg(nn,1:3)-Center_Rot(1,1:3)))
      endif
     enddo
    enddo

    do i = 1, mesh%NBOUND
      if (mesh%bound(i)%FACE_ID == 21) then     
        do j = 1, mesh%bound(i)%NNODE
          n = mesh%bound(i)%BNODES(j) 
          st%ug(n,:) =  st%ugm(n,:)
          st%acg(n,:) = st%acgm(n,:)
        end do
      end if
    end do

    if (ismaster) then
      write(*,'(60("="))')
      write(*,*) "Time Step Number FEM_move:"
      write(*,'(60("="))')
      write(*,*)  "Rotation 1:",Center_Rot(1,1:3)
      write(*,*)  "Rotation 2:",Center_Rot(2,1:3)      
      write(*,*) "Current theta is:",  theta_base*180.0d0/pi
      write(*,*) "Current velocity is:",thetd_base*180.0d0/pi
      write(*,*) "Current acceleration is:",  thedd_base*180.0d0/pi
      write(*,'(60("="))')
    end if

   
  end subroutine FinIC_move    

  !-------------------------------------------------------------------------------------------------------          
  subroutine getAlphaState(alpha, old, new) 
    use mpi
    use params
    implicit none
  
    type(state), intent(inout) :: alpha
    type(state), intent(in)    :: old,new


  if (ismaster) then
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "almi", almi
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "alfi", alfi
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "gami", gami
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "beti", beti
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "ogam", ogam
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "obet", obet
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "mgam", mgam
    ! write(*,"(a20,1x,' = ',1x,ES12.4)") "malm", malm
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "malf", malf
    !write(*,"(a20,1x,' = ',1x,ES12.4)") "mbet", mbet
  endif
    malm = 1.0d0-almi
   ! if(ismaster) write(*,"(a20,1x,' = ',1x,ES12.4)") "malm1", malm
    alpha%acg  = almi*new%acg  + malm*old%acg
    alpha%acgm = almi*new%acgm + malm*old%acgm
    alpha%ug   = alfi*new%ug   + malf*old%ug
    alpha%ugm  = alfi*new%ugm  + malf*old%ugm
    alpha%dg   = alfi*new%dg   + malf*old%dg 
    !alpha%pg   = new%pg
    alpha%pg   = alfi*new%pg   + malf*old%pg 
    alpha%phig  = alfi*new%phig  + malf*old%phig
  !  if(ismaster) write(*,"(a20,1x,' = ',1x,ES12.4)") "malm2", malm
    alpha%rphig = almi*new%rphig + malm*old%rphig   
    alpha%phidg = alfi*new%phidg + malf*old%phig      ! NOTE: Modified old value
! YB
!    alpha%phidg = new%phidg

    alpha%phicg = alfi*new%phicg                      ! NOTE: Modified old value
     
    alpha%rhog  = alfi*new%rhog  + malf*old%rhog  
    alpha%drhog = alfi*new%drhog                      ! NOTE: Modified old value
    alpha%rho0g = alfi*new%rho0g + malf*old%rho0g
    alpha%mug   = alfi*new%mug   + malf*old%mug   

    alpha%hg   = alfi*new%hg   + malf*old%hg  
    alpha%heg  = alfi*new%heg  + malf*old%heg  
    alpha%hepg = alfi*new%hepg + malf*old%hepg  
                
  end subroutine getAlphaState  
  !-------------------------------------------------------------------------------------------------------          
  subroutine getMidPointState(alpha, old, new) 
  
    use params
    implicit none
  
    type(state), intent(inout) :: alpha
    type(state), intent(in)    :: old,new

    alpha%acg  = 0.5d0*(new%acg  + old%acg)
    alpha%acgm = 0.5d0*(new%acgm + old%acgm)
    alpha%ug   = 0.5d0*(new%ug   + old%ug)
    alpha%ugm  = 0.5d0*(new%ugm  + old%ugm)
    alpha%dg   = 0.5d0*(new%dg   + old%dg)
    !alpha%pg   = new%pg
    alpha%pg  = 0.5d0*(new%pg   + old%pg)    

    alpha%phig  = 0.5d0*(new%phig  + old%phig)
    alpha%rphig = 0.5d0*(new%rphig + old%rphig)   
    alpha%phidg = 0.5d0*(new%phidg + old%phig)   ! NOTE: Modified old value 
    alpha%phicg = 0.5d0*new%phicg                ! NOTE: Modified old value
     
    alpha%rhog  = 0.5d0*(new%rhog  + old%rhog)  
    alpha%drhog = 0.5d0* new%drhog               ! NOTE: Modified old value
    alpha%rho0g = 0.5d0*(new%rho0g + old%rho0g)
    alpha%mug   = 0.5d0*(new%mug   + old%mug)   

    alpha%hg   = 0.5d0*(new%hg   + old%hg)  
    alpha%heg  = 0.5d0*(new%heg  + old%heg)  
    alpha%hepg = 0.5d0*(new%hepg + old%hepg)  
                
  end subroutine getMidPointState   
  
  !-------------------------------------------------------------------------------------------------------          
  subroutine predictState(new, old)     
    use params
    use mpi
    implicit none
    
    type(state), intent(inout)  :: old,new    
   ! write(*,*) myid,"prediction stage"
    new%ug  = old%ug 
    new%acg = (gami-1.0d0)/gami*old%acg 
    new%pg  = old%pg
 
    new%phig  = old%phig
    new%rphig = (gami-1.0d0)/gami*old%rphig
    
    new%phidg = old%phig
    new%phicg = 0d0
       
    new%ugm  = old%ugm
    new%acgm = (gami-1.0d0)/gami*old%acgm
    new%dg   = old%dg  + Delt*old%ugm + &
             0.5d0*Delt*Delt*((1d0-2d0*beti)*old%acgm + 2d0*beti*new%acgm)   

  ! new%ugm  = old%ugm
  ! new%acgm = old%acgm
  ! new%dg   = old%dg
                                                 
  end subroutine predictState  

  !-------------------------------------------------------------------------------------------------------          
  subroutine incrementState(st, vec, fact,flag)   
  
    use params
    implicit none
    
    type(state), intent(inout)  :: st
    real(8),     intent(in)     :: vec(st%NNODE,10),fact
    integer,     intent(in) :: flag(4)
    real(8) :: fact1,fact2,fact3
   
    
    fact1 = gami*Delt
    fact2 = alfi*gami*Delt
    fact3 = beti*Delt*Delt
    if(flag(1).eq.1) then
     st%acg   = st%acg   +       fact*vec(:,1:3)
     st%ug    = st%ug    + fact*fact1*vec(:,1:3)  
     st%pg    = st%pg    + fact*fact1*vec(:,4)
     st%rphig = st%rphig +       fact*vec(:,5) 
     st%phig  = st%phig  + fact1*fact*vec(:,5) 
   ! st%phidg = st%phidg + fact*fact1*vec(:,6)   !  vec(:,6)=0d0
   ! st%phidg = st%phig
    endif
  
   !  st%phicg = st%phicg +  Delt*fact*vec(:,10)  !  vec(:,10)=0d0
    if(flag(2).eq.1) then
     st%acgm  = st%acgm  +       fact*vec(:,7:9)
     st%ugm   = st%ugm   + fact1*fact*vec(:,7:9)
     st%dg    = st%dg    + fact3*fact*vec(:,7:9)
    endif
  
  end subroutine incrementState

  subroutine incrementState_mass(st, vec, fact)

    use params
    implicit none

    type(state), intent(inout)  :: st
    real(8),     intent(in)     :: vec(st%NNODE,10),fact
    real(8) :: fact1,fact2,fact3
    st%phicg = st%phicg +  Delt*fact*vec(:,10)  !  vec(:,10)=0d0

  end subroutine incrementState_mass

  subroutine incrementState_rd(st,vec)   
    use params
    implicit none
    type(state), intent(inout)  :: st
    real(8),     intent(in)     :: vec(st%NNODE,1)
    st%phidg = st%phidg+vec(:,1)
  end subroutine incrementState_rd


 subroutine incrementState_DG(st, vec, fact)   
  
    use params
    implicit none
    type(state), intent(inout)  :: st
    real(8),     intent(in)     :: vec(st%NNODE,4),fact
    real(8) :: fact1,fact2,fact3
    fact1 = gami*Delt
    fact2 = alfi*gami*Delt
    fact3 = beti*Delt*Delt
    st%acg   = st%acg   +       fact*vec(:,1:3)
    st%ug    = st%ug    + fact*fact1*vec(:,1:3)  
    st%pg    = st%pg    + fact*fact1*vec(:,4)
 
  end subroutine incrementState_DG


 subroutine incrementState_DG_LS(st, vec, fact)   
  
    use params
    implicit none
    type(state), intent(inout)  :: st
    real(8),     intent(in)     :: vec(st%NNODE),fact
    real(8) :: fact1,fact2,fact3
    fact1 = gami*Delt
    fact2 = alfi*gami*Delt
    fact3 = beti*Delt*Delt
    st%rphig = st%rphig +       fact*vec 
    st%phig  = st%phig  + fact1*fact*vec
 
  end subroutine incrementState_DG_LS
  


 subroutine incrementState_DG_LS_RD(st, vec, fact)   
  
    use params
    implicit none
    type(state), intent(inout)  :: st
    real(8),     intent(in)     :: vec(st%NNODE),fact
    st%phidg = st%phidg+vec(:)*fact
  end subroutine incrementState_DG_LS_RD

!!$!-------------------------------------------------------------------------------------------------------          
!!$
!!$  subroutine increment_mfree(st, vec, fact)   
!!$  
!!$    use params
!!$    implicit none
!!$    
!!$    type(state), intent(inout)  :: st
!!$    real(8),     intent(in)     :: vec(st%NNODE,10),fact
!!$    real(8) :: fact1,fact2,fact3
!!$    
!!$    fact1 = gami*Delt
!!$    fact2 = alfi*gami*Delt
!!$    fact3 = beti*Delt*Delt
!!$    
!!$    st%acg   = st%acg   +       fact*vec(:,1:3)
!!$    st%ug    = st%ug    + fact*fact1*vec(:,1:3)  
!!$    st%pg    = st%pg    + fact*fact2*vec(:,4)
!!$
!!$!YB
!!$
!!$    st%rphig = st%rphig +       fact*vec(:,5) 
!!$    st%phig  = st%phig  + fact1*fact*vec(:,5) 
!!$    st%phidg = st%phidg + fact1*fact*vec(:,6)  
!!$  
!!$    st%phicg = st%phicg +  Delt*fact*vec(:,10) 
!!$  
!!$    st%acgm  = st%acgm  +       fact*vec(:,7:9)
!!$    st%ugm   = st%ugm   + fact1*fact*vec(:,7:9)
!!$    st%dg    = st%dg    + fact3*fact*vec(:,7:9)
!!$  
!!$  end subroutine increment_mfree

end module solution
