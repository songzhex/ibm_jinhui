module matrix
  implicit none
  
  type sparseStruct 
    integer :: icnt, nnode 
    integer, allocatable :: row(:), col(:)
  end type sparseStruct  
  
  !------------------------------------------------------------------------      
  contains  
  !------------------------------------------------------------------------
  subroutine genSparseStruct(mat,mesh)

    use meshData
    implicit none
    
    type(sparseStruct), intent(out) :: mat
    type(meshStruct), intent(in)    :: mesh
    
    integer :: tmpr(mesh%NNODE) 
    integer :: adjcnt(mesh%NNODE), mloc(1)
    integer :: i, j, k, imin, ibig, ncol, icnt 
    integer :: row_fill_list(mesh%NNODE,27*mesh%maxNSHL)

    ! compute sparse matrix data structures  
    row_fill_list = 0   
    adjcnt = 0  
    call Asadj(mesh,row_fill_list, adjcnt)
  
    ! allocate
    mat%icnt  = sum(adjcnt)
    mat%nnode = mesh%NNODE

    allocate(mat%col(mesh%NNODE+1), mat%row(mat%icnt))

    ! build the colm array  
    mat%col(1) = 1
    do i = 1, mesh%NNODE
      mat%col(i+1) = mat%col(i) + adjcnt(i)
    enddo
  
    ! sort the rowp into increasing order  
    ibig = 10*mesh%NNODE
    icnt = 0
    do i = 1, mesh%NNODE
      ncol = adjcnt(i)
      tmpr(1:ncol) = row_fill_list(i,1:ncol)
      do j = 1, ncol
        icnt = icnt+1
        imin = minval(tmpr(1:ncol))
        mloc = minloc(tmpr(1:ncol))
        mat%row(icnt) = imin
        tmpr(mloc(1)) = ibig
      enddo
    enddo

  end subroutine genSparseStruct

  !------------------------------------------------------------------------
  subroutine Asadj(mesh, row_fill_list, adjcnt)
  
    use meshData
    implicit none
  
    type(meshStruct), intent(in)  :: mesh
    integer, intent(out) :: row_fill_list(mesh%NNODE,27*mesh%maxNSHL)
    integer, intent(out) :: adjcnt(mesh%NNODE)

    integer :: k, i, j, ni, nj, nk, l, ibroke, knd, nshl, jnd, jlngth
  
    integer, allocatable :: ndlist(:)

    adjcnt = 0

    do i = 1, mesh%NELEM
     
      nshl = mesh%ELMNSHL(i)
      allocate(ndlist(nshl))

      do j = 1, nshl
        ndlist(j) = mesh%IEN(i,j)  ! gen list of global "nodes" for this element
      end do
     
      do j = 1, nshl
        jnd = ndlist(j)   ! jnd is the global "node" we are working on
        jlngth = adjcnt(jnd)  ! current length of j's list
        do k = 1, nshl
          knd = ndlist(k)
          ibroke = 0
       
          do l = 1, jlngth   ! row_fill_list is, for each node, the
            ! list of nodes that I have already
            ! detected interaction with
            if (row_fill_list(jnd,l) == knd) then
              ibroke = 1
              exit
            end if
          end do
            
          ! to get here k was not in  j's list so add it     
          if (ibroke == 0) then
            jlngth = jlngth + 1 ! lengthen list     
            if (jlngth.gt.27*mesh%maxNSHL) write(*,*) "Stop the Music!!!"
      
            row_fill_list(jnd,jlngth) = knd ! add unique entry to list
          end if
        end do       ! finished checking all the k's for this j
        adjcnt(jnd) = jlngth  ! update the counter
      end do      ! done with j's
  
      deallocate(ndlist)    
     
    end do         ! done with elements in this block
  
  end subroutine Asadj

  !------------------------------------------------------------------------
  subroutine SparseMatLoc(list, n, goal, locat)  
    implicit none
    integer, intent(in) :: n, list(n), goal
    integer, intent(out):: locat
    integer :: rowvl, rowvh, rowv

    ! Initialize     
    rowvl = 1
    rowvh = n + 1
     
    ! do a binary search
    do 
      if (rowvh-rowvl > 1) then
        rowv = (rowvh + rowvl) / 2
        if (list(rowv) > goal) then
          rowvh = rowv
        else
          rowvl = rowv
        end if
      else
        exit
      end if
    end do
    locat = rowvl    
  end subroutine SparseMatLoc
  
  !------------------------------------------------------------------------
  subroutine FillSparseMat(nshl, IEN, NVAR, LHSloc, mat, LHSglb) 

    implicit none
 
    integer,            intent(in)    :: nshl, IEN(nshl), NVAR  
    real(8),            intent(in)    :: LHSloc(NVAR*NVAR,NSHL,NSHL) 
    type(sparseStruct), intent(in)    :: mat  
    real(8),            intent(inout) :: LHSglb(NVAR*NVAR,mat%icnt)  
      
    integer :: a, b, c, n, k, locn, i
  
    do a = 1, NSHL
      i = IEN(a)
      c = mat%col(i)
      n = mat%col(i+1) - c
      do b = 1, NSHL
          
        call SparseMatLoc(mat%row(c:c+n-1), n, IEN(b), locn)

        k = locn + c - 1
 
        LHSglb(:,k) = LHSglb(:,k) + LHSloc(:,a,b)
       
      enddo    
    enddo
  
  end subroutine FillSparseMat 

  !------------------------------------------------------------------------
  subroutine FillSparseMat2(nshl, IEN, NVAR, LHSloc, mat, LHSglb) 

    implicit none
 
    integer,            intent(in)    :: nshl, IEN(nshl), NVAR  
    real(8),            intent(in)    :: LHSloc(NVAR*NVAR,NSHL,NSHL) 
    type(sparseStruct), intent(in)    :: mat  
    real(8),            intent(inout) :: LHSglb(NVAR*NVAR,mat%icnt)  
      
    integer :: a, b, c, n, k, locn, i
  
    do a = 1, NSHL
      i = IEN(a)
      c = mat%col(i)
      n = mat%col(i+1) - c
      do b = 1, NSHL
          
        call SparseMatLoc(mat%row(c:c+n-1), n, IEN(b), locn)

        k = locn + c - 1
        do i = 1, NVAR*NVAR
          LHSglb(i,k) = LHSglb(i,k) + LHSloc(i,a,b)
        enddo 
      enddo    
    enddo
  
  end subroutine FillSparseMat2
 
  !------------------------------------------------------------------------  
  subroutine GetSparseMat(nshl, IEN, NVAR, LHSloc, mat, LHSglb)  

    implicit none
    
    integer,            intent(in)    :: nshl, IEN(nshl), NVAR  
    real(8),            intent(out)   :: LHSloc(NVAR*NVAR,NSHL,NSHL) 
    type(sparseStruct), intent(in)    :: mat  
    real(8),            intent(in)    :: LHSglb(NVAR*NVAR,mat%icnt)  
      
    integer :: a, b, c, n, k, locn, i
    
    do a = 1, NSHL
      i = IEN(a)
      c = mat%col(i)
      n = mat%col(i+1) - c
      do b = 1, NSHL
    
        call SparseMatLoc(mat%row(c:c+n-1), n, IEN(b), locn)
    
        k = locn + c - 1
   
        LHSloc (:,a,b) = LHSglb(:,k)
    
      enddo     
    enddo

  end subroutine GetSparseMat
      
  !----------------------------------------------  
  subroutine SparseMatVec(NVAR, mat, lhs, input, output)
  
    use mpi    
    implicit none 
    
    integer,            intent(in)  :: NVAR  
    type(sparseStruct), intent(in)  :: mat  
    real(8),            intent(in)  :: LHS(NVAR*NVAR,mat%icnt)                
    real(8),            intent(in)  :: input (mat%NNODE,NVAR)
    real(8),            intent(out) :: output(mat%NNODE,NVAR)

    real(8) :: tmp(NVAR), soltmp(NVAR)   
    integer :: aa, bb, cc, i, ii
    
    call commu(input, NVAR, 'out')
    
    do aa = 1, mat%NNODE    
      tmp = 0d0
      do bb = mat%col(aa), mat%col(aa+1)-1
        cc = mat%row(bb)
        soltmp = input(cc,:)
  
        do i = 1, NVAR      
          ii = (i-1)*NVAR        
          tmp(i) = tmp(i) + sum(LHS(ii+1:ii+NVAR, bb)*soltmp)
        enddo
        
      end do
    
      output(aa,:) = tmp
    
    end do  
           
    call commu(output, NVAR, 'in ')
    call zeroslaves(input, NVAR)          
  
  end subroutine  SparseMatVec

  !----------------------------------------------  
  subroutine SparseMatVec_DG(NVAR, mat, lhs, input, output)
  
    use mpi    
    implicit none 
    
    integer,            intent(in)  :: NVAR  
    type(sparseStruct), intent(in)  :: mat  
    real(8),            intent(in)  :: LHS(NVAR*NVAR,mat%icnt)                
    real(8),            intent(in)  :: input (mat%NNODE,NVAR)
    real(8),            intent(out) :: output(mat%NNODE,NVAR)

    real(8) :: tmp(NVAR), soltmp(NVAR)   
    integer :: aa, bb, cc, i, ii
    
    call commu(input, NVAR, 'out')
    
    do aa = 1, mat%NNODE    
      tmp = 0d0
      do bb = mat%col(aa), mat%col(aa+1)-1
        cc = mat%row(bb)
        soltmp = input(cc,:)
  
        do i = 1, NVAR      
          ii = (i-1)*NVAR        
          tmp(i) = tmp(i) + sum(LHS(ii+1:ii+NVAR, bb)*soltmp)
        enddo
        
      end do
    
      output(aa,:) = tmp
    
    end do  
           
!    call commu(output, NVAR, 'in ')
!    call zeroslaves(input, NVAR)          
  
  end subroutine  SparseMatVec_DG
  
        
end module matrix
