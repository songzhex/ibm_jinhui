subroutine setBCs_turbine_norotation(SH,NRB_BEA)
  use aAdjKeep
  use commonvars
  use defs_shell
  use types_beam
  use params
  use globdata
  implicit none
    
  integer:: i, j, ii, jj
  type(shell_bld), intent(inout) :: SH
  type(mesh_beam), intent(inout) :: NRB_BEA
  real(8) :: Radius



      do i = 1, SH%NRB%NNODE
 !     Radius = sqrt((SH%NRB%B_NET_U(i,1)+11d0)**2+SH%NRB%B_NET_U(i,2)**2 + (SH%NRB%B_NET_U(i,3)-60)**2)

      if(abs(SH%NRB%B_NET_U(i,3) + 350) <0.00001d0) then  
           SH%NRB%IBC(i,:) = 1
           NRB_BEA%IBC(i,:) = 1
           write(123,*) i,SH%NRB%B_NET_U(i,1:3)
           do j = 1, 3
            SH%NRB%dsh(i,j) = 0.0d0
            SH%NRB%ush(i,j) = 0.0d0
            SH%NRB%ash(i,j) = 0.0d0    
           end do
         endif
      end do
    return
    do i = 5,15
       SH%NRB%IBC(i,:) = 1
       NRB_BEA%IBC(i,:) = 1
       write(123,*) i,SH%NRB%B_NET_U(i,1:3)
       do j = 1, 3
            SH%NRB%dsh(i,j) = 0.0d0
            SH%NRB%ush(i,j) = 0.0d0
            SH%NRB%ash(i,j) = 0.0d0    
        end do
    enddo

    do i = 17,27
       SH%NRB%IBC(i,:) = 1
       NRB_BEA%IBC(i,:) = 1
       write(123,*) i,SH%NRB%B_NET_U(i,1:3)
       do j = 1, 3
            SH%NRB%dsh(i,j) = 0.0d0
            SH%NRB%ush(i,j) = 0.0d0
            SH%NRB%ash(i,j) = 0.0d0    
        end do
    enddo

    do i = 29,39
       SH%NRB%IBC(i,:) = 1
       NRB_BEA%IBC(i,:) = 1
       write(123,*) i,SH%NRB%B_NET_U(i,1:3)
       do j = 1, 3
            SH%NRB%dsh(i,j) = 0.0d0
            SH%NRB%ush(i,j) = 0.0d0
            SH%NRB%ash(i,j) = 0.0d0    
        end do
    enddo


! The following is the previous boundary conditions

!  do i = 1, SH%NRB%NNODE
!    if(abs(SH%NRB%B_NET_U(i,3)+350) <= 1.0d-5 ) then
!      SH%NRB%IBC(i,:) = 1
!      NRB_BEA%IBC(i,:) = 1 
!      SH%NRB%dsh(i,:) = 0d0
!      SH%NRB%ush(i,:) = 0d0
!      SH%NRB%ash(i,:) = 0d0
!    endif
!  enddo
     
!  do i = 1, SH%FEM%NNODE
!    if(abs(SH%FEM%B_NET_U(i,3)+350) <= 1.0d-5 ) then
!      SH%FEM%IBC(i,:) = 1
!      SH%FEM%dsh(i,:) = 0d0
!      SH%FEM%ush(i,:) = 0d0
!      SH%FEM%ash(i,:) = 0d0
!    endif
!  enddo

end subroutine setBCs_turbine_norotation
