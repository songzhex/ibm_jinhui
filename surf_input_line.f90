!--------------------------------------------------------------------
! program to read in fem mesh (linear triangles so far)
!--------------------------------------------------------------------
subroutine surf_input_line(NSD, SUR)

  use defs_shell
  implicit none

  type(smesh), intent(out) :: SUR
  integer,     intent(in)  :: NSD
  integer :: i, j, k, ier, mf, iel, itmp(10)
  character(len=30) :: fname, cname, ctmp

  !===============================================
  ! read in FEM format
  !===============================================
  mf = 12

  fname = 'cylinder.msh'
  open(mf, file=fname, status='old')
     
  ! right now assume quad only
  SUR%maxNSHL   = 4
  SUR%maxNGAUSS = 4

  !------------
  ! Nodes
  do while (ctmp /= '$Nodes')  
    read(mf,*) ctmp
  end do
  read(mf,*) SUR%NNODE

  allocate(SUR%B_NET(  SUR%NNODE,NSD+1), &
           SUR%B_NET_U(SUR%NNODE,NSD+1), &
           SUR%B_NET_D(SUR%NNODE,NSD+1), &
           SUR%FORCE(  SUR%NNODE,NSD),   &
           SUR%IBC(    SUR%NNODE,NSD), stat=ier)
  if (ier /= 0) stop 'Allocation Error: SUR%B_NET'
  SUR%B_NET = 0.0d0; SUR%B_NET_U = 0.0d0; SUR%B_NET_D = 0.0d0 
  SUR%FORCE = 0.0d0
  SUR%IBC   = 0

  do i = 1, SUR%NNODE
    read(mf,*) itmp(1), (SUR%B_NET(i,j), j = 1, NSD)
  end do
  SUR%B_NET(:,4) = 1.0d0
  SUR%B_NET_U    = SUR%B_NET
  SUR%B_NET_D    = SUR%B_NET

  !-----------------
  ! quad Elements
  do while (ctmp /= '$Elements')
    read(mf,*) ctmp
  end do
  read(mf,*) SUR%NEL

  allocate(SUR%IEN(SUR%NEL,SUR%maxNSHL), SUR%NSHL(SUR%NEL), &
           SUR%NGAUSS(SUR%NEL), stat=ier)
  if (ier /= 0) stop 'Allocation Error: SUR%IEN'
  SUR%IEN    = 0
  SUR%NSHL   = SUR%maxNSHL
  SUR%NGAUSS = SUR%maxNGAUSS

  do iel = 1, SUR%NEL
    read(mf,*) itmp(1:5), (SUR%IEN(iel,j), j = 1, SUR%NSHL(iel))
  end do

  close(mf)

  ! allocate arrays for storing integration info
  allocate(SUR%gp%nor( SUR%NEL,SUR%maxNGAUSS,3), &
           SUR%gp%xg(  SUR%NEL,SUR%maxNGAUSS,3), &
           SUR%gp%gw(  SUR%NEL,SUR%maxNGAUSS),   &
           SUR%gp%detJ(SUR%NEL,SUR%maxNGAUSS), stat=ier)
  if (ier /= 0) stop 'Allocation Error: SUR%gp'

end subroutine surf_input_line



!--------------------------------------------------------------------
! program to read in fem mesh (linear triangles so far)
!--------------------------------------------------------------------
subroutine surf_input_line_new(NSD, SUR)

  use defs_shell
  implicit none

  type(smesh), intent(out) :: SUR
  integer,     intent(in)  :: NSD
  integer :: i, j, k, ier, mf, iel, itmp(10)
  character(len=30) :: fname, cname, ctmp

  !===============================================
  ! read in FEM format
  !===============================================
  mf = 12

  !fname = 'cylinder.dat'
  fname = 'bmesh.15.dat'
  open(mf, file=fname, status='old')
     
  read(mf,*) sur%maxnshl, sur%maxngauss

  ! right now assume triangles
  !SUR%maxNSHL   = 3
  !SUR%maxNGAUSS = 3

  read(mf,*) SUR%NNODE, SUR%NEL

  allocate(SUR%B_NET(  SUR%NNODE,NSD+1), &
           SUR%B_NET_U(SUR%NNODE,NSD+1), &
           SUR%B_NET_D(SUR%NNODE,NSD+1), &
           SUR%FORCE(  SUR%NNODE,NSD),   &
           SUR%IBC(    SUR%NNODE,NSD), stat=ier)
  if (ier /= 0) stop 'Allocation Error: SUR%B_NET'
  SUR%B_NET = 0.0d0; SUR%B_NET_U = 0.0d0; SUR%B_NET_D = 0.0d0 
  SUR%FORCE = 0.0d0
  SUR%IBC   = 0

  allocate(SUR%IEN(SUR%NEL,SUR%maxNSHL), SUR%NSHL(SUR%NEL), &
           SUR%NGAUSS(SUR%NEL), stat=ier)
  if (ier /= 0) stop 'Allocation Error: SUR%IEN'
  SUR%IEN    = 0
  SUR%NSHL   = SUR%maxNSHL
  SUR%NGAUSS = SUR%maxNGAUSS

  do i = 1, SUR%NNODE
    read(mf,*) (SUR%B_NET(i,j), j = 1, NSD)
  end do
  SUR%B_NET(:,4) = 1.0d0
  SUR%B_NET_U    = SUR%B_NET
  SUR%B_NET_D    = SUR%B_NET

  do iel = 1, SUR%NEL
    read(mf,*) (SUR%IEN(iel,j), j = 1, SUR%NSHL(iel))
  end do

  close(mf)

  ! allocate arrays for storing integration info
  allocate(SUR%gp%nor( SUR%NEL,SUR%maxNGAUSS,3), &
           SUR%gp%xg(  SUR%NEL,SUR%maxNGAUSS,3), &
           sur%gp%traction(sur%nel,sur%maxngauss,3),&
           sur%gp%ush(sur%nel,sur%maxngauss,3),&
           sur%gp%backelem(sur%nel,sur%maxngauss),&
           sur%gp%backxi(sur%nel,sur%maxngauss,3),&
           SUR%gp%gw(  SUR%NEL,SUR%maxNGAUSS),   &
           SUR%gp%detJ(SUR%NEL,SUR%maxNGAUSS), stat=ier)
  if (ier /= 0) stop 'Allocation Error: SUR%gp'

end subroutine surf_input_line_new
