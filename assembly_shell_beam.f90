!======================================================================
! Solving the Shell problem using fluid force
!======================================================================
subroutine assembly_shell_beam(msh, SH, STRU, NRB_BEA, BEA, mNRB_BEA, newJac, bblade)

  use aAdjKeep
  use mpi
  use commonvars
  use commonpars
  use defs_shell
  use types_structure
  use types_beam
  use params 
  use meshData
  implicit none 
  type(meshStruct), intent(in) :: msh
  type(shell_bld), intent(inout) :: SH
  type(structure), intent(inout) :: STRU
  type(mesh_beam), intent(inout) :: NRB_BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh), intent(inout) :: bblade(3)
  integer,intent(in) :: newJac
  
  integer, parameter :: NSD = 3
  real(8), allocatable :: r2tmp1(:,:), r2tmp2(:,:), SHfemTq(:), SHnrbTq(:),r2tmp11(:,:) ! The torque
!  real(8) :: dshAlpha(SH%TSP%NNODE,NSD), ushAlpha(SH%TSP%NNODE,NSD), &
!            ashAlpha(SH%TSP%NNODE,NSD), ltq
  real(8) :: dshAlpha(SH%NRB%NNODE,NSD), ushAlpha(SH%NRB%NNODE,NSD), &
             ashAlpha(SH%NRB%NNODE,NSD), ltq, length, ForceSum(3,3), &
             DispSum(3,3), DispMax(2,3)
  integer :: i, j, k, bb, ii, inewt_SH, ibld
  character(len=80) :: fname, iname, cname
  real(8) :: temp_in(3), temp_out(3), temp_sum(3)
  integer :: seed1 = 1
  integer :: seed2 = 2
  integer :: seed3 = 454500000
  !===============================================================
  ! Begin SHELL analysis
  !===============================================================
  
  do ibld = 1, NBlade !CHANGE
    allocate(r2tmp1(bblade(ibld)%NNODE,NSD))
    r2tmp1 = 0d0
   ! allocate(bblade(ibld)%FORCE(bblade(ibld)%NNODE,NSD)) ! Jinhui
    bblade(ibld)%FORCE = 0.0d0
    bb = ibld + SH%bmap   ! mapping between blade and boundary...
    do i = 1, msh%bound(bb)%NNODE
    !  write(*,*) myid,bb,msh%bound(bb)%NNODE,msh%bound(bb)%FACE_ID,":",i,msh%bound(bb)%L2SNODE(i),msh%bound(bb)%BNODES(i)
      r2tmp1(msh%bound(bb)%L2SNODE(i),:) = RHSGu(msh%bound(bb)%BNODES(i),:)
    end do
    ! RHSGu
    bblade(ibld)%FORCE = r2tmp1

    ! sum up the local force such that the shell force vector is complete
    if (numnodes > 1) then
     

     call MPI_ALLREDUCE(r2tmp1, bblade(ibld)%FORCE, &
                         bblade(ibld)%NNODE*NSD, &
                         MPI_DOUBLE_PRECISION, &
                         MPI_SUM, MPI_COMM_WORLD, mpi_err)

    end if

    deallocate(r2tmp1)
  end do
   ! broadcast to all the processes
  STRU%RHSG     = 0.0d0    ! initialize
  STRU%RHSG_EXT = 0.0d0
  STRU%LHSK  = 0.0d0

  STRU%RHSG_beam  = 0.0d0  
  STRU%LHSK_beam  = 0.0d0

  STRU%RHSG_shell  = 0.0d0  
  STRU%LHSK_shell  = 0.0d0



 if (solshell) then
    !-----------------------------------------------------
    ! Since fluid force is pre-integrated RHS, we need to
    ! Project it to nodal points
    !-----------------------------------------------------
    SH%FEM%FORCE = bblade(1)%FORCE
 
    if(ismaster) write(*,*) "fluid", sum(SH%FEM%FORCE(:,1)),sum(SH%FEM%FORCE(:,2)),sum(SH%FEM%FORCE(:,3))
    allocate(r2tmp1(SH%FEM%NNODE,NSD))
    r2tmp1 = 0.0d0
    call f2s_l2project(SH%FEM, nsd, SH%FEM%FORCE, r2tmp1)
    !--------------------------------------------------------------
    ! Get the external force (pre-integrated rhs) for t-spline
    ! Only TSP%FORCE is useful. SH%LHSK, SH%mg are useless here
    !--------------------------------------------------------------
    allocate(r2tmp2(SH%NRB%NNODE,NSD))
    r2tmp2 = 0.0d0
    !SH%TSP%FORCE = 0.0d0
    SH%NRB%FORCE = 0.0d0

    call f2n_IntElmAss(SH%NRB, SH%FEM, &
                       nsd, r2tmp1, SH%NRB%FORCE, r2tmp2)
     
    if(ismaster) write(*,*) "Struc", sum(SH%NRB%FORCE(:,1)), sum(SH%NRB%FORCE(:,2)),sum(SH%NRB%FORCE(:,3))
    SH%LHSK = 0.0d0
    deallocate(r2tmp1, r2tmp2)
    ! converge geometrical nonlinearity
   
      ashAlpha = SH%NRB%ashOld + almi*(SH%NRB%ash-SH%NRB%ashOld)
      ushAlpha = SH%NRB%ushOld + alfi*(SH%NRB%ush-SH%NRB%ushOld)
      dshAlpha = SH%NRB%dshOld + alfi*(SH%NRB%dsh-SH%NRB%dshOld)

    !  STRU%RHSG     = 0.0d0    ! initialize
    !  STRU%RHSG_EXT = 0.0d0
    !  STRU%LHSK  = 0.0d0

      SH%NRB%B_NET_D(:,1:3) = SH%NRB%B_NET_U(:,1:3) + dshAlpha(:,:)

      NRB_BEA%B_NET_D_alphaf(:,:)= SH%NRB%B_NET_D(:,:)

      NRB_BEA%B_NET_Dt_alphaf(:,:)= ushAlpha

      NRB_BEA%B_NET_DDt_alpham(:,:)= ashAlpha
   
      NRB_BEA%maxNSHL=max(NRB_BEA%maxNSHL, SH%NRB%maxNSHL)
    if(newJac > 0) then
     call IntElmAss_beam(NRB_BEA, STRU%icnt, STRU%col, STRU%row, &
              NSD, BEA%NMat, &
               BEA%Ec, BEA%Dens, BEA%Rad, BEA%C_dp, &
               BEA%G_fact, length, &
               STRU%RHSG_beam, STRU%LHSK_beam, &
               Delt, almi, alfi, gami, beti, newJac)
   else
     call IntElmAss_beam_rhsonly(NRB_BEA, STRU%icnt, STRU%col, STRU%row, &
             NSD, BEA%NMat, &
             BEA%Ec, BEA%Dens, BEA%Rad, BEA%C_dp, &
              BEA%G_fact, length, &
              STRU%RHSG_beam, STRU%LHSK_beam, &
              Delt, almi, alfi, gami, beti, newJac)
   endif
    if(ismaster) write(*,*) "after beam:", sum(STRU%RHSG_beam(:,1)),sum(STRU%RHSG_beam(:,2)),sum(STRU%RHSG_beam(:,3))


  endif
 
   
    call MPI_BCAST(ashAlpha , SH%NRB%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, 0, &
                   MPI_COMM_WORLD, mpi_err)

    call MPI_BCAST(ushAlpha , SH%NRB%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, 0, &
                   MPI_COMM_WORLD, mpi_err)

    call MPI_BCAST(dshAlpha , SH%NRB%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, 0, &
                   MPI_COMM_WORLD, mpi_err)

    call MPI_BCAST(SH%NRB%B_NET_D, SH%NRB%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, 0, &
                   MPI_COMM_WORLD, mpi_err)

    call MPI_BCAST(SH%NRB%IBC , SH%NRB%NNODE*NSD, &
                   MPI_INTEGER, 0, &
                   MPI_COMM_WORLD, mpi_err)


  if (numnodes > 1) call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)
   


  !  STRU%RHSG = 0d0
    if(newJac > 0) then
     call IntElmAss_shell1(SH, SH%NRB, STRU%icnt, STRU%col, STRU%row, &
                            nsd, &
                            SH%G_fact, STRU%RHSG_shell, STRU%LHSK_shell,  &
                            ashAlpha, ushAlpha, alfi, beti, almi, &
                            gami,Delt, newJac)

    else       
    call IntElmAss_shell1_rhsonly(SH, SH%NRB, STRU%icnt, STRU%col, STRU%row, &
                            nsd, &
                            SH%G_fact, STRU%RHSG_shell, STRU%LHSK_shell,  &
                            ashAlpha, ushAlpha, alfi, beti, almi, &
                            gami,Delt, newJac)
   endif 
   if(solshell) then
    STRU%RHSG = STRU%RHSG_shell + STRU%RHSG_beam   
    STRU%LHSK = STRU%LHSK_shell + STRU%LHSK_beam
    if(ismaster) write(*,*) "after shell",sum(STRU%RHSG(:,1)),sum(STRU%RHSG(:,2)),sum(STRU%RHSG(:,3))
     STRU%RHSG_EXT = SH%NRB%FORCE
     do i = 1, SH%NRB%NNODE
        if (SH%NRB%IBC(i,1) == 1) then
         STRU%RHSG_EXT(i,:) = 0.0d0
        end if
     end do
      write(*,*) 'Inner res:',sum(STRU%RHSG(:,1)),sum(STRU%RHSG(:,2)),sum(STRU%RHSG(:,3))
      STRU%RHSG = STRU%RHSG + STRU%RHSG_EXT ! external force
      write(*,*) 'Total res:', sum(STRU%RHSG(:,1)),sum(STRU%RHSG(:,2)),sum(STRU%RHSG(:,3))  
   endif ! if (solshell) then
   if(myid>0) then 
    STRU%RHSG     = 0.0d0    ! initialize
    STRU%RHSG_EXT = 0.0d0
    STRU%LHSK  = 0.0d0
    STRU%RHSG_beam  = 0.0d0  
    STRU%LHSK_beam  = 0.0d0
    STRU%RHSG_shell  = 0.0d0  
    STRU%LHSK_shell  = 0.0d0
   endif

end subroutine assembly_shell_beam
