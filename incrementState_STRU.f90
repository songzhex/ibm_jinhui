
!======================================================================
! Solving the Shell problem using fluid force
!======================================================================
subroutine incrementState_STRU(NNODE,vec,fact,SH,STRU, NRB_BEA, BEA, mNRB_BEA,bblade)  
! Jinhui: project everything
  use aAdjKeep
  use mpi
  use commonvars
  use commonpars
  use defs_shell
  use types_structure
  use types_beam
  use params
  use globdata
  implicit none 
  
  integer,parameter::NSD = 3
  type(shell_bld), intent(inout) :: SH
  type(structure), intent(inout) :: STRU
  type(mesh_beam), intent(inout) :: NRB_BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh), intent(inout) :: bblade(3)
  integer,intent(in) :: NNODE
  real(8),intent(in) :: vec(NNODE,NSD)
  real(8),intent(in) :: fact

  real(8), allocatable :: SHfemTq(:), SHnrbTq(:)  ! The torque
  real(8) :: dshAlpha(SH%NRB%NNODE,NSD), ushAlpha(SH%NRB%NNODE,NSD), &
             ashAlpha(SH%NRB%NNODE,NSD), ltq, length, ForceSum(3,3), &
             DispSum(3,3), DispMax(2,3)
  integer :: i, j, k, bb, ii, inewt_SH, ibld
  character(len=80) :: fname, iname, cname

  if(NNODE.ne.SH%NRB%NNODE) write(*,*) "Error: NNODE in incrementState_STRU"

  if(ismaster.and.move) then 
     
      ashAlpha = 0.0d0
      ashAlpha = vec

      SH%NRB%ash(:,:) = SH%NRB%ash(:,:) + ashAlpha(:,:)*fact
      SH%NRB%ush(:,:) = SH%NRB%ush(:,:) + ashAlpha(:,:)*gami*Delt*fact
      SH%NRB%dsh(:,:) = SH%NRB%dsh(:,:) + ashAlpha(:,:)*beti*Delt*Delt*fact
! Jinhui: two parts
      NRB_BEA%B_NET_Dt = SH%NRB%ush
      NRB_BEA%B_NET_DDt = SH%NRB%ash
      NRB_BEA%B_NET_D(:,1:3) = NRB_BEA%B_NET_D(:,1:3) + ashAlpha(:,:)*beti*Delt*Delt*fact
! Jinhui: Update the solution on IGA   
    !  write(*,*) "nrb ash:", sqrt(sum(SH%NRB%ash(:,1)**2 + &
    !                       SH%NRB%ash(:,2)**2 + &
    !                       SH%NRB%ash(:,3)**2 ))
    ! write(*,*) "nrb ush:", sqrt(sum(SH%NRB%ush(:,1)**2 + &
    !                      SH%NRB%ush(:,2)**2 + &
    !                      SH%NRB%ush(:,3)**2 ))
    ! write(*,*) "nrb dsh:", sqrt(sum(SH%NRB%dsh(:,1)**2 + &
    !                      SH%NRB%dsh(:,2)**2 + &
    !                      SH%NRB%dsh(:,3)**2 ))
    if(abs(fact)<1.0d0) then
     ash_pertubation = 0d0
     call n2f_l2project(SH%FEM, SH%NRB, NSD, ashAlpha(:,:), ash_pertubation)
   !  write(*,*) "increment ash:", sqrt(sum(ash_pertubation(:,1)**2 + &
   !                            ash_pertubation(:,2)**2 + &
   !                            ash_pertubation(:,3)**2 ))
     write(*,*) "fluid: project the pertubation"
    endif

      do i = 1, SH%FEM%NNODE
       if (SH%FEM%IBC(i,1) .ne. 1) then
          SH%FEM%ash(i,:) = ash_base(i,:) + ash_pertubation(i,:)*fact
          SH%FEM%ush(i,:) = ush_base(i,:) + ash_pertubation(i,:)*gami*Delt*fact
          SH%FEM%dsh(i,:) = dsh_base(i,:) + ash_pertubation(i,:)*beti*Delt*Delt*fact
       endif
      enddo

     ! write(*,*) "fem ash:", sqrt(sum(SH%FEM%ash(:,1)**2 + &
     !                      SH%FEM%ash(:,2)**2 + &
     !                      SH%FEM%ash(:,3)**2 ))
     ! write(*,*) "fem ush:", sqrt(sum(SH%FEM%ush(:,1)**2 + &
     !                      SH%FEM%ush(:,2)**2 + &
     !                      SH%FEM%ush(:,3)**2 ))
     ! write(*,*) "fem dsh:", sqrt(sum(SH%FEM%dsh(:,1)**2 + &
     !                      SH%FEM%dsh(:,2)**2 + &
     !                      SH%FEM%dsh(:,3)**2 ))

   endif

  ! all processors allocate the boundary solution array
  do ibld = 1, NBlade
!    allocate(bblade(ibld)%ash(bblade(ibld)%NNODE,NSD))
!    allocate(bblade(ibld)%ush(bblade(ibld)%NNODE,NSD))
!    allocate(bblade(ibld)%dsh(bblade(ibld)%NNODE,NSD))
    bblade(ibld)%ash = 0.0d0
    bblade(ibld)%ush = 0.0d0
    bblade(ibld)%dsh = 0.0d0
  end do

  ! only those who solve the shell problem would assign
  ! the solution
  if (solshell) then
    if (myid == 0) then
      bblade(1)%ash = SH%FEM%ash
      bblade(1)%ush = SH%FEM%ush
      bblade(1)%dsh = SH%FEM%dsh
    end if
  end if


  ! broadcast such that all processors will have the solutions
  do ibld = 1, NBlade
    call MPI_BCAST(bblade(ibld)%ash, bblade(ibld)%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)

    call MPI_BCAST(bblade(ibld)%ush, bblade(ibld)%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)

    call MPI_BCAST(bblade(ibld)%dsh, bblade(ibld)%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)
  end do


  if (numnodes > 1) call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)


end subroutine incrementstate_stru



!======================================================================
! Solving the Shell problem using fluid force
!======================================================================
subroutine incrementState_STRU1(NNODE,vec,fact,SH,STRU, NRB_BEA, BEA, mNRB_BEA,bblade)  
! Jinhui: project everything
  use aAdjKeep
  use mpi
  use commonvars
  use commonpars
  use defs_shell
  use types_structure
  use types_beam
  use params
  use globdata
  implicit none 
  
  integer,parameter::NSD = 3
  type(shell_bld), intent(inout) :: SH
  type(structure), intent(inout) :: STRU
  type(mesh_beam), intent(inout) :: NRB_BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh), intent(inout) :: bblade(3)
  integer,intent(in) :: NNODE
  real(8),intent(in) :: vec(NNODE,NSD)
  real(8),intent(in) :: fact



  real(8), allocatable :: r2tmp1(:,:), r2tmp2(:,:),r2tmp3(:,:), SHfemTq(:), SHnrbTq(:)  ! The torque
!  real(8) :: dshAlpha(SH%TSP%NNODE,NSD), ushAlpha(SH%TSP%NNODE,NSD), &
!             ashAlpha(SH%TSP%NNODE,NSD), ltq
  real(8) :: dshAlpha(SH%NRB%NNODE,NSD), ushAlpha(SH%NRB%NNODE,NSD), &
             ashAlpha(SH%NRB%NNODE,NSD), ltq, length, ForceSum(3,3), &
             DispSum(3,3), DispMax(2,3)
  integer :: i, j, k, bb, ii, inewt_SH, ibld
  character(len=80) :: fname, iname, cname

  if(NNODE.ne.SH%NRB%NNODE) write(*,*) "Error: NNODE in incrementState_STRU"

  if(ismaster.and.move) then 
     
      ashAlpha = 0.0d0
      ashAlpha = vec

      SH%NRB%ash(:,:) = SH%NRB%ash(:,:) + ashAlpha(:,:)*fact
      SH%NRB%ush(:,:) = SH%NRB%ush(:,:) + ashAlpha(:,:)*gami*Delt*fact
      SH%NRB%dsh(:,:) = SH%NRB%dsh(:,:) + ashAlpha(:,:)*beti*Delt*Delt*fact
! Jinhui: two parts
      NRB_BEA%B_NET_Dt = SH%NRB%ush
      NRB_BEA%B_NET_DDt = SH%NRB%ash
      NRB_BEA%B_NET_D(:,1:3) = NRB_BEA%B_NET_D(:,1:3) + ashAlpha(:,:)*beti*Delt*Delt*fact
! Jinhui: Update the solution on IGA   
      allocate(r2tmp1(SH%FEM%NNODE,NSD))
      allocate(r2tmp2(SH%FEM%NNODE,NSD))
      allocate(r2tmp3(SH%FEM%NNODE,NSD))
      r2tmp1 = 0.0d0
      r2tmp2 = 0.0d0
      r2tmp3 = 0.0d0
    ! write(*,*) "maxmum SH%NRB%dsh:", maxval(abs(SH%NRB%dsh(:,:)))
      call n2f_l2project(SH%FEM, SH%NRB, NSD, SH%NRB%ash(:,:), r2tmp1) ! Jinhui" project the complete solution
      call n2f_l2project(SH%FEM, SH%NRB, NSD, SH%NRB%ush(:,:), r2tmp2) ! Jinhui" project the complete solution
      call n2f_l2project(SH%FEM, SH%NRB, NSD, SH%NRB%dsh(:,:), r2tmp3) ! Jinhui" project the complete solution     

      do i = 1, SH%FEM%NNODE
       if (SH%FEM%IBC(i,1) .ne. 1) then
          SH%FEM%ash(i,:) = r2tmp1(i,:)
          SH%FEM%ush(i,:) = r2tmp2(i,:)
          SH%FEM%dsh(i,:) = r2tmp3(i,:)
       endif
      enddo
    ! write(*,*) "maxmum SH%FEM%dsh1:", maxval(abs(SH%FEM%dsh(:,:)))

      deallocate(r2tmp1)
      deallocate(r2tmp2)
      deallocate(r2tmp3)
   endif
 
  do ibld = 1, NBlade
    bblade(ibld)%ash = 0.0d0
    bblade(ibld)%ush = 0.0d0
    bblade(ibld)%dsh = 0.0d0
  end do

  ! only those who solve the shell problem would assign
  ! the solution
  if (solshell) then
    if (myid == 0) then
      bblade(1)%ash = SH%FEM%ash
      bblade(1)%ush = SH%FEM%ush
      bblade(1)%dsh = SH%FEM%dsh
    end if
  end if


  ! broadcast such that all processors will have the solutions
  do ibld = 1, NBlade
    call MPI_BCAST(bblade(ibld)%ash, bblade(ibld)%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)

    call MPI_BCAST(bblade(ibld)%ush, bblade(ibld)%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)

    call MPI_BCAST(bblade(ibld)%dsh, bblade(ibld)%NNODE*NSD, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)
  end do

  if (numnodes > 1) call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)


end subroutine incrementstate_stru1
