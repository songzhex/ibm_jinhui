!======================================================================
! subroutine to evaluate shape or basis functions for volume
!======================================================================
subroutine eval_shape(nshl, iel, gp, xl, wl,         &
                      shg, shgradg, shhessg,  DetJ,  &
                      dxdxi,dxidx,Gij,Ginv,hess_flag, iga)     
  use mpi
  implicit none
  
  logical, intent(in) :: iga 
  integer, intent(in) :: nshl
       
  integer, intent(in)  :: iel, hess_flag
  real(8), intent(in)  :: gp(3), xl(NSHL,3), wl(NSHL)
  real(8), intent(out) :: shg(NSHL), shgradg(NSHL,3), &
                          shhessg(NSHL,3,3), DetJ,&
                          dxdxi(3,3),dxidx(3,3),Gij(3,3), Ginv(3,3)
			  
  real(8) :: shgradl(NSHL,3),shhessl(NSHL,6)
			  
  integer :: i,j,k

  real(8) :: dxidlxi(3,3),dxdlxi(3,3), dxdxixj(3,6), locLHS(6,6), tmp

  if (iga) then   
    call NURBS3Dshl(gp, NSHL, iel, wl, &
                    shg, shgradl, shhessl, hess_flag)    
		       
    dxidlxi(1,:) = (/  2d0, 0d0, 0d0 /)     
    dxidlxi(2,:) = (/  0d0, 2d0, 0d0 /)     
    dxidlxi(3,:) = (/  0d0, 0d0, 2d0 /)  
  else   
    if (nshl.eq.4) then
      call lintetshl(gp, shg, shgradl) 
      
      dxidlxi(1,:) = (/  -1.000000000000000d0,  -0.577350269189626d0,  -0.408248290463863d0 /)
      dxidlxi(2,:) = (/   1.000000000000000d0,  -0.577350269189626d0,  -0.408248290463863d0 /)
      dxidlxi(3,:) = (/   0.000000000000000d0,   0.000000000000000d0,   1.224744871391589d0 /)    
    else if (nshl == 6) then
      call linprishl(gp, shg, shgradl)  
      
      dxidlxi(1,:) = (/  -1.000000000000000d0,  -0.577350269189626d0,   0.000000000000000d0 /)
      dxidlxi(2,:) = (/   1.000000000000000d0,  -0.577350269189626d0,   0.000000000000000d0 /)
      dxidlxi(3,:) = (/   0.000000000000000d0,   0.000000000000000d0,   2.000000000000000d0 /)
      
    else if (nshl == 8) then
      call linhexshl(gp, shg, shgradl) 
      dxidlxi(1,:) = (/  2d0, 0d0, 0d0 /)     
      dxidlxi(2,:) = (/  0d0, 2d0, 0d0 /)     
      dxidlxi(3,:) = (/  0d0, 0d0, 2d0 /)   
    else
      write(*,*) "Undefined nshl in eval_shape"
      stop
    end if
  end if
  
  ! Calculate dx/dxi
  dxdxi = 0.0d0
  do i = 1, 3
    do j = 1, 3
      dxdxi(i,j) = sum(xl(:,i)*shgradl(:,j))
    end do
  end do
          
  ! Calculate dxi/dx and jacobian
  call get_inverse_3x3(dxdxi, dxidx, DetJ)
 
  ! Global shape gradients
  do i = 1, NSHL
    do j = 1, 3
      shgradg(i,j) = sum(shgradl(i,:)*dxidx(:,j))
    end do
  end do
       
  if (hess_flag.eq.1) then
  
    ! Second derivatives of the geometrical map
    dxdxixj = 0.0d0
    do i = 1, NSHL
      do j = 1, 3
        dxdxixj(j,:) = dxdxixj(j,:) + xl(i,j)*shhessl(i,:)
      end do
    end do
            
    ! RHS of the matrix equation for the second derivatives of bases.
    ! Reuse local hess. array    
    do i = 1, 6
      shhessl(:,i) = shhessl(:,i) - shgradg(:,1)*dxdxixj(1,i) - &
                                    shgradg(:,2)*dxdxixj(2,i) - &
                                    shgradg(:,3)*dxdxixj(3,i)
    enddo
    
    ! LHS (6x6, same for every basis function)
    locLHS(1,1) = dxdxi(1,1)*dxdxi(1,1)
    locLHS(1,2) = 2d+0*dxdxi(1,1)*dxdxi(2,1)
    locLHS(1,3) = 2d+0*dxdxi(1,1)*dxdxi(3,1)
    locLHS(1,4) = dxdxi(2,1)*dxdxi(2,1)
    locLHS(1,5) = 2d+0*dxdxi(2,1)*dxdxi(3,1)
    locLHS(1,6) = dxdxi(3,1)*dxdxi(3,1)

    locLHS(2,1) = dxdxi(1,1)*dxdxi(1,2)
    locLHS(2,2) = dxdxi(1,1)*dxdxi(2,2) + dxdxi(1,2)*dxdxi(2,1)
    locLHS(2,3) = dxdxi(1,1)*dxdxi(3,2) + dxdxi(1,2)*dxdxi(3,1)
    locLHS(2,4) = dxdxi(2,1)*dxdxi(2,2)
    locLHS(2,5) = dxdxi(2,1)*dxdxi(3,2) + dxdxi(2,2)*dxdxi(3,1)
    locLHS(2,6) = dxdxi(3,1)*dxdxi(3,2)

    locLHS(3,1) = dxdxi(1,1)*dxdxi(1,3)
    locLHS(3,2) = dxdxi(1,1)*dxdxi(2,3) + dxdxi(1,3)*dxdxi(2,1)
    locLHS(3,3) = dxdxi(1,1)*dxdxi(3,3) + dxdxi(1,3)*dxdxi(3,1)
    locLHS(3,4) = dxdxi(2,1)*dxdxi(2,3)
    locLHS(3,5) = dxdxi(2,1)*dxdxi(3,3) + dxdxi(2,3)*dxdxi(3,1)
    locLHS(3,6) = dxdxi(3,1)*dxdxi(3,3)

    locLHS(4,1) = dxdxi(1,2)*dxdxi(1,2)
    locLHS(4,2) = 2d+0*dxdxi(1,2)*dxdxi(2,2)
    locLHS(4,3) = 2d+0*dxdxi(1,2)*dxdxi(3,2)
    locLHS(4,4) = dxdxi(2,2)*dxdxi(2,2)
    locLHS(4,5) = 2d+0*dxdxi(2,2)*dxdxi(3,2)
    locLHS(4,6) = dxdxi(3,2)*dxdxi(3,2)

    locLHS(5,1) = dxdxi(1,2)*dxdxi(1,3)
    locLHS(5,2) = dxdxi(1,2)*dxdxi(2,3) + dxdxi(1,3)*dxdxi(2,2)
    locLHS(5,3) = dxdxi(1,2)*dxdxi(3,3) + dxdxi(1,3)*dxdxi(3,2)
    locLHS(5,4) = dxdxi(2,2)*dxdxi(2,3)
    locLHS(5,5) = dxdxi(2,2)*dxdxi(3,3) + dxdxi(2,3)*dxdxi(3,2)
    locLHS(5,6) = dxdxi(3,2)*dxdxi(3,3)

    locLHS(6,1) = dxdxi(1,3)*dxdxi(1,3)
    locLHS(6,2) = 2d+0*dxdxi(1,3)*dxdxi(2,3)
    locLHS(6,3) = 2d+0*dxdxi(1,3)*dxdxi(3,3)
    locLHS(6,4) = dxdxi(2,3)*dxdxi(2,3)
    locLHS(6,5) = 2d+0*dxdxi(2,3)*dxdxi(3,3)
    locLHS(6,6) = dxdxi(3,3)*dxdxi(3,3)

    ! (6x6) - Gaussian elimination    
    do k = 1, 6
      do i = k+1, 6
        tmp = locLHS(i,k)/locLHS(k,k)
        do j = k+1,6
          locLHS(i,j) = locLHS(i,j) - tmp*locLHS(k,j)
        end do
        shhessl(:,i) = shhessl(:,i) - tmp*shhessl(:,k)
      end do
    end do
    
    do i = 6, 1, -1
      do j = i+1, 6
        shhessl(:,i) = shhessl(:,i) - locLHS(i,j)*shhessl(:,j)
      end do
      shhessl(:,i) = shhessl(:,i)/locLHS(i,i)
    end do

    ! Assign to global hessian of basis functions    
    shhessg(:,1,1) = shhessl(:,1)
    shhessg(:,1,2) = shhessl(:,2)
    shhessg(:,1,3) = shhessl(:,3)
    
    shhessg(:,2,1) = shhessl(:,2)
    shhessg(:,2,2) = shhessl(:,4)
    shhessg(:,2,3) = shhessl(:,5)
    
    shhessg(:,3,1) = shhessl(:,3)
    shhessg(:,3,2) = shhessl(:,5)
    shhessg(:,3,3) = shhessl(:,6)    
  end if
  
  ! Map to to logic element - Gij used as temporary
 ! do j = 1, 3
 !   do i = 1, 3
 !     dxdlxi(i,j) = sum(dxdxi(i,:)*dxidlxi(:,j))
 !   end do
 ! end do
  
  ! Element metric tensor   
 ! do j = 1, 3
 !   do i = 1, 3
 !     Ginv(i,j) = sum(dxdlxi(i,:)*dxdlxi(j,:))
 !   end do
 ! end do
          
  ! Inverse metric tensor
  !call get_inverse_3x3(Ginv, Gij, tmp)  

   ! Inverse metric tensor
  !call get_inverse_3x3(Gij, Ginv, tmp)  
 
  ! Check element
   do j = 1, 3
    do i = 1, 3
      Gij(i,j)  = sum(dxidx(:,i)*dxidx(:,j))
      Ginv(i,j) = sum(dxdxi(i,:)*dxdxi(j,:))
    end do
  end do

  if ((DetJ< 0.0d0).or.(isNAN(DetJ))) then
    write(*,*) "Warning - negative determinant Jacobian in FEM!" 
    write(*,*) "id",myid
    write(*,*) "Element nr   = ", iel
    write(*,*) "Element nshl = ", nshl
    write(*,*) "DetJ         = ", DetJ
    write(*,*) "Coordinates1 = ",  xl(1,:)
    write(*,*) "Coordinates2 = ",  xl(2,:)
    write(*,*) "Coordinates3 = ",  xl(3,:)
    write(*,*) "Coordinates4 = ",  xl(4,:)
    !write(*,*) "shg          = ", sum(shg)
    !write(*,*) "shgradl      = ", sum(shgradl(:,1)), sum(shgradl(:,2)), sum(shgradl(:,3))
    !write(*,*) "shgradg      = ", sum(shgradg(:,1)), sum(shgradg(:,2)), sum(shgradg(:,3))
    !write(*,*) "gp           = ", gp
    !write(*,*) "dxdxi        ="
    !write(*,*) dxdxi(1,:) 
    !write(*,*) dxdxi(2,:)     
    !write(*,*) dxdxi(3,:)     
 !   stop
  end if
  
end subroutine eval_shape
!======================================================================
! subroutine to evaluate shape or basis functions for surface
!======================================================================
subroutine eval_faceshape(nshl, iel, bgp, faceor, xl, wl,&
                          shg, shgradg, DetJb,&
                          dxdxi,dxidx, Gij, Ginv, nor, iga)
  use mpi
  implicit none
  
  logical, intent(in)  :: iga 
  integer, intent(in)  :: nshl,iel,faceor
  real(8), intent(in)  :: bgp(2), xl(NSHL,3), wl(NSHL)
  real(8), intent(out) :: shg(NSHL), shgradg(NSHL,3), DetJb,&
                          dxdxi(3,3),dxidx(3,3), Gij(3,3), Ginv(3,3), nor(3)

  integer :: i,j,hess_flag
  real(8) :: DetJ,shhessg(NSHL,6), nor0(3), igp(3), detJbFac
	
  ! For each element and face orientation	
  ! - Get volume integration point form boundary point
  ! - Get the normal in the reference configutration				    
  detJbFac = 1d0
  
  !write(*,*) nshl, iel, faceor 
  if (nshl == 4) then  ! Tetrahedral element
    if (faceor == 1) then
      nor0 = (/ 1d0, 1d0, 1d0 /)
      !nor0 = (/ 0.57735026918962573d0, 0.57735026918962573d0, 0.57735026918962573d0 /)
      igp  = (/ 1d0-bgp(1)-bgp(2),bgp(1), bgp(2) /)
    else if (faceor == 2) then    
      nor0 = (/-1d0, 0d0,    0d0    /)
      igp  = (/ 0d0, bgp(2), bgp(1) /)           
    else if (faceor == 3) then
      nor0 = (/ 0d0,   -1d0, 0d0    /)
      igp  = (/ bgp(1), 0d0, bgp(2) /)
    else if (faceor == 4) then
      nor0 = (/ 0d0,    0d0,   -1d0 /)
      igp  = (/ bgp(2), bgp(1), 0d0 /) 
    else
      write(*,*) "Wrong face orientation for tet bnd", faceor
      stop
    endif      
  else if (nshl == 6) then ! Prismatic element
  
    if (faceor == 1) then     
      nor0 = (/ 0d0, 0d0, -1d0 /)     
      igp  = (/ bgp(1), bgp(2),-1d0 /) 
    else if (faceor == 2) then
      nor0 = (/ 0d0, 0d0,  1d0 /)     
      igp  = (/ bgp(1), bgp(2), 1d0 /) 
      
    ! Mapping (-1,1) for quad to (0,1) for prism   
    else if (faceor == 3) then      
      nor0 = (/ 1d0, 1d0,  0d0 /)    
      igp  = (/ 0.5d0+0.5d0*bgp(1),0.5d0-0.5d0*bgp(1), bgp(2) /) 
      detJbFac = 0.5d0             
    else if (faceor == 4) then
      nor0 = (/-1d0, 0d0,  0d0 /)     
      igp  = (/0d0, 0.5d0+0.5d0*bgp(1), bgp(2) /)
      detJbFac = 0.5d0       
    else if (faceor == 5) then
      nor0 = (/0d0,-1d0,  0d0 /)     
      igp  = (/ 0.5d0+0.5d0*bgp(1),0d0, bgp(2) /) 
      detJbFac = 0.5d0     
    else
      write(*,*) "Wrong face orientation for PRISM bnd", faceor
      stop
    endif      
  
  else   ! Linear or IGA Hexahedral element  
    if (faceor == 1) then
      nor0 = (/ 0d0,   0d0,    -1d0 /)      
      igp  = (/ bgp(1), bgp(2),-1d0 /)
    else if (faceor == 2) then
      nor0 = (/ 0d0,   -1d0, 0d0    /)     
      igp  = (/ bgp(1),-1d0, bgp(2) /)  
    else if (faceor == 3) then
      nor0 = (/ 1d0, 0d0,    0d0    /)     
      igp  = (/ 1d0, bgp(1), bgp(2) /)
    else if (faceor == 4) then
      nor0 = (/ 0d0,    1d0, 0d0    /)     
      igp  = (/ bgp(1), 1d0, bgp(2) /)   
    else if (faceor == 5) then
      nor0 = (/-1d0, 0d0,    0d0    /)    
      igp  = (/-1d0, bgp(1), bgp(2) /)
    else if (faceor == 6) then
      nor0 = (/ 0d0,    0d0,    1d0 /)    
      igp  = (/ bgp(1), bgp(2), 1d0 /)    
    else
      write(*,*) myid, "Wrong face orientation for HEX bnd",iel,nshl,faceor
      stop
    endif    
       
  end if  
    
  ! Evaluate volume shapefunction -- for normal derivatives
  call  eval_shape(nshl, iel, igp, xl, wl,          &
                   shg, shgradg, shhessg,  DetJ,   &
                   dxdxi,dxidx,Gij,Ginv, 0, iga)   

  ! Nanson's formula 
  ! Maps normal from reference to physical space		   
  do i = 1, 3
    nor(i) = DetJ*sum(dxidx(:,i)*nor0(:))
  enddo
  
  ! Integration Area
  DetJb = sqrt(sum(nor*nor))

  ! Scale normal to unit length 
  nor = nor/DetJb    
    
  ! Scale determinant -- if 2D and 3D interval is different (e.g. [-1,1] ==> [0,1] in case of quad face of prism)  

!YB
!  DetJb = DetJbFac*DetJb
    		       		       
  ! Check element
  if ((DetJb< 0.0d0).or.(isNAN(DetJb))) then
    write(*,*) "Warning face - negative determinant Jacobian !" 
    write(*,*) "Element nr   = ",myid, iel 
    write(*,*) "Face    nr   = ", faceor
    write(*,*) "Element nshl = ", nshl
    write(*,*) "DetJb        = ", DetJb
    write(*,*) "nor          = ", nor   
    write(*,*) "nor0         = ", nor0 

    write(*,*) "DetJ         = ", DetJ
    write(*,*) "shg          = ", sum(shg)
    write(*,*) "shgradg      = ", sum(shgradg(:,1)), sum(shgradg(:,2)), sum(shgradg(:,3))
    !write(*,*) xl    
    stop
  endif
      
end subroutine eval_faceshape

!======================================================================
! shape function for tetrahedral element
!======================================================================  
subroutine lintetshl(gp, shl, shgradl)
  implicit none

  real(8) :: gp(3), zi, eta, zeta
  real(8) :: shl(4), shgradl(4,3)
  
  zi   = gp(1)
  eta  = gp(2)
  zeta = gp(3)

  shl(1) = zi
  shl(2) = eta
  shl(3) = zeta
  shl(4) = 1.0d0-zi-eta-zeta

  shgradl = 0.0d0

  shgradl(1,1) =  1.0d0
  shgradl(2,2) =  1.0d0
  shgradl(3,3) =  1.0d0
  shgradl(4,1) = -1.0d0
  shgradl(4,2) = -1.0d0
  shgradl(4,3) = -1.0d0
  
end subroutine lintetshl

!=======================================================================
! shape function for prism element
!=======================================================================
subroutine linprishl(gp, shl, shgradl)
  implicit none
      
  real(8) :: gp(3), zi, eta, zeta, sixth
  real(8) :: shl(6), shgradl(6,3)
      
  zi   = gp(1)
  eta  = gp(2)
  zeta = gp(3)
	  
  shl(1) = 0.5d0*zi*(1d+0 - zeta)
  shl(2) = 0.5d0*eta*(1d+0 - zeta)
  shl(3) = 0.5d0*(1d+0 - zi - eta)*(1d+0 - zeta)
  shl(4) = 0.5d0*zi*(1d+0 + zeta)
  shl(5) = 0.5d0*eta*(1d+0 + zeta)
  shl(6) = 0.5d0*(1d+0 - zi - eta)*(1d+0 + zeta)

  shgradl = 0.0d0
  shgradl(1,1) =  0.5d0*(1d+0 - zeta)
  shgradl(2,1) =  0.0d0
  shgradl(3,1) =  -0.5d0*(1d+0 - zeta)
  shgradl(4,1) =  0.5d0*(1d+0 + zeta)
  shgradl(5,1) =  0.0d0
  shgradl(6,1) =  -0.5d0*(1d+0 + zeta)

  shgradl(1,2) = 0.0d0
  shgradl(2,2) = 0.5d0*(1d+0 - zeta)
  shgradl(3,2) = -0.5d0*(1d+0 - zeta)
  shgradl(4,2) = 0.0d0
  shgradl(5,2) = 0.5d0*(1d+0 + zeta)
  shgradl(6,2) = -0.5d0*(1d+0 + zeta)
  
  shgradl(1,3) =  -0.5d0*zi
  shgradl(2,3) =  -0.5d0*eta
  shgradl(3,3) =  -0.5d0*(1d+0 - zi - eta)
  shgradl(4,3) =  0.5d0*zi
  shgradl(5,3) =  0.5d0*eta
  shgradl(6,3) =  0.5d0*(1d+0 - zi - eta)
end subroutine linprishl

!======================================================================
! shape function for hexahedral element
!======================================================================  
subroutine linhexshl(gp, shl, shgradl)
  implicit none
    
  real(8) :: gp(3), zi, eta, zeta, eighth
  real(8) :: shl(8), shgradl(8,3)
  
  zi   = gp(1)
  eta  = gp(2)
  zeta = gp(3)
  
  eighth = 0.125d0
  
  shl(1) =  (zi - 1d+0)*(eta + 1d+0)*(zeta - 1d+0)*eighth
  shl(2) = -(zi + 1d+0)*(eta + 1d+0)*(zeta - 1d+0)*eighth
  shl(3) =  (zi + 1d+0)*(eta + 1d+0)*(zeta + 1d+0)*eighth
  shl(4) = -(zi - 1d+0)*(eta + 1d+0)*(zeta + 1d+0)*eighth
  shl(5) = -(zi - 1d+0)*(eta - 1d+0)*(zeta - 1d+0)*eighth
  shl(6) =  (zi + 1d+0)*(eta - 1d+0)*(zeta - 1d+0)*eighth
  shl(7) = -(zi + 1d+0)*(eta - 1d+0)*(zeta + 1d+0)*eighth
  shl(8) =  (zi - 1d+0)*(eta - 1d+0)*(zeta + 1d+0)*eighth  
  
  shgradl(1,1) =  (eta + 1d+0)*(zeta - 1d+0)*eighth
  shgradl(2,1) = -(eta + 1d+0)*(zeta - 1d+0)*eighth
  shgradl(3,1) =  (eta + 1d+0)*(zeta + 1d+0)*eighth
  shgradl(4,1) = -(eta + 1d+0)*(zeta + 1d+0)*eighth
  shgradl(5,1) = -(eta - 1d+0)*(zeta - 1d+0)*eighth
  shgradl(6,1) =  (eta - 1d+0)*(zeta - 1d+0)*eighth
  shgradl(7,1) = -(eta - 1d+0)*(zeta + 1d+0)*eighth 
  shgradl(8,1) =  (eta - 1d+0)*(zeta + 1d+0)*eighth
  
  shgradl(1,2) =  (zi - 1d+0)*(zeta - 1d+0)*eighth
  shgradl(2,2) = -(zi + 1d+0)*(zeta - 1d+0)*eighth
  shgradl(3,2) =  (zi + 1d+0)*(zeta + 1d+0)*eighth
  shgradl(4,2) = -(zi - 1d+0)*(zeta + 1d+0)*eighth
  shgradl(5,2) = -(zi - 1d+0)*(zeta - 1d+0)*eighth
  shgradl(6,2) =  (zi + 1d+0)*(zeta - 1d+0)*eighth
  shgradl(7,2) = -(zi + 1d+0)*(zeta + 1d+0)*eighth
  shgradl(8,2) =  (zi - 1d+0)*(zeta + 1d+0)*eighth
  
  shgradl(1,3) =  (zi - 1d+0)*(eta + 1d+0)*eighth
  shgradl(2,3) = -(zi + 1d+0)*(eta + 1d+0)*eighth
  shgradl(3,3) =  (zi + 1d+0)*(eta + 1d+0)*eighth
  shgradl(4,3) = -(zi - 1d+0)*(eta + 1d+0)*eighth
  shgradl(5,3) = -(zi - 1d+0)*(eta - 1d+0)*eighth
  shgradl(6,3) =  (zi + 1d+0)*(eta - 1d+0)*eighth
  shgradl(7,3) = -(zi + 1d+0)*(eta - 1d+0)*eighth
  shgradl(8,3) =  (zi - 1d+0)*(eta - 1d+0)*eighth
end subroutine linhexshl

!======================================================================
! shape function for  3D NURBS element
!======================================================================
subroutine NURBS3Dshl(gp, nshl, iel, wl, &
                      shl, shgradl, shhessl, hess_flag)

  use globdata  
  implicit none

  integer, intent(in) :: nshl, iel, hess_flag
  real(8), intent(in) :: gp(3), wl(NSHL)
             
  real(8), intent(out) :: shl(NSHL), shgradl(NSHL,3), &
                          shhessl(NSHL,6)
  integer :: ni, nj, nk
  type (patchStruct) :: np
    
  integer :: P, Q, R, i, j, k, aa, nders
  real(8) :: du, dv, dw, da
  
  real(8) :: tempshl(NSHL),tempshgradl(NSHL,3),tempshhessl(NSHL,6)

  real(8), allocatable :: Nn(:,:),Mm(:,:),Oo(:,:)   
  
  real(8) :: u, v, w, denom_sum, derv_sum_U, derv_sum_V, &
             derv_sum_W, derv_sum_UU, &
             derv_sum_UV, derv_sum_UW,derv_sum_VV, derv_sum_VW, &
             derv_sum_WW, tmp
	     
	     
  np = mesh_fluid%patch(mesh_fluid%EPID(iel))
  ni = mesh_fluid%EIJK(iel,1)
  nj = mesh_fluid%EIJK(iel,2)
  nk = mesh_fluid%EIJK(iel,3)
  
  allocate (Nn(2+hess_flag,np%P+1), & 
            Mm(2+hess_flag,np%Q+1), &
            Oo(2+hess_flag,np%R+1) )          
  P = np%P
  Q = np%Q
  R = np%R
  
  ! Get u and v coordinates of integration point  
  u = ((np%U_KNOT(ni+1) - np%U_KNOT(ni))*gp(1) + &
        np%U_KNOT(ni+1) + np%U_KNOT(ni))/2.0d0
  v = ((np%V_KNOT(nj+1) - np%V_KNOT(nj))*gp(2) + &
        np%V_KNOT(nj+1) + np%V_KNOT(nj))/2.0d0
  w = ((np%W_KNOT(nk+1) - np%W_KNOT(nk))*gp(3) + &
        np%W_KNOT(nk+1) + np%W_KNOT(nk))/2.0d0

  ! Get knot span sizes  
  du = np%U_KNOT(ni+1) - np%U_KNOT(ni)
  dv = np%V_KNOT(nj+1) - np%V_KNOT(nj)
  dw = np%W_KNOT(nk+1) - np%W_KNOT(nk)
  da = du*dv*dw/8.0d0

  ! Evaluate 1D shape functions and derivatives each direction  
  if (hess_flag == 0) then
    nders = 1       ! number of derivatives to take
  else
    nders = 2
  end if
  
  ! calculate in u,v,w dir.
  call dersbasisfuns(ni, P, np%MCP, u, nders, np%U_KNOT, Nn) 
  call dersbasisfuns(nj, Q, np%NCP, v, nders, np%V_KNOT, Mm)
  call dersbasisfuns(nk, R, np%OCP, w, nders, np%W_KNOT, Oo)
      
  ! Form basis functions and derivatives dR/du and dR/dv  
  denom_sum = 0.0d0
  derv_sum_U = 0.0d0
  derv_sum_V = 0.0d0
  derv_sum_W = 0.0d0
    
  aa = 0  
  do k = 0, R
    do j = 0, Q
      do i = 0, P
        aa = aa+1
    
        ! basis functions
        shl(aa) = Nn(1,P+1-i)*Mm(1,Q+1-j)*Oo(1,R+1-k)*wl(aa)
        denom_sum = denom_sum + shl(aa)
    
        ! derivatives (u, v and w)
        shgradl(aa,1) = Nn(2,P+1-i)*Mm(1,Q+1-j)*Oo(1,R+1-k)*wl(aa)
        derv_sum_U = derv_sum_U + shgradl(aa,1)

        shgradl(aa,2) = Nn(1,P+1-i)*Mm(2,Q+1-j)*Oo(1,R+1-k)*wl(aa)
        derv_sum_V = derv_sum_V + shgradl(aa,2)

        shgradl(aa,3) = Nn(1,P+1-i)*Mm(1,Q+1-j)*Oo(2,R+1-k)*wl(aa)
        derv_sum_W = derv_sum_W + shgradl(aa,3)
    
      end do
    end do
  end do
  

  if (hess_flag == 1) then  
    tempshl = shl
    tempshgradl = shgradl
  endif
   
  ! Divide through by denominator      
  do i = 1, NSHL
    shgradl(i,1) = shgradl(i,1)/denom_sum - &
                    (shl(i)*derv_sum_U)/(denom_sum**2)
    shgradl(i,2) = shgradl(i,2)/denom_sum - &
                    (shl(i)*derv_sum_V)/(denom_sum**2)
    shgradl(i,3) = shgradl(i,3)/denom_sum - &
                    (shl(i)*derv_sum_W)/(denom_sum**2)
    shl(i) = shl(i)/denom_sum
  end do
     
  !----------------------------------------------
  ! 2nd derivatives
  !----------------------------------------------
  if (hess_flag == 1) then
    
    aa = 0
    derv_sum_UU = 0.0d0
    derv_sum_UV = 0.0d0
    derv_sum_UW = 0.0d0
    derv_sum_VV = 0.0d0
    derv_sum_VW = 0.0d0
    derv_sum_WW = 0.0d0
    do k = 0, R
      do j = 0, Q
        do i = 0, P
          aa = aa+1
      
          ! 2nd derivatives
          ! ,uu
          tempshhessl(aa,1) = Nn(3,P+1-i)*Mm(1,Q+1-j)*Oo(1,R+1-k)*wl(aa)  
          derv_sum_UU = derv_sum_UU + tempshhessl(aa,1)
          ! ,uv
          tempshhessl(aa,2) = Nn(2,P+1-i)*Mm(2,Q+1-j)*Oo(1,R+1-k)*wl(aa)  
          derv_sum_UV = derv_sum_UV + tempshhessl(aa,2)
          ! ,uw
          tempshhessl(aa,3) = Nn(2,P+1-i)*Mm(1,Q+1-j)*Oo(2,R+1-k)*wl(aa)  
          derv_sum_UW = derv_sum_UW + tempshhessl(aa,3)
          ! ,vv
          tempshhessl(aa,4) = Nn(1,P+1-i)*Mm(3,Q+1-j)*Oo(1,R+1-k)*wl(aa)  
          derv_sum_VV = derv_sum_VV + tempshhessl(aa,4)
          ! ,vw
          tempshhessl(aa,5) = Nn(1,P+1-i)*Mm(2,Q+1-j)*Oo(2,R+1-k)*wl(aa)  
          derv_sum_VW = derv_sum_VW + tempshhessl(aa,5)
          ! ,ww
          tempshhessl(aa,6) = Nn(1,P+1-i)*Mm(1,Q+1-j)*Oo(3,R+1-k)*wl(aa)  
          derv_sum_WW = derv_sum_WW + tempshhessl(aa,6)
        end do
      end do
    end do
    
    ! local Hessian
    do i = 1, NSHL
      shhessl(i,1) = tempshhessl(i,1)/denom_sum - &
        tempshgradl(i,1)*derv_sum_U/(denom_sum**2) - &
        ((tempshgradl(i,1)*derv_sum_U+tempshl(i)*derv_sum_UU)/ &
        (denom_sum**2) - 2d+0*tempshl(i)*derv_sum_U*derv_sum_U/ &
        (denom_sum**3))
      shhessl(i,2) = tempshhessl(i,2)/denom_sum - &
        tempshgradl(i,1)*derv_sum_V/(denom_sum**2) - &
        ((tempshgradl(i,2)*derv_sum_U+tempshl(i)*derv_sum_UV)/ &
        (denom_sum**2) - 2d+0*tempshl(i)*derv_sum_U*derv_sum_V/ &
        (denom_sum**3))
      shhessl(i,3) = tempshhessl(i,3)/denom_sum - &
        tempshgradl(i,1)*derv_sum_W/(denom_sum**2) - &
        ((tempshgradl(i,3)*derv_sum_U+tempshl(i)*derv_sum_UW)/ &
        (denom_sum**2) - 2d+0*tempshl(i)*derv_sum_U*derv_sum_W/ &
        (denom_sum**3))     
      shhessl(i,4) = tempshhessl(i,4)/denom_sum - &
        tempshgradl(i,2)*derv_sum_V/(denom_sum**2) - &
        ((tempshgradl(i,2)*derv_sum_V+tempshl(i)*derv_sum_VV)/ &
        (denom_sum**2) - 2d+0*tempshl(i)*derv_sum_V*derv_sum_V/ &
        (denom_sum**3))
      shhessl(i,5) = tempshhessl(i,5)/denom_sum - &
        tempshgradl(i,2)*derv_sum_W/(denom_sum**2) - &
        ((tempshgradl(i,3)*derv_sum_V+tempshl(i)*derv_sum_VW)/ &
        (denom_sum**2) - 2d+0*tempshl(i)*derv_sum_V*derv_sum_W/ &
        (denom_sum**3))  
      shhessl(i,6) = tempshhessl(i,6)/denom_sum - &
        tempshgradl(i,3)*derv_sum_W/(denom_sum**2) - &
        ((tempshgradl(i,3)*derv_sum_W+tempshl(i)*derv_sum_WW)/ &
        (denom_sum**2) - 2d+0*tempshl(i)*derv_sum_W*derv_sum_W/ &
        (denom_sum**3))
    end do 
  
  else
    shhessl = 0d0
  end if
  
  du = 0.5d0*du 
  dv = 0.5d0*dv 
  dw = 0.5d0*dw 
  
  shgradl(:,1) = shgradl(:,1)*du 
  shgradl(:,2) = shgradl(:,2)*dv 
  shgradl(:,3) = shgradl(:,3)*dw 
    
  if (hess_flag == 1) then
    shhessl(:,1) = shhessl(:,1)*du*du
    shhessl(:,2) = shhessl(:,2)*du*dv
    shhessl(:,3) = shhessl(:,3)*du*dw 
  
    shhessl(:,4) = shhessl(:,4)*dv*dv
    shhessl(:,5) = shhessl(:,5)*dv*dw 
    shhessl(:,6) = shhessl(:,6)*dw*dw 
  endif   
       
  deallocate(Mm, Nn, Oo) 
  
end subroutine NURBS3Dshl

!======================================================================
! This subroutine evaluates the basis functions and first derivatives 
! functions at a given parameter value u.
!
! Algorithm from Piegl, Les. "The NURBS Book". Springer-Verlag: 
! Berlin 1995; pp. 72-73.
!======================================================================   
subroutine dersbasisfuns(i, pl, ml, u, nders, u_knotl, ders)
  
  implicit none

  ! knot span, degree of curve, number of control points, counters
  integer :: i, pl, ml, j, r, k, j1, j2, s1, s2, rk, pk, nders

  ! parameter value, vector of knots, derivative matrix
  real(8) :: u, u_knotl(pl+ml+1), ders(nders+1,pl+1), ndu(pl+1,pl+1), d
  real(8) :: left(pl+1), right(pl+1), saved, temp, a(2,pl+1)

  ndu(1,1) = 1.0d0
  do j = 1, pl
    left(j+1) = u - u_knotl(i+1-j)
    right(j+1) = u_knotl(i+j) - u
    saved = 0.0d0
    do r = 0, j-1
      ndu(j+1,r+1) = right(r+2) + left(j-r+1)
      temp = ndu(r+1,j)/ndu(j+1,r+1)
      ndu(r+1,j+1) = saved + right(r+2)*temp
      saved = left(j-r+1)*temp
    end do
    ndu(j+1,j+1) = saved
  end do
  
  ! load basis functions
  do j = 0, pl
    ders(1,j+1) = ndu(j+1,pl+1)
  end do

  ! compute derivatives
  do r = 0, pl ! loop over function index
    s1 = 0
    s2 = 1         ! alternate rows in array a
    a(1,1) = 1.0d0

    ! loop to compute kth derivative
    do k = 1,nders
      d = 0.0d0
      rk = r-k
      pk = pl-k
      if (r >= k) then
        a(s2+1,1) = a(s1+1,1)/ndu(pk+2,rk+1)
        d = a(s2+1,1)*ndu(rk+1,pk+1)
      end if
      if (rk >= -1) then
        j1 = 1
      else 
        j1 = -rk
      end if
      if ((r-1) <= pk) then
        j2 = k-1
      else 
        j2 = pl-r
      end if
      do j = j1, j2
        a(s2+1,j+1) = (a(s1+1,j+1) - a(s1+1,j))/ndu(pk+2,rk+j+1)
        d = d + a(s2+1,j+1)*ndu(rk+j+1,pk+1)
      end do
      if (r <= pk) then
        a(s2+1,k+1) = -a(s1+1,k)/ndu(pk+2,r+1)
        d = d + a(s2+1,k+1)*ndu(r+1,pk+1)
      end if
      ders(k+1,r+1) = d
      j = s1
      s1 = s2
      s2 = j      ! switch rows
    end do
  end do
  
  ! Multiply through by the correct factors
  r = pl
  do k = 1, nders
    do j = 0, pl
      ders(k+1,j+1) = ders(k+1,j+1)*real(r)
    end do
    r = r*(pl-k)
  end do

end subroutine dersbasisfuns