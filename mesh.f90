module meshData
  use tetim
  implicit none
  
  !------------------------------------------------------------------------  
  type boundStruct
    
    integer :: FACE_ID
    integer :: NFACE,maxNSHLB 
  
    integer, allocatable :: FACE_IEN(:,:)
    
    integer, allocatable :: F2E(:)
    integer, allocatable :: FACE_OR(:)
    integer, allocatable :: NSHLB(:)
    integer, allocatable :: NGAUSSB(:)
          
    integer :: NNODE
    integer, allocatable :: BNODES(:)
  
    ! mapping between local boundary node (using global node numbering)
    ! and shell nodes
    integer, allocatable :: L2SNODE(:), L2SELEM(:)
  end type boundStruct
  
  !------------------------------------------------------------------------   
  type patchStruct
 
    integer :: P, Q, R
    integer :: MCP, NCP, OCP
    real(8), allocatable :: U_KNOT(:), V_KNOT(:), W_KNOT(:)
    
  end type patchStruct

  !------------------------------------------------------------------------   
  type meshStruct
  
    integer :: NSD, NNODE, NELEM, NBOUND, NPATCH, &
               maxNSHL,maxNSHLB
    
    type(quadptlist), allocatable :: elmgplists(:)
    real(8), allocatable :: includeflags(:)
    integer :: IM_flag

    real(8), allocatable :: xg(:,:), wg(:),Coord_ID(:)

    integer, allocatable :: IEN(:,:), EPID(:), EIJK(:,:)
    integer, allocatable :: l2g(:)
    integer, allocatable :: ELMNSHL(:),ELM_ID(:), ELMNGAUSS(:)
    
    type(boundStruct), allocatable :: bound(:) 
    type(patchStruct), allocatable :: patch(:)
    
    ! Periodicity flags
    integer, allocatable :: IPER(:)
    
    ! Type flags
    integer, allocatable :: EL_TYP(:), D_FLAG(:), P_FLAG(:)
    
    logical :: iga
    
    ! Domain bounds  
    real(8) :: minX(3), maxX(3)
                  
  end type meshStruct
  
  !------------------------------------------------------------------------      
  contains
  !------------------------------------------------------------------------      
  subroutine readMesh(mesh, fname)
    use mpi
    implicit none
    type(meshStruct) :: mesh

    character(*) :: fname

     character(len=30) :: iiname, ffname

    integer :: mfid, ierr, i, j, k, uks,vks,wks,kkkk
    integer :: itmp1, itmp2, itmp3
    ! open mesh files
    mfid = 111 
    open(mfid, file=fname, status='old', iostat=ierr)   
  !  write(*,*) fname

    read(mfid,*) mesh%NSD,mesh%maxNSHL, mesh%maxNSHLB
    read(mfid,*) mesh%NNODE, mesh%NELEM, mesh%NBOUND, mesh%NPATCH
  
    ! read nodes
    allocate(mesh%xg(mesh%NNODE,mesh%NSD),mesh%Coord_ID(mesh%NNODE))
    do i = 1, mesh%NNODE
      read(mfid,*) (mesh%xg(i,j), j = 1,mesh%NSD),mesh%Coord_ID(i)
    end do 

    ! read elements 
    allocate(mesh%ELM_ID(mesh%NELEM)) 
    allocate(mesh%ELMNSHL(mesh%NELEM), mesh%IEN(mesh%NELEM,mesh%maxNSHL))
    do i = 1, mesh%NELEM
      read(mfid,*) mesh%ELMNSHL(i), (mesh%IEN(i,j), j = 1, mesh%ELMNSHL(i)),mesh%ELM_ID(i)
    end do

    ! read faces  
    allocate(mesh%bound(mesh%NBOUND))   
    do i = 1, mesh%NBOUND
      mesh%bound(i)%maxNSHLB = mesh%maxNSHLB
      call readBound(mesh%bound(i), mfid)
      if(myid.eq.54) then
      !do j = 1,mesh%bound(i)%NFACE
       ! write(*,*) "***",mesh%bound(i)%FACE_OR(j)
       ! write(*,*) (mesh%bound(i)%FACE_IEN(j,k), k = 1, mesh%bound(i)%NSHLB(j))
       ! kkkk=mesh%bound(i)%F2E(j)
       ! write(*,*) "***",kkkk,mesh%ELMNSHL(kkkk)
       ! write(*,*) (mesh%IEN(kkkk,k),k=1,mesh%ELMNSHL(kkkk))
     ! enddo
     endif
  
    end do
 
    ! read nurbs data
    allocate(mesh%wg(mesh%NNODE))
  
    if (mesh%NPATCH > 0) then 
      mesh%iga = .true.
      do i = 1,mesh%NNODE
        read(mfid,*) mesh%wg(i)
      end do   
        
      allocate(mesh%EPID(mesh%NELEM), mesh%EIJK(mesh%NELEM,mesh%NSD))
   
      do i = 1,mesh% NELEM
        read(mfid,*) mesh%EPID(i),(mesh%EIJK(i,j), j = 1,mesh%NSD)
      end do
    else  
      mesh%iga = .false.
      mesh%wg  = 1.0d0
    end if
   
    ! read patches 
    allocate(mesh%patch(mesh%NPATCH)) 
    do i = 1, mesh%NPATCH
      call readPatch(mesh%patch(i),mfid)
    end do  
  
    close(mfid)

    ! Init Flags 
    allocate(mesh%IPER(mesh%NNODE))     
    do i = 1, mesh%NNODE
      mesh%IPER(i) = i 
    end do  
        
    allocate(mesh%EL_TYP(mesh%NELEM))
    mesh%EL_TYP = 0  
  
    allocate(mesh%P_Flag(mesh%NNODE))   
    mesh%P_Flag = 1     
    
    allocate(mesh%D_Flag(mesh%NNODE))       
    mesh%D_Flag = 0 

    ! Get element gauss points -- Assuming Gauss points equals the number of shape functions
    allocate(mesh%ELMNGAUSS(mesh%NELEM))  
    mesh%ELMNGAUSS = mesh%ELMNSHL
    do i = 1, mesh%NBOUND
      allocate(mesh%bound(i)%NGAUSSB(mesh%bound(i)%NFACE))
      mesh%bound(i)%NGAUSSB = mesh%bound(i)%NSHLB
    enddo
   

  write(iiname,'(I8)') myid+1
  ffname = 'l2b.'// trim(adjustl(iiname))//'.dat'
  open(mfid, file= ffname, status= 'old')    

  do i = 1, mesh%NBOUND
    read(mfid,*) itmp1, itmp2, itmp3
    if (itmp1 /= mesh%bound(i)%FACE_ID .or. itmp2 /= mesh%bound(i)%NNODE .or. &
        itmp3 /= mesh%bound(i)%NFACE) then
        write(*,*) myid,itmp1 ,"~", mesh%bound(i)%FACE_ID,itmp2, "~", mesh%bound(i)%NNODE, &
        itmp3,"~",mesh%bound(i)%NFACE
      write(*,*) "ERROR in l2b: FaceID or NNODE or NFACE doesn't match"
      stop
    end if

    if (mesh%bound(i)%NNODE > 0) then
      allocate(mesh%bound(i)%L2SNODE(mesh%bound(i)%NNODE))

      do j = 1, mesh%bound(i)%NNODE
        ! read partitioned boundary node number and the corresponding
        ! shell mesh node number
        read(mfid,*) itmp1, mesh%bound(i)%L2SNODE(j)
        if (itmp1 /= mesh%bound(i)%BNODES(j)) then
          write(*,*) "ERROR in l2b: Boundary node doesn't match"
          stop
        end if
      end do
    end if

    if (mesh%bound(i)%NFACE > 0) then
      allocate(mesh%bound(i)%L2SELEM(mesh%bound(i)%NFACE))

      do j = 1, mesh%bound(i)%NFACE
        ! read partitioned boundary element number and the corresponding
        ! shell mesh element number
        read(mfid,*) itmp1, mesh%bound(i)%L2SELEM(j)
        if (itmp1 /= mesh%bound(i)%F2E(j)) then
          write(*,*) "ERROR in l2b: Boundary element doesn't match"
          stop
        end if
      end do
    end if

  end do
  close(mfid)








  end subroutine readMesh 
   
  !------------------------------------------------------------------------      
  subroutine readBound(bound, mfid)

    implicit none
    type(boundStruct), intent(inout) :: bound 
    integer,           intent(inout) :: mfid
    
    integer :: j,k
     
    read(mfid,*) bound%FACE_ID, bound%NFACE, bound%NNODE
 
    allocate(bound%FACE_IEN(bound%NFACE,bound%maxNSHLB))
    allocate(bound%F2E(bound%NFACE))
    allocate(bound%FACE_OR(bound%NFACE))
    allocate(bound%NSHLB(bound%NFACE))
    
    do j = 1, bound%NFACE
       bound%NSHLB(j)=3
      read(mfid,*) (bound%FACE_IEN(j,k), k = 1, bound%NSHLB(j))
    end do

    do j = 1,bound%NFACE
      read(mfid,*) bound%F2E(j), bound%FACE_OR(j)
    end do
    
    allocate(bound%BNODES(bound%NNODE))   
    do j = 1,bound%NNODE
      read(mfid,*) bound%BNODES(j)
    end do    
    
  end subroutine readBound 
          
  !------------------------------------------------------------------------      
  subroutine readPatch(patch, mfid)

    implicit none
    type(patchStruct), intent(inout) :: patch  
    integer,           intent(inout) :: mfid
    
    integer :: i, uks,vks,wks
        
    read(mfid,*) patch%P,   patch%Q,   patch%R    
    read(mfid,*) patch%MCP, patch%NCP, patch%OCP
    
    uks = patch%MCP + patch%P + 1
    vks = patch%NCP + patch%Q + 1
    wks = patch%OCP + patch%R + 1
    
    allocate(patch%U_KNOT(uks))
    allocate(patch%V_KNOT(vks))
    allocate(patch%W_KNOT(wks))

    read(mfid,*) patch%U_KNOT   
    read(mfid,*) patch%V_KNOT   
    read(mfid,*) patch%W_KNOT
    
  end subroutine readPatch   

  subroutine getMeshExtremes(mesh)  
    use mpi
    implicit none
    type(meshStruct) :: mesh
    real(8) bmin(mesh%NSD),bmax(mesh%NSD),loc(mesh%NSD)
    integer :: b,i,j,d
         
    ! global bounds	       
    mesh%minX = minval(mesh%xg,1) 
      
    loc = mesh%minX        
    call MPI_ALLReduce(loc, mesh%minX, mesh%NSD, MPI_DOUBLE_PRECISION, &
                       MPI_MIN, MPI_COMM_WORLD,mpi_err) 
    
    mesh%maxX = maxval(mesh%xg,1) 
 
    loc = mesh%maxX        
    call MPI_ALLReduce(loc,mesh%maxX, mesh%NSD, MPI_DOUBLE_PRECISION, &
                       MPI_MAX, MPI_COMM_WORLD,mpi_err)                                             
    if (ismaster) then
      write(*,*) "Number of face:", mesh%NBOUND
      write(*,*) "Face_ID:",mesh%bound(:)%FACE_ID
      write(*,'(a,1x,3F16.5)') " Domain minimum :", mesh%minX 
      write(*,'(a,1x,3F16.5)') " Domain maximum :", mesh%maxX
    endif 
  
    ! Face bounds    
    do b =1, mesh%NBOUND 
      bmin = 9d9 
      bmax =-9d9
       do j = 1, mesh%bound(b)%NNODE
         i = mesh%bound(b)%BNODES(j)      
         
         do d = 1,mesh%NSD 
           bmin(d) = min(mesh%xg(i,d),bmin(d)) 
           bmax(d) = max(mesh%xg(i,d),bmax(d)) 
         enddo
      enddo
        
      loc = bmin         
      call MPI_ALLReduce(loc, bmin, mesh%NSD, MPI_DOUBLE_PRECISION, &
                         MPI_MIN, MPI_COMM_WORLD,mpi_err)
      loc = bmax        
      call MPI_ALLReduce(loc, bmax, mesh%NSD, MPI_DOUBLE_PRECISION, &
                         MPI_MAX, MPI_COMM_WORLD,mpi_err)                                             
      if (ismaster) then
        write(*,'(a,I2,a,1x,3F16.5)') " Bound(",b,") minimum :", bmin 
        write(*,'(a,I2,a,1x,3F16.5)') " Bound(",b,") maximum :", bmax
      endif  
    enddo
  
  end subroutine getMeshExtremes   
         
end module meshData

