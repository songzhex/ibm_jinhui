!======================================================================
! Get the rotation matrices
!======================================================================
subroutine get_Rmat(theta, thetd, thedd, Rmat, Rdot, Rddt)
  implicit none

  real(8), intent(in ) :: theta, thetd, thedd
  real(8), intent(out) :: Rmat(3,3), Rdot(3,3), Rddt(3,3)

  Rmat      = 0.0d0
  Rmat(2,2) = cos(theta) - 1.0d0
  Rmat(2,3) =-sin(theta)
  Rmat(3,2) = sin(theta)
  Rmat(3,3) = cos(theta) - 1.0d0

  Rdot      = 0.0d0
  Rdot(2,2) =-sin(theta)*thetd
  Rdot(2,3) =-cos(theta)*thetd
  Rdot(3,2) = cos(theta)*thetd
  Rdot(3,3) =-sin(theta)*thetd

  Rddt      = 0.0d0
  Rddt(2,2) =-cos(theta)*thetd**2 - sin(theta)*thedd
  Rddt(2,3) = sin(theta)*thetd**2 - cos(theta)*thedd
  Rddt(3,2) =-sin(theta)*thetd**2 + cos(theta)*thedd
  Rddt(3,3) =-cos(theta)*thetd**2 - sin(theta)*thedd
end subroutine get_Rmat



!======================================================================
! Get the rotation matrices
!======================================================================
subroutine get_Rmat_Z(theta, thetd, thedd, Rmat, Rdot, Rddt)
  implicit none

  real(8), intent(in ) :: theta, thetd, thedd
  real(8), intent(out) :: Rmat(3,3), Rdot(3,3), Rddt(3,3)

  Rmat      = 0.0d0
  Rmat(1,1) = cos(theta) - 1.0d0
  Rmat(1,2) =-sin(theta)
  Rmat(2,1) = sin(theta)
  Rmat(2,2) = cos(theta) - 1.0d0

  Rdot      = 0.0d0
  Rdot(1,1) =-sin(theta)*thetd
  Rdot(1,2) =-cos(theta)*thetd
  Rdot(2,1) = cos(theta)*thetd
  Rdot(2,2) =-sin(theta)*thetd

  Rddt      = 0.0d0
  Rddt(1,1) =-cos(theta)*thetd**2 - sin(theta)*thedd
  Rddt(1,2) = sin(theta)*thetd**2 - cos(theta)*thedd
  Rddt(2,1) =-sin(theta)*thetd**2 + cos(theta)*thedd
  Rddt(2,2) =-cos(theta)*thetd**2 - sin(theta)*thedd
end subroutine get_Rmat_Z

!======================================================================
! Get the rotation matrices
!======================================================================
subroutine get_Rmat_Y(theta, thetd, thedd, Rmat, Rdot, Rddt)
  implicit none

  real(8), intent(in ) :: theta, thetd, thedd
  real(8), intent(out) :: Rmat(3,3), Rdot(3,3), Rddt(3,3)

  Rmat      = 0.0d0
  Rmat(1,1) = cos(theta) - 1.0d0
  Rmat(1,3) =-sin(theta)
  Rmat(3,1) = sin(theta)
  Rmat(3,3) = cos(theta) - 1.0d0

  Rdot      = 0.0d0
  Rdot(1,1) =-sin(theta)*thetd
  Rdot(1,3) =-cos(theta)*thetd
  Rdot(3,1) = cos(theta)*thetd
  Rdot(3,3) =-sin(theta)*thetd

  Rddt      = 0.0d0
  Rddt(1,1) =-cos(theta)*thetd**2 - sin(theta)*thedd
  Rddt(1,3) = sin(theta)*thetd**2 - cos(theta)*thedd
  Rddt(3,1) =-sin(theta)*thetd**2 + cos(theta)*thedd
  Rddt(3,3) =-cos(theta)*thetd**2 - sin(theta)*thedd
end subroutine get_Rmat_Y

