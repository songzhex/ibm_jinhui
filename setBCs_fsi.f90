!======================================================================
! Set the BC for the case of rotating 5MW with tower
! Jinhui: I copied from the fsi code, just in case for the future
!======================================================================
subroutine setMeshBCs_tower(IBCtmp, msh, st0, st1)
  use mpi
  use meshData
  use params
  use solution
  use globdata
  use commonvars
  use aAdjkeep
  use defs_shell
  implicit none
  type(meshStruct), intent(in)  :: msh   
  integer,          intent(out) :: IBCtmp(msh%NNODE,3)
  type(state),      intent(in)    :: st0
  type(state),      intent(inout) :: st1 
  integer :: b, i, j, k, dd

  ! for the fixed outer box domain (including the tower)
!A  do i = 1, NNODE
!A    if (NodeID(i) == 31) then   
!A      IBC(i,4:6) = 1 
!A    end if
!A  end do

  ! for the disk and the blade
  do b = 1, msh%NBOUND

    if ( (msh%bound(b)%FACE_ID == 11) ) then

      do i = 1, msh%bound(b)%NNODE
        j = msh%bound(b)%BNODES(i)     
      !  IBC(j,4:6) = 1 ! Jinhui: For FSI IBC(1:7),1:3 for navier stokes,4:6 for mesh motion
         IBCtmp(j,1:3)=1
!A-------------------------------------S 05/11/13 Apply rotation to
!A the surface of rotating cylinder corresponding to the structural
!A rotation computed from Rt
        do dd = 1, msh%NSD
          st1%dg(j,dd) = sum((Rmat(dd,:) - Identity1(dd,:))*(msh%xg(j,1:3) - &
                     xrotOld(1:3))) + (xrot(dd) - xrotOld(dd))

          st1%acgm(j,dd) = (((st1%dg(j,dd)-st0%dg(j,dd)-Delt*st0%ugm(j,dd)))* &
                       2.0d0/(Delt*Delt) - &
                       (1.0d0-2.0d0*beti)*st0%acgm(j,dd))/(2.0d0*beti)

          st1%ugm(j,dd) = st0%ugm(j,dd) + &
                       Delt*((1.0d0-gami)*st0%acgm(j,dd)+gami*st1%acg(j,dd))

        end do
!A-------------------------------------E          
      end do
!A fixed the boundary of outer domain
    elseif  ( (msh%bound(b)%FACE_ID == 1) .or. (msh%bound(b)%FACE_ID == 2) .or. (msh%bound(b)%FACE_ID == 3) ) then  
      do i = 1, msh%bound(b)%NNODE
        j = msh%bound(b)%BNODES(i)     !A Points to volumetric node from surface node
        if(abs(msh%xg(j,3) - 10.0d0) < 1.0d-3) then
        !  IBC(j,6) = 1   
          IBCtmp(j,3)=1
          st1%dg(j,3) = 0.0d0
          st1%acgm(j,3) = 0.0d0
          st1%ugm(j,3) = 0.0d0
        else
        !  IBC(j,4:6) = 1  ! commented out by Jinhui
          IBCtmp(j,1:3)=1
          st1%dg(j,1:3) = 0.0d0
          st1%acgm(j,1:3) = 0.0d0
          st1%ugm(j,1:3) = 0.0d0
        end if
      end do

    elseif  ( (msh%bound(b)%FACE_ID == 21) ) then  
      do i = 1, msh%bound(b)%NNODE
        j = msh%bound(b)%BNODES(i)     !mesh BC on a rotor
       ! IBC(j,4:6) = 1   ! commented out by Jinhui
        IBCtmp(j,1:3)=1
      end do


    elseif ( (msh%bound(b)%FACE_ID == 12) ) then

      do i = 1, msh%bound(b)%NNODE
        j = msh%bound(b)%BNODES(i)     
       ! IBC(j,4:6) = 1
        IBCtmp(j,1:3)=1
        do dd = 1, msh%NSD
          st1%dg(j,dd) = sum((Rtang(dd,:) - Identity1(dd,:))*(msh%xg(j,1:3) - &
                     xrotOld(1:3))) + (xrot(dd) - xrotOld(dd))

          st1%acgm(j,dd) = (((st1%dg(j,dd)-st0%dg(j,dd)-Delt*st0%ugm(j,dd)))* &
                       2.0d0/(Delt*Delt) - &
                       (1.0d0-2.0d0*beti)*st0%acgm(j,dd))/(2.0d0*beti)

          st1%ugm(j,dd) = st0%ugm(j,dd) + &
                       Delt*((1.0d0-gami)*st0%acgm(j,dd)+gami*st1%acg(j,dd))
        end do
      end do
    end if
  end do     
end subroutine setMeshBCs_tower


subroutine setMeshBCs_hawt(IBCtmp, msh, st0, st1)
  use mpi
  use meshData
  use params
  use solution
  use globdata
  use commonvars
  use aAdjkeep
  use defs_shell
  implicit none
  type(meshStruct), intent(in)  :: msh   
  integer,          intent(out) :: IBCtmp(msh%NNODE,3)
  type(state),      intent(in)    :: st0
  type(state),      intent(inout) :: st1 
  integer :: b, i, j, k, dd
  real(8) :: rot(msh%NSD)


  do b = 1, msh%NBOUND
    ! apply the same mesh BC for inlet/side/outlet/rotor
    do i = 1, msh%bound(b)%NNODE
      j = msh%bound(b)%BNODES(i)     
     ! IBC(j,4:6) = 1  ! This is for the mesh motion
       IBCtmp(j,1:3)=1
!A-------------------------------------S 05/11/13 Apply rotation to
!A the surface of rotating cylinder corresponding to the structural
!A rotation computed from Rt
        do dd = 1, msh%NSD
          st1%dg(j,dd) = sum((Rmat(dd,:) - Identity1(dd,:))*(msh%xg(j,1:3) - &
                     xrotOld(1:3))) + (xrot(dd) - xrotOld(dd))

          st1%acgm(j,dd) = (((st1%dg(j,dd)-st0%dg(j,dd)-Delt*st0%ugm(j,dd)))* &
                       2.0d0/(Delt*Delt) - &
                       (1.0d0-2.0d0*beti)*st0%acgm(j,dd))/(2.0d0*beti)

          st1%ugm(j,dd) = st0%ugm(j,dd) + &
                       Delt*((1.0d0-gami)*st0%acgm(j,dd)+gami*st1%acg(j,dd))
        end do
!A-------------------------------------E 
    end do      
  end do  
end subroutine setMeshBCs_hawt



