module mpi

  implicit none


  include "mpif.h"
  
  integer, parameter :: maxtask    = 70
  integer, parameter :: maxseg     = 15000
      
  integer :: status(MPI_STATUS_SIZE)
  integer :: lstride, numtask
  integer :: lfront,  maxfront
  integer :: nlwork,  NDOF
  integer :: itag, iacc, iother, numseg, isgbeg, itkbeg, isgend
  integer :: sevsegtype(maxtask,17)            
  integer, allocatable :: ilwork(:)


  integer, parameter :: mpi_master = 0
  integer :: numnodes, myid, mpi_err  
  logical :: ismaster 

  real(8) :: damping_reading, DM_reading, DC_reading, DB_reading
  real(8) :: GGG
  real(8) :: angle_acl, clearance
  real(8), allocatable :: rhsql(:),  rhsqu(:)
  !------------------------------------------------------------------------      
  contains
  !------------------------------------------------------------------------   
  subroutine ctypes(NNODE)
  
    implicit none
    integer, intent(in) :: NNODE
  
    integer :: sizeofdouble, is, itask, workf, kdof
    integer :: isbegin(maxseg), lenseg(maxseg), ioffset(maxseg)
    character(len=30) :: fname
    character(len=10) :: cname
    
    NDOF = NNODE
    
    if (numnodes == 1) then
      numtask  = 0
      maxfront = 1
      return
    endif 

    !-- READ LOCAL WORK FOR U AND P --------------------
  
    workf = 17 
    write(fname,'(I8)') myid+1
    fname = 'lwork.' // trim(adjustl(fname)) // '.dat'
    open(workf, file=fname, status='old')
  
    read(workf,*) numtask, nlwork
  
    allocate(ilwork(nlwork))
    ilwork    = 0
    ilwork(1) = numtask
    
    itkbeg = 1
  
    do itask = 1, numtask
      read(workf,*) ilwork(itkbeg+1) ! tag
      read(workf,*) ilwork(itkbeg+2) ! iacc - role in communication
      read(workf,*) ilwork(itkbeg+3) ! partner node
      read(workf,*) ilwork(itkbeg+4) ! number of segments
      do is = 1, ilwork(itkbeg+4)
        read(workf,*) ilwork(itkbeg+3+2*is) ! isgbegin
        read(workf,*) ilwork(itkbeg+4+2*is) ! lenseg
      end do
      itkbeg = itkbeg + 4 + 2*ilwork(itkbeg+4)
    end do

    close(workf)

    !-- DONE READING LOCAL WORK FOR D, U and P --------------
  
    call MPI_TYPE_EXTENT(MPI_DOUBLE_PRECISION,sizeofdouble,mpi_err)
    lstride = NDOF * sizeofdouble
  
    ! maxfront is a common variable being set in this routine
    maxfront = 0  
    numtask  = ilwork(1)
    itkbeg   = 1

    if (numtask > maxtask) then
      write(*,*) "ERROR: numtask > maxtask", numtask, maxtask 
      call MPI_abort(MPI_COMM_WORLD, 911, mpi_err)
    end if

    do itask = 1, numtask
   
      ! iacc = 0 ==> this task is a send (slave)
      !  = 1 ==> this task is a recieve (master)
      iacc = ilwork(itkbeg + 2)

      ! numseg : number of data segments to be communicated
      numseg = ilwork(itkbeg + 4)

      if (numseg > maxseg) then
        write(*,*) "ERROR: numseg > maxseg at task ", itask 
        call MPI_abort(MPI_COMM_WORLD, 911, mpi_err)
      end if

      ! adjust the number of the other processor, since processors
      ! are numbered here starting from 0, not 1.
      ilwork(itkbeg + 3) = ilwork(itkbeg + 3) - 1 

      ! lfront = total number of nodes involved in this task
      isbegin = 0  ! Initialize
      lenseg  = 0
      lfront  = 0
      do is = 1, numseg

        ! isbegin(is): starting node number for each segment
        isbegin(is) = ilwork(itkbeg + 3 + 2*is)

        ! lenseg(is): length of each segment (number of nodes)
        lenseg(is) = ilwork(itkbeg + 4 + 2*is)

        ! increment the total node counter
        lfront = lfront + lenseg(is)

      end do

      ! maxfront: number of nodes which will be communicated, including
      !   all segments. Note that after the loop over tasks
      !   is complete, maxfront will contain the maximum number
      !   of nodes for any of the tasks.
      maxfront = max(maxfront,lfront)

      ! ioffset: array offset from the first node in the first segment
      ioffset = 0     ! Initialize
      do is = 1, numseg
        ioffset(is) = isbegin(is) - isbegin(1)
      end do

      !-- now set up the MPI data types which will be used in commu.f.
      ! These data types represent the indexed sets that will be sent
      ! and recieved.
      !
      ! the following call to MPI_TYPE_INDEXED will create a new data
      ! type which will represent the blocks of data we wish to transfer
      ! for this task. A handle to the new type is returned 
      ! (sevsegtype(itask,1)). This data type describes the blocks of
      ! data to be transferred in terms of segments.
      ! Input to this routine:
      !     numseg: number of segments in this task
      !     lenseg: length of each segment (number of nodes)
      !     ioffset: where to begin each block with respect to the
      !      first segment
      !     MPI_DOUBLE_PRECISION: type to set for each of the blocks --
      call MPI_TYPE_INDEXED(numseg, lenseg, ioffset, &
                            MPI_DOUBLE_PRECISION, sevsegtype(itask,1), &
                            mpi_err)


      ! now create a new data type for each of the types of arrays we 
      ! may wish to communicate with. For example ndof will be used when
      ! communicating the residual vector. Each one of these is derived
      ! from the first data type defined above, sevsegtype(itask,1).
      !call MPI_TYPE_HVECTOR(NSD, 1, lstrideu, sevsegtypeu(itask,1), &
      !                      sevsegtypeu(itask,2), mpi_err)

      !call MPI_TYPE_HVECTOR(NSD*NSD, 1, lstrideu,sevsegtypeu(itask,1), &
      !                      sevsegtypeu(itask,3), mpi_err)

      !call MPI_TYPE_HVECTOR((NSD+1)*(NSD+1), 1,  lstrideu, &
      !                      sevsegtypeu(itask,1), &
      !                      sevsegtypeu(itask,4), mpi_err)
      do kdof = 2, 16
        call MPI_TYPE_HVECTOR(kdof, 1,  lstride, &
                             sevsegtype(itask,1), &
                             sevsegtype(itask,kdof), mpi_err)    
      enddo

      call MPI_TYPE_INDEXED(numseg, lenseg, ioffset, &
                            MPI_INTEGER, sevsegtype(itask,17), mpi_err) 

      ! now this must be done to make MPI recognize each of the data
      ! types that were just defined
      do kdof = 1, 17
        call MPI_TYPE_COMMIT(sevsegtype(itask,kdof), mpi_err)
      end do

      ! set the counter to the index in ilwork where the next task
      ! begins
      itkbeg = itkbeg + 4 + 2*numseg

    end do    !end loop over tasks
  
  end subroutine ctypes
!======================================================================
! This subroutine is responsible for interprocessor communication of
! the residual and solution vectors.
!
! input:   
!     global(nshg,n): global vector to be communicated. Note that
!          this vector is local to the processor, (i.e.
!          not distributed across processors)
!     n:       second dimension of the array to be communicated
!     code:    = 'in' for communicating with the residual
!          = 'out' for cummunicating the solution 
!---------------------------------------------------------------------
! The array ilwork describes the details of the communications. 
! Each communication step (call of this routine) consists of a 
! sequence of "tasks", where a task is defined as a communication 
! between two processors where data is exchanged. This would imply 
! that for a given processor, there will be as many tasks as there
! are processors with which it must communicate. Details of the 
! ilwork array appear below.
!======================================================================

  subroutine commu(global, n, code)

    implicit none
  
    character(len=3), intent(in) :: code
  
    integer :: stat(MPI_STATUS_SIZE, 2*numnodes), req(2*numnodes)
    integer :: idl, jdl, itask, n, m, is, itemp, idof, kdof, j
    integer :: lenseg

    real(8) :: global(NDOF,n), rtemp(maxfront*n,numnodes)
 
    if (numnodes == 1)  return
  
    kdof = n
    if (n .ge.16) then
      kdof = -1
      write(*,*) "ERROR in commu, no kdof for n =", n
      stop  
    end if
  
    rtemp = 0.0d0      ! Initialize

!... Note that when adding another kdof to the above set, we must
!... also make changes in ctypes.f and auxmpi.h

!---------------------------------------------------------------------
!  ilwork(1): number of tasks
!
!  The following information is contained in ilwork for each task:
!     itag: tag of the communication
!     iacc: == 0 if task is a send
!       == 1 if task is a recieve
!     iother: rank of processor with which this communication occurs
!     numseg: number of data "segments" to be sent or recieved. A 
!     segment is defined as a continuous section of the global
!     vector to be communicated, (i.e. a group of nodes (or,
!     rather, "shape function coefficients") which occur 
!     sequentially in the array global(nshg,n)).
!     isbeg:  location of the first segment in the array owned by the
!     current processor.
!
! The two types of communication are 'in', where the residual is being
! communicated, and 'out', where the solution is being communicated.
! Note that when the type is 'out', senders recieve and recievers send.
!
! The following comment pertains to a communication of type 'in':
!
!     If the task is a send, then all of the numseg segments are
!     sent with a single call to MPI_SEND. Where these segments live in 
!     the array is built into the array sevsegtype, which is a common 
!     array constructed in the subroutine "ctypes.f". In other words,
!     sevsegtype is a data type that describes the indices of the blocks
!     to be sent, in terms of there beginning index, and the length of 
!     each segment. Using this, we can make a single send to take care of
!     all the segments for this task. 
!  
!     If the task is a recieve, then once the vector is recieved, the
!     recieved segments must be added to the correct locations in the
!     current array. These locations are described in ilwork as the
!     beginning position, then the length of the segment.    
!---------------------------------------------------------------------

    numtask = ilwork(1)
  
    itkbeg = 1
    m      = 0
    idl    = 0

    ! loop through all the communication tasks
    do itask = 1, numtask
      m  = m + 1
      itag   = ilwork(itkbeg + 1)
      iacc   = ilwork(itkbeg + 2)
      iother = ilwork(itkbeg + 3)

      if (iother < 0) iother = MPI_PROC_NULL
     
      numseg = ilwork(itkbeg + 4)

      ! when there's no communication, 
      ! there should be no isgbeg..  
      if (iother < 0) then
        isgbeg = 1
      else
        isgbeg = ilwork(itkbeg + 5)
      end if
  
      !--------------------------------------------------
      ! if iacc == 0, then this task is a send. (slave)
      !--------------------------------------------------
      if (iacc == 0) then   

        ! residual communication
        if (code == 'in ')  then       
          call MPI_ISEND(global(isgbeg,1), 1, &
                         sevsegtype(itask,kdof), iother, &
                         itag, MPI_COMM_WORLD, req(m), mpi_err)     
        ! solution communication
        else if (code == 'out') then
          call MPI_IRECV(global(isgbeg,1), 1, &
                       sevsegtype(itask,kdof), iother,  &
                       itag, MPI_COMM_WORLD, req(m), mpi_err)       
        end if

      !------------------------------------------------------
      ! if iacc == 1, then this task is a recieve. (master)
      !------------------------------------------------------
      else

        ! masters receive the residuals
        if (code == 'in ') then        
          ! determine the number of total number of nodes involved 
          ! in this communication (lfront), including all segments
          lfront = 0
          do is = 1, numseg
            lenseg = ilwork(itkbeg + 4 + 2*is)
            lfront = lfront + lenseg
          end do

          ! recieve all segments for this task in a single step
          idl = idl + 1  ! stands for i Do Later, the number to fix later
             
          call MPI_IRECV(rtemp(1,idl), lfront*n, &
                         MPI_DOUBLE_PRECISION, iother,  &
                         itag, MPI_COMM_WORLD, req(m), mpi_err)        
        ! masters send out the solutions
        else if (code == 'out') then       
          call MPI_ISEND(global(isgbeg,1), 1, &
                         sevsegtype(itask,kdof), iother,  &
                         itag, MPI_COMM_WORLD, req(m), mpi_err)
        endif
      endif

      itkbeg = itkbeg + 4 + 2*numseg

      call MPI_WAIT(req(m), stat(:,m), mpi_err)
   
    end do          ! end tasks loop
  
!!!  call MPI_WAITALL(m, req, stat, mpi_err)


    !-----------------------------------------------------------------
    ! Stuff added below is a delayed assembly of that which was 
    ! communicated above but due to the switch to non-blocking 
    ! receives could not be assembled until after the waitall. 
    ! Only necessary for commu "in"
    !-----------------------------------------------------------------
    if (code == 'in ') then
  
      itkbeg = 1
      jdl    = 0

      ! time to do all the segments that needed to be
      ! assembled into the global vector
      do j = 1, numtask  

        iacc   = ilwork(itkbeg + 2)
        numseg = ilwork(itkbeg + 4)

        ! if it's a master
        if (iacc == 1) then

          ! keep track of order of rtemp's   
          jdl = jdl + 1    

          ! add the recieved data to the global array on the current processor.
          ! Note that this involves splitting up the chunk of recieved data
          ! into its correct segment locations for the current processor.
          itemp = 1
          do idof = 1, n
            do is = 1, numseg
              isgbeg = ilwork(itkbeg + 3 + 2*is)
              lenseg = ilwork(itkbeg + 4 + 2*is)
              isgend = isgbeg + lenseg - 1

              global(isgbeg:isgend,idof) = global(isgbeg:isgend,idof) &
                                         + rtemp(itemp:itemp+lenseg-1,jdl)

              itemp = itemp + lenseg
            enddo
          enddo
       
        else  ! end of receive (iacc=1), now do iacc=0 (zero sent)
!!!      else if (iacc == 0) then       

          do is = 1, numseg
            isgbeg = ilwork(itkbeg + 3 + 2*is)
            lenseg = ilwork(itkbeg + 4 + 2*is)
            isgend = isgbeg + lenseg - 1 
            global(isgbeg:isgend,:) = 0.0d0
          enddo
       
        endif
        
        itkbeg = itkbeg + 4 + 2*numseg

      enddo
    endif    ! end of commu "in"

  end subroutine commu


  !====================================================================== 
  ! This subroutine is responsible for interprocessor communication.
  ! it sets teh slave dofs to zero vectors.
  !======================================================================
  subroutine zeroslaves(global, n)

    implicit none
  
    integer :: stat(MPI_STATUS_SIZE, 2*numnodes), req(2*numnodes)

    integer :: idl, jdl, itask, n, m, is, itemp, idof, kdof, j
    integer :: lenseg
    real(8) :: global(NDOF,n)
    real(8) :: rtemp(maxfront*n,numnodes) 

    if (numnodes == 1)  return
  
    itkbeg = 1
    jdl    = 0
    do j = 1, numtask

      iacc   = ilwork(itkbeg + 2)
      numseg = ilwork(itkbeg + 4)

      if (iacc == 0) then
    
        do is = 1, numseg
          isgbeg = ilwork (itkbeg + 3 + 2*is)
          lenseg = ilwork (itkbeg + 4 + 2*is)
          isgend = isgbeg + lenseg - 1 
          global(isgbeg:isgend,:) = 0.0d0
        enddo
     
      endif
         
      itkbeg = itkbeg + 4 + 2*numseg
    enddo

  end subroutine zeroslaves
  
!======================================================================
! This subroutine is responsible for interprocessor communication of
! the residual and solution vectors.
!
! input:   
!     global(nshg,n): global vector to be communicated. Note that
!          this vector is local to the processor, (i.e.
!          not distributed across processors)
!     n:       second dimension of the array to be communicated
!     code:    = 'in' for communicating with the residual
!          = 'out' for cummunicating the solution 
!
!---------------------------------------------------------------------
!
! The array ilwork describes the details of the communications. 
! Each communication step (call of this routine) consists of a 
! sequence of "tasks", where a task is defined as a communication 
! between two processors where data is exchanged. This would imply 
! that for a given processor, there will be as many tasks as there
! are processors with which it must communicate. Details of the 
! ilwork array appear below.
!======================================================================
  subroutine icommu(global)

    implicit none
  
    integer :: stat(MPI_STATUS_SIZE, 2*numnodes), req(2*numnodes)
    integer :: idl, jdl, itask,n, m, is, itemp, idof, kdof, j
    integer :: lenseg
    integer :: global(NDOF), rtemp(NDOF)

    if (numnodes == 1)  return
    
    n    = 1
    kdof = 17

    numtask = ilwork(1)
  
    itkbeg = 1
    m   = 0
    idl = 0
  
    do itask = 1, numtask
      m  = m + 1
      itag   = ilwork (itkbeg + 1)
      iacc   = ilwork (itkbeg + 2)
      iother = ilwork (itkbeg + 3)
      numseg = ilwork (itkbeg + 4)

      ! when there's no communication, 
      ! there should be no isgbeg..  
      if (iother < 0) then
        isgbeg = 1
        iother = MPI_PROC_NULL
      else
        isgbeg = ilwork (itkbeg + 5)
      end if
      rtemp = 0
      if (iacc == 0) then  
        call MPI_ISEND(global(isgbeg),1, &
               sevsegtype(itask,kdof), &
               iother,itag,MPI_COMM_WORLD,req(m), &
               mpi_err)       
      else       
        call MPI_IRECV(rtemp(isgbeg),1, &
               sevsegtype(itask,kdof),  &
               iother,itag,MPI_COMM_WORLD, req(m),&
               mpi_err)

      endif
	 	 
      call MPI_WAIT(req(m),status,mpi_err)
	 
      global = global + rtemp
	 
      itkbeg = itkbeg + 4 + 2*numseg
    end do   
  
    itkbeg = 1
    m   = 0
    idl = 0

    do itask = 1, numtask
      m  = m + 1
      itag   = ilwork (itkbeg + 1)
      iacc   = ilwork (itkbeg + 2)
      iother = ilwork (itkbeg + 3)
      numseg = ilwork (itkbeg + 4)

      ! when there's no communication, 
      ! there should be no isgbeg..  
      if (iother < 0) then
        isgbeg = 1
        iother = MPI_PROC_NULL
      else
        isgbeg = ilwork (itkbeg + 5)
      end if
	 
      if (iacc == 0) then             
        call MPI_IRECV(global(isgbeg),1, &
             sevsegtype(itask,kdof),  &
             iother,itag,MPI_COMM_WORLD,req(m), &
             mpi_err)       
      else       
        call MPI_ISEND(global(isgbeg),1, &
              sevsegtype(itask,kdof),  &
              iother,itag,MPI_COMM_WORLD,req(m), &
              mpi_err)      
      end if
      call MPI_WAIT(req(m),status,mpi_err)
      itkbeg = itkbeg + 4 + 2*numseg
   
    end do          ! end tasks loop

  end subroutine icommu  

   
end module mpi




