module globdata

  use meshData
  use solution
  use matrix
  use elasticBody
  use mpi
  use hashed_octree
  implicit none
  save  
  
  !------------------------------------------------------------------------  
  type(meshStruct) :: mesh_fluid
  type(bodyParams), allocatable :: bodies_param(:)
  
  !------------------------------------------------------------------------
  type(state) :: new  
  type(state) :: new_eps
  type(state) :: new_eps_DG
  type(state) :: old
  type(state) :: alpha
  
  type(state) :: old_rd
  type(state) :: new_rd
  
  type(bodyState), allocatable :: bodies_new(:)    
  type(bodyState), allocatable :: bodies_new_eps(:)
  type(bodyState), allocatable :: bodies_old(:)
  type(bodyState), allocatable :: bodies_alpha(:)  

  ! octree to help locate points in background mesh
  type(octree) :: global_octree_background
  
  !------------------------------------------------------------------------  
  integer, allocatable :: IBCns(:,:), IBClsc(:),IBCmesh(:,:)
  real(8), allocatable :: ugbc(:,:), phigbc(:)
  real(8), allocatable :: xO(:,:)
      
  real(8), allocatable :: ashBC(:,:), ushBC(:,:), dshBC(:,:)
  !------------------------------------------------------------------------  
  real(8), allocatable :: rhsg0(:,:)

  real(8), allocatable :: rhsg0_dg(:,:)
  ! These are from Jinhui
  real(8), allocatable ::  rhsg_fluid(:,:),rhsg_mesh(:,:),rhsg_rm(:,:),rhsg_rd(:,:), rhsg_mass(:,:)
  ! These are from Jinhui

  type(sparseStruct)   :: sparseMat
  real(8), allocatable :: LHSns(:,:), LHSlsc(:), LHSlsrd(:), &
                          LHSlsmc(:), LHSmesh(:,:), LHSbody(:) 

  real(8), allocatable :: lhslscu(:,:),lhsrdphi(:),lhsmcu(:,:),lhsmcrd(:)
  
  !------------------------------------------------------------------------    
  real(8), allocatable :: bdpcNSu(:,:,:),bdpcNSp(:),bdpcMesh(:,:,:),&
                          dpcLSC(:),dpcLSRD(:),dpcLSMC(:),dpcBody(:)

  real(8), allocatable :: PCns(:,:), PClsc(:), PClsrd(:), &
                          PClsmc(:), PCmesh(:,:)
                              
  !------------------------------------------------------------------------
  contains  
  !------------------------------------------------------------------------
  subroutine allocData(NNODE,icnt,NSD)  
    implicit none  
    integer,  intent(in)  :: NNODE, icnt, NSD
    integer :: b
     
    call allocState(old,     NNODE, NSD)
    call allocState(alpha,   NNODE, NSD)
    call allocState(new,     NNODE, NSD)
    call allocState(new_eps, NNODE, NSD) 
    call allocState(new_eps_DG, NNODE, NSD) 
    call allocState(old_rd, NNODE, NSD)
    call allocState(new_rd, NNODE, NSD)
    
   ! allocate(bodies_new    (NBODIES))    
   ! allocate(bodies_new_eps(NBODIES))
   ! allocate(bodies_old    (NBODIES))
   ! allocate(bodies_alpha  (NBODIES))  
    
    
    allocate(IBCns(NNODE,NSD+1))
    allocate(IBClsc(NNODE)) 
    allocate(IBCmesh(NNODE,NSD))

    allocate(rhsql(NNODE)) 
    allocate(rhsqu(NNODE)) 

    allocate(ashBC(NNODE,NSD))
    allocate(ushBC(NNODE,NSD))
    allocate(dshBC(NNODE,NSD))
    
    allocate(ugbc(NNODE,NSD))
    allocate(phigbc(NNODE))
        
    allocate(rhsg0(NNODE,13))
    allocate(rhsg0_dg(NNODE,6))
  
!  These are from Jinhui 
    allocate(rhsg_fluid(NNODE,5))
    allocate(rhsg_mesh(NNODE,3))
    allocate(rhsg_rm(NNODE,2))
    allocate(rhsg_rd(NNODE,1))
    allocate(rhsg_mass(NNODE,1))
!  These are from Jinhui

    
    allocate(LHSns((NSD+1)*(NSD+1),icnt))  
    allocate(LHSlsc (icnt)) 
    allocate(LHSlsrd(icnt))
    allocate(LHSlsmc(icnt))
    allocate(LHSmesh(NSD*NSD,icnt))
    
    allocate(lhslscu(NSD,icnt))
    allocate(lhsrdphi(icnt))
    allocate(lhsmcu(NSD,icnt))
    allocate(lhsmcrd(icnt))
    
    allocate(PCns((NSD+1)*(NSD+1),icnt))  
    allocate(PClsc (icnt)) 
    allocate(PClsrd(icnt))
    allocate(PClsmc(icnt))
    allocate(PCmesh(NSD*NSD,icnt))
        
    allocate(bdpcNSu (NNODE,NSD,NSD))    
    allocate(bdpcMesh(NNODE,NSD,NSD))
    allocate(bdpcNSp(NNODE))
    allocate(dpcLSC (NNODE))
    allocate(dpcLSRD(NNODE))
    allocate(dpcLSMC(NNODE))
    
    allocate(LhsBody(NNODE))
    allocate(dpcBody(NNODE))
         
  end subroutine allocData
  
  !------------------------------------------------------------------------
  subroutine deallocData()  

    implicit none      
    
    deallocate(IBCns)
    deallocate(IBClsc)         
    deallocate(rhsg0 ) 
    deallocate(rhsg0_dg)


    deallocate(ashBC)
    deallocate(ushBC)
    deallocate(dshBC)

!  These are from Jinhui 
    deallocate(rhsg_fluid)
    deallocate(rhsg_mesh)
    deallocate(rhsg_rm)
    deallocate(rhsg_rd)
    deallocate(rhsg_mass)
!  These are from Jinhui
    
    deallocate(LHSns  )
    deallocate(LHSlsc ) 
    deallocate(LHSlsrd)
    deallocate(LHSlsmc)
    deallocate(LHSmesh)
    
    deallocate(lhslscu )
    deallocate(lhsrdphi)
    deallocate(lhsmcu  )
    deallocate(lhsmcrd )
        
    deallocate(PCns  )
    deallocate(PClsc ) 
    deallocate(PClsrd)
    deallocate(PClsmc)
    deallocate(PCmesh)
        
    deallocate(bdpcNSu)
    deallocate(bdpcNSp)
    deallocate(dpcLSC )
    deallocate(dpcLSRD)
    deallocate(dpcLSMC)
    deallocate(bdpcMesh)
    deallocate(rhsql)
    deallocate(rhsqu)

            
  end subroutine deallocData  
  
end module globdata
