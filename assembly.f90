subroutine assemble(st, rhsg, rhsg_dg, newJac, flag, SH, NM, & 
                    NRB_BEA, BEA, mNRB_BEA, STRU, bblade, sur)

  
  use globdata   
  use params   
  use boundaryConditions
  use aAdjKeep
  use mpi
  use defs_shell
  use types_beam
  use types_structure
  implicit none
   
  type(state),      intent(inout) :: st   
 ! type(bodyState),  intent(in)    :: bodies_st(NBODIES)      
  real(8),          intent(inout) :: rhsg(mesh_fluid%NNODE,13)  
  real(8),          intent(inout) :: rhsg_dg(mesh_fluid%NNODE,6)  
  integer,          intent(in)    :: newJac
  integer,          intent(in) :: flag(4)
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  type(mesh_beam), intent(inout)    :: NRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(structure) ,intent(inout) :: STRU
  type(mesh),intent(in) :: bblade(3)
  type(smesh), intent(inout)  :: sur
  integer :: b,i,j
  real(8) ::  MomResA, MomResW,ConResA,ConResW
  real(8) ::  MomResA1, MomResW1,ConResA1,ConResW1
  ! Initialize Jacobians
  if (newJac >= 1) then
    LHSns   = 0d0  
    LHSlsc  = 0d0
    LHSlsrd = 0d0
    LHSlsmc = 0d0
    LHSmesh = 0d0
    LHSBody = 0d0 
    STRU%RHSG     = 0.0d0    ! initialize
    STRU%RHSG_EXT = 0.0d0
    STRU%LHSK  = 0.0d0
  endif
  if (newJac >= 2) then
    LHSlscu  = 0d0
    LHSrdphi = 0d0
    LHSmcu   = 0d0
    LHSmcrd  = 0d0

  endif
  ! Initialize Residuals
  rhsg  = 0d0  
  rhsg_dg  = 0d0       
  ! Set boundary conditions
   if(ismaster ) then
    call setBCs_turbine_sliding(SH,NRB_BEA)
   endif
  ! if(ismaster) write(*,*) myid, "*************apply BD on the structure*************"   
    call getvalBC(mesh_fluid, SH, bblade) ! Jinhui: to get the solution from the strucutre  
  !  if(ismaster)  write(*,*) myid,"*************get the BD values from the structure*************"   
   if((flag(2).eq.1).or.(flag(1).eq.1)) then
      if(move) then
       call setBCs_Mesh_Fin_move(IBCmesh, mesh_fluid, old, st) 
      else
       call setBCs_Mesh_Fin(IBCmesh, mesh_fluid, old, st) 
      endif
   ! if(ismaster) write(*,*) myid, "*************apply BD condition for the mesh*************" 
    endif

    call setBCs_CFD_Fin (IBCns, mesh_fluid, old, st)
   ! if(ismaster) write(*,*) myid,"*************apply BD condition for NS*************" 

    call setBCs_LSC_Fin (IBClsc,  mesh_fluid, old, st)   
   ! if(ismaster) write(*,*) myid,"*************apply BD condition for LS*************" 

    ugbc   = alfi*ugbc   + malf*old%ug 
    phigbc = alfi*phigbc + malf*old%phig   

    call ComputeNodalRhoMu(st, mesh_fluid) 
   ! if(ismaster) write(*,*) myid,"*************compute nodal value*************" 
    call getAlphaState(alpha, old, st)
   ! if(ismaster) write(*,*) myid,"*************compute alpha value*************" 

    if((flag(1).eq.1).or.(flag(2).eq.1)) then           
      ! Navier-Stokes, Convection and redistance  
      if (mesh_fluid%IM_flag == 1) then                
        call IntAssembly_IM (alpha, mesh_fluid, rhsg(:,1:9), newJac,flag) 
        if (numnodes > 1) then
            call commu(mesh_fluid%includeflags, 1  , 'in ') ! Global
            call commu(mesh_fluid%includeflags, 1  , 'out') ! Global
        endif
        ! now call a routine that will set a dirichlet condition on all nodes with
        ! zero includeflags (gets all of its information from global vars)
        call remove_interior(mesh_fluid, rhsg(:,1:9))
        call weak_BC_IM(sur, alpha, mesh_fluid, rhsg(:,1:9), newJac,flag)
      else
        call IntAssembly (alpha, mesh_fluid, rhsg(:,1:9), newJac,flag) 
      endif
   ! if(ismaster) write(*,*) myid,"*************assmeble interior for NS*************"  
      call FaceAssembly(alpha, mesh_fluid, rhsg(:,1:9), newJac,flag)  
   ! if(ismaster)   write(*,*) myid,"*************assmeble face for NS*************"
   endif
  ! if(ismaster) write(*,*) myid,"*************assemble for the interial and surface*************"
    if((flag(1).eq.1) .and. move) then
      !call FaceAsse_DG_w1t1(dgAlpha, ugAlpha, ugmAlpha, pgAlpha, NM)
      call FaceAsse_DG_w1t1(alpha, mesh_fluid, rhsg(:,1:5), newJac,flag, NM,1,1)  
   !   if(ismaster) write(*,*) "local contribution"
      !call FaceAsse_DG_w1t2(dgAlpha, ugAlpha, ugmAlpha, pgAlpha, RHSGu2, RHSGp2, NM)
      call FaceAsse_DG_w1t2(alpha, mesh_fluid, rhsg_dg(:,1:5), newJac, flag, NM,1,1) 
   !   if(ismaster) write(*,*) "cross contribution"
   endif

    ! call MPI_BARRIER (MPI_COMM_WORLD,mpi_err) 
    ! stop
  !if(ismaster) write(*,*) myid,"*************assemble for sliding interface*************"         
  ! Get midpoint level 
  ! call getMidPointState (alpha, old, st) 
  ! if(ismaster)write(*,*) myid,"*************get mid-point level*************"
            
  ! Mass conservation    
  ! TODO_FX: implement immersed for mass fix
  ! under what scenario do we need this?   
  if(flag(4).eq.1) then      
   call IntAssembly_massfix (old, alpha, st, mesh_fluid, rhsg(:,10), newJac)    
   call FaceAssembly_massfix(alpha,          mesh_fluid, rhsg(:,10), newJac) 
  endif
   
  ! Elastic bodies
    if (solfsi) then 
       if(flag(1).eq.1) then 
          ugbc   = (1d0/alfi)*(ugbc   - malf*old%ug) 
          phigbc = (1d0/alfi)*(phigbc - malf*old%phig)  
          ugbc   = 0.5d0*(ugbc   + old%ug) 
          phigbc = 0.5d0*(phigbc + old%phig)
          RHSGu = 0d0
          RHSGm = 0d0
          call ForceAssembly(alpha, mesh_fluid, rhsg(:,11), newJac)  
          if(ismaster) write(*,*) myid, "*************assemble force*************"
          call assembly_shell_beam(mesh_fluid, SH, STRU, NRB_BEA, BEA, mNRB_BEA, newJac, bblade) 
         ! write(*,*) myid, "*************assemble the structure is done"  
       endif
    else
      rhsg(:,7:9) = 0d0 
      rhsg(:,11:13) = 0d0 ! if move is false, then we do not need to solve structure and mesh movtion
      STRU%RHSG(:,1:3) = 0d0
    endif   
    if(level_flag.eq.0) then          
     rhsg(:,5) = 0d0
     rhsg_dg(:,5:6) = 0d0 
    endif                
      call commu(rhsg, 13, 'in ')
      call commu(rhsg_dg, 6, 'in ')
      rhsg(:,1:6) = rhsg(:,1:6) + rhsg_dg(:,1:6)

end subroutine assemble



subroutine assemble1(st, rhsg,rhsg_dg, newJac, flag, SH, NM, &
          NRB_BEA, BEA, mNRB_BEA, STRU, bblade,assembling_flag,sur)
   
  use globdata   
  use params   
  use boundaryConditions
  use aAdjKeep
  use mpi
  use defs_shell
  use types_beam
  use types_structure
  implicit none
   
  type(state),      intent(inout) :: st   
  type(smesh),      intent(inout) :: sur 
 ! type(bodyState),  intent(in)    :: bodies_st(NBODIES)      
  real(8),          intent(inout) :: rhsg(mesh_fluid%NNODE,13)  
  real(8),          intent(inout) :: rhsg_dg(mesh_fluid%NNODE,6)  
  integer,          intent(in)    :: newJac
  integer,          intent(in) :: flag(4), assembling_flag
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  type(mesh_beam), intent(inout)    :: NRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(structure) ,intent(inout) :: STRU
  type(mesh),intent(in) :: bblade(3)
  integer :: b,i,j
  real(8) ::  MomResA, MomResW,ConResA,ConResW
  real(8) ::  MomResA1, MomResW1,ConResA1,ConResW1
  ! Initialize Jacobians
  if (newJac >= 1) then
    LHSns   = 0d0  
    LHSlsc  = 0d0
    LHSlsrd = 0d0
    LHSlsmc = 0d0
    LHSmesh = 0d0
    LHSBody = 0d0 
    STRU%RHSG     = 0.0d0    ! initialize
    STRU%RHSG_EXT = 0.0d0
    STRU%LHSK  = 0.0d0
  endif
  if (newJac >= 2) then
    LHSlscu  = 0d0
    LHSrdphi = 0d0
    LHSmcu   = 0d0
    LHSmcrd  = 0d0
  endif
  ! Initialize Residuals
  rhsg  = 0d0
  rhsg_dg = 0d0           
  ! Set boundary conditions
  !  write(*,*) myid,"~~~~~~~~~~~~~~~~~~~~~ASDDFFSDF~~~~~~~~~~~~~~~~~~~~~~~"
   if(ismaster ) then
    call setBCs_turbine_sliding(SH,NRB_BEA)
   endif
 !  if(ismaster) write(*,*) myid, "*************apply BD on the structure*************"   
   call getvalBC(mesh_fluid, SH, bblade) ! Jinhui: to get the solution from the strucutre  
  !  if(ismaster)  write(*,*) myid,"*************get the BD values from the structure*************"   
   ! if(flag(2).eq.1) then
   if((flag(2).eq.1).or.(flag(1).eq.1)) then
      if(move) then
       call setBCs_Mesh_Fin_move(IBCmesh, mesh_fluid, old, st) 
      else
       call setBCs_Mesh_Fin(IBCmesh, mesh_fluid, old, st) 
    endif
    endif
     

    call setBCs_CFD_Fin (IBCns, mesh_fluid, old, st)
   ! if(ismaster) write(*,*) myid,"*************apply BD condition for NS*************" 

    call setBCs_LSC_Fin (IBClsc,  mesh_fluid, old, st)   
   ! if(ismaster) write(*,*) myid,"*************apply BD condition for LS*************" 

    ugbc   = alfi*ugbc   + malf*old%ug 

    phigbc = alfi*phigbc + malf*old%phig   
   ! if(ismaster) write(*,*) myid,"*************apply BD condition for LS*************"  
  ! Get interface quantaties    
    call ComputeNodalRhoMu(st, mesh_fluid) 
   !  if(ismaster) write(*,*) myid,"*************compute nodal value*************" 
    call getAlphaState(alpha, old, st)

     if((flag(1).eq.1).or.(flag(2).eq.1)) then
   ! Get alpha level    
   ! if(ismaster)   write(*,*) myid,"*************getting alpha level*************"         
   ! Navier-Stokes, Convection and redistance                 ! Navier-Stokes, Convection and redistance  
      if (mesh_fluid%IM_flag == 1) then                
        call IntAssembly_IM (alpha, mesh_fluid, rhsg(:,1:9), newJac,flag) 
        if (numnodes > 1) then
            call commu(mesh_fluid%includeflags, 1  , 'in ') ! Global
            call commu(mesh_fluid%includeflags, 1  , 'out') ! Global
        endif
        ! now call a routine that will set a dirichlet condition on all nodes with
        ! zero includeflags (gets all of its information from global vars)
        call remove_interior(mesh_fluid, rhsg(:,1:9))
        call weak_BC_IM(sur, alpha, mesh_fluid, rhsg(:,1:9), newJac,flag)
      else
        call IntAssembly (alpha, mesh_fluid, rhsg(:,1:9), newJac,flag) 
      endif
   ! if(ismaster) write(*,*) myid,"*************assmeble interior for NS*************"  
      call FaceAssembly(alpha, mesh_fluid, rhsg(:,1:9), newJac,flag)  
  ! if(ismaster)   write(*,*) myid,"*************assmeble face for NS*************"
   endif
  !endif
    if((flag(1).eq.1) .and. move) then
      !call FaceAsse_DG_w1t1(dgAlpha, ugAlpha, ugmAlpha, pgAlpha, NM)
      call FaceAsse_DG_w1t1(alpha, mesh_fluid, rhsg(:,1:5), newJac,flag, NM,1,1)  
       !call FaceAsse_DG_w1t2(dgAlpha, ugAlpha, ugmAlpha, pgAlpha, RHSGu2, RHSGp2, NM)
      call FaceAsse_DG_w1t2(alpha, mesh_fluid, rhsg_dg(:,1:5), newJac, flag, NM,1,1) 
   endif  
  ! Get midpoint level 
!   call getMidPointState (alpha, old, st) 
  ! if(ismaster)write(*,*) myid,"*************get mid-point level*************"        
  ! Elastic bodies
  if(assembling_flag.eq.1) then
    if (solfsi) then 
       if(flag(1).eq.1) then 
          ugbc   = (1d0/alfi)*(ugbc   - malf*old%ug) 
          phigbc = (1d0/alfi)*(phigbc - malf*old%phig)  
          ugbc   = 0.5d0*(ugbc   + old%ug) 
          phigbc = 0.5d0*(phigbc + old%phig)
          RHSGu = 0d0
          RHSGm = 0d0
          call ForceAssembly(alpha, mesh_fluid, rhsg(:,11), newJac)  
          if(ismaster) write(*,*) myid, "*************assemble force*************"
          call assembly_shell_beam(mesh_fluid, SH, STRU, NRB_BEA, BEA, mNRB_BEA, newJac, bblade) 
         ! write(*,*) myid, "*************assemble the structure is done"  
       endif
    else
      rhsg(:,7:9) = 0d0 
      rhsg(:,11:13) = 0d0 ! if move is false, then we do not need to solve structure and mesh movtion
      STRU%RHSG(:,1:3) = 0d0
    endif
  endif       
  if(level_flag.eq.0) then          
     rhsg(:,5) = 0d0
     rhsg_dg(:,5:6) = 0d0 
   endif                   
   call commu(rhsg, 13, 'in ')
   call commu(rhsg_dg, 6, 'in ')
   rhsg(:,1:6) = rhsg(:,1:6) + rhsg_dg(:,1:6)

end subroutine assemble1

!======================================================================
!
!======================================================================
subroutine IntAssembly(st, msh,  rhsg, newJac,flag)
                        
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use BoundaryConditions
  use aAdjKeep
  implicit none
  
  type(state),      intent(in) :: st
  type(meshStruct), intent(in) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,2*msh%NSD+3)  
  integer, intent(in) :: newJac
  integer, intent(in) :: flag(4)

  integer :: iel, igauss, i, j, k,ii,jj,kk, hess_flag, idx, nshl, NGAUSS,aa,bb
  integer :: IBCns_loc(msh%maxNSHL,msh%NSD+1), IBClsc_loc(msh%maxNSHL,1), &
             IBCmesh_loc(msh%maxNSHL,msh%NSD)

  real(8), allocatable :: eKns(:,:,:), eKlsc(:,:),eKlsrd(:,:),eKmesh(:,:,:),&
                          eKlscu(:,:,:),eKrdphi(:,:)
  real(8), allocatable :: eRns(:,:), eRlsc(:),eRlsrd(:), eRmesh(:,:)

  real(8), allocatable :: gp(:,:), gw(:)                          
  real(8), allocatable :: shl(:), shgradg(:,:),shgradgold(:,:), shhessg(:,:,:)
  real(8) :: Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD),&
             dxdxi(msh%NSD,msh%NSD),dxidx(msh%NSD,msh%NSD)
  real(8) :: DetJ, detJgwt,shconv(msh%maxNSHL)
  
  real(8), allocatable :: xl(:,:),wl(:), dl(:,:),dlold(:,:),uml(:,:), acml(:,:),&
                          ul(:,:),acl(:,:),pl(:),rhol(:),mul(:),&
                          ulold(:,:),rho0l(:), &
                          phil(:),rphil(:), philold(:),&
                          phi1l(:),phidl(:),  &
                          hl(:), hel(:), hepl(:)

  real(8), allocatable :: Rhsq(:), Rhsl(:)

  real(8) :: di(msh%NSD), umi(msh%NSD), acmi(msh%NSD),           &
             ui(msh%NSD), aci(msh%NSD), pri, rho, mu, rho0,  &
             phi , rphi, phi1, phid, he, hep,hi 
  real(8) :: ddidxi(msh%NSD,msh%NSD), duidxi(msh%NSD,msh%NSD),duidxixj(msh%NSD,msh%NSD,msh%NSD),&
             duiolddxi(msh%NSD,msh%NSD), dpridxi(msh%NSD), &
             dphidxi(msh%NSD),dphiolddxi(msh%NSD),dphiddxi(msh%NSD), &
             drhodxi(msh%NSD), rhoconv, DetJold, uiold(msh%NSD)

  real(8) :: uadvi(msh%NSD), Se, ai(msh%NSD),ai1(msh%NSD), fi(msh%NSD)
  real(8) :: res_u(msh%NSD), res_c, res_rd
  real(8) :: tauM_ns, tauC_ns, tauP_ns, tauBar_ns, tau_c, tau_rd, gradmag
  real(8) :: kdc_ns, kdc_c, kdc_rd, Emod, phimax
  real(8) :: fact1,fact2
  real(8) :: Qi, Omegai(msh%NSD,msh%NSD), Si(msh%NSD,msh%NSD)
    
  real(8), parameter :: damp = 0.5d0
  real(8) :: kappa, Ftens(msh%NSD,msh%NSD), Stens(msh%NSD,msh%NSD), Ctens(msh%NSD,msh%NSD,msh%NSD,msh%NSD) 
  real(8) :: cfl(2), gcfl(2), cfl_loc(2)
  real(8) :: pse_time_loc,pse_time_loc1,pse_time_glob,pse_time_glob1,tmp_val
  cfl=0.0d0
  if(flag(2).eq.1) then
  pse_time=100000000000d0
  pse_time1=100000000000d0
  endif

  if((flag(1) == 1).and.(newJac>=1)) then 
   rhsqu = 0d0
   rhsql = 0d0
  endif

  fact1 = almi
  fact2 = alfi*gami*Delt 

  phimax = max(maxval(st%phig),-minval(st%phig))

  phi = phimax
  call MPI_ALLREDUCE (phi, phimax ,1, MPI_DOUBLE_PRECISION, &
                      MPI_SUM, MPI_COMM_WORLD, mpi_err)  

!...  Loop over elements
  NSHL   = -1  
  do iel = 1, msh%NELEM 

!...  Allocate        
    if (NSHL /= msh%ELMNSHL(iel)) then
      
      if (NSHL >= 0) then
        deallocate(shl, shgradg,  shgradgold,shhessg, gp, gw,              &
                   eRns, eRlsc,eRlsrd,eRmesh,                  &
                   xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul,       &
                   ulold,rho0l,phil,rphil,philold,phi1l,phidl, &
                   hl, hel, hepl)
        !if (newJac >= 1) then          
        deallocate(eKns,eKlsc,eKlsrd, eKmesh,eKlscu,eKrdphi,Rhsq, Rhsl)
        !endif           
      endif
     
      NSHL   = msh%ELMNSHL(iel)
      NGAUSS = msh%ELMNGAUSS(iel)

      allocate(shl(NSHL), shgradg(NSHL,msh%NSD), shgradgold(NSHL,msh%NSD),shhessg(NSHL,msh%NSD,msh%NSD),&
               gp(NGAUSS,msh%NSD), gw(NGAUSS),                        &
               eRns(msh%NSD+1,NSHL), eRlsc(NSHL),eRlsrd(NSHL),eRmesh(msh%NSD,NSHL),  &
               dl(NSHL,msh%NSD),dlold(NSHL,msh%NSD), ul(NSHL,msh%NSD),wl(NSHL),      &
               acl(NSHL,msh%NSD), uml(NSHL,msh%NSD), acml(NSHL,msh%NSD),             &
               pl(NSHL), xl(NSHL,msh%NSD), rhol(NSHL),mul(NSHL),                     &
               ulold(NSHL,msh%NSD),rho0l(NSHL),phil(NSHL),rphil(NSHL),philold(NSHL), &
               phi1l(NSHL),phidl(NSHL),hl(NSHL),hel(NSHL),hepl(NSHL))
      !if (newJac >= 1) then                    
        allocate(eKns((msh%NSD+1)*(msh%NSD+1),NSHL,NSHL), &
                 eKlsc(NSHL,NSHL),eKlsrd(NSHL,NSHL),      & 
                 eKmesh(msh%NSD*msh%NSD,NSHL,NSHL),      &
                 eKlscu(msh%NSD,NSHL,NSHL), eKrdphi(NSHL,NSHL),Rhsq(NSHL), Rhsl(NSHL))
      !endif        

      call genGPandGW(gp, gw, NGAUSS, NSHL)
    end if

!...  Get local solution arrays 
    do i = 1, NSHL 
    
      idx = msh%IEN(iel,i)
      xl   (i,:) = msh%xg    (idx,:)     
      wl   (i)   = msh%wg    (idx)
      
      dl   (i,:) = st%dg   (idx,:)      
      dlold(i,:) = old%dg  (idx,:)
      uml  (i,:) = st%ugm  (idx,:)
      acml (i,:) = st%acgm (idx,:)
            
      ul   (i,:) = st%ug   (idx,:)
      acl  (i,:) = st%acg  (idx,:)
      pl   (i)   = st%pg   (idx)
      rhol (i)   = old%rhog (idx)          
      mul  (i)   = old%mug  (idx) 
      ulold(i,:) = old%ug  (idx,:)   
      rho0l(i)   = st%rho0g(idx)
      
      phil(i)    = st%phig (idx)       
      rphil(i)   = st%rphig(idx)     
      philold(i) = old%phig(idx)
     
      phi1l(i)   = st%phig   (idx)

      phidl(i)   = st%phidg  (idx)

      hl(i)      = st%hg  (idx)             
      hel(i)     = st%heg (idx)             
      hepl(i)    = st%hepg(idx)     
    end do
         
!...  Initialize element jacobians
    if (newJac >= 1) then
      eKns   = 0d0           
      eKlsc  = 0d0
      eKlsrd = 0d0  
      eKmesh = 0d0     
    endif
    if (newJac >= 2) then    
      eKlscu  = 0d0     
      eKrdphi = 0d0      
    endif   
        
!...  Initialize element residuals    
    eRns   = 0d0
    eRlsc  = 0d0
    eRlsrd = 0d0
    eRmesh = 0d0
    Rhsq   = 0d0 
    Rhsl   = 0d0

!---------------------------------------------------------------------------------------    
!...  Fluid
!---------------------------------------------------------------------------------------  
    if (msh%EL_TYP(iel) == 0) then    
         
!...  Loop over integration points
      do igauss = 1, NGAUSS
        shl = 0d0;  shgradg = 0d0; shhessg = 0d0; 
        
        call eval_shape(nshl, iel, gp(igauss,:), xl+dlold, wl,         &
                        shl, shgradgold, shhessg,  DetJold,  &
                        dxdxi,dxidx,Gij,Ginv,0, msh%iga)  
 
        
        call eval_shape(nshl, iel, gp(igauss,:), xl+dl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,NS_hess_flag, msh%iga)  

        detJgwt = DetJ*gw(igauss) 
       
!...  Interpolate     

        do i = 1, msh%NSD
          ui (i) = sum(ul (:,i)*shl)
          uiold (i) = sum(ulold (:,i)*shl)
          umi(i) = sum(uml(:,i)*shl)
          aci(i) = sum(acl(:,i)*shl)
          dpridxi(i) = sum(pl*shgradg(:,i))

          dphidxi(i)    = sum(phil*shgradg(:,i))
          dphiolddxi(i) = sum(philold *shgradg(:,i))
          dphiddxi(i)   = sum(phidl*shgradg(:,i)) 
          
          do j = 1, msh%NSD
            duidxi(i,j) = sum(ul(:,i)*shgradg(:,j))
            duiolddxi(i,j) = sum(ulold(:,i)*shgradg(:,j))
            ddidxi(i,j) = sum((dl(:,i)-dlold(:,i))*shgradgold(:,j))
            do k = 1, msh%NSD
              duidxixj(i,j,k) = sum (ul(:,i)*shhessg(:,j,k))         
            end do
          end do
        end do  
        
        pri  = sum(pl*shl)
        rho  = sum(rhol *shl)

        do i = 1, msh%NSD
           drhodxi(i) = sum(rhol(:)*shgradg(:,i))
        enddo

        mu   = sum(mul  *shl)    
        
        rho0 = sum(rho0l*shl)
        
        phi  = sum(phil *shl)         
        rphi = sum(rphil*shl)                
        
        phid = sum(phidl*shl)  

        phi1 = sum(phi1l*shl)  
        
        He   = sum(hel *shl)
        Hep  = sum(hepl*shl)
             
!...  Other variables             
        uadvi = ui - umi    
        rhoconv = 0d0

        Se = 2d0*(He-0.5d0) 
        ai(:) = dphiddxi(:)*Se/(sqrt(sum(dphiddxi*dphiddxi))+1d-15) 
        ai1(:)= dphidxi(:)*Se/(sqrt(sum(dphidxi*dphidxi))+1d-15) 
        
        if(flag(2).eq.1) then
          pse_time_loc=0
          pse_time_loc1=0
         do ii=1,3
           do jj=1,3
            pse_time_loc= pse_time_loc+ai(ii)*Gij(ii,jj)*ai(jj)
            pse_time_loc1=pse_time_loc1+ai1(ii)*Gij(ii,jj)*ai1(jj)
           enddo
          enddo
            pse_time_loc=1/sqrt(pse_time_loc)
            pse_time_loc1=1/sqrt(pse_time_loc1)
            pse_time=min(pse_time,pse_time_loc)
            pse_time1=min(pse_time1,pse_time_loc1)
            pse_time=2*pse_lamda*pse_time
            pse_time1=2*pse_lamda*pse_time1
       endif
              
     

!...  Strong residuals
        fi    = (rho-rho0)*gravvec         
        res_u = rho*aci + &
                rho*(duidxi(:,1)*uadvi(1) + &
                     duidxi(:,2)*uadvi(2) + &
                     duidxi(:,3)*uadvi(3)) + &
                dpridxi - fi - mu*(duidxixj(:,1,1)+duidxixj(:,2,2)+duidxixj(:,3,3))

!Calculate symmetric S and antisymmetric Omega components of the velocity gradient tensor:
          do i = 1, msh%NSD
            do j = 1, msh%NSD
              Omegai(i,j) = 0.5d0*(duidxi(i,j) - duidxi(j,i))
              Si(i,j) = 0.5d0*(duidxi(i,j) + duidxi(j,i))
            end do
          end do
!Calculate Q-criterion
          Qi = 0d0
          do i = 1, msh%NSD
            Qi = Qi + 0.5d0*(sum(Omegai(i,:)*Omegai(i,:)) - sum(Si(i,:)*Si(i,:)))
          end do
          Rhsq(:) = Rhsq(:) + shl(:)*Qi*DetJ*gw(igauss)
          Rhsl(:) = Rhsl(:) + shl(:)*DetJ*gw(igauss)

         
        res_c  = rphi + sum(uadvi*dphidxi)

        gradmag = sqrt(sum(dphiddxi*dphiddxi))

        res_rd = sum(ai*dphiddxi) - Se

!...  NavSto - Numerical parameters
       if(ido_flag.eq.1) then 
         call e3STAB_3D_ido(Gij, Ginv, uadvi, res_u, rho, mu,  &
                       tauM_ns, tauP_ns, tauC_ns, tauBar_ns,cfl_loc)
  
       else
         call e3STAB_3D(Gij, Ginv, uadvi, res_u, rho, mu,  &
                       tauM_ns, tauP_ns, tauC_ns, tauBar_ns,cfl_loc)
       endif

        cfl(1)  = max(cfl(1), cfl_loc(1))

        call e3DC_beta2(ui,duidxi,Gij,Ginv,res_u,dxidx,tauM_ns,kdc_ns)
       
        kdc_ns = (1d0-He)*NS_kdc_a*kdc_ns + He*NS_kdc_w*kdc_ns

!...  Convection - Numerical parameters 
        call e3STAB_TAU(uadvi, Gij, tau_c)
        call e3DC_CAU(dphidxi,uadvi,Gij,Ginv,res_c,tau_c,kdc_c)
                   
        kdc_c = LSC_kdc*kdc_c  
            
!...  Redistance - Numerical parameters         
        
        if(flag(3).eq.1) then !!!!!!!!!!!!!if of compute tau_rd and kdc_rd
          tau_rd = 0d0
          do i = 1, msh%NSD
           do j = 1, msh%NSD
             tau_rd = tau_rd + ai(i)*Gij(i,j)*ai(j)
           enddo
         enddo 
      !  tau_rd = 1d0/sqrt(4/(pse_time*pse_time)+tau_rd + 1d-15)
         tau_rd = 1d0/sqrt(4/(pse_time*pse_time)+tau_rd)
      !  tau_rd = 0d0
         kdc_rd = 0d0
!!$        do i = 1, msh%NSD
!!$          do j = 1, msh%NSD
!!$            kdc_rd = kdc_rd + dphiddxi(i)*Gij(i,j)*dphiddxi(j)
!!$          enddo
!!$        enddo 

!        kdc_rd = LSRD_kdc*abs(Se)*1d0/sqrt(Gij(1,1)+Gij(2,2)+Gij(3,3))
         tmp_val=0
         do ii=1,3
           do jj=1,3
              tmp_val=tmp_val+dphiddxi(ii)*Gij(ii,jj)*dphiddxi(jj)
           enddo
         enddo
             
    !   kdc_rd = LSRD_kdc*abs(Se)*sqrt(Ginv(1,1)+Ginv(2,2)+Ginv(3,3)) 
        kdc_rd = LSRD_kdc*abs(res_rd)/sqrt(tmp_val+1d-15) 
 
!!$        if (runcase==ship) then
!!$           kdc_rd = LSRD_kdc*abs(Se)*sqrt(Ginv(3,3)) 
!!$        else
!!$           kdc_rd = LSRD_kdc*abs(Se)*sqrt(Ginv(1,1)+Ginv(2,2)+Ginv(3,3)) 
!!$        endif
       
!...  Mesh motion - Numerical parameters  

       endif !!!!!!!!!!!!!end if of compute tau_rd and kdc_rd
        Emod = 1d0
           
!...  NavSto Residual 

     if(flag(1).eq.1) then 

        if(ido_flag.eq.1) then 
         call e3RHS_3D_fluid_ido(nshl, ui, aci, umi, acmi, uadvi,&
                             pri, res_u, fi, duidxi, ddidxi, &
                             tauM_ns, tauP_ns, tauC_ns, tauBar_ns, kdc_ns,&
                             rho, mu, DetJ, gw(igauss), shl, shgradg, &
                             eRns(1:3,:), eRns(4,:))
        else
         call e3RHS_3D_fluid(nshl, ui, aci, umi, acmi, uadvi,&
                             pri, res_u, fi, duidxi, ddidxi, &
                             tauM_ns, tauP_ns, tauC_ns, tauBar_ns, kdc_ns,&
                             rho, mu, DetJ, gw(igauss), shl, shgradg, &
                             eRns(1:3,:), eRns(4,:))
        endif

        do aa = 1, NSHL

           eRns(1:3,aa) = eRns(1:3,aa) - shl(aa)*rhoconv*ui(:)*detJgwt
           
        enddo

!...  Convection Residual	             
        do aa = 1, NSHL
          shconv(aa) = sum(shgradg(aa,:)*uadvi)
          eRlsc(aa) = eRlsc(aa) - &
                    ( (shl(aa) + tau_c*shconv(aa))*res_c &
                    + kdc_c*sum(shgradg(aa,:)*dphidxi(:)) )*detJgwt
        enddo
      endif  ! endif of flag(1).eq.1
     
!...  Redistance Residual
       
     if(flag(3).eq.1) then
        do aa = 1, NSHL
          eRlsrd(aa) = eRlsrd(aa) -  &
            ( shl(aa)*res_rd &
            + sum(ai(:)*shgradg(aa,:))*tau_rd*res_rd &
            + kdc_rd*sum(shgradg(aa,:)*dphiddxi(:)) &
            + shl(aa)*LSRD_penalty_fac*Hep*(phid-phi1) )*detJgwt
        enddo
      endif  ! endif of flag(3).eq.1
        
!...  Mesh Residual
     if(flag(2).eq.1) then ! endif of flag(2).eq.1
        call e3Rhs_3D_mesh(nshl, Emod, ddidxi, gw(igauss), shl, shgradgold, eRmesh)
     endif ! endif of flag(2).eq.1

        if (newJac >= 1) then
!...  NavSto Jacobian     
         
         if(flag(1).eq.1) then

          if(ido_flag.eq.1) then 
           call e3LHS_3D_fluid_ido(nshl, ui, umi, aci, pri, duidxi, dpridxi, &
                                   res_u, tauM_ns, tauP_ns, tauC_ns, tauBar_ns, &
                                   kdc_ns, rho, mu, DetJ,gw(igauss),&
                                   shl, shgradg, shhessg,eKns)
          else
           call e3LHS_3D_fluid(nshl, ui, umi, aci, pri, duidxi, dpridxi, &
                                   res_u, tauM_ns, tauP_ns, tauC_ns, tauBar_ns, &
                                   kdc_ns, rho, mu, DetJ,gw(igauss),&
                                   shl, shgradg, shhessg,eKns)
          endif

           do bb = 1, NSHL
              do aa = 1, NSHL
                 eKns(1,aa,bb) = eKns(1,aa,bb) + shl(aa)*rhoconv*shl(bb)*fact2*detJgwt
                 eKns(6,aa,bb) = eKns(6,aa,bb) + shl(aa)*rhoconv*shl(bb)*fact2*detJgwt
                 eKns(11,aa,bb) = eKns(11,aa,bb) + shl(aa)*rhoconv*shl(bb)*fact2*detJgwt
              enddo
           enddo

!...  Convection Jacobian    
           do bb = 1, NSHL
              do aa = 1, NSHL
                eKlsc(aa,bb) = eKlsc(aa,bb) + &
                    ( (shl(aa) + tau_c*shconv(aa))*(fact1*shl(bb) + fact2*shconv(bb)) &
                    + fact2*kdc_c*sum(shgradg(aa,:)*shgradg(bb,:)) )*detJgwt
             enddo
           enddo 
        
         endif !  if(flag(1).eq.1) then
          

!...  Redistance Jacobian
        if(flag(3).eq.1) then
         do aa = 1, NSHL
            shconv(aa) = sum(shgradg(aa,:)*ai(:))
         enddo
          do bb = 1, NSHL
            do aa = 1, NSHL
       !      eKlsrd(aa,bb) = eKlsrd(aa,bb) +     &
       !                     ( shl(aa)*shconv(bb) &
       !                     + shconv(aa)*tau_rd*shconv(bb) &
       !                     + kdc_rd*sum(shgradg(aa,:)*shgradg(bb,:)) &
       !                     + shl(aa)*LSRD_penalty_fac*Hep*shl(bb) )*fact2*detJgwt

             eKlsrd(aa,bb) = eKlsrd(aa,bb) +     &
                            ( shl(aa)*shl(bb)/pse_time & ! add for pse_time
                            + shl(aa)*shconv(bb) &
                            + shconv(aa)*tau_rd*shconv(bb) &
                            + shconv(aa)*tau_rd*shl(bb)/pse_time  & ! add for pse_time
                            + kdc_rd*sum(shgradg(aa,:)*shgradg(bb,:)) &
                            + shl(aa)*LSRD_penalty_fac*Hep*shl(bb) )*detJgwt



            enddo
          enddo 
        endif !if(flag(3).eq.1) then
          
!...  Mesh Residual    
       if(flag(2).eq.1) then        
          call e3Lhs_3D_mesh(nshl, Emod, gw(igauss), shgradgold, eKmesh)  
       endif
                          
        endif
        if (newJac >= 2) then     
                                       
!...  Convection Jacobian wrt u   
          do bb = 1, NSHL
             do aa = 1, NSHL
               eKlscu(:,aa,bb) = eKlscu(:,aa,bb) + &
                   ( shl(aa)*fact2*shl(bb)*dphidxi  + &
                     tau_c*shgradg(aa,:)*fact2*shl(bb)*res_c )*detJgwt
            enddo
          enddo  
          
!...  Redist Jacobian wrt phi 
            
          do bb = 1, NSHL
             do aa = 1, NSHL
               eKrdphi(aa,bb) = eKrdphi(aa,bb) - &
                   fact2*shl(aa)*LSRD_penalty_fac*Hep*shl(bb)*detJgwt  
            enddo
          enddo            
                 
        endif                      
      enddo  ! end  loop integration points 
          
 !... Enforce strong BCs         
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        if(flag(1).eq.1) then
         IBCns_loc  (aa,:) = IBCns  (bb,:)
         IBClsc_loc (aa,1) = IBClsc (bb)
        endif
        if(flag(2).eq.1) then
         IBCmesh_loc(aa,:) = IBCmesh(bb,:)  
        endif
      enddo

      if(flag(1).eq.1) then
       call applyBCs(nshl, msh%NSD+1, IBCns_loc  (1:nshl,:), eKns ,   eRns)                    
       call applyBCs(nshl,         1, IBClsc_loc (1:nshl,:), eKlsc,   eRlsc)  
      endif 
      if(flag(2).eq.1) then
       call applyBCs(nshl, msh%NSD,   IBCmesh_loc(1:nshl,:), eKmesh , eRmesh)
      endif
             
!... Assemble global residual           
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        if(flag(1).eq.1) then
         RHSG(bb,1:4) = RHSG(bb,1:4) + eRns  (:,aa)              
         RHSG(bb,5)   = RHSG(bb,5)   + eRlsc (aa)
         if(newJac >=1) then 
          rhsqu(bb)    = rhsqu(bb)    + rhsq(aa)
          rhsql(bb)    = rhsql(bb)    + rhsl(aa)
         endif
        endif
        if(flag(3).eq.1) then
         RHSG(bb,6)   = RHSG(bb,6)   + eRlsrd(aa) 
        endif
        if(flag(2).eq.1) then
          RHSG(bb,7:9) = RHSG(bb,7:9) + eRmesh(:,aa)   
        endif             
      enddo

!... Assemble global jacobian           
      if (newJac >= 1) then  
        if(flag(1).eq.1) then    
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl), msh%NSD+1, eKns,   sparseMat, LHSns  )     
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl),         1, eKlsc,  sparseMat, LHSlsc )
        endif
        if(flag(3).eq.1) then  
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl),         1, eKlsrd, sparseMat, LHSlsrd)   
        endif   
        if(flag(2).eq.1) then      
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl),   msh%NSD, eKmesh, sparseMat, LHSmesh)  
        endif 
      end if 
      
      if (newJac >= 2) then         
        do i = 1 , msh%NSD
          call applyColBCs(nshl, IBCns_loc(1:nshl,i), eKlscu(i,:,:))
          call applyRowBCs(nshl,IBClsc_loc(1:nshl,1), eKlscu(i,:,:))
          call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKlscu(i,:,:),   sparseMat, LHSlscu(i,:) ) 
        enddo 
                                             
        call applyColBCs(nshl,IBClsc_loc(1:nshl,1),eKrdphi) 
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKrdphi,   sparseMat, LHSrdphi) 
      end if         
        
                           
!---------------------------------------------------------------------------------------    
!...  Solid
!--------------------------------------------------------------------------------------- 
    else if (msh%EL_TYP(iel) == 1) then

!...  Loop over integration points
      do igauss = 1, NGAUSS       
        Ftens = 0.0d0; Stens = 0.0d0; Ctens = 0.0d0
      
        call eval_shape(nshl, iel, gp(igauss,:), xl, wl,         &
                        shl, shgradg, shhessg,  DetJ,  &
                        dxdxi,dxidx,Gij,Ginv,hess_flag, msh%iga)  

        do i = 1, msh%NSD
          di (i) = sum(dl(:,i)*shl)
          ui(i)  = sum(ul(:,i)*shl)
          aci(i) = sum(acl(:,i)*shl)          
          do j = 1, msh%NSD
            ddidxi(i,j) = sum(dl(:,i)*shgradg(:,j))
          end do
        end do
        fi = 0d0
     
        call e3tensors_nonlin(msh%NSD, ddidxi, mu, kappa, Ftens, &
                              Stens, Ctens)

        Stens = Stens !!+ FPKS(iel,:,:)

!...  Solid Residual        
        call e3Rhs_3D_struct(nshl, aci, ui, fi, Ftens, Stens, gw(igauss), &
                              shl, shgradg, eRns(1:3,:), damp)
       
!...  Solid Jacobian        
        if (newJac >= 1) then
          call e3LHS_3D_struct(nshl, gw(igauss), shl, shgradg, Ftens, &
                               Stens, Ctens, eKns, damp)
        end if
    
      enddo  ! end of integration point loop
       
!... Enforce strong BCs
      !call applyBCs(nshl,msh%IEN(iel,1:nshl), msh%NSD+1, IBC, eKns , eRns) 
                         
!... Assemble global residual

      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        RHSG(bb,1:3) = RHSG(bb,1:3) + eRns  (aa,1:3)  
      enddo
              
!... Assemble global jacobian           
      !if (newJac >= 1) then
      !  call FillSparseMat_3D(nshl, iel, eKns, eGns, eDns, eMns)             
      !end if     
    endif ! end Fluid/Solid if 
      
  enddo ! end of element loop 
  
!...  Deallocate
  if (NSHL >= 0) then
    deallocate(shl, shgradg,shgradgold, shhessg, gp, gw, &
               eRns,eRlsc,eRlsrd, eRmesh, &
               xl,wl,dl,dlold,uml,acml,acl,ul,pl,rhol,mul, &
               ulold,rho0l,phil,rphil,philold,phi1l,phidl, &
               hl, hel, hepl)
               
    !if (newJac >= 1) then          
       deallocate(eKns,eKlsc,eKlsrd, eKmesh,eKlscu,eKrdphi)
    !endif           
  endif

    if (numnodes > 1) then
       gcfl = cfl
       call MPI_ALLReduce(gcfl, cfl, 2, MPI_DOUBLE_PRECISION, &
                         MPI_MAX, MPI_COMM_WORLD,mpi_err)
    endif

    if ((ismaster).and.(flag(1).eq.1)) then
       !write(*,'(40("-"))')
       write(*,*) myid, "***********CFL = ", cfl(1),"****************"
    endif

  !*************************************************
  ! get the pse_time step
   if((numnodes>1).and.(flag(2).eq.1)) then
     pse_time_glob=pse_time
     call MPI_ALLReduce(pse_time_glob, pse_time, 2, MPI_DOUBLE_PRECISION, &
                         MPI_MIN, MPI_COMM_WORLD,mpi_err)
    endif

   if((numnodes>1).and.(flag(2).eq.1)) then
     pse_time_glob1=pse_time1
     call MPI_ALLReduce(pse_time_glob1, pse_time1, 2, MPI_DOUBLE_PRECISION, &
                         MPI_MIN, MPI_COMM_WORLD,mpi_err)
    endif

  !  if ((ismaster).and.(flag(2).eq.1)) then
  !     write(*,'(40("-"))')
  !     write(*,*) "    pse_time = ", pse_time
  !     write(*,*) "    pse_time1 = ", pse_time1
  !   endif
   ! ************************************************
    
end subroutine IntAssembly

!======================================================================
!
!======================================================================
subroutine FaceAssembly(st, msh,  rhsg, newJac,flag)
                        
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use boundaryConditions  
  use matrix  
  use aAdjKeep
  implicit none
  
  type(state),      intent(in) :: st
  type(meshStruct), intent(in) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,9)  
  integer, intent(in) :: newJac
  integer, intent(in) :: flag(4)

  ! Local variables  
  integer :: NSHL,NSHLB, NGAUSSB 
  integer :: b, ifac, iel, igauss, i, j, aa, bb, id, opt, faceor
  integer :: IBCns_loc(msh%maxNSHL,msh%NSD+1), IBClsc_loc(msh%maxNSHL,1)
               
  real(8), allocatable :: eKns(:,:,:),eKlsc(:,:),eKlscu (:,:,:) 
  real(8), allocatable :: eRns(:,:),eRlsc(:)
    
  real(8), allocatable :: shl(:), shgradg(:,:), shhessg(:,:,:)
  real(8), allocatable :: gp(:,:), gw(:) 
     
  real(8) :: Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD),&
             dxdxi(msh%NSD,msh%NSD),dxidx(msh%NSD,msh%NSD)
  real(8) :: DetJb, nor(msh%NSD)
  real(8) :: fact1,fact2,atime
    
  real(8), allocatable :: xl(:,:), wl(:), dl(:,:), uml(:,:), acml(:,:), &
                          ul(:,:), acl(:,:), pl(:), rhol(:),mul(:), gl(:,:), phil(:), philbc(:)

  real(8) :: xi(msh%NSD), umi(msh%NSD), ui(msh%NSD), pri, rho, mu, urbi(msh%NSD), phi , phibc
  real(8) :: duidxi(msh%NSD,msh%NSD)
               
  real(8) :: tauB, tauNor
  
  real(8) :: gi(msh%NSD), gneg !!, getBCval_NS,gneg,getBCval_LSC
  real(8) :: unor,uneg,upos,p0 
  real(8) :: ti(msh%NSD), tmp1(msh%NSD), tmp2(msh%NSD,msh%NSD)
  real(8) :: force_temp(3), force_temp_taub(3)
  real(8) :: torque_temp, torque_temp_taub
  real(8) :: force_temp1(3), force_temp_taub1(3)
  real(8) :: torque_temp1, torque_temp_taub1 
 logical :: findIndex

  real(8), allocatable :: Rhsu(:,:), Rhsp(:), Rhsm(:,:) 
  
  fact1 = almi
  fact2 = alfi*gami*Delt
  atime = time - (1d0-alfi)*Delt

  gi = 0.0d0

  NSHL = -1
  NSHLB = -1 
  NGAUSSB = -1

if (newJac >= 1) then
  force_trac = 0d0
  force_trac_taub = 0d0
  torque_trac = 0d0
  torque_trac_taub = 0d0

  force_temp = 0d0
  force_temp_taub = 0d0
  torque_temp = 0d0
  torque_temp_taub = 0d0


  force_trac1 = 0d0
  force_trac_taub1 = 0d0
  torque_trac1 = 0d0
  torque_trac_taub1 = 0d0

  force_temp1 = 0d0
  force_temp_taub1 = 0d0
  torque_temp1 = 0d0
  torque_temp_taub1 = 0d0
endif
  ! loop over faces 
  do b = 1, msh%NBOUND    
  
  !if (findIndex(BC_NBOUND, BC_FACEID,msh%bound(b)%FACE_ID, id)) then   
      id = msh%bound(b)%FACE_ID
      if ((maxval(NS_BC_Type(id,:)).ge.2).or. &
          (       LSC_BC_Type(id)  .eq.2)) then 
  !    write(*,*) "NS_BC_Type:",id,NS_BC_Type(id,:)
  !    write(*,*) "LSC_BC_Type:",id,LSC_BC_Type(id)

      do ifac = 1, msh%bound(b)%NFACE
        iel = msh%bound(b)%F2E(ifac)               
        faceor =  msh%bound(b)%FACE_OR(ifac)
        if ( (NSHL    /= msh%ELMNSHL(iel)).or. &
             (NSHLB   /= msh%bound(b)%NSHLB(ifac)).or. &
             (NGAUSSB /= msh%bound(b)%NGAUSSB(ifac)) ) then
             
          if (NSHL >= 0) then
            deallocate(shl, shgradg, shhessg,gp,gw, &
                       dl, ul, acl, uml, acml, pl, xl, wl,rhol, mul,phil, philbc,&
                       eKns,eKlsc, eRns, eRlsc, gl,eKlscu, Rhsu, Rhsp, Rhsm)
          endif

          NSHL    = msh%ELMNSHL(iel)
          NSHLB   = msh%bound(b)%NSHLB(ifac) 
          NGAUSSB = msh%bound(b)%NGAUSSB(ifac)
               
          allocate(shl(NSHL), shgradg(NSHL,msh%NSD), shhessg(NSHL,msh%NSD,msh%NSD), & 
                   gp(NGAUSSB,msh%NSD-1),gw(NGAUSSB), &
                   dl(NSHL,msh%NSD), ul(NSHL,msh%NSD), acl(NSHL,msh%NSD), uml(NSHL,msh%NSD), &
                   acml(NSHL,msh%NSD), pl(NSHL), xl(NSHL,msh%NSD), &
                   wl(NSHL), rhol(NSHL),mul(NSHL),phil(NSHL),philbc(NSHL),&
                   eKns((msh%NSD+1)*(msh%NSD+1),NSHL,NSHL) , eKlsc(NSHL,NSHL), eKlscu(msh%NSD,NSHL,NSHL), &   
                   eRns(msh%NSD+1,NSHL), eRlsc(NSHL),gl(NSHL,msh%NSD), &
                   Rhsu(msh%NSD,NSHL), Rhsp(NSHL), Rhsm(msh%NSD,NSHL))
          call genGPandGWb(gp,gw,NGAUSSB,NSHLB)  
        endif
           
        ! Get local solution arrays   	       	     
        do i = 1, NSHL
          j = msh%IEN(iel,i)
          xl   (i,:) = msh%xg (j,:)      
          wl   (i)   = msh%wg (j)
          dl   (i,:) = st%dg  (j,:)
          ul   (i,:) = st%ug  (j,:)
          acl  (i,:) = st%acg (j,:)
          uml  (i,:) = st%ugm (j,:)
          acml (i,:) = st%acgm(j,:)
          pl   (i)   = st%pg  (j)
          rhol (i)   = old%rhog(j)
          mul (i)    = old%mug (j)
          phil(i)    = st%phig(j) 
          
          gl (i,:)   = ugbc(j,:)
          philbc (i) = phigbc(j)
        end do

        Rhsu = 0.0d0    
        Rhsp = 0.0d0   
        Rhsm = 0.0d0
          
        ! initialize local resistance matrix	
        if (newJac >= 1) then
          eKns = 0d0         
          eKlsc = 0d0
        endif
        if (newJac >= 2) then       
          eKlscu = 0d0
        endif
                
        ! initialize local load vector
        eRns = 0d0      
        eRlsc = 0d0

        ! Loop over integration points (NGAUSSb in each direction) 
        do igauss = 1, NGAUSSb

          call eval_faceshape(nshl, iel, gp(igauss,:), &
                              faceor,xl+dl, wl, &
                              shl, shgradg,Detjb, dxdxi,dxidx, &
                              Gij, Ginv, nor, msh%iga)
          ! Interpolate	    
          pri = sum(pl*shl)          
          phi = sum(phil*shl)
          phibc = sum(philbc*shl) 
          
          do i = 1, msh%NSD
            xi (i)  = sum((xl (:,i) + dl(:,i))*shl)
            ui (i)  = sum(ul (:,i)*shl)
            umi(i)  = sum(uml(:,i)*shl)
            gi(i)   = sum(gl(:,i)*shl)
          end do 
          
          unor = sum((ui-umi)*nor)  
          upos = 5d-1*(unor+abs(unor))
          uneg = 5d-1*(unor-abs(unor))  
	  
          do j = 1, msh%NSD
            do i = 1, msh%NSD
              duidxi(i,j) = sum(ul(:,i)*shgradg(:,j))
            end do
          end do

          rho = sum(rhol*shl)
          mu  = sum(mul *shl)  

          ! Boundary condition selection
          if (NS_BC_Type(id,1) == 2) then
            opt = 1
          else if (NS_BC_Type(id,1) == 3) then
            opt = 2
!!$          else if (NS_BC_Type(id,1) == 4) then
!!$            if (sum((gi-umi)*nor).le.0d0) then 
!!$              opt = 1
!!$            else  
!!$              opt = 2
!!$            endif       
          else
            opt = -1  
          endif 
                        
          ! Actual RHS LHS terms    
          if (opt == 1) then ! Weak

            call e3bSTAB_weak(tauB,tauNor,rho,mu,ui-umi,nor,Gij)
	                                 
            call wbcStrss(tmp1, tmp2, rho, mu, nor, tauB, tauNor, &
                             Rhsu, Rhsp, nshl, shl, shgradg, DetJb, gw(igauss), ti, &
                             pri, ui, umi, duidxi, gi)

          if (newJac >= 1) then
            if((msh%bound(b)%FACE_ID == 21).and.(xi(1)<rotor_tower)) then 
              do aa = 1, NSHL
               force_temp(:) = force_temp(:) + shl(aa)*ti(:)*DetJb*gw(igauss)
               force_temp_taub(:) = force_temp_taub(:) + shl(aa)*(ti(:)+tauB*(ui-gi))*DetJb*gw(igauss)
               enddo 

              do aa = 1, NSHL
               torque_temp = torque_temp + shl(aa)*(xi(2)*ti(3) &
                                                 -  xi(3)*ti(2))*DetJb*gw(igauss)
               torque_temp_taub = torque_temp_taub + shl(aa)*(xi(2)*(ti(3)+tauB*(ui(3)-gi(3))) &
                                                           -  xi(3)*(ti(2)+tauB*(ui(2)-gi(2))))*DetJb*gw(igauss)
               enddo 

            endif 
           endif


            if((msh%bound(b)%FACE_ID == 21).and.(xi(1)>rotor_tower+2.2).and.(xi(1)<rotor_tower+clearance)) then
              do aa = 1, NSHL
               force_temp1(:) = force_temp1(:) + shl(aa)*ti(:)*DetJb*gw(igauss)
               force_temp_taub1(:) = force_temp_taub1(:) +shl(aa)*(ti(:)+tauB*(ui-gi))*DetJb*gw(igauss)
               enddo

              do aa = 1, NSHL
               torque_temp1 = torque_temp1 + shl(aa)*(xi(2)*ti(3) &
                                                  - xi(3)*ti(2))*DetJb*gw(igauss)
               torque_temp_taub1 = torque_temp_taub1 + shl(aa)*(xi(2)*(ti(3)+tauB*(ui(3)-gi(3))) &
                                                   -xi(3)*(ti(2)+tauB*(ui(2)-gi(2))))*DetJb*gw(igauss)
               enddo

            endif
                          
            ! gnor = 0.0d0
            do aa = 1, NSHL
              do i = 1, msh%NSD
                eRns(i,aa) = eRns(i,aa) - &
                 ( shl(aa)*tmp1(i) + &
                   sum(shgradg(aa,:)*tmp2(i,:)) )*DetJb*gw(igauss)
              end do

              eRns(msh%NSD+1,aa) = eRns(msh%NSD+1,aa) + &
                               shl(aa)*sum((ui-gi)*nor)*DetJb*gw(igauss)     
            end do       
            

                            
            if (newJac >= 1) then
              call e3bLHS_weak(nshl, ui, umi, duidxi, tauB, tauNor, &
                               rho, mu, DetJb, gw(igauss), shl, shgradg, eKns, nor)
            endif                               
          else  if (opt==2) then  ! Do Nothing/Outflow

            do aa = 1, NSHL         
               eRns(1:msh%NSD,aa) = eRns(1:msh%NSD,aa) + &
                    shl(aa)*uneg*rho*ui(1:msh%NSD)*DetJb*gw(igauss)
            enddo
                      
            if (newJac >= 1) then
              do bb = 1, NSHL        
                do aa = 1, NSHL
                 eKns(1,aa,bb) = eKns(1,aa,bb) -    &
                   fact2*shl(aa)*uneg*rho*shl(bb)*DetJb*gw(igauss)
                 eKns(6,aa,bb) = eKns(6,aa,bb) -    &
                   fact2*shl(aa)*uneg*rho*shl(bb)*DetJb*gw(igauss)
                 eKns(11,aa,bb) = eKns(11,aa,bb) -    &
                   fact2*shl(aa)*uneg*rho*shl(bb)*DetJb*gw(igauss)
                enddo
              enddo  
            endif

             if (xi(3).gt.water_level) then ! Air
                p0 = 0d0*(1d0-useRho0)*rhoa*9.81d0*(water_level-xi(3))
             else ! Water 
                p0 = (1d0-useRho0)*rhow*9.81d0*(water_level-xi(3))
             endif

             do aa = 1, NSHL         
                eRns(1:msh%NSD,aa) = eRns(1:msh%NSD,aa) - &
                     shl(aa)*p0*nor(1:msh%NSD)*DetJb*gw(igauss)
             enddo

          endif
                           
!...  Calculate residual
          if (LSC_BC_Type(id) == 2) then
            gneg = phi - phibc   
            do aa = 1, NSHL
              eRlsc(aa) = eRlsc(aa) + uneg*gneg*shl(aa)*DetJb*gw(igauss)
            enddo
            
!...  Calculate Jacobian
            if (newJac >= 1) then
              do bb = 1, NSHL
                do aa = 1, NSHL
                  eKlsc(aa,bb) = eKlsc(aa,bb) - fact2*uneg*shl(aa)*shl(bb)*DetJb*gw(igauss) 
                enddo
              enddo           
            endif
            
!...  Calculate Off-diag Jacobian
            if ((newJac >= 2).and.(uneg < 0d0)) then            
               do bb = 1, NSHL
                 do aa = 1, NSHL
                   eKlscu(:,aa,bb) = eKlscu(:,aa,bb) - fact2*nor*shl(bb)*gneg*shl(aa)*DetJb*gw(igauss)
                 enddo
               enddo                        
            endif            
            
          endif                                
      enddo
      
 !... Enforce strong BCs         
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        IBCns_loc  (aa,:) = IBCns  (bb,:)
        IBClsc_loc (aa,1) = IBClsc (bb) 
      enddo
      
       
      call applyBCs(nshl, msh%NSD+1, IBCns_loc(1:nshl,:),  eKns,  eRns)        
      call applyBCs(nshl, 1 ,       IBClsc_loc(1:nshl,:), eKlsc, eRlsc)

!... Assemble global residual           
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)   
        RHSG(bb,1:4) = RHSG(bb,1:4) + eRns  (:,aa)              
        RHSG(bb,5)   = RHSG(bb,5)   + eRlsc (aa)              
      enddo
      
!... Assemble global jacobian           
      if (newJac >= 1) then           
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl), msh%NSD+1, eKns,   sparseMat, LHSns )            
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl),         1, eKlsc,  sparseMat, LHSlsc )         
      end if      
      !if (newJac >= 2) then         
      !  do i = 1 , msh%NSD
      !    call applyColBCs(nshl, IBCns_loc(1:nshl,i), eKlscu(i,:,:))
      !    call applyRowBCs(nshl,IBClsc_loc(1:nshl,1), eKlscu(i,:,:))
      !    call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKlscu(i,:,:),   sparseMat, LHSlscu ) 
      !  enddo 
      !end if   
      
    enddo 
    endif
  enddo

if (newJac >= 1) then
  force_trac = 0d0
  force_trac_taub = 0d0
  call MPI_ALLREDUCE(force_temp, force_trac, 3, MPI_DOUBLE_PRECISION, &
                     MPI_SUM, MPI_COMM_WORLD, mpi_err)
  call MPI_ALLREDUCE(force_temp_taub, force_trac_taub, 3, MPI_DOUBLE_PRECISION, &
                     MPI_SUM, MPI_COMM_WORLD, mpi_err)

  torque_trac = 0d0
  torque_trac_taub = 0d0
  call MPI_ALLREDUCE(torque_temp, torque_trac, 1, MPI_DOUBLE_PRECISION, &
                     MPI_SUM, MPI_COMM_WORLD, mpi_err)
  call MPI_ALLREDUCE(torque_temp_taub, torque_trac_taub, 1, MPI_DOUBLE_PRECISION, &
                     MPI_SUM, MPI_COMM_WORLD, mpi_err)

  force_trac1 = 0d0
  force_trac_taub1 = 0d0
  call MPI_ALLREDUCE(force_temp1, force_trac1, 3, MPI_DOUBLE_PRECISION, &
                     MPI_SUM, MPI_COMM_WORLD, mpi_err)
  call MPI_ALLREDUCE(force_temp_taub1, force_trac_taub1, 3, MPI_DOUBLE_PRECISION,&
                     MPI_SUM, MPI_COMM_WORLD, mpi_err)

  torque_trac1 = 0d0
  torque_trac_taub1 = 0d0
  call MPI_ALLREDUCE(torque_temp1, torque_trac1, 1, MPI_DOUBLE_PRECISION, &
                     MPI_SUM, MPI_COMM_WORLD, mpi_err)
  call MPI_ALLREDUCE(torque_temp_taub1, torque_trac_taub1, 1,MPI_DOUBLE_PRECISION, &
                     MPI_SUM, MPI_COMM_WORLD, mpi_err)

  

  if (ismaster) then
    write(*,*) "    force-turbine1 =", force_trac, force_trac_taub
    write(*,*) "    force-turbine2 =", force_trac1, force_trac_taub1

    write(*,*) "    torque-turbine1 =", torque_trac, torque_trac_taub
    write(*,*) "    torque-turbine2 =", torque_trac1,torque_trac_taub1
  end if
 endif
  if (NSHL >= 0) then
    deallocate(shl, shgradg, shhessg,gp,gw)
    deallocate(dl, ul, acl, uml, acml, pl,xl, wl,rhol, mul,phil,philbc)
    deallocate(eKns,eKlsc, eRns, eRlsc, gl,eKlscu)
  endif
100 format (6ES17.8)
101 format (2ES17.8)
end subroutine FaceAssembly
!======================================================================
!
!======================================================================
  subroutine IntAssembly_massfix(st0,st0o5, st1, msh,  rhsg, newJac)
                        
    use params 
    use mpi
    use meshData
    use solution
    use matrix  
    use matvec
    use aAdjKeep
    implicit none
  
    type(state),      intent(in) :: st0,st0o5,st1
    type(meshStruct), intent(in) :: msh
    real(8), intent(inout) :: rhsg(msh%NNODE)  
    integer, intent(in) :: newJac

!...  Local variables
    integer :: nshl, iel, igauss, hess_flag,i,aa,bb,idx, NGAUSS
        
    real(8), allocatable :: shl(:),shgradg(:,:), &
                            shhessg(:,:,:)
    real(8), allocatable :: dl0(:,:), wl(:),ul(:,:), uml(:,:), &
                            dl1(:,:), xl(:,:), phicl(:), &
                            rhol(:),  rholold(:), drhol(:), hl(:)
    real(8), allocatable :: xMebe(:,:), rhs(:)                          
                        
    real(8)  DetJ0, DetJ1, rho0,rho1,drho1,drho, &
             ui(msh%NSD), umi(msh%NSD), uadvi(msh%NSD), dphicdx(msh%NSD), kdc, tau
    real(8), allocatable :: gp(:,:), gw(:)

    real(8) ltmp(2),He, Hep,rho,tauLSMC,h 
    real(8) dxdxi(msh%NSD,msh%NSD),dxidx(msh%NSD,msh%NSD),&
            Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD), DetJ

    real(8) u_ref, rho_ref 

!... Initialize
    NGAUSS = -1
    NSHL   = -1 
                  
!...  loop over elements
      do iel = 1, msh%NELEM
  
          if (NSHL /= msh%ELMNSHL(iel)) then
		 
            if (NSHL >= 0) then
              deallocate(shl,shgradg,shhessg,&
                 dl0,rholold, wl, drhol, ul, uml, phicl,hl, &
                 dl1,rhol, xl,gp, gw ,xMebe,rhs)
            endif
     
            NSHL   = msh%ELMNSHL(iel)
            NGAUSS = msh%ELMNGAUSS(iel)
            
            allocate(shl(NSHL),shgradg(NSHL,msh%NSD), &
                 shhessg(NSHL,msh%NSD,msh%NSD),xMebe(NSHL,NSHL),rhs(NSHL),&
                 dl0(NSHL,msh%NSD),rholold(NSHL), wl(NSHL),drhol(NSHL), &
                 dl1(NSHL,msh%NSD),rhol(NSHL), xl(NSHL,msh%NSD),hl(NSHL),&
                 gp(NGAUSS,msh%NSD), gw(NGAUSS), &
                 ul(NSHL,msh%NSD), uml(NSHL,msh%NSD), phicl(NSHL))
		 
            call genGPandGW(gp, gw, NGAUSS, NSHL)
          endif
	  
          rhs   = 0d0
          if (newJac >= 1) xMebe = 0d0
          do i = 1,NSHL
             idx = msh%IEN(iel,i)
             xl(i,:)    = msh%xg       (idx,:)
             wl(i)      = msh%wg       (idx)
             dl0(i,:)   = st0%dg       (idx,:)
             dl1(i,:)   = st1%dg       (idx,:)             
             rholold(i) = st0%rhog     (idx)
             rhol(i)    = st1%rhog     (idx)
             
             drhol(i)   = st1%drhog    (idx)
             ul(i,:)    = st0o5%ug     (idx,:) 
             uml(i,:)   = st0o5%ugm    (idx,:) 
             phicl(i)   = st0o5%phicg  (idx)   
             hl(i)      = st1%hg       (idx)
          enddo
	    	    
!...  Loop over integration points (NGAUSS in each direction)
          do igauss = 1, NGAUSS
       
!...  Get Element Shape functions and their gradients
                    
             shl     = 0d0   ! initialize
             shgradg = 0d0
             shhessg = 0d0             	   
             hess_flag = 0   ! Not

             call eval_shape(nshl, iel, gp(igauss,:), xl+dl0, wl,    &
                             shl, shgradg, shhessg,  DetJ0,       &
                             dxdxi,dxidx,Gij,Ginv,hess_flag, msh%iga) 
		          
             rho0 = sum(rholold*shl)
    
!...  Get Element Shape functions and their gradients
             shl     = 0d0   ! initialize
             shgradg = 0d0
             shhessg = 0d0                   
             hess_flag = 0   ! Not
    
             call eval_shape(nshl, iel, gp(igauss,:), xl+dl1, wl,    &
                             shl, shgradg, shhessg,  DetJ1,       &
                             dxdxi,dxidx,Gij,Ginv,hess_flag, msh%iga) 
	            
             rho1  = sum( rhol*shl)
             drho1 = sum(drhol*shl)
             
 !... Get Element Shape functions and their gradients            
             shl     = 0d0   ! initialize
             shgradg = 0d0
             shhessg = 0d0                  
             hess_flag = 0   
    
             call eval_shape(nshl, iel, gp(igauss,:), xl+0.5d0*(dl1+dl0), wl,    &
                             shl, shgradg, shhessg,  DetJ,       &
                             dxdxi,dxidx,Gij,Ginv,hess_flag, msh%iga) 
                             
             rho  = 0.5d0*(rho1+rho0)
                             
             h = sum(hl*shl)                                         
             do i = 1, msh%NSD                
               ui(i)      = sum(ul(:,i)*shl)
               umi(i)     = sum(uml(:,i)*shl)
               dphicdx(i) = sum(phicl*shgradg(:,i))
             enddo 
             uadvi = ui - umi
            
!...  Get kdc            
             call getHeps2(He, Hep, 0d0, h) 
             call e3STAB_TAU(uadvi, Gij, tauLSMC)
            
             !kdc = LSMC_kdc*(rhow-rhoa)*Hep*max(sum(uadvi*uadvi),1d-5)*tauLSMC !0.5d0*Delt   !max(sqrt(sum(uadvi*uadvi)), 1d0)*h
             !kdc = LSMC_kdc*(rhow-rhoa)*Hep*h  !!*tauLSMC !0.5d0*Delt   !max(sqrt(sum(uadvi*uadvi)), 1d0)*h     
              
             ! IDO old kdc = LSMC_kdc*(rhow-rhoa)*Hep* (Ginv(1,1)+Ginv(2,2)+Ginv(3,3))/tauLSMC
             
!kdc = LSMC_kdc*(rhow-rhoa)*Hep*(sum(uadvi**2) +  0d0*(Dtgl**2)*(Ginv(1,1)+Ginv(2,2)+Ginv(3,3)) *tauLSMC
                    
            u_ref =  sqrt(sum(uadvi**2) + (Dtgl**2)*(Ginv(1,1)+Ginv(2,2)+Ginv(3,3))) 
            rho_ref = rhow-rhoa !rho
            kdc = LSMC_kdc



           ! Works :perfect:
            kdc = LSMC_kdc*(rhow-rhoa)*Hep*max(sum(uadvi*uadvi)*tauLSMC,Dtgl*(Ginv(1,1)+Ginv(2,2)+Ginv(3,3)))
             
             
!...  Calculate residual & Jacobian
             do aa = 1, NSHL 
               rhs(aa) = rhs(aa)   & 
                       - shl(aa)*(rho1*DetJ1-rho0*DetJ0)*Dtgl*gw(igauss)  &
                       + sum(uadvi*shgradg(aa,:))*rho*DetJ*gw(igauss)     &
                       - kdc*sum(shgradg(aa,:)*dphicdx)*DetJ*gw(igauss)
             enddo
!...  Calculate Jacobian             
             if (newJac >= 1) then
               !write(*,*) drho1, kdc
 	       do  aa = 1, NSHL  
                 do  bb = 1, NSHL 
                   xMebe(aa,bb) = xMebe(aa,bb)   & 
                            + shl(aa)*drho1*shl(bb)*DetJ1*Dtgl*gw(igauss)           &
                            - sum(uadvi*shgradg(aa,:))*drho1*shl(bb)*0.5d0*DetJ*gw(igauss)  &
                            + kdc*sum(shgradg(aa,:)*shgradg(bb,:))*0.5d0*DetJ*gw(igauss)
                 enddo                                        
               enddo                
             endif    
             
!...  Calculate off-diagonal Jacobians    
            
             if (newJac >= 2) then             
!...  Mass Conservation Jacobian wrt u   
          !do bb = 1, NSHL
          !   do aa = 1, NSHL
          !     eKmcu(:,aa,bb) = eKmcu(:,aa,bb)  &
          !         - 0.5d0*shl(bb)*shgradg(aa,:)*rho*DetJ*gw(igauss)*DetJ*gw(igauss)
          !  enddo
          !enddo 
          
!...  Mass Conservation Jacobian wrt phid   
          !do bb = 1, NSHL
          !   do aa = 1, NSHL
          !     eKmcrd(aa,bb) = eKmcrd(aa,bb) + &
          !                   - shl(aa)*drho1*shl(bb)*DetJ1*Dtgl*gw(igauss)  &
          !                   + sum(uadvi*shgradg(aa,:))*0.5d0*drho1*shl(bb)*DetJ*gw(igauss)     &                    
          !  enddo
          !enddo           
            endif                    
             
                                          
          enddo                          
          if (newJac >= 1) xMebe = Delt*xMebe
          
!...  Assemble into the Sparse Global Stiffness Matrix and Rhs Vector               
          do aa = 1, NSHL  
            bb = msh%IEN(iel,aa)   
            RHSG(bb) = RHSG(bb) + rhs(aa)                
          enddo          
          if (newJac >= 1) then
            call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, xMebe,sparseMat, LHSlsmc)    
	  endif
      enddo
		 
      if (NSHL >= 0) then
        deallocate(shl,shgradg,shhessg,&
           dl0,rholold, wl, ul, uml, phicl, &
           dl1,rhol, xl, drhol, hl, &
           gp, gw, xMebe, rhs)
      endif

         
  end subroutine IntAssembly_massfix

  !=============================================================================
  !     Compute Tangent contribution
  !=============================================================================
  subroutine FaceAssembly_massfix(st, msh,  rhsg, newJac)

      use params 
      use mpi
      use meshData
      use solution
      use matrix  
      use matvec
      use aAdjKeep
      implicit none
      
      type(state),      intent(in) :: st
      type(meshStruct), intent(in) :: msh
      real(8), intent(inout) :: rhsg(msh%NNODE)  
      integer, intent(in) :: newJac
	     
      integer :: NSHL,NSHLB, NGAUSSB       
      integer b,id,ifac,idx,iel,igauss, i,aa,bb
      	  
      real(8), allocatable :: rhol(:),drhol(:),  wl(:), &
              ul(:,:), uml(:,:), xl(:,:), dl(:,:)
      real(8), allocatable :: gp(:,:), gw(:)            
      real(8), allocatable :: shl(:), shgradg(:,:)
      real(8), allocatable :: xMebe(:,:), rhs(:)
            
      real(8) :: nor(msh%NSD)
	  
      real(8) ui(msh%NSD),umi(msh%NSD),unor,phi
           
      real(8) h,He,Hep, rho, drho
     
      real(8) ::dxdxi(msh%NSD,msh%NSD),dxidx(msh%NSD,msh%NSD), &
                Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD), DetJb
      real(8) :: fact1, fact2 
      logical :: findIndex
           
      fact1 = almi
      fact2 = alfi*gami*Delt 
                                      
!... Initialize
      NSHL   = -1 
      NSHLB = -1 
      NGAUSSB = -1 
      	      
!...  Loop over Faces  
      do b = 1, msh%NBOUND      
      if (findIndex(BC_NBOUND, BC_FACEID, msh%bound(b)%FACE_ID, id)) then
        do ifac = 1,msh%bound(b)%NFACE

           iel =msh% bound(b)%F2E(ifac)
	      
           if ( (NSHL    /= msh%ELMNSHL(iel)).or. &
                (NSHLB   /= msh%bound(b)%NSHLB(ifac)).or. &
                (NGAUSSB /= msh%bound(b)%NGAUSSB(ifac)) ) then 
             if (NSHL >= 0) then
               deallocate(rhol,drhol, wl, ul, uml, xl, dl, rhs, xMebe,shl,shgradg,gp,gw)
             endif  
             NSHL    = msh%ELMNSHL(iel)
             NSHLB   = msh%bound(b)%NSHLB(ifac) 
             NGAUSSB = msh%bound(b)%NGAUSSB(ifac)
             
             allocate(xl(NSHL,msh%NSD), dl(NSHL,msh%NSD),wl(NSHL),gp(NGAUSSB,msh%NSD-1),gw(NGAUSSB), &
             ul(NSHL,msh%NSD),uml(NSHL,msh%NSD), rhol(NSHL),drhol(NSHL), rhs(NSHL), xMebe(NSHL,NSHL))
             allocate(shl(NSHL), shgradg(NSHL,msh%NSD))
             call genGPandGWb(gp,gw,NGAUSSB,NSHLB) 
           endif
           
           do i = 1, NSHL
              idx = msh%IEN(iel,i)              
              xl(i,:)  = msh%xg  (idx,:)       
              wl(i)    = msh%wg  (idx)
              dl(i,:)  = st%dg  (idx,:)
              rhol(i)  = st%rhog(idx)  
              ul(i,:)  = st%ug  (idx,:)
              uml(i,:) = st%ugm (idx,:)
              drhol(i) = st%drhog(idx)
           enddo
	 
           rhs   = 0d0
           if (newJac >= 1) xMebe = 0d0 
	              
!... Loop over integration points            
           do igauss = 1, NGAUSSb
                                  
             call eval_faceshape(nshl, iel,gp(igauss,:), &
                                 msh%bound(b)%FACE_OR(ifac), xl+dl,wl, &
                                 shl,shgradg,DetJb, &
                                 dxdxi,dxidx,Gij,Ginv,nor, msh%iga)
     
!... Interpolate	     
             rho  = sum(rhol*shl)
	     drho = sum(drhol*shl)       
             do i = 1, msh%NSD 
               ui(i)  = sum(ul(:,i) *shl(:))
               umi(i) = sum(uml(:,i)*shl(:))
             enddo
         
             unor = sum((ui-umi)*nor)
             
!... Residual
	     do aa = 1, NSHL 
               rhs(aa) = rhs(aa) - shl(aa)*unor*rho*DetJb*gw(igauss)  
             enddo
!... Jacobian
             if (newJac >= 1) then
	       do aa = 1, NSHL     
                 do bb = 1, NSHL 
                   xMebe(aa,bb) = xMebe(aa,bb) + shl(aa)*unor*drho*shl(bb)*DetJb*gw(igauss)  
                 enddo
               enddo
             endif 
             
!... Off-diag Jacobian             
             if (newJac >= 2) then 
                         
!...  Mass Conservation Jacobian wrt u   
               !do bb = 1, NSHL
               !   do aa = 1, NSHL
               !     eKmcu(:,aa,bb) = eKmcu(:,aa,bb) + 0.5d0*shl(aa)*nor*shl(bb)*rho*DetJb*gw(igauss)  
               !  enddo
               !enddo 
          
!...  Mass Conservation Jacobian wrt phid   
               !do bb = 1, NSHL
               !   do aa = 1, NSHL
               !     eKmcrd(aa,bb) = eKmcrd(aa,bb) + shl(aa)*unor*0.5d0*drho*shl(bb)*DetJb*gw(igauss)                
               !  enddo
               !enddo           
             endif                           	       
           enddo
           if (newJac >= 1) xMebe = Delt*xMebe
           
           do aa = 1, NSHL  
             bb = msh%IEN(iel,aa)   
             RHSG(bb)   = RHSG(bb)   + rhs(aa)                
           enddo          
           if (newJac >= 1) then
             call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, xMebe,sparseMat, LHSlsmc)    
	   endif      
        enddo 
      endif       
      enddo
        
      if (NSHL >= 0) then    
        deallocate(rhol,drhol, wl, ul, uml, xl, dl, rhs, xMebe,shl, shgradg, gp,gw)
      endif     
      
  end subroutine FaceAssembly_massfix
  
!=============================================================================
!     Compute Tangent contribution
!=============================================================================
subroutine ForceAssembly(st, msh, rhsg, newJac)
     
   use params 
   use mpi
   use elasticBody
   use meshData
   use solution 
   use matrix  
   use matvec
   use boundaryConditions
   use aAdjKeep
   implicit none
    
   type(state),      intent(in) :: st
   type(meshStruct), intent(in) :: msh
   real(8), intent(inout) :: rhsg(msh%NNODE)  
   integer, intent(in)    :: newJac
     
   integer :: NSHL,NSHLB,NGAUSSB
   integer, parameter :: NSD = 3
   logical :: findIndex
     
   ! Local variables
   integer :: b,bnd,id,ifac, iel,for,igauss, i, j, k, aa, bb 
  
   real(8), allocatable  :: gp(:,:), gw(:)
   real(8) :: gwt,fact1,fact2

   real(8) :: DetJb,dxidx(NSD,NSD),dxdxi(NSD,NSD),Gij(NSD,NSD), Ginv(NSD,NSD)
  
   real(8), allocatable :: shl(:), shgradg(:,:),shhessg(:,:,:)
     
   real(8), allocatable :: dl(:,:), ul(:,:), acl(:,:), uml(:,:), &
                           acml(:,:), pl(:), dlold(:,:), gl(:,:),  xl(:,:), &
                           rhol(:),rho0l(:),mul(:), plold(:), wl(:), phil(:)
          
   real(8) :: ui(NSD), pri, duidxi(NSD,NSD), tmp1(NSD),tmp2(NSD,NSD), &
              ti(NSD),phi
   real(8) :: nor(NSD),umi(NSD),unor, upos, uneg, gmul
   real(8) :: rho, mu
   real(8) :: tauB, tauNor, gi(NSD),h,He0,rho0, xm(NSD)    
   real(8) :: di(NSD),xi(NSD),db(NSD)

   real(8) :: eFd(NSD), Fd(NSD)

    real(8), allocatable :: Rhsu(:,:), Rhsp(:), Rhsm(:,:)  ! Jinhui: added to collect force
   
   integer cpu
       
   fact1 = almi
   fact2 = alfi*gami*Delt
   NSHL    = -1
   NSHLB   = -1 
   NGAUSSB = -1
   Fd   = 0d0

  ! db = 0.5d0*(bst0%db + bst1%db)
  ! loop over faces
  ! write(*,*) myid, "AAAAAAAAAAAAA",msh%NBOUND,msh%bound(7)%FACE_ID,msh%bound(7)%NNODE 
  ! do b = 1, msh%NBOUND
  !   write(*,*) myid,b, "BBBBBBBBB"
  ! enddo 
   do b = 1, msh%NBOUND
   if (msh%bound(b)%FACE_ID.eq.21) then   
    ! Jinhui: Replace Ido's stuff   
    ! write(*,*) b, msh%bound(b)%FACE_ID,msh%bound(b)%NFACE 
      do ifac = 1,msh%bound(b)%NFACE

         iel = msh%bound(b)%F2E(ifac)
         for = msh%bound(b)%FACE_OR(ifac)
         if ( (NSHL    /= msh%ELMNSHL(iel)).or. &
              (NSHLB   /= msh%bound(b)%NSHLB(ifac)).or. &
              (NGAUSSB /= msh%bound(b)%NGAUSSB(ifac)) ) then
            if (NSHL >= 0) then
               deallocate(shl, shgradg, shhessg, gp, gw, Rhsu, Rhsp, Rhsm)
               deallocate(dl, ul, acl, uml, acml, pl, dlold, gl, xl, &
                          plold, wl, rhol, rho0l, mul)
            endif

            NSHL    = msh%ELMNSHL(iel)
            NSHLB   = msh%bound(b)%NSHLB(ifac) 
            NGAUSSB = msh%bound(b)%NGAUSSB(ifac) 
           	
            allocate(shl(NSHL), shgradg(NSHL,NSD),shhessg(NSHL,NSD,NSD),gp(NGAUSSB,NSD-1),gw(NGAUSSB), &
             Rhsu(NSD,NSHL), Rhsp(NSHL), Rhsm(NSD,NSHL))
            allocate(dl(NSHL,NSD), ul(NSHL,NSD), acl(NSHL,NSD), uml(NSHL,NSD), &
                     acml(NSHL,NSD), pl(NSHL), dlold(NSHL,NSD),gl(NSHL,NSD), xl(NSHL,NSD), &
                     plold(NSHL),wl(NSHL), rhol(NSHL),rho0l(NSHL),mul(NSHL),phil(NSHL))                 
            call genGPandGWb(gp,gw,NGAUSSB,NSHLB) 

         endif 
            
         ! Get local solution arrays   

         ! write(*,*) myid, "CCCCCCCCC",msh%bound(b)%FACE_ID	   
            do i = 1, NSHL
               j = msh%IEN(iel,i)
               xl(i,:)    = msh%xg(j,:)               
               wl(i)      = msh%wg(j)
               dlold(i,:) = old%dg(j,:)               
               plold(i)   = old%pg(j)
               
               dl(i,:)    = st%dg(j,:)
               ul(i,:)    = st%ug(j,:)
               acl(i,:)   = st%acg(j,:)
               uml(i,:)   = st%ugm(j,:)
               acml(i,:)  = st%acgm(j,:)
               pl(i)      = st%pg(j)
               rhol(i)    = st%rhog(j)
               rho0l(i)   = st%rho0g(j)               
               mul(i)     = st%mug(j)   
               phil(i)    = st%phig(j)
              ! gl(i,:) = ugbc(j,:)          ! Jinhui: attention
               gl(i,:)   = st%ugm(j,:)                            
             enddo  ! do i = 1, NSHL

           Rhsu = 0.0d0    
           Rhsp = 0.0d0   
           Rhsm = 0.0d0
           eFd= 0d0
            ! Loop over integration points
            do igauss = 1, NGAUSSb
               gwt = gw(igauss)

               call eval_faceshape(nshl, iel, gp(igauss,:),for, &
                                   xl+dl, wl,&
                                   shl, shgradg, DetJb,&
                                   dxdxi,dxidx, Gij, Ginv, nor, msh%iga) 
	       ! Interpolate	    
               pri = sum(pl*shl)   
  	       rho = sum(rhol*shl)   
               rho0 = sum(rho0l*shl)
               mu  = sum(mul*shl)
               phi = sum(phil*shl)
  	    !   write(*,*) myid, "eeeeeeeee",msh%bound(b)%FACE_ID
               do i = 1, NSD
                  ui(i)  = sum(ul(:,i)*shl)
                  umi(i) = sum(uml(:,i)*shl)  
                  xi(i)  = sum(xl(:,i)*shl) 
                  di(i)  = sum(dl(:,i)*shl)  
                  gi(i)  = sum(gl(:,i)*shl)     
               enddo     

               do j = 1, NSD
                  do i = 1, NSD
                     duidxi(i,j) = sum(ul(:,i)*shgradg(:,j))
                  enddo
               enddo
               
                                 
               ! Derived stuff
               call e3bSTAB_weak(tauB, tauNor, rho,mu, ui-umi, nor, Gij)
                                          
               call wbcStrss(tmp1, tmp2, rho, mu, nor, tauB, tauNor, &
                             Rhsu, Rhsp, nshl, shl, shgradg, DetJb, gw(igauss),ti, &
                             pri, ui, umi, duidxi, gi)


               eFd = eFd + tmp1*DetJb*gwt     
            
               do aa = 1, NSHL
               do i = 1, NSD
               !! Rhsm(i,aa) = Rhsm(i,aa) - shlu(aa)*pri*nor(i)*DetJb*gw(igauss)
                  Rhsm(i,aa) = Rhsm(i,aa) + shl(aa)*ti(i)*DetJb*gw(igauss)
               enddo    
               enddo   
      !                 write(*,*) myid, "aaaaaa",msh%bound(b)%FACE_ID                                                    
            enddo      
       !            write(*,*) myid, "bbbbb",msh%bound(b)%FACE_ID
            do bb = 1, NSHL
        !           write(*,*) myid, bb,"cccc",msh%bound(b)%FACE_ID
              RHSGu(msh%IEN(iel,bb),:) = RHSGu(msh%IEN(iel,bb),:) - Rhsu(:,bb)    
              RHSGm(msh%IEN(iel,bb),:) = RHSGm(msh%IEN(iel,bb),:) - Rhsm(:,bb)  
            enddo ! Jinhui

            Fd = Fd + eFd
      enddo
    !  write(*,*) myid, "fffffffffff",msh%bound(b)%FACE_ID  
     endif
    
   enddo   
   
  !  if((myid.eq.0).or.(myid.eq.1)) write(*,*) myid,"HAHAHAHAHA"
   if (NSHL.ge.0) then
      deallocate(shl, shgradg, shhessg, gp,gw)
      deallocate(dl, ul, acl, uml, acml, pl, dlold, gl, xl, &
                 plold, wl,rhol,rho0l, mul)
   end if

!   if (numnodes > 1) then
!      call commu(RHSGu, NSD, 'in ')
!      call commu(RHSGm, NSD, 'in ')
!   endif


    eFd = Fd
    call MPI_REDUCE(eFd, Fd, NSD, &
                    MPI_DOUBLE_PRECISION,MPI_SUM,  &
                    mpi_master,MPI_COMM_WORLD,mpi_err)  

  ! if(ismaster) then
  !   write(*,*) "All the force: ",Fd
  ! endif

end subroutine ForceAssembly

!======================================================================
!
!======================================================================
subroutine IntPostPro(istep)

  use mpi
  use params 
  use elasticBody
  use meshData
  use solution 
  use globdata
  use matrix  
  use matvec
  use aAdjKeep  
  implicit none
 
  integer,          intent(in) :: istep
  
  integer, parameter:: NSD = 3
  ! 
  real(8) :: Mass0, Mom0(NSD), Ekin0, Epot0, Impls(NSD) 
  real(8) :: Mass1, Mom1(NSD), Ekin1, Epot1, Etot0, Etot1
  real(8) :: maxu, maxp, maxpc, minpc,gcfl1, gcfl2, cfl1, cfl2, nrm,nrm1,nrm2
  real(8) :: minnrm1,minnrm2,maxnrm1,maxnrm2 ! For redistancing
  real(8) :: minnrm11,minnrm21,maxnrm11,maxnrm21 ! For checking
  real(8) :: minnrm12,minnrm22,maxnrm12,maxnrm22 ! final level set
  real(8) :: loc(15), glob(15)
  real(8),  save :: Mass_init = -1d0
  real(8),  save :: Etot_init = -1d0
  integer , save :: dfile = -1
  character(len=30) :: fname, cname
     
  ! Local variables  
  integer :: iel, igauss, i, j, k, hess_flag, idx, nshl, NGAUSS

  real(8), allocatable :: shl(:), shgradg(:,:), shhessg(:,:,:)

  real(8), allocatable :: xl(:,:), wl(:), dl(:,:), dlold(:,:),&
                          ul(:,:), advl(:,:),ulold(:,:),phidl(:), phidl1(:),phidl2(:),rhol(:), rholold(:)

  real(8), allocatable :: gp(:,:), gw(:)

  real(8) :: ui(NSD), advi(3), xi(NSD), dphiddx(NSD),dphiddx1(NSD),dphiddx2(NSD), rho

  real(8) :: Gij(NSD,NSD), Ginv(NSD,NSD),dxidx(NSD,NSD), dxdxi(NSD,NSD), DetJ
  real(8) :: quality,minq,maxq, mindetJ(mesh_fluid%NNODE), maxDetJ(mesh_fluid%NNODE), gmindetJ, gmaxDetJ
  
  integer :: cpu 

!... Initialize
  minq    = 1d10
  maxq    = 0d0 
  minDetJ = 1d10
  maxDetJ = 0d0

  maxnrm1 = 0d0
  minnrm1 = 1d10
  maxnrm2 = 0d0
  minnrm2 = 1d10

  maxnrm11 = 0d0
  minnrm11 = 1d10
  maxnrm21 = 0d0
  minnrm21 = 1d10

  maxnrm12 = 0d0
  minnrm12 = 1d10
  maxnrm22 = 0d0
  minnrm22 = 1d10

  gcfl1   = 0d0
  gcfl2   = 0d0
  Mass0   = 0d0
  Mom0    = 0d0
  Ekin0   = 0d0
  Epot0   = 0d0
  Impls   = 0d0
  Mass1   = 0d0
  Mom1    = 0d0
  Ekin1   = 0d0
  Epot1   = 0d0
  Etot0   = 0d0
  Etot1   = 0d0

!...  loop over elements
  NGAUSS = -1
  NSHL   = -1    
     
  do iel = 1, mesh_fluid%NELEM 

    if (NSHL /= mesh_fluid%ELMNSHL(iel)) then
      
      if (NSHL >= 0) then
        deallocate(shl, shgradg, shhessg, gp, gw, &
                   xl, wl, dl, dlold, ul,advl, ulold,phidl,phidl1,phidl2,rhol,rholold)
      end  if
     
      NSHL   = mesh_fluid%ELMNSHL(iel)
      NGAUSS = mesh_fluid%ELMNGAUSS(iel)

      allocate(shl(NSHL), shgradg(NSHL,NSD),shhessg(NSHL,NSD,NSD), &
               xl(NSHL,NSD),wl(NSHL), dl(NSHL,NSD), dlold(NSHL,NSD),&
               ul(NSHL,NSD),advl(NSHL,NSD), ulold(NSHL,NSD),phidl(NSHL), phidl1(NSHL),phidl2(NSHL),rhol(NSHL),rholold(NSHL),&
               gp(NGAUSS,NSD), gw(NGAUSS))

      ! get Gaussian points and weights
      call genGPandGW(gp, gw, NGAUSS, NSHL)
    end if

    ! Get local solution arrays 
    do i = 1, NSHL 
    
      idx = mesh_fluid%IEN(iel,i)
      xl   (i,:) = mesh_fluid%xg      (idx,:)      
      wl   (i)   = mesh_fluid%wg      (idx)     
      dl   (i,:) = new%dg       (idx,:)
      ul   (i,:) = new%ug       (idx,:)
      advl (i,:) = new%ugm      (idx,:) 
      rhol (i)   = new%rhog     (idx)
      phidl(i)   = new%phidg    (idx)   ! RD
      phidl1(i)   = new%phig_check   (idx) ! level set before RD
      phidl2(i)   = new%phig    (idx)  ! level set updated
      dlold   (i,:) = old%dg    (idx,:)
      ulold   (i,:) = old%ug    (idx,:)
      rholold (i)   = old%rhog  (idx)      
    end do
    
    advl = ul - advl
    
    ! Loop over integration points (NGAUSS in each direction)
    do igauss = 1, NGAUSS
 
!...  Evaluate shape functions 
      call eval_shape(nshl, iel, gp(igauss,:), xl+dlold, wl,         &
                      shl, shgradg, shhessg,  DetJ,  &
                      dxdxi,dxidx,Gij,Ginv,0, mesh_fluid%iga)  

!...  Interpolate      
      do i = 1, NSD
        ui (i) = sum(ulold (:,i)*shl)
        xi (i) = sum((xl(:,i)+dlold (:,i))*shl)
      end do 
        
      rho = sum(rholold*shl)
    
!.... Integrate      
      mass0 = mass0 +       rho               *DetJ*gw(igauss)      
      Mom0  = Mom0  +       rho*ui            *DetJ*gw(igauss)                       
      Ekin0 = Ekin0 + 0.5d0*rho*sum(ui*ui)    *DetJ*gw(igauss)
      Epot0 = Epot0 +       rho*gravity*xi(3) *DetJ*gw(igauss)      
      Impls = Impls + 0.5d0*Delt*rho*gravVec  *DetJ*gw(igauss)
    
!...  Evaluate shape functions 
      call eval_shape(nshl, iel, gp(igauss,:), xl+dl, wl,         &
                      shl, shgradg, shhessg,  DetJ,  &
                      dxdxi,dxidx,Gij,Ginv,0, mesh_fluid%iga)  
    
!...  Mesh quality
      quality = 0.25d0*(Gij(1,1) + Gij(2,2) + Gij(3,3))*(Ginv(1,1) + Ginv(2,2) + Ginv(3,3))
      minq = min(quality, minq)
      maxq = max(quality, maxq)     
      
      do i = 1, NSHL    
        idx = mesh_fluid%IEN(iel,i)
        minDetJ(idx) = min(DetJ, minDetJ(idx))
        maxDetJ(idx) = max(DetJ, maxDetJ(idx))       
      enddo
      
!...  Interpolate      
      do i = 1, NSD
        ui  (i) = sum(ul  (:,i)*shl)
        advi(i) = sum(advl(:,i)*shl)
        xi  (i) = sum((xl (:,i)+dl(:,i))*shl)  
        dphiddx1(i)  = sum(phidl1*shgradg(:,i))   ! level set before RD        
        dphiddx(i)  = sum(phidl*shgradg(:,i))     ! rd
        dphiddx2(i)  = sum(phidl2*shgradg(:,i))   ! level set updated
      end do 
        
      rho = sum(rhol*shl)

!... Maximum gradient norm   
      nrm =  sqrt(sum(dphiddx*dphiddx)) ! rd
      nrm1 = sqrt(sum(dphiddx1*dphiddx1)) ! ls
      nrm2 = sqrt(sum(dphiddx2*dphiddx2)) ! ls updated
      if (rho.gt.rhot) then
       maxnrm1 = max(nrm, maxnrm1) ! rd
       minnrm1 = min(nrm, minnrm1) ! rd

       maxnrm11 = max(nrm1, maxnrm11)
       minnrm11 = min(nrm1, minnrm11) ! before rd

       maxnrm12 = max(nrm2, maxnrm12)
       minnrm12 = min(nrm2, minnrm12) ! after uod


      else
        maxnrm2 = max(nrm, maxnrm2)
        minnrm2 = min(nrm, minnrm2)

        maxnrm21 = max(nrm1, maxnrm21)
        minnrm21 = min(nrm1, minnrm21)

        maxnrm22 = max(nrm2, maxnrm22)
        minnrm22 = min(nrm2, minnrm22)
      endif
      
!... Maximum CFL
      cfl1 = 0d0
      cfl2 = 0d0         
      do j = 1, NSD
        do i = 1, NSD
          cfl1 = cfl1 + advi(i)*Gij (i,j)*advi(j)
          cfl2 = cfl2 + advi(i)*Ginv(i,j)*advi(j)
        end do
      end do
      
      cfl1 = Delt*sqrt(cfl1)
      cfl2 = Delt*sum(advi*advi)/sqrt(cfl2+1d-12)      

      gcfl1 = max(gcfl1, cfl1)
      gcfl2 = max(gcfl2, cfl2)
                               
!.... Integrate      
      mass1 = mass1 +       rho               *DetJ*gw(igauss)      
      Mom1  = Mom1  +       rho*ui            *DetJ*gw(igauss)                       
      Ekin1 = Ekin1 + 0.5d0*rho*sum(ui*ui)    *DetJ*gw(igauss)
      Epot1 = Epot1 +       rho*gravity*xi(3) *DetJ*gw(igauss)      
      Impls = Impls + 0.5d0*Delt*rho*gravVec  *DetJ*gw(igauss)

                           
    end do    
  end do

  deallocate(shl, shgradg, shhessg, gp, gw, &
             xl, wl, dl, dlold, ul,advl,ulold,phidl, rhol,rholold)

    
  maxu  = 0d0  
  maxp  = 0d0 
  maxpc = 0d0
  minpc = 0d0 
  do i = 1, mesh_fluid%NNODE
    maxu  = max(maxu, sum(new%ug(i,:)**2))
    maxp  = max(maxp, new%pg(i)**2)    
    maxpc = max(maxpc,new%phicg(i))   
    minpc = min(minpc,new%phicg(i))
  enddo 
   
    
  gminDetJ = 1d10 
  gmaxDetJ = 0d0  
  do i = 1, mesh_fluid%NNODE    
    DetJ = maxDetJ(i)/minDetJ(i)
    gminDetJ = min(DetJ, gminDetJ)
    gmaxDetJ = max(DetJ, gmaxDetJ)       
  enddo  
  
!  do cpu = 1, numnodes
!    if(cpu == myid+1) then        
!       write(*,"(a,I3,a,2F12.6)")   "   Mesh (",myid,") Volume Ratio = ", gminDetJ, gmaxDetJ    
!    endif
!    call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)
!  enddo

                        
  ! Communicate 

    ! Global maximum
    loc(1)=maxnrm11
    loc(2)=maxnrm21 !ls
    loc(3)=maxnrm12
    loc(4)=maxnrm22 !ls updated  
    call MPI_Reduce(loc, glob, 4, MPI_DOUBLE_PRECISION, &
                    MPI_MAX,mpi_master, MPI_COMM_WORLD,mpi_err) 
    if (ismaster) then
     maxnrm11=glob(1)
     maxnrm21=glob(2)
     maxnrm12=glob(3)
     maxnrm22=glob(4)     
    endif  
    ! Global minimum                                                                  
    loc(1)=minnrm11
    loc(2)=minnrm21
    loc(3)=minnrm12
    loc(4)=minnrm22        
    call MPI_Reduce(loc, glob, 4, MPI_DOUBLE_PRECISION, &
                    MPI_Min,mpi_master, MPI_COMM_WORLD,mpi_err) 
    if (ismaster) then                    
     minnrm11=glob(1)
     minnrm21=glob(2)
     minnrm12=glob(3)
     minnrm22=glob(4) 
    endif                          
    ! Global maximum
    loc(1) = maxu
    loc(2) = maxp                 
    loc(3) = maxpc                   
    loc(4) = gcfl1                             
    loc(5) = gcfl2 
    loc(6) = maxnrm1
    loc(7) = maxnrm2
    loc(8) = maxq    
    loc(9) = maxval(maxDetJ)      
    call MPI_Reduce(loc, glob, 9, MPI_DOUBLE_PRECISION, &
                    MPI_MAX,mpi_master, MPI_COMM_WORLD,mpi_err) 
  
    if (ismaster) then
       maxu    = glob(1)
       maxp    = glob(2)     
       maxpc   = glob(3)         
       gcfl1   = glob(4)                           
       gcfl2   = glob(5)
       maxnrm1 = glob(6)
       maxnrm2 = glob(7)
       maxq    = glob(8)
       gmaxDetJ = glob(9)       
    endif  
    ! Global minimum                                                                  
    loc(1) = minpc 
    loc(2) = minnrm1
    loc(3) = minnrm2   
    loc(4) = minq     
    loc(5) = minval(minDetJ)       
    call MPI_Reduce(loc, glob, 5, MPI_DOUBLE_PRECISION, &
                    MPI_Min,mpi_master, MPI_COMM_WORLD,mpi_err) 

    if (ismaster) then                    
      minpc   = glob(1)                     
      minnrm1 = glob(2)     
      minnrm2 = glob(3) 
      minq    = glob(4) 
      gminDetJ = glob(5)
    endif    
                       
    ! Global sum                      
    loc(1)   = mass0      
    loc(2:4) = Mom0                      
    loc(5)   = Ekin0
    loc(6)   = Epot0 
       
    loc(7)     = mass1    
    loc(8:10)  = Mom1                      
    loc(11)    = Ekin1
    loc(12)    = Epot1    
    loc(13:15) = Impls
    
    call MPI_Reduce(loc, glob, 15, MPI_DOUBLE_PRECISION, &
                    MPI_SUM,mpi_master, MPI_COMM_WORLD,mpi_err)       

    if (ismaster) then        
      mass0 = glob(1)     
      Mom0  = glob(2:4)                      
      Ekin0 = glob(5)   
      Epot0 = glob(6)   
    
      mass1 = glob(7)      
      Mom1  = glob(8:10)                       
      Ekin1 = glob(11)   
      Epot1 = glob(12)      
      Impls = glob(13:15)  
    endif                        
 
  if (ismaster) then
    Etot0 = Ekin0 + Epot0
    Etot1 = Ekin1 + Epot1

    if (dfile.eq.-1) then
      Mass_init = mass0
      Etot_init = Etot0
    endif
         
    ! Print global info
    write(*,'(60("-"))')
    write(*,"(a,2F12.6)")   "   Mesh  Quality  = ", sqrt(minq), sqrt(maxq)
    write(*,"(a,2F12.6)")   "   Elem  Length   = ", gminDetJ**0.3333d0, gmaxDetJ**0.3333d0   
    write(*,"(a,2ES12.4)")  "   Elem  Volume   = ", gminDetJ, gmaxDetJ
    write(*,"(a,2F12.6)")   "   CFL            = ", gcfl1, gcfl2
    
    write(*,'(15("  .."))') 
    write(*,"(a, F14.5)")   "   Max. velocity  = ", sqrt(maxu)
    write(*,"(a, F14.5)")   "   Max. pressure  = ", sqrt(maxp) 
    write(*,'(15("  .."))')    
    
    write(*,'(F12.5,a,F12.5)') minnrm11," ls :<||grad phi||<",maxnrm11
    write(*,'(F12.5,a,F12.5)') minnrm21," ls :<||grad phi||<",maxnrm21
 
    write(*,'(F12.5,a,F12.5)') minnrm1," rd: <||grad phi||<",maxnrm1
    write(*,'(F12.5,a,F12.5)') minnrm2," rd: <||grad phi||<",maxnrm2
         
    write(*,'(F12.5,a,F12.5)') minnrm12," uls :<||grad phi||<",maxnrm12
    write(*,'(F12.5,a,F12.5)') minnrm22," uls :<||grad phi||<",maxnrm22

    write(*,'(15(" .."))')   
    write(*,"(a,2ES12.4)") "   phi correction = ", minpc,   maxpc      
    write(*,"(a,3ES12.4)") "   Mass n+1, n, 0 = ", mass1, mass0, Mass_init 
    write(*,"(a,2ES12.4)") "   Mass loss (%)  = ", 1d2*(mass1-mass0)/mass0,&
                                                   1d2*(mass1-Mass_init)/Mass_init 
         
    write(*,'(15("  .."))')        
    write(*,"(a,2ES12.4)") "   E kin n+1, n   = ", Ekin1, Ekin0 
    write(*,"(a,2ES12.4)") "   E pot n+1, n   = ", Epot1, Epot0 
    write(*,"(a,3ES12.4)") "   E tot n+1, n,0 = ", Etot1, Etot0, Etot_init 
    write(*,"(a,2ES12.4)") "   E tot loss (%) = ", 1d2*(Etot1-Etot0)/Etot0,&
                                                   1d2*(Etot1-Etot_init)/Etot_init

    if (dfile.eq.-1) then
      dfile = 214
      if (istep.eq.1) then
        open(dfile, file='data.dat', status='replace')
      else
        open(dfile, file='data.dat', status='old', position='append')
      endif
      
    endif
    
    !write(dfile,"(21ES16.7)") time,minnrm11,maxnrm11,minnrm21,maxnrm21,minnrm1,maxnrm1,minnrm2,maxnrm2,minnrm12,maxnrm12,minnrm22,maxnrm22
       
  endif 
   
end subroutine IntPostpro
