!======================================================================
!
!======================================================================
subroutine compRhoMu(phi,dphidxi,Gij,Ginv)
     
  use params
  use commonpars
  implicit none
  
  integer, parameter :: NSD = 3

  real(8) Gij(NSD,NSD), Ginv(NSD,NSD)
  real(8) phi,dphidxi(NSD)
  real(8) h, He  
  
  call getElemSize(h,dphidxi,Gij,Ginv)
  call getHeps(He,phi,h)

  write(*,*) "Obsolete : compRhoMu"
  stop  

end subroutine compRhoMu
 
!======================================================================
!
!======================================================================    
subroutine compRhoMu2(phi,dphidxi,Gij,Ginv)
     
  use params
  use commonpars
  implicit none
  
  integer, parameter :: NSD = 3

  real(8) Gij(NSD,NSD),Ginv(NSD,NSD)
  real(8) phi,dphidxi(NSD)
  real(8) h, He, Hep  

  call getElemSize(h,dphidxi,Gij,Ginv)
  call getHeps2(He, Hep, phi, h) 
  
  write(*,*) "Obsolete : compRhoMu2"
  stop  

	    
end subroutine compRhoMu2

      
!======================================================================
!
!======================================================================
subroutine getElemSize(h,dphidxi,Gij,Ginv)
     
  use params
  use commonpars
  implicit none
  
  integer, parameter :: NSD = 3
  
  real(8) Gij(NSD,NSD),Ginv(NSD,NSD)
  real(8) h, dphidxi(NSD), h2
  real(8) temp1, tmp3(NSD)

  integer i,j
  
!!$  ! Isotropic h as fallback option
!!$  temp1 = 0d0 
!!$
!!$  do i = 1, NSD
!!$    do j = 1, NSD
!!$      temp1 = temp1 + Ginv(i,j)*Ginv(i,j)
!!$    enddo
!!$  enddo
!!$ 
!!$  h2 = sqrt(temp1)
!!$ 
!!$  ! h in the direction of the gradient	   	    
!!$  temp1 = 0d0 
!!$
!!$  do i = 1, NSD
!!$    do j = 1, NSD
!!$      temp1 = temp1 + dphidxi(i)*Ginv(i,j)*dphidxi(j)
!!$    enddo
!!$  enddo
!!$
!!$  h = sqrt( (temp1 + 1d-10*h2)/(sum(dphidxi*dphidxi) + 1d-10))
!!$  !!h = sqrt(h2)
 
  tmp3(1) = dphidxi(1)/(sqrt(sum(dphidxi(:)*dphidxi(:)) + 1d-15))
  tmp3(2) = dphidxi(2)/(sqrt(sum(dphidxi(:)*dphidxi(:)) + 1d-15))
  tmp3(3) = dphidxi(3)/(sqrt(sum(dphidxi(:)*dphidxi(:)) + 1d-15)) 

  temp1 = 0d0

  do i = 1, NSD
    do j = 1, NSD
      temp1 = temp1 + tmp3(i)*Gij(i,j)*tmp3(j)
    enddo
  enddo
  
  ! h based on \grad \phi

!  h = 2d0/(sqrt(temp1) + 1d-15)
  
  ! Pick minumum between \grad \phi and isotropic

!  h = min(h,sqrt(Ginv(1,1)+Ginv(2,2)+Ginv(3,3)))

! YB

!!$  if (runcase==ship) then
!!$    h = 2d0*sqrt(Ginv(3,3)) ! h from vertical mesh size
!!$  else
!!$    h = 2d0*sqrt(Ginv(1,1)+Ginv(2,2)+Ginv(3,3))
!!$  endif

  h = 2d0*sqrt(Ginv(1,1)+Ginv(2,2)+Ginv(3,3))

end subroutine 

!======================================================================
!
!======================================================================
subroutine getEps(eps, h) 
  use params
  use commonpars
  implicit none
  
  real(8)   h, eps

  eps = mp_eps*h
  
end subroutine
  
!======================================================================
!
!======================================================================
subroutine getHeps(He, phi, h) 

  use params
  use commonpars
  implicit none
  
  real(8) He,phi,h, eps
  
  call getEps(eps, h)
      
  if (phi.lt.-eps) then
    He  = 0d0
  else if (phi.gt.eps) then
    He  = 1d0
  else
    He  = 5d-1*(1d0 + (phi/eps) + (sin(pi*phi/eps)/pi))
  endif   
  
end subroutine getHeps

!======================================================================
!
!======================================================================
subroutine getHeps2(He, Hep, phi, h) 

  use params
  use commonpars
  implicit none
  
  real(8) He,Hep,phi,h, eps

  call getEps(eps, h)
      
  if (phi.lt.-eps) then
    He  = 0d0
    Hep = 0d0
  else if (phi.gt.eps) then
    He  = 1d0  
    Hep = 0d0   
  else
    He  = 5d-1*(1d0 + phi/eps + sin(pi*phi/eps)/pi)
    Hep = 5d-1*(1d0/eps + cos(pi*phi/eps)/eps)
  endif

end subroutine getHeps2

!======================================================================
!
!======================================================================
subroutine getHpH0(HpH0,phi,h) 

  use params
  use commonpars
  implicit none
  
  real(8) HpH0,phi,h, eps
  
  call getEps(eps, h)
      
  if (phi.lt.-eps) then
    HpH0 = 0d0
  else if (phi.gt.eps) then  
    HpH0 = 0d0   
  else
    HpH0 = 5d-1*(1d0 + cos(pi*phi/eps))
  endif
    
end subroutine getHpH0

!##########################################################################
!
!##########################################################################
subroutine ComputeNodalRhoMu(st, msh) 
          
    use params
    use mpi
    use meshData
    use solution
    use commonpars
    implicit none
    
    type(state),      intent(inout) :: st  
    type(meshstruct), intent(in)    :: msh	  
    
!...  Local variables
    real(8) ::rhsg(msh%NNODE), dlhsg(msh%NNODE)

    integer  :: nshl, iel, igauss,hess_flag, i, j, k,idx, aa,bb, NGAUSS
      
    real(8), allocatable :: shl(:), shgradg(:,:), shhessg(:,:,:)
    real(8), allocatable :: phil(:), phil1(:),dl(:,:), xl(:,:), wl(:), hl(:)
    real(8), allocatable :: lhs(:),  rhs(:), hepl(:)
      
    real(8), allocatable :: gp(:,:), gw(:) 
             
    real(8) :: dphidxi(msh%NSD),dphidxi1(msh%NSD),h, He, Hep,vint, vglob, loc      
      
    real(8) :: dxdxi(msh%NSD,msh%NSD),  dxidx(msh%NSD,msh%NSD),  &
               Gij(msh%NSD,msh%NSD), Ginv(msh%NSD,msh%NSD), DetJ     
        
!...  Initialize
    NGAUSS = -1
    NSHL   = -1
    rhsg   = 0d0
    dlhsg  = 0d0
 !   write(*,*) myid, "Compute the nodal value:" 
!...  Loop over elements  
    do iel = 1,msh% NELEM
 
       if (NSHL /= msh%ELMNSHL(iel)) then     
		 
         if (NSHL >= 0) then
            deallocate(shl, shgradg, shhessg,&   
                  phil,phil1,dl, xl ,wl,&
                  lhs,rhs,&
                  gp, gw)
         endif
         NSHL   = msh%ELMNSHL(iel)
         NGAUSS = msh%ELMNGAUSS(iel)
         allocate(shl(NSHL), shgradg(NSHL,msh%NSD),shhessg(NSHL,msh%NSD,msh%NSD),&   
                  phil (NSHL),phil1(NSHL),dl(NSHL,msh%NSD), xl(NSHL,msh%NSD),wl(NSHL),&
                  lhs(NSHL),rhs(NSHL),&
                  gp(NGAUSS,msh%NSD), gw(NGAUSS))

         call genGPandGW(gp,gw,NGAUSS, NSHL)
          
       endif
 
       lhs = 0d0
       rhs = 0d0
	
!...  Get local solution vectors	      
       do i = 1, NSHL
          idx = msh%IEN(iel,i)
          xl(i,:) = msh%xg(idx,:)
          wl(i)   = msh%wg(idx)
          dl(i,:) = st%dg(idx,:)          
         !phil(i) = st%phidg(idx)+st%phicg(idx) 
          phil(i) = st%phig(idx)  !!!!!!!!!! modified for the quasi-coupled fashion
          phil1(i) = st%phig(idx)  !!!!!!!!!! modified for the quasi-coupled fashion
       enddo
	
!...  Loop over integration points 
       do igauss = 1, NGAUSS
!...  Get Element Shape functions and their gradients
          shl     = 0d0   
          shgradg = 0d0
          shhessg = 0d0
          hess_flag = 0
         
          call eval_shape(nshl, iel, gp(igauss,:), xl+dl, wl,         &
                          shl, shgradg, shhessg,  DetJ,  &
                          dxdxi,dxidx,Gij,Ginv,hess_flag, msh%iga)
!... Interpolate	                    
          do i = 1, msh%NSD   
             dphidxi(i) = sum(phil*shgradg(:,i))
             dphidxi1(i) = sum(phil1*shgradg(:,i))
          enddo  
	        		
!... Compute h
          call getElemSize(h,dphidxi,Gij,Ginv)
 
!... Calculate residual	             
          do aa = 1, NSHL
             rhs(aa) = rhs(aa) + shl(aa)*h*DetJ*gw(igauss)
             lhs(aa) = lhs(aa) + shl(aa)*  DetJ*gw(igauss)
          enddo
                	      
       enddo
        
!...  Assemble into the Sparse Global Stiffness Matrix and Rhs Vector
       do aa = 1, NSHL  
          bb = msh%IEN(iel,aa)   
          RHSG (bb) =  RHSG(bb) + rhs  (aa)  
          dlhsg(bb) = dlhsg(bb) + lhs  (aa) 
        enddo
    enddo
    
!...  Deallocate
    if (NSHL > 0) then       
      deallocate(shl, shgradg, shhessg)
      deallocate(phil, dl, xl, wl)
      deallocate(lhs, rhs, gp, gw)
    endif
!... Communicate
   
    call commu(rhsg,  1,'in ')    
    call commu(dlhsg, 1,'in ')
            
    call MPI_BARRIER (MPI_COMM_WORLD,mpi_err)

    call commu(rhsg,  1,'out')
    call commu(dlhsg, 1,'out')

          
!...  Compute nodal element size, density and viscosity
    do i = 1, msh%NNODE      
      st%hg(i) = rhsg(i)/dlhsg(i)  
      
      ! Current
    ! call getHeps2(He, Hep,  st%phidg(i)+st%phicg(i), st%hg(i)) 
     call getHeps2(He, Hep, st%phig(i), st%hg(i))

      st%heg(i)  = He
      st%hepg(i) = Hep
      st%rhog(i)  = (1.0d0-He)*rhoa + He*rhow
      st%drhog(i) = Hep*(rhow-rhoa) 
      st%mug (i)  = (1.0d0-He)*mua  + He*muw  
     
   !   call getHeps2(He, Hep, st%phig(i), st%hg(i)) 
  
    !  st%heg(i)  = He
    !  st%hepg(i) = Hep
        
      ! Initial      
      !phi0 = water_level-msh%xg(i,3)-st%dg(i,3)      
      call getHeps(He, water_level-msh%xg(i,3)-st%dg(i,3),st%hg(i))   
      st%rho0g(i)  = useRho0*( (1.0d0-He)*rhoa + He*rhow)
             
    enddo   
       
end
