!======================================================================
! Output aerodynamic torque for HAWT
!======================================================================
subroutine writeTQ(istep)
  use commonvars
  use mpi
  use params
  use globdata
  implicit none

  integer, intent(in) :: istep
  integer :: ifile

  character(len=30) :: fname
  character(len=10) :: cname

  ifile = 15
  fname = 'torque'//trim(cname(istep))
  open(ifile, file=fname, status='replace')
  write(ifile,"(4ES17.8)") time, theta, torque1, torque2
  write(ifile,"(4ES17.8)") torque3, torque4, moment1, moment2
  close(ifile)
end subroutine writeTQ

!======================================================================

!======================================================================
! Generate initial condition
!======================================================================
subroutine generateIC(SH, NM)
  
  use aAdjKeep
  use commonpars
  use params
  use globdata
  use mpi
  use defs_shell  

  implicit none
  
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  

  integer :: i, j, n, point
  real(8) :: xi(3), xi1, xi3, rr, temp(3), sita, phi
  integer :: component_id
  real(8) :: time1

 integer :: fin_num1, fin_num2
 real(8) :: dis
 time1 = 0d0

        
   SH%NRB%dshOld = 0.0d0
   SH%NRB%ushOld = 0.0d0
   SH%NRB%ashOld = 0.0d0

   SH%FEM%ushOld = 0.0d0
   SH%FEM%ashOld = 0.0d0
   SH%FEM%dshOld = 0.0d0



    SH%NRB%dsh = SH%NRB%dshOld
    SH%NRB%ush = SH%NRB%ushOld 
    SH%NRB%ash = SH%NRB%ashOld

    SH%FEM%dsh = SH%FEM%dshOld
    SH%FEM%ush = SH%FEM%ushOld
    SH%FEM%ash = SH%FEM%ashOld

   blade(1)%ash = SH%FEM%ash
   blade(1)%ush = SH%FEM%ush
   blade(1)%dsh = SH%FEM%dsh


end subroutine generateIC

!======================================================================
! Generate initial condition
!======================================================================
subroutine generateIC_move(SH, NM)
  
  use aAdjKeep
  use commonpars
  use params
  use globdata
  use mpi
  use defs_shell  

  implicit none
  
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  

  integer :: i, j, n, point
  real(8) :: xi(3), xi1, xi3, rr, temp(3), sita, phi
  integer :: component_id
  real(8) :: time1

 integer :: fin_num1, fin_num2
 real(8) :: dis
 time1 = 0d0
 ! real(8) :: Rt(3,3), Rdt(3,3), Rddt(3,3), NRt(3,3), NRdt(3,3), NRddt(3,3)   !

    time1 = 0d0
    thedd_base  = angle_acl
    thetd_base  = angle_acl*time1
    theta_base  = 0.5d0*angle_acl*time1**2


  theta(2) =   theta_base
  thetd(2) =   thetd_base
  thedd(2) =   thedd_base

  theta(1) =  0d0
  thetd(1) =  0d0
  thedd(1) =  0d0

  call get_Rmat(theta(1), thetd(1), thedd(1), Rmat(1,:,:), Rdot(1,:,:), Rddt(1,:,:))
  call get_Rmat(thetaold(1), thetdold(1), theddold(1), RmatOld(1,:,:), RdotOld(1,:,:), RddtOld(1,:,:))

  call get_Rmat(theta(2), thetd(2), thedd(2), Rmat(2,:,:), Rdot(2,:,:), Rddt(2,:,:))
  call get_Rmat(thetaold(2), thetdold(2), theddold(2), RmatOld(2,:,:), RdotOld(2,:,:), RddtOld(2,:,:))
        
  if (ismaster) then
      write(*,'(60("="))')
      write(*,*) "Time Step Number IGA_move:"
      write(*,'(60("="))')
      write(*,*) "Current theta is:",  theta_base*180.0d0/pi
      write(*,*) "Current velocity is:",thetd_base*180.0d0/pi
      write(*,*) "Current acceleration is:",  thedd_base*180.0d0/pi
      write(*,'(60("="))')
  end if
  if(.false.) then
   fin_num1 = 0 
   fin_num2 = 0 
   do i = 1, SH%NRB%NNODE
      component_id =SH%NRB%component_id(i)
      ! write(*,*) "component_id:", component_id 
      if(component_id == 1) fin_num1 = fin_num1 + 1
      if(component_id == 2) fin_num2 = fin_num2 + 1 
      do j = 1, 3
        SH%NRB%dshOld(i,j) = sum(Rmat(component_id,j,:)*SH%NRB%B_NET_U(i,1:3))
        SH%NRB%ushOld(i,j) = sum(Rdot(component_id,j,:)*SH%NRB%B_NET_U(i,1:3))
        SH%NRB%ashOld(i,j) = sum(Rddt(component_id,j,:)*SH%NRB%B_NET_U(i,1:3))
      end do
    end do
    if(ismaster)  write(*,*) "num of two fins IGA:", fin_num1, fin_num2

     fin_num1 = 0 
     fin_num2 = 0 
     do i = 1, SH%FEM%NNODE   
      dis = SH%FEM%B_NET_U(i,3) + 3.9974d0*(SH%FEM%B_NET_U(i,1) -0.12d0) +0.047d0
      if(dis< 0d0) then 
         fin_num1 = fin_num1 + 1
         component_id = 1
      endif
      if(dis> 0d0) then
         component_id = 2  
         fin_num2 = fin_num2 + 1
      endif


      do j = 1, 3
        SH%FEM%dshOld(i,j) = sum(Rmat(component_id,j,:)*SH%FEM%B_NET_U(i,1:3))
        SH%FEM%ushOld(i,j) = sum(Rdot(component_id,j,:)*SH%FEM%B_NET_U(i,1:3))
        SH%FEM%ashOld(i,j) = sum(Rddt(component_id,j,:)*SH%FEM%B_NET_U(i,1:3))
      end do
   end do
   
  if(ismaster) then     
   write(*,*) "IGA:",asin(Rmat(1,2,3)), asin(Rmat(2,2,3))
   write(*,*) "num of two fins FEM:", fin_num1, fin_num2
  endif


    SH%NRB%dsh = SH%NRB%dshOld
    SH%NRB%ush = SH%NRB%ushOld 
    SH%NRB%ash = SH%NRB%ashOld

    SH%FEM%dsh = SH%FEM%dshOld
    SH%FEM%ush = SH%FEM%ushOld
    SH%FEM%ash = SH%FEM%ashOld

    blade(1)%ash = SH%FEM%ash
    blade(1)%ush = SH%FEM%ush
    blade(1)%dsh = SH%FEM%dsh
   !endif
   ! write(*,*) "max FEM dsh0:",maxval(SH%FEM%dsh(:,2))
   ! write(*,*) "max FEM ush0:",maxval(SH%FEM%ush(:,2))
   ! write(*,*) "max FEM ash0:",maxval(SH%FEM%ash(:,2))

  endif ! if(.false.) then

end subroutine generateIC_move




!======================================================================
! Read shell solution
!======================================================================
subroutine readShellSol(istep, SH)
  use mpi
  use defs_shell  
  use params
  use globdata
  use commonpars
  implicit none
  
  type(shell_bld), intent(inout) :: SH
  integer,         intent(in)    :: istep

  integer :: solf, i, j
  character(len=30) :: fname, iname(2)
  	     
  solf = 97

  ! T-Spline
!  write(iname(1),'(I30)') istep
!  write(iname(2),'(I30)') myid + 21

!  fname = 'sh.rest.tsp.'//trim(adjustl(iname(1)))//'.'&
!                        //trim(adjustl(iname(2)))
!  open(solf, file=fname, status='old')

!  do i = 1, SH%TSP%NNODE
!    read(solf,*) (SH%TSP%dshOld(i,j), j = 1, 3)
!  end do
!  do i = 1, SH%TSP%NNODE
!    read(solf,*) (SH%TSP%ushOld(i,j), j = 1, 3)
!  end do
!  do i = 1, SH%TSP%NNODE
!    read(solf,*) (SH%TSP%ashOld(i,j), j = 1, 3)
!  end do

!  close(solf)


  ! NURBS
  write(iname(1),'(I30)') istep
  write(iname(2),'(I30)') myid + 21

  fname = 'sh.rest.nrb.'//trim(adjustl(iname(1)))//'.'&
                        //trim(adjustl(iname(2)))
  open(solf, file=fname, status='old')

  do i = 1, SH%NRB%NNODE
    read(solf,*) (SH%NRB%dshOld(i,j), j = 1, 3)
  end do
  do i = 1, SH%NRB%NNODE
    read(solf,*) (SH%NRB%ushOld(i,j), j = 1, 3)
  end do
  do i = 1, SH%NRB%NNODE
    read(solf,*) (SH%NRB%ashOld(i,j), j = 1, 3)
  end do

  close(solf)

  ! FEM
  fname = 'sh.rest.fem.'//trim(adjustl(iname(1)))//'.'&
                        //trim(adjustl(iname(2)))
  open(solf, file=fname, status='old')

  do i = 1, SH%FEM%NNODE
    read(solf,*) (SH%FEM%dshOld(i,j), j = 1, 3)
  end do
  do i = 1, SH%FEM%NNODE
    read(solf,*) (SH%FEM%ushOld(i,j), j = 1, 3)
  end do
  do i = 1, SH%FEM%NNODE
    read(solf,*) (SH%FEM%ashOld(i,j), j = 1, 3)
  end do

  close(solf)
     
end subroutine readShellSol



!======================================================================
! Output shell solutionsh.rest.nrb.1200.21
!======================================================================
subroutine writeShellSol(istep, SH)
  use commonvars
  use mpi
  use defs_shell  
  use params
  use globdata
  use commonpars
  implicit none
  
  type(shell_bld), intent(in) :: SH
  integer,         intent(in) :: istep

  integer :: solf, i, j
  character(len=30) :: fname, iname(2)  
  real(8) :: Xmin, Xmax, Ymin, Ymax, Zmin, Zmax
  integer :: iXmin, iXmax,iYmin, iYmax,iZmin,iZmax

  real(8) :: Xmin1, Xmax1, Ymin1, Ymax1, Zmin1, Zmax1
  integer :: iXmin1,  iXmax1, iYmin1,  iYmax1, iZmin1, iZmax1

!!!  if (ismaster) then
     
    solf = 98

    ! T-Spline
!    write(iname(1),'(I30)') istep
!    write(iname(2),'(I30)') myid + 21

!    fname = 'sh.rest.tsp.'//trim(adjustl(iname(1)))//'.'&
!                          //trim(adjustl(iname(2)))
!    open(solf, file=fname, status='replace')

!    do i = 1, SH%TSP%NNODE
!      write(solf,*) (SH%TSP%dsh(i,j), j = 1, 3)
!    end do
!    do i = 1, SH%TSP%NNODE
!      write(solf,*) (SH%TSP%ush(i,j), j = 1, 3)
!    end do
!    do i = 1, SH%TSP%NNODE
!      write(solf,*) (SH%TSP%ash(i,j), j = 1, 3)
!    end do

!    write(solf,'(60("="))')
 !   write(solf,*) istep, time, Delt
 !   write(solf,*) theta, thetd, thedd
 !   write(solf,*) SH%TSP%dsh(SH%TSP%TipLoc,3), SH%Tq2
 !   close(solf)


    ! NURBS
    write(iname(1),'(I30)') istep
    write(iname(2),'(I30)') myid + 21

    fname = 'sh.rest.nrb.'//trim(adjustl(iname(1)))//'.'&
                          //trim(adjustl(iname(2)))
    open(solf, file=fname, status='replace')

    Xmax = -1000d0
    Ymax = -1000d0
    Zmax = -1000d0

    Xmin = 1000d0
    Ymin = 1000d0
    Zmin = 1000d0

    Xmax1 = -1000d0
    Ymax1 = -1000d0
    Zmax1 = -1000d0

    Xmin1= 1000d0
    Ymin1 = 1000d0
    Zmin1= 1000d0

    do i = 1,SH%NRB%NNODE
     write(solf,*) (SH%NRB%dsh(i,j), j = 1, 3)
    enddo

    do i = 1, SH%NRB%NNODE
      write(solf,*) (SH%NRB%ush(i,j), j = 1, 3)
    end do
    do i = 1, SH%NRB%NNODE
      write(solf,*) (SH%NRB%ash(i,j), j = 1, 3)
    end do

    write(solf,'(60("="))')
    write(solf,*) istep, time, Delt
    write(solf,*) theta, thetd, thedd
    write(solf,*) SH%NRB%dsh(SH%NRB%TipLoc,3), SH%Tq2
    close(solf)
    ! FEM
    fname = 'sh.rest.fem.'//trim(adjustl(iname(1)))//'.'&
                          //trim(adjustl(iname(2)))
    open(solf, file=fname, status='replace')

    do i = 1, SH%FEM%NNODE
      write(solf,*) (SH%FEM%dsh(i,j), j = 1, 3)
    end do
    do i = 1, SH%FEM%NNODE
      write(solf,*) (SH%FEM%ush(i,j), j = 1, 3)
    end do
    do i = 1, SH%FEM%NNODE
      write(solf,*) (SH%FEM%ash(i,j), j = 1, 3)
    end do

    write(solf,'(60("="))')
    write(solf,*) istep, time, Delt
    write(solf,*) theta_base ,thetd_base,thedd_base
  !  write(solf,*) SH%FEM%dsh(SH%FEM%TipLoc,3), SH%Tq1
    close(solf)

!!!  end if
     
end subroutine writeShellSol


!======================================================================
! Output tip displacement
!======================================================================
subroutine writeTip(istep, SH)
  use commonvars
  use mpi
  use defs_shell
  use params
  use globdata
  use commonpars
  implicit none

  type(shell_bld), intent(in) :: SH
  integer,         intent(in) :: istep
  integer :: ifile

  character(len=30) :: fname, iname(2)

  write(iname(1),'(I30)') istep
  write(iname(2),'(I30)') myid + 21
    
  ifile = 15
  fname = 'tipdispLeading.'//trim(adjustl(iname(1)))//'.'&
                    //trim(adjustl(iname(2)))
  open(ifile, file=fname, status='replace')
  write(ifile,"(4ES17.8)") time, theta, SH%NRB%dsh(SH%NRB%TipLoc,3), SH%Tq2
  write(ifile,"(4ES17.8)") time, theta, SH%FEM%dsh(SH%FEM%TipLoc,3), SH%Tq1
  close(ifile)

 
  ifile = 15
  fname = 'tipdispTrailing.'//trim(adjustl(iname(1)))//'.'&
                    //trim(adjustl(iname(2)))
  open(ifile, file=fname, status='replace')
  write(ifile,"(4ES17.8)") time, theta, SH%NRB%dsh(SH%NRB%TipLocTr,3), SH%Tq2
  write(ifile,"(4ES17.8)") time, theta, SH%FEM%dsh(SH%FEM%TipLocTr,3), SH%Tq1
  close(ifile) 
end subroutine writeTip




!======================================================================
! Read shell solution
!======================================================================
subroutine readShellSol_NM(istep, NM)
  use defs_shell 
  use params
  use globdata 
  use commonpars
  implicit none
  
  type(shell_nmb), intent(inout) :: NM
  integer,         intent(in)    :: istep

  integer :: solf, i, j, aa, bb
  character(len=30) :: fname, iname
  	     
  solf = 97

  write(iname,'(I30)') istep

  fname = 'nm.rest.fem.'//trim(adjustl(iname))
  open(solf, file=fname, status='old')

  do aa = 1, NM%NTurb
    do bb = 1, 2
      do i = 1, NM%FEM(aa,bb)%NNODE
        read(solf,*) (NM%FEM(aa,bb)%dshOld(i,j), j = 1, 3)
      end do
      do i = 1, NM%FEM(aa,bb)%NNODE
        read(solf,*) (NM%FEM(aa,bb)%ushOld(i,j), j = 1, 3)
      end do
      do i = 1, NM%FEM(aa,bb)%NNODE
        read(solf,*) (NM%FEM(aa,bb)%ashOld(i,j), j = 1, 3)
      end do
    end do
  end do

  close(solf)
end subroutine readShellSol_NM



!======================================================================
! Output shell solution
!======================================================================
subroutine writeShellSol_NM(istep, NM)
  use commonvars
  use defs_shell 
  use params
  use globdata 
  use commonpars
  implicit none
  
  type(shell_nmb), intent(in) :: NM
  integer,         intent(in) :: istep

  integer :: solf, i, j, aa, bb
  character(len=30) :: fname, iname  	
     
  solf = 98

  write(iname,'(I30)') istep

  fname = 'nm.rest.fem.'//trim(adjustl(iname))
  open(solf, file=fname, status='replace')

  do aa = 1, NM%NTurb
    do bb = 1, 2
      do i = 1, NM%FEM(aa,bb)%NNODE
        write(solf,*) (NM%FEM(aa,bb)%dsh(i,j), j = 1, 3)
      end do
      do i = 1, NM%FEM(aa,bb)%NNODE
        write(solf,*) (NM%FEM(aa,bb)%ush(i,j), j = 1, 3)
      end do
      do i = 1, NM%FEM(aa,bb)%NNODE
        write(solf,*) (NM%FEM(aa,bb)%ash(i,j), j = 1, 3)
      end do
    end do
  end do

  write(solf,'(60("="))')
  write(solf,*) istep, time, Delt
  write(solf,*) theta_base ,thetd_base,thedd_base
  close(solf)
end subroutine writeShellSol_NM








