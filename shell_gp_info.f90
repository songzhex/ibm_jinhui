! (copied, pasted, and trimmed down from the shell IntElmAss)

! subroutine to fill in the displacements, weights, normals, and determinants
! for gauss points on a NURBS surface

subroutine shell_gp_info_im(NRB, ushAlpha, nsd)
  
  use defs_shell
  use mpi
  implicit none
  
  type(smesh), intent(inout) :: NRB
  integer, intent(in) :: nsd
  real(8), intent(in) :: ushAlpha(NRB%NNODE,nsd)

  ! for testing purposes
!  real(8) :: testsum

  !  Local variables
  integer :: p, q, nshl, nuk, nvk, ptype, iel, igauss, jgauss, &
             i, j, ni, nj, aa, bb, igp, s

  real(8) :: gwt, da, VVal, &
             DetJb_SH, nor(NSD), thi, Dm(3,3), Dc(3,3), Db(3,3),&
             xu(NSD), xd(NSD), dxdxi(NSD,2), ddxddxi(nsd,3), &
             dens, bvec(NSD), bscale

  real(8), allocatable :: gp(:,:), gw(:)

  ! TODO: move this to somewhere more appropriate
  real(8) :: samplingDistance

  integer, allocatable :: lIEN(:)
  real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:)
  real(8) :: gpvelocity(3)


  ! loop over elements
  do iel = 1, NRB%NEL

     ! get Gaussian points and weights
     allocate(gp(nrb%ngauss(iel),2), gw(nrb%ngauss(iel)))
     gp = 0.0d0; gw = 0.0d0
     call genGPandGWb_line(gp, gw, NRB%NGAUSS(iel)) 

     ! allocate space for shape functions and derivs
     nshl = NRB%NSHL(iel)
     allocate(shl(nshl), shgradl(nshl,2), shhessl(nshl,3), lIEN(nshl))

     lIEN = -1
     do i = 1, nshl
        lIEN(i) = NRB%IEN(iel,i)
     end do

     ! counter for gauss points within this element
     do igp = 1,nrb%ngauss(iel)
        
        ! Get Element Shape functions and their gradients
        shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
        xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
        nor = 0.0d0
           
        ! use the triangle/quad shape function; ngp tells it which version
        ! to evaluate
        call eval_SHAPE_line(gp(igp,:), nrb%ngauss(iel), shl, &
             shgradl, nor, xu, xd, &
             dxdxi, nsd, nshl, lIEN, nrb%nnode, nrb%B_NET_U,   &
             nrb%B_NET_D, DetJb_sh)
        
        ! compute velocity at gauss point from ushAlpha input array and
        ! shape function values
        gpvelocity = 0.0
        do s = 1,nshl
           gpvelocity(:) = gpvelocity(:) + shl(s)*ushAlpha(lIEN(s),:)
        enddo
        

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!!!!!!   we need to agree on a sign convention   !!!!!!!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        ! the sphere in bmesh.21.dat has outward-pointing normals, but
        ! the weak BC routine assumes that the normal points away from the
        ! fluid domain, i.e. inward for the sphere
        nor = -nor

        !write(*,*) sum(nor*xd)  ! should be -0.5 for weak BC to make sense

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




        ! fill in the gp data structure entries for this gauss point
        NRB%gp%xg(iel,igp,:) = xd(:)
        NRB%gp%nor(iel,igp,:) = &
             nor(:)/sqrt(nor(1)**2 + nor(2)**2 + nor(3)**2)
        NRB%gp%gw(iel,igp) = gw(igp)
        NRB%gp%detJ(iel,igp) = DetJb_SH
        NRB%gp%ush(iel,igp,:) = gpvelocity(:)

     enddo ! end loop over this element's gps
     
     deallocate(shl, shgradl, shhessl,lIEN)
     deallocate(gp,gw)
     
  end do    ! end loop elements



!--------------------------------------------------------------
! write in a matlab-readable format to plot and check

!testsum = 0.0
!do iel = 1, NRB%NEL
!   if(NRB%gp%gw(iel,1) == 0.0) then
!      cycle
!   endif
!   do igp = 1, NRB%NGAUSS*NRB%NGAUSS
!       write(*,*) NRB%gp%xg(iel,igp,:), NRB%gp%gw(iel,igp), &
!            NRB%gp%detJ(iel,igp)
!      testsum = testsum + NRB%gp%gw(iel,igp)*NRB%gp%detJ(iel,igp)
!   enddo
!enddo
!write(*,*) '-------------------------------' , testsum
!stop

end subroutine shell_gp_info_im
