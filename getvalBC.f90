subroutine getvalBC(msh,SH,bblade)
  use mpi  
  use params
  use globdata
  use postpro
  use commonpars
  use aAdjKeep
  use commonvars
  use defs_shell
  use types_beam
  use types_structure
  implicit none
  type(shell_bld), intent(in) :: SH
  type(meshStruct), intent(in) :: msh
  type(mesh),intent(inout) :: bblade(3)
  integer :: i, j, ibld, bb
 if(solfsi) then 
  if(solshell) then 
         bblade(1)%ash = SH%FEM%ash
         bblade(1)%ush = SH%FEM%ush
         bblade(1)%dsh = SH%FEM%dsh
        ! write(*,*) "maxmum SH%FEM%olddsh:", maxval(abs(SH%FEM%dshold(:,:)))
        ! write(*,*) "maxmum SH%FEM%dsh2:",    maxval(abs(SH%FEM%dsh(:,:)))
   endif
  ! broadcast such that all processors will have the solutions
     do ibld = 1, 1
      call MPI_BCAST(bblade(ibld)%ash, bblade(ibld)%NNODE*3, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)

     call MPI_BCAST(bblade(ibld)%ush, bblade(ibld)%NNODE*3, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)

     call MPI_BCAST(bblade(ibld)%dsh, bblade(ibld)%NNODE*3, &
                   MPI_DOUBLE_PRECISION, ibld-1, &
                   MPI_COMM_WORLD, mpi_err)
   end do
  ashBC = 0d0
  ushBC = 0d0
  dshBC = 0d0


  do ibld = 1, NBlade
   do  bb =1 , msh%NBOUND    
    if(msh%bound(bb)%FACE_ID == 21) then
      do i = 1, msh%bound(bb)%NNODE
        ashBC(msh%bound(bb)%BNODES(i),:) = bblade(ibld)%ash(msh%bound(bb)%L2SNODE(i),:)
        ushBC(msh%bound(bb)%BNODES(i),:) = bblade(ibld)%ush(msh%bound(bb)%L2SNODE(i),:)
        dshBC(msh%bound(bb)%BNODES(i),:) = bblade(ibld)%dsh(msh%bound(bb)%L2SNODE(i),:)
      enddo
    endif
   enddo
  enddo
  if (numnodes > 1) call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)
 endif
end subroutine getvalBC
