! hide the ugly junks in one subroutine
subroutine locate_pts_in_bg(sur)
  use defs_shell
  use mpi
  use hashed_octree
  use globdata

  implicit none

  type(smesh), intent(inout)  :: SUR

  integer :: nlevels, nbins, init_bin_cap, countloc, countglob
  logical :: sur_intersects_mesh
  integer :: i, j, k

  nlevels = 7
  nbins = 8**(nlevels-1)
  init_bin_cap = 3

  call octree_allocate(global_octree_background, nlevels, nbins, init_bin_cap)

    ! initialize the octree using the background mesh; needs to be called
  ! each time the mesh changes
  call initialize_pointsearch(sur,sur_intersects_mesh)
  ! background elements and parameters of gauss points (assumes that positions
  ! have been calculated and stored already; info about background mesh is
  ! provided to this routine through global variables in aadjkeep and 
  ! commonvars modules)
  call elm_surf_int_tree(SUR,sur_intersects_mesh)

  call mpi_barrier(mpi_comm_world, mpi_err)

end subroutine locate_pts_in_bg

! add all tet elements to global_octree_background; mesh data is global, so no
! arguments are given
subroutine initialize_pointsearch(sur, intersects)
  use aadjkeep
  use hashed_octree
  use globdata
  use defs_shell
  use mpi
  implicit none

  type(smesh), intent(in) :: sur
  logical, intent(out) :: intersects

  integer :: i,j
  real(8) :: box(3,2), verts(4,3)

  real(8), parameter :: eps=1.0d-7

  ! get overall bounds of mesh and set bounds for octree, based on 
  ! intersecting the bounding boxes of SUR and the background mesh
  intersects = .true.
  do i=1,3
     box(i,1) = max(minval(mesh_fluid%xg(:,i)),minval(sur%b_net_d(:,i)))-eps
     box(i,2) = min(maxval(mesh_fluid%xg(:,i)),maxval(sur%b_net_d(:,i)))+eps
     ! if the structure and mesh do not overlap at all, there is no point
     ! to this
     if(box(i,1) > box(i,2)) then
        intersects = .false.
        return
     end if
  end do ! end loop over spatial dimensions
  global_octree_background%bounds = box

  ! for each element, get its bounding box, then add it to the octree
  do i=1,mesh_fluid%nelem
     ! extract element verts
     do j=1,4
        verts(j,:) = mesh_fluid%xg(mesh_fluid%ien(i,j),:)
     end do ! end loop over verticies
     ! get bounds in each direction
     do j=1,3
        box(j,1) = minval(verts(:,j))
        box(j,2) = maxval(verts(:,j))
     end do ! end loop over spatial dimension

     ! add bounding box for element i to the tree
     call octree_add_object(global_octree_background,i,box)
     
  end do ! end loop over elements

end subroutine initialize_pointsearch

! locate the gauss point igp in element iel of sur in subcell i of 
! octree ot at level n.  assume that the backelem parameters were 
! originally cleared to 0 for sur%gp and use this to short-circuit
recursive subroutine locatePointInCell(ot, i, n, sur, iel, igp)
  use defs_shell
  use hashed_octree
  use aadjkeep
  use globdata
  implicit none
  integer, intent(in) :: i, n, iel,igp
  type(smesh), intent(inout) :: sur
  type(octree), intent(in) :: ot

  integer :: ibin, ii, j, backelem, k, subcelli
  logical :: isinside, incell
  real(8) :: xgp(3), xi(3), verts(4,3), box(3,2)

  real(8), parameter :: eps = 1.0d-7
 
  ! if the point has been located in another branch, short circuit the search
  if(sur%gp%backelem(iel,igp) > 0) then
     return
  end if

  ! if we are not in the cell, return

  ! get the coordinates of the gauss point
  xgp(:) = SUR%gp%xg(iel,igp,:)
  ! get the bounding box of the cell
  call octree_getbounds(ot,i,n,box)
  ! check position against bounds
  do j=1,3
     if(xgp(j) > box(j,2)+eps .or. xgp(j) < box(j,1)-eps) then
        return
     end if ! end if outside box
  end do ! end loop over directions

  ! get the hashed index
  call octree_hash(ot, i, ibin)

  ! if the cell is not occupied by any background mesh elements, leave
  if(.not. ot%occupied(n+1,ibin)) then
     return
  end if

  ! if we are at the bottom level of the tree, check whether the point is
  if(n == ot%nlevels-1) then
     ! loop over all elements whose indices are stored in this bin
     do j=1,ot%bins(ibin)%size
        backelem = ot%bins(ibin)%content(j)
        ! extract element verts
        do k=1,4
           verts(k,:) = mesh_fluid%xg(mesh_fluid%ien(backelem,k),:)
        end do ! end loop over verticies

        ! find its parameters and whether or not it is inside
        call invtetparam(verts,xgp,xi,isinside)

        if(isinside) then

           ! associate surface gp with background element and parameter values
           SUR%gp%backelem(iel,igp) = backelem
           ! associate this parametric location with the surface gp
           SUR%gp%backxi(iel,igp,:) = xi        
           ! short circuit the search
           return

        end if ! end if is inside
     end do ! end loop over background elements in cell

     ! leave; there are no children to recurse through
     return

  end if ! end if bottom level

  ! otherwise, recurse

  ! if not at bottom of tree, recursively call at level n+1, for all
  ! children; intersections will be checked within recursive calls
  do ii=1,8
     
     ! for each child we need the global index
     call octree_global_subcell_index(i, n, ii, subcelli)
     
     ! check whether point is in each child subcell
     call locatePointInCell(ot, subcelli, n+1, sur, iel, igp)
     
  end do ! end loop over subcells
  
end subroutine locatePointInCell

! wrapper to avoid screwing up indexes
subroutine locatePointInTree(ot, sur, iel, igp)
  use defs_shell
  use hashed_octree
  implicit none
  integer, intent(in) :: iel,igp
  type(smesh), intent(inout) :: sur
  type(octree), intent(in) :: ot
  ! call using top level cell
  call locatePointInCell(ot,1,0,sur,iel,igp)
end subroutine locatePointInTree

! replacement for elm_surf_int, using tree
subroutine elm_surf_int_tree(sur,intersects)
  use defs_shell
  use aadjkeep
  use mpi
  use globdata
  implicit none
  type(smesh), intent(inout) :: sur
  logical, intent(in) :: intersects

  integer :: jel,igp

  ! reset background elements of gauss points
  SUR%gp%backelem(:,:) = 0

  ! just use this to reset the background elements in the case that the
  ! surface and mesh do not intersect
  if(.not. intersects) then
     return
  end if

  ! for every background element, check if there are surface
  ! integration points by looping through surface elements    
  do jel = 1, SUR%NEL

     ! if element has zero area, it has no real gauss points, so move on.
     ! a cheap way to check this is just to see whether the first gauss 
     ! weight is zero (i.e. was allocated but never set; assume we don't
     ! have detJ going to zero at a gauss point in a nontrivial element)
     if(SUR%gp%gw(jel,1) == 0) then
        cycle
     endif

     do igp = 1, SUR%NGAUSS(jel) 

        ! find it in the background mesh; this modifies sur directly
        call locatePointInTree(global_octree_background, sur,jel,igp )
     end do
  end do

end subroutine elm_surf_int_tree

! subroutine to locate a surface quadrature point in a background tet mesh.
! for now, use a brute force approach, and just do it once at the beginning. 
! TODO: optimize this for time-dependent geometry
subroutine locatePointInBackground(x,xi,iel)

  use commonvars
  use tetim
  use globdata

  implicit none

  ! x is the spatial position of the surface quadrature point
  real(8), intent(in) :: x(3)
  ! xi is the parameters of the point, within background tet element iel
  real(8), intent(out) :: xi(3)
  integer, intent(out) :: iel

  ! local variables
  real(8) :: parentverts(4,3)
  integer :: i,j
  logical :: isinside

  ! initialize iel to sentinel value
  iel = 0

  ! loop over all background elements (brute force search)
  do i=1,mesh_fluid%nelem

     ! spatial verticies of tetrahedron (TODO: should add displacement
     ! for more general case)
     do j=1,4
        parentverts(j,:) = mesh_fluid%xg(mesh_fluid%ien(i,j),:)
     enddo

     ! invert parameterization of point x, testing whether it is inside of the
     ! tet in the process.  if it is, set iel and exit loop
     call invtetparam(parentverts,x,xi,isinside)
     if(isinside) then
        iel = i
        exit
     endif

  enddo
  ! at this point, either the point x was found in this processors patch and
  ! iel and xi are set accordingly, or the point is not in this processor's 
  ! domain and iel == 0 (and xi is some bogus parameters outside of whatever
  ! element was last checked)

end subroutine locatePointInBackground


! subroutine to check all surface quadrature points, assuming that the 
! field xg of sur%gp has been computed previously (for starters, it would just
! come from the undeformed b-net of the nurbs surface, then, later, it would
! come from the alpha-level position of the shell structure in a time-dependent
! computation) (weird routine name comes from previous code)
subroutine elm_surf_int(SUR)
  use defs_shell
  use mpi
  use aadjkeep
  use tetim
  use commonvars
  implicit none
  
  type(smesh),  intent(inout) :: SUR

  !integer, intent(in) :: NSD, NEL

  !testing
  real(8) :: testsum

  integer :: iel, jel, igp, sumSNGP, globalsum, i,j,k, wantedsum
  real(8) :: xgp(3), xeb(2,3), xi(3)
  
  character(len=30) :: fname
  character(len=10) :: cname
 
  sumSNGP = 0
  SUR%gp%backelem(:,:) = 0

  ! for every background element, check if there are surface
  ! integration points by looping through surface elements   
  do jel = 1, SUR%NEL

     ! if element has zero area, it has no real gauss points, so move on.
     ! a cheap way to check this is just to see whether the first gauss 
     ! weight is zero (i.e. was allocated but never set; assume we don't
     ! have detJ going to zero at a gauss point in a nontrivial element)
     if(SUR%gp%gw(jel,1) == 0) then
        cycle
     endif

     do igp = 1, SUR%NGAUSS(jel) 

        testsum = testsum + SUR%gp%gw(jel,igp)*SUR%gp%detJ(jel,igp)

        ! get the coordinates of the gauss point
        xgp(:) = SUR%gp%xg(jel,igp,:)      
        ! find it in the background mesh
        call locatePointInBackground(xgp, xi, iel)
        ! associate surface gp with background element and parameter values
        SUR%gp%backelem(jel,igp) = iel
        ! associate this parametric location with the surface gp
        SUR%gp%backxi(jel,igp,:) = xi(:)

        sumSNGP = sumSNGP + 1
		
     end do
  end do

  ! checking whether we have the correct number of gauss points with NURBS is
  ! hindered a bit by the fact that some of the elements have zero volume
  ! and thus no real gauss points.  we might need to add some extra fields
  ! to the mesh data type, or just hope that we're getting all the gauss
  ! points with the above procedure (it worked for polygon meshes...)

end subroutine elm_surf_int
