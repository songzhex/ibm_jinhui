!======================================================================
! FaceAssembly to build local RHS and LHS for non-matching boundaries.
!======================================================================
!subroutine FaceAsse_DG_w1t1(dgAlpha, ugAlpha, ugmAlpha, pgAlpha, NM)
subroutine FaceAsse_DG_RD_w1t1(st, msh,  rhsg, newJac,NM)
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use boundaryConditions  
  use matrix  
  use aAdjKeep
  implicit none
  type(state),      intent(in) :: st
  type(meshStruct), intent(in) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,6)  
  integer, intent(in) :: newJac
  type(shell_nmb), intent(in) :: NM

 ! real(8), intent(in) :: dgAlpha(NNODE,NSD), ugAlpha(NNODE,NSD), &
 !                        ugmAlpha(NNODE,NSD), pgAlpha(NNODE)                        
 ! Local variables
!**************************************************************************
  integer, parameter :: NSD = 3
  integer :: NSHL,NSHLB, NGAUSSB 
  real(8), allocatable :: eKlsrd(:,:)
  real(8), allocatable :: eRlsrd(:)
  real(8), allocatable :: gp(:,:), gw(:) 
  real(8), allocatable :: phil(:), phidl(:), hel(:),dphidxi(:), dphiddxi(:)
  integer :: faceor 
  integer :: IBClsc_loc(msh%maxNSHL,1)
  real(8) :: DetJb
!**************************************************************************
  real(8), allocatable :: shlu(:), shgradgu(:,:), shhessgu(:,:,:)       
  real(8), allocatable :: dl(:,:), xl(:,:), wl(:)

 integer :: a, b, ifac, iel, igauss, i, j, k, hess_flag
 integer :: aa, bb, aaaa, bbbb, FID, iturb, itemp
     
  real(8) :: phi, phid, Se, He, ai1(NSD), Gij(NSD,NSD),Ginv(NSD,NSD),nor(NSD),dxidx(NSD,NSD),dxdxi(NSD,NSD)
  real(8) ::  tauC, sgn

  NSHL = 4
  NSHLB = 3
  NGAUSSB = 3

allocate(shlu(NSHL), shgradgu(NSHL,NSD), shhessgu(NSHL,NSD,NSD), &
                 dl(NSHL,NSD), xl(NSHL,NSD), wl(NSHL))

allocate(gp(NGAUSSB,NSD-1),gw(NGAUSSB),phil(NSHL),phidl(NSHL), hel(NSHL), &
                 dphidxi(msh%NSD), dphiddxi(msh%NSD),&  
                 eKlsrd(NSHL,NSHL), eRlsrd(NSHL))
!  call genGPandGWb(gp, gw, NGAUSSb) ! Old code
! loop over faces
!return
  do b = msh%NBOUND,1, -1
    ! non-matching boundaries
   FID = msh%bound(b)%Face_ID
   itemp = 9
   do iturb = 1, NM%NTurb
    itemp = itemp + 1
    if (FID==itemp + iturb .or. FID==itemp + 1 + iturb) then
      do ifac = 1, msh%bound(b)%NFACE   
         iel = msh%bound(b)%F2E(ifac)
         faceor =  msh%bound(b)%FACE_OR(ifac)      
          NSHL    = msh%ELMNSHL(iel)
        call genGPandGWb(gp,gw,NGAUSSB,NSHLB)    	       	     
        do i = 1, NSHL
          j = msh%IEN(iel,i)
          xl (i,:) = msh%xg(j,:)
          dl (i,:) = st%dg  (j,:)
          phil(i)    = st%phig(j)
          phidl(i)   = st%phidg(j) 
          hel(i)     = st%heg (j)  
          wl (i)   = 1d0  
        end do
         eRlsrd = 0d0
        if (newJac >= 1) then
         eKlsrd =0d0
        endif
        ! Loop over integration points (NGAUSSb in each direction) 
        do igauss = 1, NGAUSSb
          call eval_faceshape(nshl, iel, gp(igauss,:), &
                              faceor,xl+dl, wl, &
                              shlu, shgradgu,Detjb, dxdxi,dxidx, &
                              Gij, Ginv, nor, msh%iga)  
          phi  = sum(phil*shlu) 
          phid = sum(phidl*shlu) 
          He   = sum(hel*shlu)
        
          do i = 1, msh%NSD
           dphidxi(i)    = sum(phil*shgradgu(:,i))
           dphiddxi(i)   = sum(phidl*shgradgu(:,i)) 
          enddo  
          Se = 2d0*(He-0.5d0) 
         ! ai1(:)= dphidxi(:)*Se/(sqrt(sum(dphidxi*dphidxi))+1d-15)  ! This the convective velocity for redistancing 
          ai1(:) = dphiddxi(:)*Se/(sqrt(sum(dphiddxi*dphiddxi))+1d-15) 
          call e3bSTAB_DG_RD(tauC, ai1, nor, dxidx)
         
         sgn = 1.0d0
         do aa = 1, NSHL
               eRlsrd(aa) = eRlsrd(aa) - sgn*shlu(aa)*tauC*phid*DetJb*gw(igauss)
         enddo
   
         if (newJac >= 1) then
           do aaaa = 1, NSHL
            do bbbb = 1, NSHL
              eKlsrd(aaaa,bbbb) = eKlsrd(aaaa,bbbb) + shlu(aaaa)*tauC*shlu(bbbb)*DetJb*gw(igauss)
             enddo
            enddo
          endif    
        end do

 !... Enforce strong BCs         
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        IBClsc_loc (aa,1) = IBClsc (bb) 
      enddo
  
      call applyBCs(nshl, 1 ,       IBClsc_loc(1:nshl,:), eKlsrd, eRlsrd)

!... Assemble global residual           
      do aa = 1, NSHL            
        RHSG(bb,6)   = RHSG(bb,6)   + eRlsrd (aa)              
      enddo
!... Assemble global jacobian           
      if (newJac >= 1) then                
        call FillSparseMat(nshl, msh%ien(iel, 1:nshl), 1, eKlsrd,  sparseMat, LHSlsrd)         
      end if    

    enddo
  endif
 enddo   
enddo
   
deallocate(shlu, shgradgu, shhessgu, dl, xl, wl)
                
deallocate(gp, gw,  phil,phidl,hel, dphidxi, dphiddxi, eKlsrd,eRlsrd)

end subroutine FaceAsse_DG_RD_w1t1


!======================================================================
! FaceAssembly to build local RHS and LHS for non-matching boundaries.
!======================================================================
!subroutine FaceAsse_DG_w1t1(dgAlpha, ugAlpha, ugmAlpha, pgAlpha, NM)
subroutine FaceAsse_DG_RD_w1t2(st, msh,  rhsg, newJac,NM)
  use params 
  use mpi
  use meshData
  use solution
  use matvec
  use boundaryConditions  
  use matrix  
  use aAdjKeep
  implicit none
  type(state),      intent(in) :: st
  type(meshStruct), intent(in) :: msh
  real(8), intent(inout) :: rhsg(msh%NNODE,6)  
  integer, intent(in) :: newJac
  type(shell_nmb), intent(in) :: NM

 ! real(8), intent(in) :: dgAlpha(NNODE,NSD), ugAlpha(NNODE,NSD), &
 !                        ugmAlpha(NNODE,NSD), pgAlpha(NNODE)                        
 ! Local variables
!**************************************************************************
  integer, parameter :: NSD = 3
  integer :: NSHL,NSHLB, NGAUSSB 
  real(8), allocatable :: eKlsrd(:,:)
  real(8), allocatable :: eRlsrd(:)
  real(8), allocatable :: gp(:,:), gw(:) 
  real(8), allocatable :: phil(:), phidl(:), hel(:),dphidxi(:), dphiddxi(:)
  real(8), allocatable :: phil1(:), phidl1(:), hel1(:)

  integer :: faceor 
  integer :: IBClsc_loc(msh%maxNSHL,1)
  real(8) :: DetJb
!**************************************************************************
  real(8), allocatable :: shlu(:), shgradgu(:,:), shhessgu(:,:,:)       
  real(8), allocatable :: dl(:,:), xl(:,:), wl(:)

  integer :: a, b, ifac, iel, igauss, i, j, k, hess_flag
  integer :: aa, bb, aaaa, bbbb, FID, iturb, itemp
     
  real(8) :: phi, phid, Se, He, ai1(NSD), Gij(NSD,NSD),Ginv(NSD,NSD),nor(NSD),dxidx(NSD,NSD),dxdxi(NSD,NSD)
  real(8) ::  tauC, sgn

  real(8) ::  NMTra_from(msh%NNODE,3), NMTra_to(msh%NNODE,3)
  real(8) ::  philLocal(msh%NNODE),phidlLocal(msh%NNODE), helLocal(msh%NNODE), r1tmp(msh%NNODE,3)

  NSHL = 4
  NSHLB = 3
  NGAUSSB = 3

allocate(shlu(NSHL), shgradgu(NSHL,NSD), shhessgu(NSHL,NSD,NSD), &
                 dl(NSHL,NSD), xl(NSHL,NSD), wl(NSHL))

allocate(gp(NGAUSSB,NSD-1),gw(NGAUSSB),phil(NSHL),phidl(NSHL), hel(NSHL),&
                 phil1(NSHL),phidl1(NSHL), hel1(NSHL),&
                 dphidxi(msh%NSD), dphiddxi(msh%NSD),&  
                 eKlsrd(NSHL,NSHL), eRlsrd(NSHL))


 r1tmp(:,1) = st%phig
 r1tmp(:,2) = st%phidg
 r1tmp(:,3) = st%heg
 call zeroslaves(r1tmp, NSD)
 NMTra_from = r1tmp

 NMTra_to = 0d0
! write(*,*) myid, "Hahaha"
 do i = 1, NM%NTurb
  call FaceAsse_Proj(msh, msh%NNODE, 3, NM%FEM(i,1), NM%FEM(i,2), NMTra_from,NMTra_to)
  call FaceAsse_Proj(msh, msh%NNODE, 3, NM%FEM(i,2), NM%FEM(i,1), NMTra_from, NMTra_to)
 enddo
! write(*,*) myid,"hbhb"
 philLocal = NMTra_to(:,1)
 phidlLocal = NMTra_to(:,2)
 helLocal = NMTra_to(:,3)

 !write(*,*) myid,"projection"
  do b = msh%NBOUND,1, -1
    ! non-matching boundaries
   FID = msh%bound(b)%Face_ID
   itemp = 9
   do iturb = 1, NM%NTurb
    itemp = itemp + 1
    if (FID==itemp + iturb .or. FID==itemp + 1 + iturb) then
      do ifac = 1, msh%bound(b)%NFACE   
         iel = msh%bound(b)%F2E(ifac)
         faceor =  msh%bound(b)%FACE_OR(ifac)      
          NSHL    = msh%ELMNSHL(iel)
        call genGPandGWb(gp,gw,NGAUSSB,NSHLB)    	       	     
        do i = 1, NSHL
          j = msh%IEN(iel,i)
          xl (i,:) = msh%xg(j,:)
          dl (i,:) = st%dg  (j,:)
          phil(i)    = philLocal(j)
          phidl(i)   = phidlLocal(j) 
          hel(i)     =  helLocal(j)  
          phil1(i)    = st%phig(j)
          phidl1(i)   = st%phidg(j)
          hel1(i)     = st%heg (j)
          wl (i)   = 1d0  
        end do
         eRlsrd = 0d0
        if (newJac >= 1) then
         eKlsrd =0d0
        endif
        ! Loop over integration points (NGAUSSb in each direction) 
        do igauss = 1, NGAUSSb
          call eval_faceshape(nshl, iel, gp(igauss,:), &
                              faceor,xl+dl, wl, &
                              shlu, shgradgu,Detjb, dxdxi,dxidx, &
                              Gij, Ginv, nor, msh%iga)  
          phi  = sum(phil*shlu) 
          phid = sum(phidl*shlu) 
          He   = sum(hel1*shlu)
        
          do i = 1, msh%NSD
           dphidxi(i)    = sum(phil1*shgradgu(:,i))
           dphiddxi(i)   = sum(phidl1*shgradgu(:,i)) 
          enddo  
          Se = 2d0*(He-0.5d0) 
        ! ai1(:)= dphidxi(:)*Se/(sqrt(sum(dphidxi*dphidxi))+1d-15)  ! This the convective velocity for redistancing 
          ai1(:) = dphiddxi(:)*Se/(sqrt(sum(dphiddxi*dphiddxi))+1d-15) 
          call e3bSTAB_DG_RD(tauC, ai1, nor, dxidx)
         
         sgn = -1.0d0
         do aa = 1, NSHL
               eRlsrd(aa) = eRlsrd(aa) - sgn*shlu(aa)*tauC*phid*DetJb*gw(igauss)
         enddo
   
        end do

 !... Enforce strong BCs         
      do aa = 1, NSHL  
        bb = msh%IEN(iel,aa)  
        IBClsc_loc (aa,1) = IBClsc (bb) 
      enddo
  
      call applyBCs(nshl, 1 ,       IBClsc_loc(1:nshl,:), eKlsrd, eRlsrd)

!... Assemble global residual           
      do aa = 1, NSHL            
        RHSG(bb,6)   = RHSG(bb,6)   + eRlsrd (aa)              
      enddo   

    enddo
  endif
 enddo   
enddo
   
deallocate(shlu, shgradgu, shhessgu, dl, xl, wl)
                
deallocate(gp, gw,  phil, phidl, hel, phil1, phidl1, hel1, dphidxi, dphiddxi, eKlsrd,eRlsrd)

end subroutine FaceAsse_DG_RD_w1t2
