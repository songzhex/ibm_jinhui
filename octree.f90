! hashed octree that can bin integer indices nominally corresponding to 
! geometrical primitives with bounding boxes

! the idea here is that allocating memory for the lowest level of the tree may
! be considered excessive, but that nlevels*nbins is a practical amount of 
! memory to allocate.  There are then nlevels hash tables of size nbins, each
! of which stores information about a level of the tree.  

module hashed_octree

  ! provides dynamically resizing arrays of integers, which will be the bins
  ! of the hash table
  use dynamic_arrays

  ! data needed to define an octree (must be allocated using subroutine below)
  type octree
     ! upper and lower bounds in each direction
     real(8) :: bounds(3,2)
     ! number of levels of tree.  ARBITRARY CONVENTION: the root element is
     ! at level n==0.  So there are 8^n boxes at level n
     integer :: nlevels
     ! number of hash bins
     integer :: nbins
     ! initial capacity of bins when reset
     integer :: init_bin_cap
     ! array of dynamic arrays, that are the bins of the table
     type(dynamic_array), allocatable :: bins(:)
     ! array of logicals, indicating occupancy at each level.  
     logical, allocatable :: occupied(:,:)
     ! dynamic arrays to store indices of occupied bins at each level (for
     ! the purpose of resetting the tree without ever looping over all
     ! bins in the hash tables for each level)
     type(dynamic_array), allocatable :: occlev(:)
  end type octree

  contains
    
    ! allocates and initializes an octree, with bins of capacity
    ! init_bin_cap
    subroutine octree_allocate(ot, nlevels, nbins, init_bin_cap)
      implicit none
      type(octree), intent(inout) :: ot
      integer, intent(in) :: nlevels, nbins, init_bin_cap

      integer :: i
      
      ot%nlevels = nlevels
      ot%nbins = nbins
      ot%init_bin_cap = init_bin_cap
      allocate(ot%bins(nbins))
      do i=1,nbins
         call dynamic_array_allocate(ot%bins(i),init_bin_cap)
      end do
      allocate(ot%occupied(nlevels,nbins))
      ot%occupied = .false.
      allocate(ot%occlev(nlevels))
      do i=1,nlevels
         call dynamic_array_allocate(ot%occlev(i),init_bin_cap)
      end do

    end subroutine octree_allocate

    ! convert a linear index i at level n to x,y,z indexes
    subroutine octree_i2xyz(n,i,xyz)
      implicit none
      integer, intent(in) :: i,n
      integer, intent(out) :: xyz(3)
      integer :: xy
      xyz(3) = (i-1) / (4**n) + 1
      xy = i - (4**n)*(xyz(3)-1)
      xyz(2) = (xy-1) / (2**n) + 1
      xyz(1) = xy - (2**n)*(xyz(2)-1)      
    end subroutine octree_i2xyz

    ! convert x,y,z indices into a linear index at level n
    subroutine octree_xyz2i(n,xyz,i)
      implicit none
      integer, intent(in) :: xyz(3),n
      integer, intent(out) :: i
      integer :: xy
      i = (4**n)*(xyz(3)-1) + (2**n)*(xyz(2)-1) + xyz(1)
    end subroutine octree_xyz2i

    ! subroutine to get the next-level index of a subcell
    subroutine octree_global_subcell_index(cell_global_index, cell_level,&
         subcell_local_index, subcell_global_index)
      implicit none
      integer, intent(in) :: cell_global_index ! index of current cell
      integer, intent(in) :: cell_level ! level of cell (NOT subcell)
      integer, intent(in) :: subcell_local_index ! in 1 through 8
      integer, intent(out) :: subcell_global_index ! index within next level
      
      integer :: cellxyz(3), subcellxyz(3), subcellxyzl(3), i

      ! split global cell coordinate into x, y, z
      call octree_i2xyz(cell_level,cell_global_index,cellxyz)

      ! split local subcell coordinate into x, y, z
      call octree_i2xyz(1,subcell_local_index,subcellxyzl)

      ! get global subcell x, y, z coordinates
      do i=1,3
         subcellxyz(i) = 2*(cellxyz(i)-1) + subcellxyzl(i)
      end do

      ! get global subcell index
      call octree_xyz2i(cell_level+1, subcellxyz, subcell_global_index)

    end subroutine octree_global_subcell_index

    ! hash function to get an array index from a global index (same for all
    ! levels occupancy arrays and for bins)
    subroutine octree_hash(ot, octree_index, bin_index)
      implicit none
      type(octree), intent(in) :: ot
      integer, intent(in) :: octree_index
      integer, intent(out) :: bin_index
      integer(8) :: octree_index_long, bin_index_long, nbins_long
      octree_index_long = octree_index
      nbins_long = ot%nbins
      ! need to use larger integers for this operation
      bin_index_long = (octree_index_long-1)*nbins_long/(8**(ot%nlevels-1))+1
      bin_index = bin_index_long
    end subroutine octree_hash

    ! function to get the bounding box of an octree cell of index i at 
    ! level n
    subroutine octree_getbounds(ot, i, n, bounds)
      implicit none
      type(octree), intent(in) :: ot
      integer, intent(in) :: i,n
      real(8), intent(out) :: bounds(3,2)

      real(8) :: cellwidth
      integer :: ii, xyz(3)

      ! get cell x,y,z coordinates
      call octree_i2xyz(n,i,xyz)
      
      ! get bounding box of this cell
      do ii=1,3
         cellwidth = (ot%bounds(ii,2) - ot%bounds(ii,1))/(2**n)
         bounds(ii,1) = cellwidth*(xyz(ii)-1) + ot%bounds(ii,1)
         bounds(ii,2) = bounds(ii,1) + cellwidth
      end do
      
    end subroutine octree_getbounds

    ! determine whether two bounding boxes intersect
    subroutine octree_box_intersect(box1,box2,intersects)
      implicit none
      real(8), intent(in) :: box1(3,2), box2(3,2)
      logical, intent(out) :: intersects
      real(8) :: center1(3), center2(3), avgwidths(3), absdists(3)
      integer :: ii
      center1 = 0.5d0*(box1(:,1) + box1(:,2))
      center2 = 0.5d0*(box2(:,1) + box2(:,2))
      avgwidths = 0.5d0*((box1(:,2)-box1(:,1)) + (box2(:,2)-box2(:,1)))
      absdists = abs(center2 - center1)
      intersects = .true.
      do ii=1,3
         ! if the boxes are too far apart in any one dimension, then
         ! we can say that they do not intersect
         if(absdists(ii) > avgwidths(ii)) then
            intersects = .false.
         end if
      end do
    end subroutine octree_box_intersect

    ! determine whether cell i at level n intersects an arbitrary bounding
    ! box (to determine wether this cell "contains" the primitive with
    ! this bounding box)
    subroutine octree_cell_intersects_box(ot,i,n,bounds,intersects)
      implicit none
      type(octree), intent(in) :: ot
      integer, intent(in) :: i,n
      real(8), intent(in) :: bounds(3,2)
      logical, intent(out) :: intersects
      
      real(8) :: cellbounds(3,2)

      ! get bounding box for cell
      call octree_getbounds(ot,i,n,cellbounds)
      
      ! check intersection
      call octree_box_intersect(bounds, cellbounds, intersects)
      
    end subroutine octree_cell_intersects_box

    ! add object with index obji bounded by box to cell i at level n of ot
    recursive subroutine octree_add_object_to_cell(ot,i,n,obji,box)
      implicit none
      type(octree), intent(inout) :: ot
      integer, intent(in) :: i,n,obji
      real(8), intent(in) :: box(3,2)

      logical :: intersects
      integer :: ibin, ii, subcelli
      real(8) :: cellbox(3,2)

      ! check intersection
      call octree_getbounds(ot,i,n,cellbox)
      call octree_box_intersect(cellbox,box,intersects)

      ! if we don't intersect, we are done; occupancy flag is false by
      ! default and we don't add the object to any bin
      if(intersects) then

         ! if intersects, set occupancy flag at level n
         call octree_hash(ot, i, ibin)
         ot%occupied(n+1,ibin) = .true.

         ! check base case of being at the bottom of the tree
         if(n == ot%nlevels-1) then
            ! add the object index to the hashed bin (each bin resizes 
            ! dynamically)
            call dynamic_array_push(ot%bins(ibin),obji)
         else
            ! if not at bottom of tree, recursively call at level n+1, for all
            ! children; intersections will be checked within recursive calls
            do ii=1,8

               ! for each child we need the global index
               call octree_global_subcell_index(i, n, ii, subcelli)

               ! check whether object is in each child subcell
               call octree_add_object_to_cell(ot,subcelli,n+1,obji,box)

            end do ! end loop over subcells
         end if ! end if at nlevels
      end if ! end if object intersected current cell

    end subroutine octree_add_object_to_cell

    ! add object to octree (just to avoid future zero versus one errors when 
    ! calling previous routine from top level)
    subroutine octree_add_object(ot, obji, box)
      implicit none
      type(octree), intent(inout) :: ot
      integer, intent(in) :: obji
      real(8), intent(in) :: box(3,2)
      call octree_add_object_to_cell(ot,1,0,obji,box)
    end subroutine octree_add_object

    ! reset cell i from level n (and all sub-cells).  if resestflag is .true.,
    ! then we re-allocate the dynamic bins.  Otherwise, we do the quicker
    ! dirtier trick of just setting their size to zero, which may be faster
    ! in one step, but, over many uses, the memory use could climb
    recursive subroutine octree_reset_cell(ot, i, n, resetflag)
      implicit none
      integer, intent(in) :: i,n
      type(octree), intent(inout) :: ot
      logical, intent(in) :: resetflag
      
      integer :: ibin, subcelli, ii

      ! get hashed index
      call octree_hash(ot, i, ibin)

      ! if not occupied, don't care
      if(ot%occupied(n+1,ibin)) then
         
         ! DO NOT reset occupation flag here, as, with hashing, it can refer
         ! to other cells as well.  just store it in a list, for now
         call dynamic_array_push(ot%occlev(n+1),ibin)

         ! if we are at a leaf, reset the corresponding bin, according to
         ! reset flag
         if(n == ot%nlevels-1) then

            if(resetflag) then
               ! automatically deallocates, then reallocates with specified 
               ! initial capacity
               call dynamic_array_allocate(ot%bins(ibin), ot%init_bin_cap)
            else
               ot%bins(ibin)%size = 0 ! memory allocation not changed
            end if
         else
            ! if not at leaf, recurse in each subcell
            do ii=1,8
               
               ! for each child we need the global index
               call octree_global_subcell_index(i, n, ii, subcelli)

               ! clean out subcell with same resetflag
               call octree_reset_cell(ot,subcelli,n+1,resetflag)

            end do ! end loop over subcells
         endif ! end if at leaf
      end if ! end if cell occupied

    end subroutine octree_reset_cell

    ! subroutine to reset entire tree; resetflag has same meaning as in 
    ! previous subroutine
    subroutine octree_reset(ot,resetflag)
      implicit none
      type(octree), intent(inout) :: ot
      logical, intent(in) :: resetflag
      
      integer :: n,i

      ! this just cleans out bins, and doesn't touch occupation lists; stores
      ! indices of occupied subtrees in occlev, which needs to be cleaned out
      ! later
      call octree_reset_cell(ot,1,0,resetflag)
      
      ! clean out occupation lists based on occlev, without directly looping
      ! over the number of bins
      do n=1,ot%nlevels
         do i=1,ot%occlev(n)%size
            ot%occupied(n,ot%occlev(n)%content(i)) = .false.
         end do

         ! reset occlev, either cheaply or fully, depending on reset flag
         if(resetflag) then
            call dynamic_array_allocate(ot%occlev(n),ot%init_bin_cap)
         else
            ot%occlev(n)%size = 0
         end if ! end if reset flag\

      end do ! end loop over levels

    end subroutine octree_reset
    
end module hashed_octree

