module KrylovSolvers
  
  implicit none
    
  ! Formating strings    
  character(len=*), parameter :: fmt1 ="(    5x,' Iter:', 2x,i4,'  Red:', 2x,f10.6,'(%)'  )"
  character(len=*), parameter :: fmt2 ="(    10x,' Iterations:', 2x,i4,'  Reduction:', 2x,f10.6,'(%)'  )"    

  !----------------------------------------------    
  ! Procedure interfaces    
  !---------------------------------------------- 
  interface CG
    module procedure CG
  end interface
          
  interface gmres
    module procedure gmres
  end interface   
          
  interface fgmres
    module procedure fgmres
  end interface          
  !----------------------------------------------              
  contains 
  !----------------------------------------------    
  
  !==============================================
  ! Conjugate Gradient solver
  !==============================================
  subroutine CG (NNODE,NVAR,sol,rhs,matvec,solve,innerprod, &
                 tol,itermin,itermax,verbose)          
  
  implicit none   
    
  integer  NNODE,NVAR
  real(8)  sol(NNODE,NVAR), rhs(NNODE,NVAR)
  external matvec,solve
  real(8)  innerprod
  real(8)  tol
  integer  itermin,itermax      
  integer  verbose
        
  integer n, i, j, k, iter, kSpace
  real(8)  rhstmp(NNODE,NVAR),prodtmp(NNODE,NVAR), &
           pv(NNODE,NVAR),zs(NNODE,NVAR) 
  real(8) rr,pp,alpha,tmprr,beta,rr0,tauk,taukm1,ercheck     

  rhstmp = rhs
  sol    = 0d0
  
  rr0 = innerprod(NNODE,NVAR,rhstmp,rhstmp) 
  if (rr0 == 0d0) then
    if (verbose.ge.0) write(*,fmt1) 0, 0d0
    return
  endif    
  
  do iter = 1, itermax
            
    ! Premultiply residual by inverse of Bdiag
    call solve(NNODE,NVAR,rhstmp,zs)                             
    tauk = innerprod(NNODE,NVAR,rhstmp,zs)  
          
    if(iter.eq.1) then
      beta = 0d+0
      pv = zs
    else
      beta = tauk/taukm1
      pv = zs + beta*pv
    endif

    call matvec (NNODE,NVAR,pv,prodtmp)                 
    pp = innerprod(NNODE,NVAR,pv,prodtmp) 
                        
    alpha = tauk/pp
    
    ! calculate the next guess, new res. and dot prod.     
    sol = sol + alpha*pv        
    rhstmp = rhstmp - alpha*prodtmp        
     
    rr = innerprod(NNODE,NVAR,rhstmp,rhstmp)   

    ! check for convergence 
    taukm1 = tauk
    
    ercheck=sqrt(rr/rr0)
    if ((ercheck <= tol).and.(iter.ge.itermin)) exit 
    if ((verbose.ge.1).and.(mod(iter,verbose).eq.0)) then
      write(*,fmt2) iter, ercheck*100d0
    endif        
  enddo
    
  if ((verbose.ge.2).or.(verbose==0)) then
      write(*,fmt1) iter, ercheck*100d0 
      write(*,*) iter, rr,rr0
      write(*,*) iter, ercheck,tol
  endif


  end subroutine CG 

   
  !==============================================
  ! GMRES solver
  !==============================================
  subroutine gmres(NNODE,NVAR,sol,rhs,matvec,solve,innerprod,&
                   tol,itermin,itermax,verbose)
        
  use mpi 
  implicit none

  integer  NNODE,NVAR
  real(8)  sol(NNODE,NVAR), rhs(NNODE,NVAR)
  external matvec,solve
  real(8)  innerprod
  real(8)  tol
  integer  itermin,itermax
  integer  verbose
        
  integer n, i, j, k, iK, iKs, jK, lK

  real(8) HBrg(itermax+1,itermax)
  real(8) Rcos(itermax), Rsin(itermax)

  real(8) uBrg  (NNODE,NVAR,itermax+1)
  real(8) rhstmp(NNODE,NVAR),temp(NNODE,NVAR)
  real(8) eBrg(itermax+1),yBrg(itermax),  &
      rr, unorm,unorm_ref, epsnrm, beta, &
      ercheck, tmp, tmp1
                          
  rhstmp = rhs
  
  !------------- zero out common values -------------- 
  Rcos = 0d0
  Rsin = 0d0
  HBrg = 0d0
  uBrg = 0d0  
      
  !---------------  Inverse Preconditioner   ---------------
  call solve (NNODE,NVAR, rhstmp, uBrg(:,:,1))    
  rr = innerprod(NNODE,NVAR,uBrg(:,:,1),uBrg(:,:,1))   
!  write(*,*) "uBrg =", rhstmp
 
  if (rr == 0d0) then
    sol =0d0
    if (verbose.ge.0) write(*,fmt1) 0, 0d0
    return
  endif                          
  
  ! set up tolerance  
  unorm  = sqrt(rr)
  epsnrm    = tol*unorm    
  unorm_ref = 1d2/unorm
 
  ! set up RHS of the Hessenberg's problem
  eBrg    = 0d0
  eBrg(1) = unorm
  
  ! normalize the first Krylov vector
  unorm = 1d0/unorm         
  uBrg(:,:,1) = uBrg(:,:,1)* unorm
  
  ! loop through GMRES iterations

  do iK = 1, itermax

    iKs = iK

    ! matrix-vector product
    call matvec(NNODE,NVAR, uBrg(:,:,iKs), temp)
    call solve (NNODE,NVAR, temp, uBrg(:,:,iKs+1))         

    do jK = 1, iKs+1       ! orthogonalize and get the norm

      if (jK .ne. 1) then
        uBrg(:,:,iKs+1) = uBrg(:,:,iKs+1) - beta * uBrg(:,:,jK-1)  
      endif 
               
      beta = innerprod(NNODE,NVAR,uBrg(:,:,iKs+1),uBrg(:,:,jK))      
     
      HBrg(jK,iKs) = beta ! put this in the Hessenberg Matrix

    enddo

    unorm           = sqrt(beta)
    HBrg(iKs+1,iKs) = unorm ! this fills the 1 sub diagonal band

    ! normalize the Krylov vector
    unorm = 1d0/unorm      
    uBrg(:,:,iKs+1) = uBrg(:,:,iKs+1) * unorm

    ! construct and reduce the Hessenberg Matrix
    !  since there is only one subdiagonal we can use a Givens rotation
    !  to rotate off each subdiagonal AS IT IS FORMED. We do this because it
    !  allows us to check progress of solution and quit when satisfied.  Note
    !  that all future K vects will put a subdiagonal in the next colmn so
    !  there is no penalty to work ahead as  the rotation for the next vector
    !  will be unaffected by this rotation.

    !  H Y = E ========>   R_i H Y = R_i E
    do jK = 1, iKs-1
      tmp =  Rcos(jK) * HBrg(jK,iKs) + Rsin(jK) * HBrg(jK+1,iKs)
      HBrg(jK+1,iKs) =- Rsin(jK) * HBrg(jK,  iKs) + Rcos(jK) * HBrg(jK+1,iKs)
      HBrg(jK,  iKs) =  tmp
    enddo

    tmp  = sqrt(HBrg(iKs,iKs)**2 + HBrg(iKs+1,iKs)**2)
    tmp1 = 1d0/tmp
    Rcos(iKs) = HBrg(iKs,  iKs) * tmp1
    Rsin(iKs) = HBrg(iKs+1,iKs) * tmp1
    HBrg(iKs,  iKs) = tmp
    HBrg(iKs+1,iKs) = 0d0
    
    ! rotate eBrg    R_i E
    tmp         = + Rcos(iKs) * eBrg(iKs) + Rsin(iKs) * eBrg(iKs+1)
    eBrg(iKs+1) = - Rsin(iKs) * eBrg(iKs) + Rcos(iKs) * eBrg(iKs+1)
    eBrg(iKs)   = tmp
    
    ! check for convergence
    ercheck = abs(eBrg(iKs+1))

    if ((verbose.ge.1) .and.(mod(iKs,verbose).eq.0)) then      
      write(*,fmt2) iKs, ercheck*unorm_ref
    endif 

    if (ercheck .le. epsnrm .and. iK >= itermin) exit
    
  enddo ! end of GMRES iteration loop
  
  ! solve for yBrg
  do jK = iKs, 1, -1
    yBrg(jK)     = eBrg(jK) / HBrg(jK,jK)
    eBrg(1:jK-1) = eBrg(1:jK-1) - yBrg(jK) * HBrg(1:jK-1,jK)
  enddo
  
  ! update Dy  
  sol = 0d0
  do jK = 1, iKs
    sol = sol + yBrg(jK) * uBrg(:,:,jK)
  enddo

  if ((verbose.ge.2).or.(verbose==0)) then 
    write(*,fmt1) iKs, ercheck*unorm_ref
    write(*,*) "    ",iKs,100d0/unorm_ref
    write(*,*) "    ",iKs,ercheck,epsnrm
  endif
!  write(*,*) unorm_ref
  end subroutine gmres


 !==============================================
  ! GMRES solver
  !==============================================
  subroutine gmres_RD(NM, NNODE, NVAR, sol, rhs0_DG_RD, rhs, matvec,solve,innerprod, &
                   tol,itermin,itermax,verbose)    
  use mpi   
  use params 
  use globdata
  use boundaryConditions
  use defs_shell
  use types_structure
  use types_beam
  use aAdjKeep
  implicit none

  integer  NNODE,NVAR
  real(8)  sol(NNODE,NVAR), rhs(NNODE,NVAR)
  type(shell_nmb), intent(in) :: NM
  real(8), intent(in) :: rhs0_DG_RD(NNODE,6) !
  real(8) :: output_DG(NNODE,6)
  external matvec,solve
  real(8)  innerprod
  real(8)  tol
  integer  itermin,itermax
  integer  verbose
        
  integer n, i, j, k, iK, iKs, jK, lK

  real(8) HBrg(itermax+1,itermax)
  real(8) Rcos(itermax), Rsin(itermax)

  real(8) uBrg  (NNODE,NVAR,itermax+1)
  real(8) rhstmp(NNODE,NVAR),temp(NNODE,NVAR)
  real(8) eBrg(itermax+1),yBrg(itermax),  &
      rr, unorm,unorm_ref, epsnrm, beta, &
      ercheck, tmp, tmp1

 integer :: flag(4)                 
 flag = 0
 flag(3) = 1
                          
  rhstmp = rhs
  
  !------------- zero out common values -------------- 
  Rcos = 0d0
  Rsin = 0d0
  HBrg = 0d0
  uBrg = 0d0  
      
  !---------------  Inverse Preconditioner   ---------------
!  if(ismaster) write(*,*) "JINHUI JINHUI0"

  call solve (NNODE,NVAR, rhstmp, uBrg(:,:,1))    

!  if(ismaster) write(*,*) "JINHUI JINHUI1"

  rr = innerprod(NNODE,NVAR,uBrg(:,:,1),uBrg(:,:,1))  

 ! if(ismaster) write(*,*) "JINHUI JINHUI2" 
!  write(*,*) "uBrg =", rhstmp
 
  if (rr == 0d0) then
    sol =0d0
    if (verbose.ge.0) write(*,fmt1) 0, 0d0
    return
  endif                          
  
  ! set up tolerance  
  unorm  = sqrt(rr)
  epsnrm    = tol*unorm    
  unorm_ref = 1d2/unorm
 
  ! set up RHS of the Hessenberg's problem
  eBrg    = 0d0
  eBrg(1) = unorm
  
  ! normalize the first Krylov vector
  unorm = 1d0/unorm         
  uBrg(:,:,1) = uBrg(:,:,1)* unorm
  
  ! loop through GMRES iterations
  do iK = 1, itermax

    iKs = iK
  
    ! matrix-vector product
   call matvec(NNODE,NVAR, uBrg(:,:,iKs), temp)
   if (move) then
   new_eps_DG = new_rd
   call incrementState_DG_LS_RD(new_eps_DG, uBrg(:,1,iKs) , eps_DG)   ! Jinhui: perturb fluid
   call assemble_DG_RD_PC(new_eps_DG,  output_DG(:, 1:6), 0, flag, NM)
   temp(:,1) = temp(:,1) - (output_DG(:,6) - rhs0_DG_RD(:,6))/eps_DG
   call commu(temp, NVAR, 'in ')
   endif
  ! call zeroslaves(temp, 4)    
    call solve (NNODE,NVAR, temp, uBrg(:,:,iKs+1))       

    do jK = 1, iKs+1       ! orthogonalize and get the norm

      if (jK .ne. 1) then
        uBrg(:,:,iKs+1) = uBrg(:,:,iKs+1) - beta * uBrg(:,:,jK-1)  
      endif 
               
      beta = innerprod(NNODE,NVAR,uBrg(:,:,iKs+1),uBrg(:,:,jK))      
     
      HBrg(jK,iKs) = beta ! put this in the Hessenberg Matrix

    enddo

    unorm           = sqrt(beta)
    HBrg(iKs+1,iKs) = unorm ! this fills the 1 sub diagonal band

    ! normalize the Krylov vector
    unorm = 1d0/unorm      
    uBrg(:,:,iKs+1) = uBrg(:,:,iKs+1) * unorm

    ! construct and reduce the Hessenberg Matrix
    !  since there is only one subdiagonal we can use a Givens rotation
    !  to rotate off each subdiagonal AS IT IS FORMED. We do this because it
    !  allows us to check progress of solution and quit when satisfied.  Note
    !  that all future K vects will put a subdiagonal in the next colmn so
    !  there is no penalty to work ahead as  the rotation for the next vector
    !  will be unaffected by this rotation.

    !  H Y = E ========>   R_i H Y = R_i E
    do jK = 1, iKs-1
      tmp =  Rcos(jK) * HBrg(jK,iKs) + Rsin(jK) * HBrg(jK+1,iKs)
      HBrg(jK+1,iKs) =- Rsin(jK) * HBrg(jK,  iKs) + Rcos(jK) * HBrg(jK+1,iKs)
      HBrg(jK,  iKs) =  tmp
    enddo

    tmp  = sqrt(HBrg(iKs,iKs)**2 + HBrg(iKs+1,iKs)**2)
    tmp1 = 1d0/tmp
    Rcos(iKs) = HBrg(iKs,  iKs) * tmp1
    Rsin(iKs) = HBrg(iKs+1,iKs) * tmp1
    HBrg(iKs,  iKs) = tmp
    HBrg(iKs+1,iKs) = 0d0
    
    ! rotate eBrg    R_i E
    tmp         = + Rcos(iKs) * eBrg(iKs) + Rsin(iKs) * eBrg(iKs+1)
    eBrg(iKs+1) = - Rsin(iKs) * eBrg(iKs) + Rcos(iKs) * eBrg(iKs+1)
    eBrg(iKs)   = tmp
    
    ! check for convergence
    ercheck = abs(eBrg(iKs+1)) 



    if ((verbose.ge.1) .and.(mod(iKs,verbose).eq.0)) then      
      write(*,fmt2) iKs, ercheck*unorm_ref
    endif 

    if (ercheck .le. epsnrm .and. iK >= itermin) exit
    
  enddo ! end of GMRES iteration loop
  
  ! solve for yBrg
  do jK = iKs, 1, -1
    yBrg(jK)     = eBrg(jK) / HBrg(jK,jK)
    eBrg(1:jK-1) = eBrg(1:jK-1) - yBrg(jK) * HBrg(1:jK-1,jK)
  enddo
  
  ! update Dy  
  sol = 0d0
  do jK = 1, iKs
    sol = sol + yBrg(jK) * uBrg(:,:,jK)
  enddo

  if ((verbose.ge.2).or.(verbose==0)) then 
    write(*,fmt1) iKs, ercheck*unorm_ref
    write(*,*) "   ",iKs,100d0/unorm_ref
    write(*,*) "   ",iKs,ercheck,epsnrm
  endif
!  write(*,*) unorm_ref
  end subroutine gmres_RD


subroutine gmres_LS(NM, NNODE,NVAR,sol,rhs, rhs0_fluid_DG, matvec,solve,innerprod,&
                   tol,itermin,itermax,verbose)

        
  use mpi   
  use params 
  use globdata
  use boundaryConditions
  use defs_shell
  use types_structure
  use types_beam
  use aAdjKeep
  implicit none

  type(shell_nmb), intent(in) :: NM
  integer  NNODE,NVAR
  real(8), intent(in) :: rhs0_fluid_DG(NNODE,5) ! 
  real(8)  sol(NNODE,NVAR), rhs(NNODE,NVAR)
  external matvec,solve
  real(8)  innerprod
  real(8)  tol
  integer  itermin,itermax
  integer  verbose
        
  integer n, i, j, k, iK, iKs, jK, lK

  real(8) HBrg(itermax+1,itermax)
  real(8) Rcos(itermax), Rsin(itermax)
 real(8) :: output_DG(NNODE,6)  ! The variable used to get the perturbed residual
  real(8) uBrg  (NNODE,NVAR,itermax+1)
  real(8) rhstmp(NNODE,NVAR),temp(NNODE,NVAR)
  real(8) eBrg(itermax+1),yBrg(itermax),  &
      rr, unorm,unorm_ref, epsnrm, beta, &
      ercheck, tmp, tmp1

  integer :: flag(4)                 
  flag = 0
  flag(1) = 1

  rhstmp = rhs
  !------------- zero out common values -------------- 
  Rcos = 0d0
  Rsin = 0d0
  HBrg = 0d0
  uBrg = 0d0  
      
  !---------------  Inverse Preconditioner   ---------------
  call solve (NNODE,NVAR, rhstmp, uBrg(:,:,1))    
  rr = innerprod(NNODE,NVAR,uBrg(:,:,1),uBrg(:,:,1))   
!  write(*,*) "uBrg =", rhstmp
 
  if (rr == 0d0) then
    sol =0d0
    if (verbose.ge.0) write(*,fmt1) 0, 0d0
    return
  endif                          
  
  ! set up tolerance  
  unorm  = sqrt(rr)
  epsnrm    = tol*unorm    
  unorm_ref = 1d2/unorm
 
  ! set up RHS of the Hessenberg's problem
  eBrg    = 0d0
  eBrg(1) = unorm
  
  ! normalize the first Krylov vector
  unorm = 1d0/unorm         
  uBrg(:,:,1) = uBrg(:,:,1)* unorm
  
  ! loop through GMRES iterations

  do iK = 1, itermax

    iKs = iK
    ! matrix-vector product
   call matvec(NNODE,NVAR, uBrg(:,:,iKs), temp)
  if (move) then
   new_eps_DG = new
  ! if(ismaster) write(*,*) myid, "To here"
   call incrementState_DG_LS(new_eps_DG, uBrg(:,1,iKs) , eps_DG)   ! Jinhui: perturb fluid
  ! if(ismaster) write(*,*) myid, "To here1"
   call assemble_DG_PC(new_eps_DG,  output_DG(:, 1:5), 0, flag, NM, 0,1)
  ! if(ismaster) write(*,*) myid, "To here2"
   temp(:,1) = temp(:,1) - (output_DG(:,5) - rhs0_fluid_DG(:,5))/eps_DG
   call commu(temp, NVAR, 'in ')
  endif
  ! call zeroslaves(temp, 4)     
    call solve (NNODE,NVAR, temp, uBrg(:,:,iKs+1))         

    do jK = 1, iKs+1       ! orthogonalize and get the norm

      if (jK .ne. 1) then
        uBrg(:,:,iKs+1) = uBrg(:,:,iKs+1) - beta * uBrg(:,:,jK-1)  
      endif 
               
      beta = innerprod(NNODE,NVAR,uBrg(:,:,iKs+1),uBrg(:,:,jK))      
     
      HBrg(jK,iKs) = beta ! put this in the Hessenberg Matrix

    enddo

    unorm           = sqrt(beta)
    HBrg(iKs+1,iKs) = unorm ! this fills the 1 sub diagonal band

    ! normalize the Krylov vector
    unorm = 1d0/unorm      
    uBrg(:,:,iKs+1) = uBrg(:,:,iKs+1) * unorm

    ! construct and reduce the Hessenberg Matrix
    !  since there is only one subdiagonal we can use a Givens rotation
    !  to rotate off each subdiagonal AS IT IS FORMED. We do this because it
    !  allows us to check progress of solution and quit when satisfied.  Note
    !  that all future K vects will put a subdiagonal in the next colmn so
    !  there is no penalty to work ahead as  the rotation for the next vector
    !  will be unaffected by this rotation.

    !  H Y = E ========>   R_i H Y = R_i E
    do jK = 1, iKs-1
      tmp =  Rcos(jK) * HBrg(jK,iKs) + Rsin(jK) * HBrg(jK+1,iKs)
      HBrg(jK+1,iKs) =- Rsin(jK) * HBrg(jK,  iKs) + Rcos(jK) * HBrg(jK+1,iKs)
      HBrg(jK,  iKs) =  tmp
    enddo

    tmp  = sqrt(HBrg(iKs,iKs)**2 + HBrg(iKs+1,iKs)**2)
    tmp1 = 1d0/tmp
    Rcos(iKs) = HBrg(iKs,  iKs) * tmp1
    Rsin(iKs) = HBrg(iKs+1,iKs) * tmp1
    HBrg(iKs,  iKs) = tmp
    HBrg(iKs+1,iKs) = 0d0
    
    ! rotate eBrg    R_i E
    tmp         = + Rcos(iKs) * eBrg(iKs) + Rsin(iKs) * eBrg(iKs+1)
    eBrg(iKs+1) = - Rsin(iKs) * eBrg(iKs) + Rcos(iKs) * eBrg(iKs+1)
    eBrg(iKs)   = tmp
    
    ! check for convergence
    ercheck = abs(eBrg(iKs+1))

    if ((verbose.ge.1) .and.(mod(iKs,verbose).eq.0)) then      
      write(*,fmt2) iKs, ercheck*unorm_ref
    endif 

    if (ercheck .le. epsnrm .and. iK >= itermin) exit
    
  enddo ! end of GMRES iteration loop
  
  ! solve for yBrg
  do jK = iKs, 1, -1
    yBrg(jK)     = eBrg(jK) / HBrg(jK,jK)
    eBrg(1:jK-1) = eBrg(1:jK-1) - yBrg(jK) * HBrg(1:jK-1,jK)
  enddo
  
  ! update Dy  
  sol = 0d0
  do jK = 1, iKs
    sol = sol + yBrg(jK) * uBrg(:,:,jK)
  enddo

  if ((verbose.ge.2).or.(verbose==0)) then 
    write(*,fmt1) iKs, ercheck*unorm_ref
    write(*,*) "    ",iKs,100d0/unorm_ref
    write(*,*) "    ",iKs,ercheck,epsnrm
  endif
!  write(*,*) unorm_ref
  end subroutine gmres_LS


subroutine gmres_NS(NM, NNODE,NVAR,sol,rhs, rhs0_fluid_DG, matvec,solve,innerprod,&
                   tol,itermin,itermax,verbose)
        
  use mpi   
  use params 
  use globdata
  use boundaryConditions
  use defs_shell
  use types_structure
  use types_beam
  use aAdjKeep
  implicit none

  type(shell_nmb), intent(in) :: NM
  integer  NNODE,NVAR
  real(8), intent(in) :: rhs0_fluid_DG(NNODE,5) ! 
  real(8)  sol(NNODE,NVAR), rhs(NNODE,NVAR)
  external matvec,solve
  real(8)  innerprod
  real(8)  tol
  integer  itermin,itermax
  integer  verbose
        
  integer n, i, j, k, iK, iKs, jK, lK

  real(8) HBrg(itermax+1,itermax)
  real(8) Rcos(itermax), Rsin(itermax)
 real(8) :: output_DG(NNODE, 6)  ! The variable used to get the perturbed residual
  real(8) uBrg  (NNODE,NVAR,itermax+1)
  real(8) rhstmp(NNODE,NVAR),temp(NNODE,NVAR)
  real(8) eBrg(itermax+1),yBrg(itermax),  &
      rr, unorm,unorm_ref, epsnrm, beta, &
      ercheck, tmp, tmp1

 integer :: flag(4)
                    
 flag = 1
  rhstmp = rhs
  !------------- zero out common values -------------- 
  Rcos = 0d0
  Rsin = 0d0
  HBrg = 0d0
  uBrg = 0d0  
      
  !---------------  Inverse Preconditioner   ---------------
  call solve (NNODE,NVAR, rhstmp, uBrg(:,:,1))    
  rr = innerprod(NNODE,NVAR,uBrg(:,:,1),uBrg(:,:,1))   
!  write(*,*) "uBrg =", rhstmp
 
  if (rr == 0d0) then
    sol =0d0
    if (verbose.ge.0) write(*,fmt1) 0, 0d0
    return
  endif                          
  
  ! set up tolerance  
  unorm  = sqrt(rr)
  epsnrm    = tol*unorm    
  unorm_ref = 1d2/unorm
 
  ! set up RHS of the Hessenberg's problem
  eBrg    = 0d0
  eBrg(1) = unorm
  
  ! normalize the first Krylov vector
  unorm = 1d0/unorm         
  uBrg(:,:,1) = uBrg(:,:,1)* unorm
  
  ! loop through GMRES iterations

  do iK = 1, itermax

    iKs = iK
 
    ! matrix-vector product
   call matvec(NNODE,NVAR, uBrg(:,:,iKs), temp)
  if (move) then
   new_eps_DG = new
  ! if(ismaster) write(*,*) myid, "To here"
   call incrementState_DG(new_eps_DG,  uBrg(:,1:4,iKs) , eps_DG)   ! Jinhui: perturb fluid
  ! if(ismaster) write(*,*) myid, "To here1"
   call assemble_DG_PC(new_eps_DG,  output_DG(:, 1:5), 0, flag, NM, 1, 0)
 !  write(*,*) myid, "To here2"
   temp(:,1:4) = temp(:,1:4) - (output_DG(:,1:4) - rhs0_fluid_DG(:,1:4))/eps_DG
   call commu(temp, 4, 'in ')
   endif
 
  ! call zeroslaves(temp, 4)     

    call solve (NNODE,NVAR, temp, uBrg(:,:,iKs+1))         

    do jK = 1, iKs+1       ! orthogonalize and get the norm

      if (jK .ne. 1) then
        uBrg(:,:,iKs+1) = uBrg(:,:,iKs+1) - beta * uBrg(:,:,jK-1)  
      endif 
               
      beta = innerprod(NNODE,NVAR,uBrg(:,:,iKs+1),uBrg(:,:,jK))      
     
      HBrg(jK,iKs) = beta ! put this in the Hessenberg Matrix

    enddo

    unorm           = sqrt(beta)
    HBrg(iKs+1,iKs) = unorm ! this fills the 1 sub diagonal band

    ! normalize the Krylov vector
    unorm = 1d0/unorm      
    uBrg(:,:,iKs+1) = uBrg(:,:,iKs+1) * unorm

    ! construct and reduce the Hessenberg Matrix
    !  since there is only one subdiagonal we can use a Givens rotation
    !  to rotate off each subdiagonal AS IT IS FORMED. We do this because it
    !  allows us to check progress of solution and quit when satisfied.  Note
    !  that all future K vects will put a subdiagonal in the next colmn so
    !  there is no penalty to work ahead as  the rotation for the next vector
    !  will be unaffected by this rotation.

    !  H Y = E ========>   R_i H Y = R_i E
    do jK = 1, iKs-1
      tmp =  Rcos(jK) * HBrg(jK,iKs) + Rsin(jK) * HBrg(jK+1,iKs)
      HBrg(jK+1,iKs) =- Rsin(jK) * HBrg(jK,  iKs) + Rcos(jK) * HBrg(jK+1,iKs)
      HBrg(jK,  iKs) =  tmp
    enddo

    tmp  = sqrt(HBrg(iKs,iKs)**2 + HBrg(iKs+1,iKs)**2)
    tmp1 = 1d0/tmp
    Rcos(iKs) = HBrg(iKs,  iKs) * tmp1
    Rsin(iKs) = HBrg(iKs+1,iKs) * tmp1
    HBrg(iKs,  iKs) = tmp
    HBrg(iKs+1,iKs) = 0d0
    
    ! rotate eBrg    R_i E
    tmp         = + Rcos(iKs) * eBrg(iKs) + Rsin(iKs) * eBrg(iKs+1)
    eBrg(iKs+1) = - Rsin(iKs) * eBrg(iKs) + Rcos(iKs) * eBrg(iKs+1)
    eBrg(iKs)   = tmp
    
    ! check for convergence
    ercheck = abs(eBrg(iKs+1))

    if ((verbose.ge.1) .and.(mod(iKs,verbose).eq.0)) then      
      write(*,fmt2) iKs, ercheck*unorm_ref
    endif 

    if (ercheck .le. epsnrm .and. iK >= itermin) exit
    
  enddo ! end of GMRES iteration loop
  
  ! solve for yBrg
  do jK = iKs, 1, -1
    yBrg(jK)     = eBrg(jK) / HBrg(jK,jK)
    eBrg(1:jK-1) = eBrg(1:jK-1) - yBrg(jK) * HBrg(1:jK-1,jK)
  enddo
  
  ! update Dy  
  sol = 0d0
  do jK = 1, iKs
    sol = sol + yBrg(jK) * uBrg(:,:,jK)
  enddo

  if ((verbose.ge.2).or.(verbose==0)) then 
    write(*,fmt1) iKs, ercheck*unorm_ref
    write(*,*) "    ",iKs,100d0/unorm_ref
    write(*,*) "    ",iKs,ercheck,epsnrm
  endif
!  write(*,*) unorm_ref
  end subroutine gmres_NS
  
  !==============================================
  ! Flexible GMRES solver
  !==============================================  
  subroutine fgmres(SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, sur, &  ! Jinhui: to include structure in
                    NNODE1, NVAR1, sol1,  rhs1, rhs1_DG, & ! Jinhui: 1~fluid
                    NNODE2, NVAR2, sol2,  rhs2, & ! Jinhui: 2~structure
                    matvec, solve, innerprod, &
                    tol, itermin, itermax, verbose)

  use mpi   
  use defs_shell
  use types_beam
  use types_structure  
  use params     
  implicit none
  type(shell_bld), intent(inout) :: SH
  type(shell_nmb), intent(inout) :: NM
  type(mesh_beam), intent(inout)    :: NRB_BEA
  type(beam), intent(inout)	 :: BEA
  type(mesh_mp_beam), intent(inout) :: mNRB_BEA
  type(structure) ,intent(inout) :: STRU
  type(smesh), intent(inout) :: sur

  integer  NNODE1, NVAR1
  real(8)  sol1(NNODE1, NVAR1), rhs1(NNODE1, NVAR1), rhs1_DG(NNODE1, NVAR1)
! Jinhui: 1~fluid
  integer  NNODE2, NVAR2
  real(8)  sol2(NNODE2, NVAR2), rhs2(NNODE2, NVAR2)
! Jinhui: 2~structure

  integer NNODE, NVAR

  external matvec,solve

  real(8)  innerprod
  real(8)  tol
  integer  itermin,itermax
  integer  verbose
        
  integer n,  j, k, iK, jK, lK,ii

  real(8) HBrg(itermax+1,itermax)
  real(8) Rcos(itermax), Rsin(itermax)

!  real(8) zBrg  (NNODE,NVAR,itermax)
!  real(8) uBrg  (NNODE,NVAR,itermax+1)
!  real(8) rhstmp(NNODE,NVAR),temp(NNODE,NVAR)

  real(8),allocatable :: zBrg  (:,:,:)
  real(8),allocatable :: uBrg  (:,:,:)
  real(8),allocatable :: rhstmp(:,:),temp(:,:)
  real(8), allocatable :: sol(:,:)
  real(8), allocatable :: rhs(:,:)


  real(8) eBrg(itermax+1),yBrg(itermax), &
          rr, unorm,unorm_ref, epsnrm, beta, &
          ercheck, tmp, tmp1,rr_tmp(itermax+1),rr_tmp1,eps1,eps2



  NVAR = NVAR1 + NVAR2
  NNODE = NNODE1
 
  if(NNODE < NNODE2) NNODE = NNODE2
!  write(*,*) myid,NNODE1,NNODE2,NNODE,NVAR1,NVAR2,NVAR
  allocate(sol(NNODE,NVAR),rhs(NNODE,NVAR))
  allocate(zBrg(NNODE,NVAR,itermax))
  allocate(uBrg(NNODE,NVAR,itermax+1))
  allocate(rhstmp(NNODE,NVAR),temp(NNODE,NVAR))
! write(*,*) myid,NNODE1,NNODE2,NVAR1,NVAR2,itermax
  sol = 0d0
  rhs = 0d0

 sol(1:NNODE1,1:NVAR1)=sol1(1:NNODE1,1:NVAR1)
 sol(1:NNODE2,1+NVAR1:NVAR)=sol2(1:NNODE2,1:NVAR2)

 rhs(1:NNODE1,1:NVAR1)=rhs1(1:NNODE1,1:NVAR1)
 rhs(1:NNODE2,1+NVAR1:NVAR)=rhs2(1:NNODE2,1:NVAR2)

 !if(ismaster) write(*,*) 'Inside fgmres:', sum(rhs2(:,1)), sum(rhs2(:,2)), sum(rhs2(:,3))

 eps1 = eps_time1
 eps2 = eps_time2
                                 
  rhstmp = rhs

  !------------- zero out common values -------------- 
  Rcos = 0d0
  Rsin = 0d0
  HBrg = 0d0
  uBrg = 0d0        

!   write(*,*) myid,"SDDDDDDD"   
  !---------------                     --------------
  uBrg(:,:,1) = rhstmp

 ! write(*,*) myid,"ASDDDDDD"
  rr = innerprod(NNODE,NVAR,rhstmp,rhstmp)

  if (rr == 0d0) then
    sol = 0d0
    if (verbose.ge.0) write(*,fmt1) 0, 0d0
    return
  endif      

  ! set up tolerance
  unorm     = sqrt(rr)   
  epsnrm    = tol * unorm    
  unorm_ref = 1d2/unorm
  
  ! set up RHS of the Hessenberg's problem
  eBrg    = 0d0
  eBrg(1) = unorm
 ! write(*,*) myid,"!@@##ASDDDDDD"
  ! normalize the first Krylov vector
  unorm = 1d0/unorm        
  uBrg(:,:,1) = rhstmp * unorm
  ! loop through GMRES iterations
 ! write(*,*) "~~~~~~~",myid,NNODE1,NNODE2,NVAR1,NVAR2,itermax
  do iK = 1, itermax
   ! if(iK>=10) then
   !   eps=0.5
   ! endif
      ! matrix-vector product
   
  !  eps2 = eps2*1.2  
    if(ismaster) write(*,*) "======================================================"
    if(ismaster) write(*,*) iK,"th iter:solve the preconditioning problem"
    if(ismaster) write(*,*) "======================================================"
    call solve (SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, & !Jinhui, modify to include structure in
                NNODE,NVAR,NNODE1,NVAR1,NNODE2,NVAR2, rhs1_DG, uBrg(:,:,iK), zBrg(:,:,iK))  

    if(ismaster) write(*,*) "======================================================"
    if(ismaster) write(*,*) iK,"th iter: matrix-free production"
    if(ismaster) write(*,*) "======================================================"
   
    call matvec(SH, NM, NRB_BEA, BEA, mNRB_BEA,STRU, sur, & !Jinhui, modify to include structure in
                NNODE,NVAR,NNODE1,NVAR1,NNODE2,NVAR2, rhs, zBrg(:,:,iK), uBrg(:,:,iK+1),eps1,eps2) 
    
!    rr_tmp1 = innerprod(NNODE,NVAR, uBrg(:,:,iK+1),uBrg(:,:,iK+1)) 
!    rr_tmp1=sqrt(rr_tmp1) ! Calculate the norm of the new vector

!    do ii=1,iK    
!     rr_tmp(ii)=innerprod(NNODE,NVAR, uBrg(:,:,iK+1)/rr_tmp1,uBrg(:,:,ii))   ! calculate the crossproduct of the two vectors
!     rr_tmp(ii)=acos(rr_tmp(ii))*180/acos(-1.0) ! calculate the angle
!    enddo
!    orthogonalize 
    do jK = 1, iK       
         
      HBrg(jK,iK)  = innerprod(NNODE,NVAR, uBrg(:,:,iK+1),uBrg(:,:,jK))                        
      uBrg(:,:,iK+1) = uBrg(:,:,iK+1) - HBrg(jK,iK)*uBrg(:,:,jK) 
     
    enddo

    rr = innerprod(NNODE,NVAR, uBrg(:,:,iK+1),uBrg(:,:,iK+1))          
   
    unorm         = sqrt(rr)
    HBrg(iK+1,iK) = unorm ! this fills the 1 sub diagonal band

    !     normalize the Krylov vector  
    unorm = 1d0/unorm      
    uBrg(:,:,iK+1) = uBrg(:,:,iK+1)   * unorm

    !.... construct and reduce the Hessenberg Matrix
    !     since there is only one subdiagonal we can use a Givens rotation
    !     to rotate off each subdiagonal AS IT IS FORMED. We do this because it
    !     allows us to check progress of solution and quit when satisfied.  Note
    !     that all future K vects will put a subdiagonal in the next colmn so
    !     there is no penalty to work ahead as  the rotation for the next vector
    !     will be unaffected by this rotation.
    !     H Y = E ========>   R_i H Y = R_i E
   
    do jK = 1, iK-1
      tmp =  Rcos(jK) * HBrg(jK,iK) + Rsin(jK) * HBrg(jK+1,iK)
      HBrg(jK+1,iK) = - Rsin(jK) * HBrg(jK,  iK) + Rcos(jK) * HBrg(jK+1,iK)
      HBrg(jK,  iK) =  tmp
    enddo

    tmp  = sqrt(HBrg(iK,iK)**2 + HBrg(iK+1,iK)**2)
    tmp1 = 1d0/tmp
    Rcos(iK) = HBrg(iK,iK) * tmp1
    Rsin(iK) = HBrg(iK+1,iK) * tmp1
    HBrg(iK,  iK) = tmp
    HBrg(iK+1,iK) = 0d0
                           ! rotate eBrg    R_i E
    tmp        = + Rcos(iK) * eBrg(iK) + Rsin(iK) * eBrg(iK+1)
    eBrg(iK+1) = - Rsin(iK) * eBrg(iK) + Rcos(iK) * eBrg(iK+1)
    eBrg(iK)   = tmp
                           ! check for convergence
    ercheck=abs(eBrg(iK+1))
    
    if ((verbose.ge.1) .and.(mod(iK,verbose).eq.0)) then  
      do ii=1,iK
    !    write(*,*) ii,"Angle,Norm:", rr_tmp(ii),rr_tmp1
      enddo   
      write(*,fmt2)  iK, ercheck*unorm_ref
      write(*,*) " ",iK, 100d0/unorm_ref
      write(*,*) " ",iK,ercheck,epsnrm
    endif 
  
    if (ercheck .le. epsnrm .and. iK >= itermin) exit

  enddo                ! end of GMRES iteration loop
  iK = min(iK,itermax)
      
  ! solve for yBrg
  do jK = iK, 1, -1
    yBrg(jK)     = eBrg(jK) / HBrg(jK,jK)
    eBrg(1:jK-1) = eBrg(1:jK-1) - yBrg(jK) * HBrg(1:jK-1,jK)
  enddo
  
  ! update Dy
  sol = 0d0
  do jK = 1, iK
    sol = sol + yBrg(jK) * zBrg(:,:,jK)
  enddo             
 
  if ((verbose.ge.2).or.(verbose==0)) write(*,fmt1) iK, ercheck*unorm_ref

  sol1(1:NNODE1,1:NVAR1) = sol(1:NNODE1,1:NVAR1)
  sol2(1:NNODE2,1:NVAR2) = sol(1:NNODE2,1+NVAR1:NVAR)

  end subroutine fgmres

end module KrylovSolvers
