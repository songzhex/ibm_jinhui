module innerprod
  implicit none
  real(8) :: ipwgt(11)
    
    
  !----------------------------------------------   
  ! Procedure interfaces    
  !---------------------------------------------- 
  interface std_iprod
    module procedure std_iprod
  end interface
  
  interface mono_iprod
    module procedure mono_iprod
  end interface 
            
  !----------------------------------------------            
  contains
  !----------------------------------------------  
  function std_iprod(NNODE,NVAR, vec1,vec2) 
  use mpi
  implicit none 
  integer , intent(in)  :: NNODE,NVAR
  real(8) , intent(in)  :: vec1(NNODE,NVAR),vec2(NNODE,NVAR)
  real(8) :: std_iprod, loc 
  integer :: i 
   
  std_iprod = 0d0
  
  do i = 1, NVAR
    std_iprod = std_iprod + dot_product(vec1(:,i),vec2(:,i))
  enddo

    loc = std_iprod
    call MPI_ALLREDUCE(loc, std_iprod, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, mpi_err)
 
    
  end function std_iprod
  !----------------------------------------------    
  function mono_iprod(NNODE,NVAR, vec1,vec2) 
  use mpi
  implicit none 
  integer , intent(in)  :: NNODE,NVAR
  real(8) , intent(in)  :: vec1(NNODE,NVAR),vec2(NNODE,NVAR)
  real(8) :: mono_iprod, loc 
!  real(8), parameter :: wgt(7)=(/ 1d0,1d0,1d0,1d4,1d-2,1d-2,1d-4 /)
  !real(8), parameter :: wgt(7)=(/ 1d0,1d0,1d0,1d-4,1d-2,1d1,1d-3 /)  
  
  real(8) :: wgt(11)
  integer :: i 
  
  wgt      = 1d0 
  wgt(5)   = 1d0
  wgt(6)   = 1d0    
  wgt(7:9) = 1d0
  wgt(10)  = 1d0
     
  mono_iprod = 0d0
  
  do i = 1, NVAR
    mono_iprod = mono_iprod + wgt(i)*dot_product(vec1(:,i),vec2(:,i))
  enddo
  
    loc = mono_iprod
    call MPI_ALLREDUCE(loc, mono_iprod, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, mpi_err) 
    
  end function mono_iprod
  !----------------------------------------------    
  function adept_iprod(NNODE,NVAR, vec1,vec2) 
  use mpi
  implicit none 
  integer , intent(in)  :: NNODE,NVAR
  real(8) , intent(in)  :: vec1(NNODE,NVAR),vec2(NNODE,NVAR)
  real(8) :: adept_iprod, loc 
  integer ::  i   
     
  adept_iprod = 0d0
  
  do i = 1, NVAR
    adept_iprod = adept_iprod + ipwgt(i)*dot_product(vec1(:,i),vec2(:,i))
  enddo
  
    loc = adept_iprod
    call MPI_ALLREDUCE(loc, adept_iprod, 1, MPI_DOUBLE_PRECISION, &
                       MPI_SUM, MPI_COMM_WORLD, mpi_err)  
    
  end function adept_iprod
            
end module innerprod
