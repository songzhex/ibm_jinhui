!------------------------------------------------------------------------
! 
!------------------------------------------------------------------------   
module class_def
  
  type NURBSpatch
  
    integer :: P, Q, R
    integer :: MCP, NCP, OCP
    real(8), allocatable :: U_KNOT(:), V_KNOT(:), W_KNOT(:)
    
  end type NURBSpatch

  type bnd_class

    integer :: FACE_ID

    integer :: NFACE  
  
    integer, allocatable :: FACE_IEN(:,:)
    
    integer, allocatable :: F2E(:)
    integer, allocatable :: FACE_OR(:)
  
    integer :: NNODE
    integer, allocatable :: BNODES(:)
  
    ! mapping between partitioned (local) boundary node/element
    ! and unpartitioned boundary (shell) node/element
    integer, allocatable :: L2SNODE(:), L2SELEM(:)

  end type bnd_class
  
end module class_def



!------------------------------------------------------------------------
! Module for defining shell types and variables
!------------------------------------------------------------------------     
module defs_shell

  implicit none

    ! type for gauss point
  type :: gptype
     ! number of actual points (okay to allocate more)
     real(8), allocatable :: xg(:,:,:), nor(:,:,:), param(:,:,:), &
          cparam(:,:,:), backxi(:,:,:), &
          gw(:,:), detJ(:,:), tv1(:,:,:), tv2(:,:,:), &
          ush(:,:,:), ash(:,:,:), &
          traction(:,:,:), lam(:,:), &
          contactForce(:,:,:), closestDist(:,:), &
          contactVelForce(:,:,:), contactLhs(:,:,:), contactLhsVel(:,:,:)
     integer, allocatable :: closestPoint(:,:,:), cien(:,:), &
          cien_sgn(:,:), cnshl(:), celem(:), backelem(:,:)
  end type gptype

  ! Declare type mesh
  type :: mesh
   
    integer :: maxNBEA
    ! degree in U and V for each patch
    integer, allocatable :: P(:), Q(:)

    ! patch type
    ! 1-blade; 0-bending strips; 2-shear web; ...
    integer, allocatable :: PTYPE(:)

    ! Knot vectors in u and v directions for each element
    real(8), allocatable :: U_KNOT(:,:), V_KNOT(:,:)

    ! Size of the knot vector for each elements (e.g. NUK=P+MCP+1)
    integer, allocatable :: NUK(:), NVK(:)

    ! Control Net
    ! B_NET is reference config, B_NET_D is current config.
    ! For the pre-bend case, reference config. could change, so 
    ! we created B_NET_U for undeformed, original config.
    real(8), allocatable :: B_NET(:,:), B_NET_U(:,:), B_NET_D(:,:)
    integer, allocatable :: component_ID(:)
    ! Boundary condition indicator for global nodes and edges
    ! respectively
    integer, allocatable :: IBC(:,:)

    ! array to store force vectors on the wind turbine blade
    real(8), allocatable :: FORCE(:,:)

    ! Global connectivity array
    integer, allocatable :: IEN(:,:), INN(:,:)

    ! number of shape functions for every element
    integer, allocatable :: NSHL(:)

    ! Bezier extraction operator
    real(8), allocatable :: Ext(:,:,:)

    ! Array for closest points (and the corresponding element)
    integer, allocatable :: CLE(:,:)
    real(8), allocatable :: CLP(:,:,:)

    ! Array for element list
    integer, allocatable :: Elm_Close(:,:,:)
    integer :: NEL_Close, NEL_Close_t

    ! Array for element gauss point location and for buiding the 
    ! element list based on radial location
    real(8) :: Elm_Size
    real(8), allocatable :: Elm_Loc(:,:,:)
    integer, allocatable :: RAD_ELM_LIST(:,:,:), RAD_ELM_NUM(:,:)

    integer :: NGAUSS, NNODE, NEL, maxNSHL, NNODE_LOC

    ! Store node number of the tip
    integer :: TipLoc, TipLocTr

    ! array for Solution vectors
    real(8), allocatable :: dsh(:,:), dshold(:,:), &
                            ush(:,:), ushold(:,:), &
                            ash(:,:), ashold(:,:)

    ! Surface ID and Boundary number
    integer :: FaceID, iBound

    integer, allocatable :: col_f2s(:), row_f2s(:)
    integer :: icnt_f2s
    logical :: already_f2s

  end type mesh


  ! Declare type mesh for multi-patch
  type :: mesh_mp
    ! degree in U and V for each patch
    integer, allocatable :: P(:), Q(:)

    ! number of control points in U and V for each patch
    ! (no need for T-spline)
    integer, allocatable :: MCP(:), NCP(:)

    ! Total number of control points and elements for each patch
    integer, allocatable :: NNODE(:), NEL(:)

    ! patch type
    ! 1-blade surface; 0-bending strips; 2-shear web; ...
    integer, allocatable :: PTYPE(:)

    ! Knot vectors in u and v directions for each patch
    real(8), allocatable :: U_KNOT(:,:), V_KNOT(:,:)

    ! Control Net
    real(8), allocatable :: B_NET(:,:,:)

    ! Boundary condition indicator for global nodes and edges
    ! respectively
    integer, allocatable :: IBC(:,:,:)

    ! array to store force vectors on the wind turbine blade
    real(8), allocatable :: FORCE(:,:,:)

    ! Mapping between patches and global reduced numbering
    ! e.g., MAP(1,2) = 3 means 1st patch, 2nd node points to 
    !   global reduced node number 3
    integer, allocatable :: MAP(:,:)
  end type mesh_mp

  type :: smesh

    type(gptype) :: gp

    ! degree in U and V for each patch
    integer, allocatable :: P(:), Q(:)

    ! patch type
    ! 0-blade; 1-bending strips; 2-shear web; ...
    integer, allocatable :: PTYPE(:)

    ! Knot vectors in u and v directions for each element
    real(8), allocatable :: U_KNOT(:,:), V_KNOT(:,:)

    ! Size of the knot vector for each elements (e.g. NUK=P+MCP+1)
    integer, allocatable :: NUK(:), NVK(:)

    ! Control Net
    real(8), allocatable :: B_NET(:,:), B_NET_U(:,:), B_NET_D(:,:)

    ! Boundary condition indicator for global nodes and edges
    ! respectively
    integer, allocatable :: IBC(:,:)

    ! array to store force vectors on the wind turbine blade
    real(8), allocatable :: FORCE(:,:)

    ! Global connectivity array
    integer, allocatable :: IEN(:,:), INN(:,:)

    ! number of shape functions for every element
    integer, allocatable :: NSHL(:)

    ! number of Gauss points for every element
    integer, allocatable :: NGAUSS(:)

    ! Bezier extraction operator
    real(8), allocatable :: Ext(:,:,:)

    ! Array for closest points
    integer, allocatable :: CLE(:,:)
    real(8), allocatable :: CLP(:,:,:)

    ! Array for element gauss point location and for buiding the 
    ! element list based on radial location
    real(8) :: Elm_Size
    real(8), allocatable :: Elm_Loc(:,:,:)
    integer, allocatable :: RAD_ELM_LIST(:,:,:), RAD_ELM_NUM(:,:)
 
    integer :: NNODE, NEL, maxNSHL, maxNGAUSS

    ! Blade rotation. 0 degree is the straight-up position
    real(8) :: BldRot

    ! Surface ID
    integer :: FaceID

    ! array for Solution vectors
    real(8), allocatable :: dsh(:,:), dshold(:,:), &
                            ush(:,:), ushold(:,:), &
                            ash(:,:), ashold(:,:)
  end type smesh

  ! Declare type shell (for wind turbine blade)
  type :: shell_bld

    type(mesh_mp) :: mNRB
    type(mesh)    :: NRB
    type(mesh)    :: TSP, BEZ
    type(mesh)    :: FEM

    ! number of patches for Blade Surface (S). Matches the fluid mesh
    ! number of patches for blade structure (B). May include shear webs
    ! number of total patches including bending strips (T)
    integer :: NPS, NPB, NPT

    integer :: M_Flag, T_Flag
    real(8) :: RHSGtol, G_fact(3), RHSGNorm

    ! row, col, and total of nonzero entries for sparse structure
    integer, allocatable :: row(:), col(:)
    integer :: icnt

    ! The right hand side load vector G and left hand stiffness matrix K
    real(8), allocatable :: RHSG(:,:), LHSK(:,:), &
                            RHSG_EXT(:,:), RHSG_GRA(:,:)

!    ! Solution vectors
!    real(8), allocatable :: yg(:,:), dg(:,:), tg(:), &
!                            mg(:), dl(:,:)

    ! material matrix for composite
    integer :: NMat
!    real(8), allocatable :: matA(:,:,:), matB(:,:,:), matD(:,:,:)
    real(8), allocatable :: matA(:,:,:,:), matB(:,:,:,:), matD(:,:,:,:), Density(:,:), Thickness(:,:)

    ! number of newton iterations for shell
    integer, allocatable :: Nnewt(:)

    ! Torque computed on shell mesh
    real(8) :: Tq1, Tq2

    ! Blade rotation. 0 degree is the straight-up position
    real(8) :: BldRot

    integer :: bmap

  end type shell_bld


  ! Declare type shell (for non-matching boundaries)
  type :: shell_nmb
    integer::NTurb
    type(mesh), allocatable :: FEM(:,:)
  end type shell_nmb

end module defs_shell



!------------------------------------------------------------------------
!     Module for storing arrays and allocation routines       
!------------------------------------------------------------------------  


!----------------------------------------------------------------------
! Module for common variables  
!---------------------------------------------------------------------- 
module commonvars

  implicit none
  save
  real(8)::DetJ,DetJb
! All the stuff have been moved to param.f90
end module commonvars

!----------------------------------------------------------------------
! Module for common parameters (e.g. pi)
!---------------------------------------------------------------------- 
module commonpars
  implicit none
  save
  ! All the stuff have been moved to param.f90
  real(8), parameter :: pi = 3.14159265358979323846264338328d0
end module commonpars


!----------------------------------------------------------------------
!     Module for mpi
!----------------------------------------------------------------------
!module mpi
!  implicit none
!  save

!  include "mpif.h"

!  integer, parameter :: mpi_master = 0
!  integer, parameter :: maxtask    = 50
!  integer, parameter :: maxseg     = 15000
      
!  integer :: numnodes, myid, mpi_err
!  integer :: status(MPI_STATUS_SIZE)
!  integer :: lstrideu, lstridep, numtask
!  integer :: lfrontu, maxfrontu
!  integer :: lfrontp, maxfrontp
!  integer :: nlworku, nlworkp
!  integer :: itag, iacc, iother, numseg, isgbeg, itkbeg, isgend
!  integer :: sevsegtypeu(maxtask,15)
!  integer :: sevsegtypep(maxtask,15)
!  logical :: ismaster
            
!  integer, allocatable :: ilworku(:), ilworkp(:)
!end module mpi



!------------------------------------------------------------------------
!    Module for beam
!------------------------------------------------------------------------     
module types_beam

  implicit none

  ! Declare type mesh
  type :: mesh_beam
    ! Order in U for each patch
    integer, allocatable :: P(:)
    integer :: maxNBEA
    ! patch type
    ! 0-cable (only tension or compression); 1-rod..
    integer, allocatable :: PTYPE(:)
    ! Knot vectors in u for each element
    real(8), allocatable :: U_KNOT(:,:)

    ! Size of the knot vector for each element (e.g. NUK=P+MCP+1)
    integer, allocatable :: NUK(:)

    ! Control Net
    real(8), allocatable :: B_NET_D_alphaf(:,:), B_NET_Dt_alphaf(:,:), B_NET_DDt_alpham(:,:), &
                            B_NET(:,:), B_NET_U(:,:), B_NET_D(:,:), B_NET_Dt(:,:), B_NET_DDt(:,:), &
                            B_NET_D_old(:,:), B_NET_Dt_old(:,:), B_NET_DDt_old(:,:)

  integer, allocatable :: component_ID(:)

    ! Boundary condition indicator for global nodes and edges
    ! respectively
    integer, allocatable :: IBC(:,:)

    ! array to store force vectors on the wind turbine blade
    real(8), allocatable :: FORCE(:,:)

    ! Global connectivity array
    integer, allocatable :: IEN(:,:), INN(:)

    ! number of shape functions for every element
    integer, allocatable :: NSHL(:)

    ! Bezier extraction operator
    real(8), allocatable :: Ext(:,:,:)

    ! Array for closest points
    integer, allocatable :: CLE(:,:)
    real(8), allocatable :: CLP(:,:,:)

    integer :: NGAUSS, NNODE, NNODE_LOC, NEL, maxNSHL

  end type mesh_beam


  ! Declare type mesh for multi-patch
  type :: mesh_mp_beam
    ! degree in U for each patch
    integer, allocatable :: P(:)

    ! number of control points in U for each patch
    ! (no need for T-spline)
    integer, allocatable :: MCP(:)

    ! Total number of control points and elements for each patch
    integer, allocatable :: NNODE(:), NEL(:)

    ! patch type
    ! 0-cable (only tension or compression); 1-rod...
    integer, allocatable :: PTYPE(:)

    ! Knot vectors in u for each patch
    real(8), allocatable :: U_KNOT(:,:)

    ! Control Net
    real(8), allocatable :: B_NET(:,:,:)

    ! Boundary condition indicator for global nodes and edges
    ! respectively
    integer, allocatable :: IBC(:,:,:)

    ! array to store force vectors on the wind turbine blade
    real(8), allocatable :: FORCE(:,:,:)

    ! Mapping between patches and global reduced numbering
    ! e.g., MAP(1,2) = 3 means 1st patch, 2nd node points to 
    !   global reduced node number 3
    integer, allocatable :: MAP(:,:)
  end type mesh_mp_beam

  ! Declare type beam
  type :: beam
    ! number of patches for cable (C)
    ! number of patches for Rod (R)
    ! number of patches
    integer :: NPC, NPR, NPT

    ! material matrix for beam
    integer :: NMat
    real(8), allocatable :: Ec(:), Dens(:), Rad(:), C_dp(:)
    real(8) :: f_fact, G_fact(3)
  end type beam

end module types_beam


!------------------------------------------------------------------------
!    Module for structure types
!------------------------------------------------------------------------     
module types_structure

  implicit none

  ! Declare type shell
  type :: structure

    integer :: S_Flag, Nnewt, Nincr, Rincr, Nstep, C_Flag, M_Flag, &
               T_Flag, Sol_Flag, FF_Flag, Nincr_step
    real(8) :: RHSGtol, DGtol, RHSGNorm, DGNorm, &
               RHSFact, Delta_t

    ! row, col, and total of nonzero entries for sparse structure
    integer, allocatable :: row(:), col(:)
    integer :: icnt

    integer, allocatable :: ndiag(:)

    ! The right hand side load vector G and left hand stiffness matrix K
    real(8), allocatable :: RHSG(:,:), LHSK(:,:), RHSG_beam(:,:), LHSK_beam(:,:),RHSG_shell(:,:), LHSK_shell(:,:), LHSKV(:), &
                            RHSG_EXT(:,:), RHSG_GRA(:,:)

    ! Solution vectors
    real(8), allocatable :: yg(:,:), dg(:,:), tg(:), &
                            mg(:), dl(:,:), ygv(:), ddu(:,:)

  end type structure

end module types_structure


module aAdjKeep
  
  use class_def
  use defs_shell
  use types_beam
  use types_structure

  implicit none
  save
   
  type(shell_bld) :: SH_eps   
  type(shell_nmb) :: NM_eps
  type(mesh_beam) :: NRB_BEA_eps
  type(beam) :: BEA_eps
  type(mesh_mp_beam) :: mNRB_BEA_eps
  type(structure) :: STRU_eps

  ! Mesh

!  real(8), allocatable :: xg(:,:), wg(:) ! Jinhui, attention

 ! integer, allocatable :: IEN(:,:), EPID(:), EIJK(:,:), NodeID(:)
  
  !type(bnd_class),  allocatable :: bound(:)  ! Jinhui, attention
  !type(NURBSpatch), allocatable :: patch(:)  ! Jinhui, attention
    
  ! Contraint flags
 ! integer, allocatable :: IPER(:)
  !integer, allocatable :: IBC(:,:) ! Jinhui, attention
  
  ! Type flags
  !integer, allocatable :: EL_TYP(:), D_FLAG(:), P_FLAG(:)

 real(8), allocatable :: rhsGu(:,:), rhsGm(:,:), rhsGp(:), rhsGls(:)

  ! Spars Struc
  !integer, allocatable :: row(:), col(:) ! Jinhui, attention

  ! The right hand side load vector G and left hand stiffness matrix K
 ! 
 ! real(8), allocatable :: lhsK11(:,:), lhsK12(:,:), lhsK22(:,:), &
 !                         lhsG(:,:),   lhsD1(:,:),  lhsD2(:,:),  &
 !                         lhsM(:),     lhsLS(:),    LHSPi(:,:),  &
 !                         LHSPti(:,:), LHSKi(:,:),  lhsMi(:),    &
 !                         LHSLSi(:),   LHSmass(:),               &
 !                         LHSlsu(:,:), LHSPls(:), LHSUls(:,:)

  ! Solution vectors
! The following is commented out by jinhui
!*******************************************
 ! real(8), allocatable :: dg(:,:),   dgold(:,:),   &
 !                         ug(:,:),   ugold(:,:),   &
 !                         ugm(:,:),  ugmold(:,:),  &
 !                         acg(:,:),  acgold(:,:),  &
 !                         acgm(:,:), acgmold(:,:), &
 !                         pg(:),     pgold(:),     &
 !                         phig(:),   phigold(:),   &
 !                         rphig(:),  rphigold(:)

 ! real(8), allocatable :: uavg(:,:), pavg(:)

  ! Rigid body
!  real(8) :: vbn0(3),  vbn1(3) 
!  real(8) :: dbn0(3),  dbn1(3) 
!  real(8) :: wbn0(3),  wbn1(3) 
!  real(8) :: Rn0(3,3), Rn1(3,3) 
!*******************************************
! The following is commented out by jinhui

      
  ! First P-K Stress
  real(8), allocatable :: FPKS(:,:,:)

  ! Array for Prism
!  integer, allocatable :: ELMNSHL(:), ELMNGAUSS(:)

  ! global information for individual blades
  type(mesh) :: blade(3)
  type(mesh) :: blade_eps(3)

end module aAdjKeep
