module tetim

  ! represent a dynamic list of quadrature points (essential to avoid insane
  ! memory requirements of allocating (12^(level))*ngp points...)

  ! (based on dynamic_arrays example module from before)

  type quadptlist
     integer :: size
     ! ASSUME shape is content(4,:), where 1:3 are parametric coordinates and
     ! 4 is the weight
     real(8), allocatable :: content(:,:) 
  end type quadptlist

contains

!!!!!!! routines for dynamic quad point lists !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! this allocates a dynamic array
  subroutine quadptlist_allocate(dynarray, initial_capacity)
    
    type(quadptlist), intent(inout) :: dynarray
    integer, intent(in) :: initial_capacity
    
    if(allocated(dynarray%content)) then
       deallocate(dynarray%content)
    endif
    
    ! this is the number of occupied entries, NOT
    ! intial_capacity, which is the amount of memory allocated
    dynarray%size = 0
    
    ! this sets the amount of allocated memory
    allocate(dynarray%content(4,initial_capacity))
    
  end subroutine quadptlist_allocate
  
  ! this adds an element to the end of the dynamic array, re-sizing
  ! if necessary.  only call this if the array is allocated already
  subroutine quadptlist_push(dynarray, new_element)

      type(quadptlist), intent(inout) :: dynarray
      real(8), intent(in) :: new_element(4)

      integer :: i, s
      real(8), allocatable :: temp(:,:)
      
      ! if we need to re-size, allocate a new array of double size
      ! and put all elements into it (assumes size > 0... otherwise 2*size
      ! is still 0, and this won't work; TODO: add special case for size==0)
      s = size(dynarray%content,2)
      if(dynarray%size == s) then
         allocate(temp(4,2*s))
         temp(:,1:s) = dynarray%content
         deallocate(dynarray%content)
         allocate(dynarray%content(4,2*s))
         dynarray%content(:,1:s) = temp(:,1:s)
      endif

      ! increase size and add new element at end
      dynarray%size = dynarray%size + 1
      dynarray%content(:,dynarray%size) = new_element

    end subroutine quadptlist_push

    ! free the memory and re-set size
    subroutine quadptlist_free(dynarray)
      type(quadptlist), intent(inout) :: dynarray
      if(allocated(dynarray%content)) then
         deallocate(dynarray%content)
      endif
      dynarray%size = 0
    end subroutine quadptlist_free



!!!!!!! helper routines for recursive AQ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  ! given physical coordinates of the vertices of a tetrahedron, get the
  ! physical location of a parametric point and its jacobian determinant
  subroutine getphyscoords(verts, params, physcoords, detjlocal)

    use commonvars ! need this to access detj, from shape function routine
    implicit none

    real(8), intent(in) :: verts(4,3),params(3)
    real(8), intent(out) :: detjlocal, physcoords(3)

    ! local variables
    real(8) :: xl(4,3), dl(4,3), shlu(4), gp(3), shgradgu(4,3), dxidx(3,3), &
         Gij(3,3), Ginv(3,3)
    integer :: nshl, aa, bb

    !!! debug, to test compile w/out commonvars !!!!!!!!!!!!!!!!!!!!!!!!
    !real(8) :: detj
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! just to keep naming consistent (good compiler probably wipes this out)
    nshl = 4
    xl = verts 
    dl = 0.0d0 ! (xl is always added to dl to get physical location)
    gp = params
    
    ! initialize other junk
    shlu = 0.0d0
    shgradgu = 0.0d0
    dxidx = 0.0d0
    gij = 0.0d0
    ginv = 0.0d0
    ! use shape function routine to get shl at params, scale shlu(aa) by 
    ! verts(aa,bb) to get bb-th physical coordinate
    call eval_shape_fem(nshl,gp,xl,dl,shlu,shgradgu,dxidx,gij,ginv)
    do bb=1,3
       physcoords(bb) = sum(shlu(:)*verts(:,bb))
    enddo
    
    ! detj is a global variable. it is set in eval_shape_fem (not my idea...)
    detjlocal = detj

  end subroutine getphyscoords

  ! given parametric coordinates of the vertices of a sub-tetrahedron, get
  ! the corresponding gauss points and weights; determine this by pretending
  ! that it is in physical space, and using the physical space locator/detJ
  ! subroutine
  subroutine getparamgps(paramverts, gp, gw, ngp)
    implicit none
    real(8), intent(in) :: paramverts(4,3)
    integer, intent(in) :: ngp
    real(8), intent(out) :: gp(ngp,3),gw(ngp)

    ! local vars
    real(8) :: gptemp(ngp,3), gwtemp(ngp), physcoords(3), detj
    integer :: aa

    ! get parent tet gps
    call genGPandGWtetpri(gptemp,gwtemp,ngp)

    ! get moved and scaled gps
    do aa=1,ngp
       call getphyscoords(paramverts, gptemp(aa,:), physcoords, detj)
       gp(aa,:) = physcoords(:)
       gw(aa) = gwtemp(aa)*detj
    enddo

  end subroutine getparamgps

  ! intialize the data structure needed for the inside/outside testing; 
  ! assumes that global_octree has already been allocated
  subroutine initialize_inout(sur)
    use defs_shell
    use raytrace
    implicit none
    
    type(smesh), intent(in) :: sur
    
    integer :: i,j
    real(8) :: treebounds(3,2)
    
    ! convert to a triangle soup
    allocate(global_trilist(sur%nel,3,3))
    do i=1,sur%nel
       do j=1,3
          global_trilist(i,j,1:3) = sur%b_net_d(sur%ien(i,j),1:3)
       end do
    end do
    
    ! get bounding box of geometry to be ray-traced
    do i=1,3
       treebounds(i,1) = minval(sur%b_net_d(:,i))
       treebounds(i,2) = maxval(sur%b_net_d(:,i))
    end do

    ! set this as the bounds of the octree
    global_octree%bounds = treebounds
    
    ! set up the octree
    call add_triangle_list_to_octree(global_trilist,sur%nel,global_octree)
    
  end subroutine initialize_inout

  ! determine whether a physical point is inside of the object
  subroutine insideTest_phys(x, isInside)
    use raytrace
    implicit none

    real(8), intent(in) :: x(3)
    integer, intent(out) :: isInside
  
    ! type(ray) :: r

    ! ! shoot a ray from x,y,z in the +z-direction, and check whether the 
    ! ! number of intersections is odd
    ! call ray_allocate(r)
    ! r%origin = x
    ! r%direction(1) = 0.0d0
    ! r%direction(2) = 0.0d0
    ! r%direction(3) = 1.0d0

    ! call ray_intersect_octree(r,global_octree,global_trilist,&
    !      size(global_trilist,1))

    ! isinside =  mod(r%params%size,2)
    
    ! call ray_free(r)

    isinside = 1
    if (x(1) > 1.d0 .or. x(1) < -1.0d0) isinside = 0
    if (x(2) > 1.d0 .or. x(2) < -1.0d0) isinside = 0
    if (x(3) > 1.d0 .or. x(3) < -1.0d0) isinside = 0

  end subroutine insideTest_phys

  ! determine whether a parametric point is inside of the object
  subroutine insideTest_param(physverts, params, isInside)
    implicit none
    real(8), intent(in) :: physverts(4,3), params(3)
    integer, intent(inout) :: isInside
    real(8) :: x(3),detj
    call getphyscoords(physverts,params,x,detj)
    call insideTest_phys(x,isInside)
  end subroutine insideTest_param

  ! get the 12 sub-tetrahedra of a parent tet, whose verticies are given
  ! in PARAMETRIC space (NOT physical space)
  subroutine getsubtets(parentverts, subtets)
    implicit none
    real(8), intent(in) :: parentverts(4,3)
    real(8), intent(out) :: subtets(12,4,3) ! indexed by (subet,vertex,param)

    ! local vars
    real(8) :: verts(11,3),centerpoint(3),centerparams(3),detj

    ! compute the center point (same parameters as 1-pt quadrature rule)
    centerparams = 0.25d0
    call getphyscoords(parentverts,centerparams,centerpoint,detj)

    ! first compute the 11 verticies (4 from before, 6 from edges, 1 in center)
    verts(1:4,:) = parentverts(:,:)
    verts(5,:)  = 0.5*(parentverts(1,:) + parentverts(2,:))
    verts(6,:)  = 0.5*(parentverts(1,:) + parentverts(3,:))
    verts(7,:)  = 0.5*(parentverts(1,:) + parentverts(4,:))
    verts(8,:)  = 0.5*(parentverts(2,:) + parentverts(3,:))
    verts(9,:)  = 0.5*(parentverts(2,:) + parentverts(4,:))
    verts(10,:) = 0.5*(parentverts(3,:) + parentverts(4,:))
    verts(11,:) = centerpoint(:)

    ! group these verts to define each of the 12 subtets
    
    ! correct way to order vertices for positive detj:
    !  4-th pt is origin in parametric space
    !  1-st pt is along x-axis
    !  2-nd pt is along y-axis
    !  3-rd pt is along z-axis, which should be x cross y (right-hand rule)
    ! make sure the deformed locations of points do not invert this ordering
    
    ! tets corresponding to original corners:
    subtets(1,1,:) = verts(5,:)
    subtets(1,2,:) = verts(7,:)
    subtets(1,3,:) = verts(6,:)
    subtets(1,4,:) = verts(1,:)
    
    subtets(2,1,:) = verts(9,:)
    subtets(2,2,:) = verts(5,:)
    subtets(2,3,:) = verts(8,:)
    subtets(2,4,:) = verts(2,:)

    subtets(3,1,:) = verts(10,:)
    subtets(3,2,:) = verts(8,:)
    subtets(3,3,:) = verts(6,:)
    subtets(3,4,:) = verts(3,:)

    subtets(4,1,:) = verts(7,:)
    subtets(4,2,:) = verts(9,:)
    subtets(4,3,:) = verts(10,:)
    subtets(4,4,:) = verts(4,:)

    ! tets corresponding to chopped-off corner faces and center

    subtets(5,1,:) = verts(6,:)
    subtets(5,2,:) = verts(7,:)  ! corner face 1
    subtets(5,3,:) = verts(5,:)
    subtets(5,4,:) = verts(11,:)
    
    subtets(6,1,:) = verts(5,:)
    subtets(6,2,:) = verts(9,:)  ! corner face 2
    subtets(6,3,:) = verts(8,:)
    subtets(6,4,:) = verts(11,:)

    subtets(7,1,:) = verts(8,:)
    subtets(7,2,:) = verts(10,:)  ! corner face 3
    subtets(7,3,:) = verts(6,:)
    subtets(7,4,:) = verts(11,:)

    subtets(8,1,:) = verts(10,:)
    subtets(8,2,:) = verts(9,:)  ! corner face 4
    subtets(8,3,:) = verts(7,:)
    subtets(8,4,:) = verts(11,:)    

    ! tets corresponding to exterior faces and center
    subtets(9,1,:) = verts(5,:)
    subtets(9,2,:) = verts(6,:)  
    subtets(9,3,:) = verts(11,:)
    subtets(9,4,:) = verts(8,:)
    
    subtets(10,1,:) = verts(9,:)
    subtets(10,2,:) = verts(7,:)  
    subtets(10,3,:) = verts(11,:)
    subtets(10,4,:) = verts(5,:)

    subtets(11,1,:) = verts(7,:)
    subtets(11,2,:) = verts(10,:) 
    subtets(11,3,:) = verts(11,:)
    subtets(11,4,:) = verts(6,:)

    subtets(12,1,:) = verts(10,:)
    subtets(12,2,:) = verts(9,:)  
    subtets(12,3,:) = verts(11,:)
    subtets(12,4,:) = verts(8,:)    
    
  end subroutine getsubtets

  ! test how many verts of a subtet are inside; parentverts are the physical
  ! verticies of the parent tetrahedron, while paramverts are the parametric
  ! verticies of the sub-tetrahedron. numinside is the return value
  subroutine vertsinside(parentverts,paramverts,numinside)
    implicit none
    real(8), intent(in) :: parentverts(4,3), paramverts(4,3)
    integer, intent(out) :: numinside

    integer :: aa,isinside
    numinside = 0

    do aa=1,4
       call insideTest_param(parentverts, paramverts(aa,:), isInside)
       numinside = numinside + isinside
    enddo
  end subroutine vertsinside



!!!!!!! main recursive routine to generate points for element !!!!!!!!!!!!!!!!!

  ! subidivide a parent tet recursively, building an array of gps
  recursive subroutine generateTetIntPts(gplist, &
       parentverts,paramverts,ngp,level)

    implicit none

    ! dynamically re-sizable list of quadrature points: assumed to be malloc'd
    ! beforehand with some initial capacity
    type(quadptlist), intent(inout) :: gplist

    integer, intent(in) :: ngp ! number of points per subtet (not total)
    integer, intent(in) :: level ! pass level-1 to recursive calls

    ! physical coords of element being sub-divided; same value passed to each
    ! recursive call
    real(8), intent(in) :: parentverts(4,3) 

    ! parametric coords of current sub-tet
    real(8), intent(in) :: paramverts(4,3)

    ! local vars
    logical :: attop
    integer :: vflag, icount, index,aa,isinside
    real(8) :: gp(ngp,3),gw(ngp), subtets(12,4,3), qp(4), physpt(3), detj

    ! check number of verts inside of object
    call vertsinside(parentverts,paramverts,vflag)

    !!!!!!! debug !!!!!!!!!!!!!!!!!11
    !if(vflag == 4) then
    !   vflag = 0
    !end if
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

    ! if all verts are inside, do nothing
    if(vflag == 4) then
       continue

    ! else, if all verts are outside OR level==0, add those quad points that
    ! are outside (generate rule, then test each point)
    elseif(vflag==0 .or. level==0) then

       ! get the appropriately weighted and located quadrature points for
       ! the current parametric subtet
       call getparamgps(paramverts, gp, gw, ngp)

       ! for each point, test its parametric location and possibly push it to
       ! the accumulating list
       do aa=1,ngp
          call insidetest_param(parentverts, gp(aa,:), isInside)
          !if( .true. ) then !!!!!!!!!!!! debug !!!!!!!!!
          if(isinside == 0)then

             qp(1:3) = gp(aa,:)
             qp(4) = gw(aa)
             call quadptlist_push(gplist,qp)
          !!!!!!!! debug !!!!!!!!!!!!!!!!
          !if(level < 2) then
          !   call getphyscoords(parentverts,gp(aa,:),physpt,detj)
          !   write(*,*) physpt
          !endif
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111111!!111
          endif
       enddo

    ! else, the cell must be cut AND we must not be at the bottom level, so
    ! we generate 12 subcells and make recursive calls on each one,
    ! passing level-1
    else

       ! generate all of the sub-tets; make sure to use PARAMETRIC verticies
       ! of current sub tet, not parent physical verticies of the element
       call getsubtets(paramverts, subtets)
       
       ! for each of the 12 sub-tets, make a recursive call
       do aa=1,12
          call generateTetIntPts(gplist, parentverts, subtets(aa,:,:), &
               ngp,level-1)
       enddo
    end if

    ! the final "output" of this routine should be a quadrature rule, 
    ! accumulated in the dynamic quadptlist data structure
    
  end subroutine generateTetIntPts

!!!!!!! routines for locating surface quadrature points in tets !!!!!!!!!!!!!!!

  ! test wether the location x is inside of the tetrahedron defined by verts,
  ! by inverting the parametric mapping for param 
  subroutine invtetparam(verts,x,param,isInside)

    implicit none
    real(8), intent(in) :: verts(4,3),x(3)
    real(8), intent(out) :: param(3)
    logical, intent(out) :: isinside

    real(8) :: a(3,3), ainv(3,3), detj, shiftx(3), mx, mn
    integer :: i,j

    ! quick bounding-box type check

    ! NOTE:  this does not really appear to be any quicker than just solving
    ! for the parameters and checking whether they are inside of tetrahedron.
    ! I think the performance must be memory-limited, so O(1) sequences of 
    ! FLOPs are essentially free here.  

    !isinside = .true.
    !do i=1,3
    !   mx = verts(1,i)
    !   mn = verts(1,i)
    !   do j=1,4
    !      if(mx < verts(j,i)) mx = verts(j,i)
    !      if(mn > verts(j,i)) mn = verts(j,i)
    !   enddo
    !   isinside = isinside .and. x(i) < mx .and. x(i) > mn
    !enddo

    ! if inside of bounding box, check for real
    !if(isinside) then

    ! if the tetrahedron's fourth vertex is translated to the origin, the 
    ! parametric mapping is linear.  build matrix for this mapping, invert,
    ! and apply to translated version of x.

    ! (is there a simpler formula for tets?)

    a(:,1) = verts(1,:) - verts(4,:)
    a(:,2) = verts(2,:) - verts(4,:)
    a(:,3) = verts(3,:) - verts(4,:)
    call get_inverse_3x3(a,ainv,detj)
    shiftx = x - verts(4,:)
    param = matmul(ainv,shiftx)

    ! test whether the inverse mapping is inside the parametric tetrahedron
    isinside = param(1) < 1.0d0 .and. param(1) > 0.0d0 .and. &
         param(2) < 1.0d0 .and. param(2) > 0.0d0 .and. &
         param(3) < 1.0d0 .and. param(3) > 0.0d0 .and. &
         sum(param) < 1.0d0

    !endif ! end if inside of bounding box

  end subroutine invtetparam  

  subroutine genAQ(sur, gplist, nelem, nnode, nsd, nshl, &
                   xg, ien)

    use hashed_octree 
    use raytrace
    use defs_shell
    use mpi
    use params
    implicit none

    ! dynamically re-sizable list of quadrature points: assumed to be malloc'd
    ! beforehand with some initial capacity
    type(smesh), intent(in)  :: SUR
    integer, intent(in) :: nelem, nnode, nsd, nshl
    integer, intent(in) :: ien(nelem, nshl)
    real(8), intent(in) :: xg(nnode, nsd)
    type(quadptlist), intent(inout) :: gplist(nelem)

    integer :: nlevels, nbins, init_bin_cap, countloc, countglob
    logical :: sur_intersects_mesh
    integer :: i, j, k
    character(len=40) :: fname
    character(len=10) :: cname

    real(8) :: paramverts(4,3), parentverts(4,3), asdf(4,3), jkl(4), qwer(4)

    ! allocate octree for ray-tracing (do only once)
    ! TODO: make these input parameters (different for ray tracing and for
    ! point location in background mesh)
    nlevels = 8
    nbins = 8**(nlevels-1)
    init_bin_cap = 3

    call octree_allocate(global_octree, nlevels, nbins, init_bin_cap) 
    ! initialize the octree to include all of the triangles from SUR
    call initialize_inout(sur)

    ! if 1 ,generate, if 0, read for GP_info.*
    if (genAQ_flag==1)then
       fname = trim('GP_info.') // cname(myid+1)
       open(199, file=fname, status='replace')
       do i=1,nelem

       !!! spatial verticies of the tetrahedron
       !!! TODO: include displacement in more general case
       do j=1,4
          parentverts(j,:) = xg(ien(i,j),:)
       enddo

       !!! parameters of the verticies (same for all top-level tets)
       paramverts(1,1) = 1.0d0
       paramverts(1,2) = 0.0d0
       paramverts(1,3) = 0.0d0

       paramverts(2,1) = 0.0d0
       paramverts(2,2) = 1.0d0
       paramverts(2,3) = 0.0d0

       paramverts(3,1) = 0.0d0
       paramverts(3,2) = 0.0d0
       paramverts(3,3) = 1.0d0

       paramverts(4,1) = 0.0d0
       paramverts(4,2) = 0.0d0
       paramverts(4,3) = 0.0d0

       call generateTetIntPts(gplist(i), parentverts, paramverts, &
            ngausstet, aq_level)
            write(199,*) gplist(i)%size
            do k=1, gplist(i)%size
               write(199,*) (gplist(i)%content(j,k), j=1,4)
            enddo
     enddo
     close(199)
   else
     fname = trim('GP_info.') // cname(myid+1)
     open(199, file=fname, status='old')
     !sgw = 1.0d0
     do i=1,nelem
        read(199,*) gplist(i)%size
        if (gplist(i)%size /= 0) then
           deallocate(gplist(i)%content)
           allocate(gplist(i)%content(4,gplist(i)%size))
           do k=1, gplist(i)%size
              read(199,*) (gplist(i)%content(j,k), j=1,4)
           enddo
        endif
      enddo
      close(199)
   endif
   call octree_reset(global_octree, .true.) 

   call mpi_barrier(mpi_comm_world, mpi_err)

  end subroutine genAQ

end module tetim